const express = require("express");
const mysql = require('mysql');
const md5 = require('md5');
const session = require('express-session');
const bodyParser = require('body-parser');
const auth = require('basic-auth');
const port = 3000;
const appUrl = 'https://hotspot4beauty.com/';


const app = express();
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
require('./functions')(app);

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'sami',
    password: 'rWuJVTOzxpAemoFM',
    database: 'hotspot_studioApp2018'
});

connection.connect(function (err) {
    if (err)
        throw err;
    console.log("Database is connected");
});

var server = app.listen(port, function () {
    console.log('listening on ' + port);
});

const io = require('socket.io').listen(server);
app.use(express.static(__dirname + '/public'));

// Chatroom

var numUsers = 0;

io.on('connection', function (socket) {
    var addedUser = false;
    var clients = [];

    function insertMessage(data) {
        var d = new Date();
        var date_time = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        var queryData = {sender_id: data.sender_id, reciver_id: data.receiver_id, room_id: data.room, message: data.message};
        connection.query('INSERT INTO messages SET ?', [queryData], function (err, rows, fields) {
            console.log(rows);
            return rows;
        });
    }

    function chatRoomLoginStatus(room_id, user_id) {
        connection.query('Select id from `chat_room_logins` WHERE room_id = ? AND user_id = ?', [room_id, user_id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return false;
            }
            connection.query('UPDATE `chat_room_logins` SET `is_online` = ? WHERE `user_id` = ?', [0, user_id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    return false;
                }
            });
            
            if (rows.length == 0) {
                var d = {room_id: room_id, user_id: user_id, is_online: 1};
                connection.query('INSERT INTO `chat_room_logins` SET ?', d, function (err, rows, fields) {});
            } else {
                connection.query('UPDATE `chat_room_logins` SET `is_online` = ? WHERE `room_id` = ? AND `user_id` = ?', [1, room_id, user_id], function (err, rows, fields) {
                    if (err) {
                        console.log(err);
                        return false;
                    }
                });
            }
        });
    }

    socket.on('checkforroomtojoin', function (session, data) {
        var roomname = data.room;
        var username = data.username;
        var clients = io.sockets.adapter.rooms[roomname];
        /*if (clients)
         {
         socket.emit('changeurl', roomname);
         } else
         {
         
         var error = "No room exists. Please create a room first";
         socket.emit('displayerror', error);
         }*/

    }
    );

    socket.on('new_message', function (data) {
        var roomname = data.room;
        console.log("Enter");
        console.log(data);
        console.log("end data");
        var title = "test Title";
        var body = "Test Message body";
        console.log("roomname:", roomname);
        data['is_read'] = "0";
        var id = insertMessage(data);
        socket.broadcast.to(roomname).emit('othermessage', data);


        //connection.query('SELECT id,`Studio_Name`,`is_online`,`device_token`  FROM users WHERE users.id=?', [data.receiver_id], function (err, rows, fields) {
        connection.query(`SELECT users.id,users.device_token,chat_room_logins.is_online  FROM users
                            LEFT JOIN chat_room_logins  ON chat_room_logins.room_id = ? AND chat_room_logins.user_id=users.id
                            WHERE users.id= ?`, [data.room, data.receiver_id], function (err, rows, fields) {
            console.log('new_message');
            console.log(rows);
            if (err) {
                console.log(err);
                return false;
            }
            if (rows.length > 0) {
                if (rows[0].is_online != 1) {
                    sendfcm(title, body, data, rows[0].device_token, 'new_message', function (response) {
                        console.log(response);
                    });
                }
            }
        });

    });

    socket.on('joinroom', function (data) {
        socket.room = data.room;
        socket.username = data.username;
        socket.user_id = data.sender_id;
        clients.push(data.username);
        socket.join(data.room);
        console.log(data);
        var roomLength = io.sockets.adapter.rooms[data.room].length;
        socket.emit('login', {
            numUsers: roomLength
        });
        connection.query('Select id from chats WHERE room_id = ?', [data.room], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return false;
            }
            if (rows.length == 0) {
                var d = {room_id: data.room, start_from: data.sender_id, receiver: data.receiver_id};
                connection.query('INSERT INTO chats SET ?', d, function (err, rows, fields) {});
            }
        });
        chatRoomLoginStatus(data.room, data.sender_id);
        /*connection.query('UPDATE users SET `is_online` = ? WHERE id = ?', [1, data.sender_id], function (err, rows, fields) {
         if (err) {
         console.log(err);
         return false;
         }
         });*/
    });

    /***************************/

    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (data) {
        // we tell the client to execute 'new message'
        console.log(data);

        /*var queryData = [];
         var obj = [
         2, (Math.random() * (100.011420 - 454.011420) + 0.011420).toFixed(7), (Math.random() * (100.011420 - 454.011420) + 0.011420).toFixed(7),
         ];
         queryData.push(obj);
         connection.query('INSERT INTO employee_location (user_id, latitude, longitude)  VALUES ?', [queryData], function (err, rows, fields) {
         connection.end();
         console.log(fields);
         if (err)
         throw err;
         res.send(rows);
         });*/
        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data
        });
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (username) {
        if (addedUser)
            return;
        // we store the username in the socket session for this client
        socket.username = username;
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
            numUsers: numUsers
        });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers
        });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function (data) {
        console.log('typing');
        console.log(data);
        socket.broadcast.to(data.room).emit('typing', {
            username: data.username
        });
    });

    // when the client emits 'stop_typing', we broadcast it to others
    socket.on('stop_typing', function () {
        socket.broadcast.emit('stop_typing', {
            username: socket.username
        });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
        console.log("disconnect");
        console.log("user_id: ", socket.user_id);

        /*connection.query('UPDATE `chat_room_logins` SET `is_online` = ? WHERE `room_id` = ? AND `user_id` = ?', [1,room_id, user_id], function (err, rows, fields) {
         if (err) {
         console.log(err);
         return false;
         }
         });*/
        connection.query('UPDATE `chat_room_logins` SET `is_online` = ? WHERE `user_id` = ?', [0, socket.user_id], function (err, rows, fields) {
            if (err) {
                console.log(err);
                return false;
            }
        });
        if (addedUser) {
            --numUsers;
            // echo globally that this client has left
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers
            });
        }
    });
});

//require('./controllers/main.js')(app, io, connection, auth);
