module.exports = function (app) {
    sendfcm = function (title, body, data, tokens, notification_type, callback) {
        var FCM = require('fcm-push');
        var serverKey = 'AIzaSyCNDZfia-imTTXU2lB2twdNsfiMBsZ9gVw';
        var fcm = new FCM(serverKey);

        var message = {
            to: tokens,
            // required fill with device token or topics
            notification: {
                title: title,
                body: body,
                type: notification_type,
                priority: "high"
            },
            data: data,
            content_available: true,
            priority: 10
        };
        console.log(message);

        //callback style
        fcm.send(message, function (err, response) {
            console.log(err);
            console.log(response);
            if (err) {
                console.log("Something has gone wrong!");
            } else {
                console.log("Successfully sent with response: ", response);
            }
            rparams = {
                "success": 1,
                "message": response,
                "error": err
            };
            callback(rparams);
        });
    };
};