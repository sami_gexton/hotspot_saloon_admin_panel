@include('shared/header')
   
<style type="text/css">
 .person-img{
        width: 100px;
        height: 100px;
        display: inline-block;
        border: 3px solid #e6e6e6;
        background-repeat: no-repeat;
        background-size: 100% 100%;
    } 

    .not-active
    {
       pointer-events: none;
       cursor: default;
    }

.edit-btn{top: 20px; right: 5px;}

</style>

@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">

    <input type="hidden" id="studio_id" value="<?php echo $result['studio_id'] ?>">
  
@include('shared/navbar')
<link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery-ui-timepicker-addon.css')}}">
<link  href="https://rawgit.com/fengyuanchen/cropperjs/master/dist/cropper.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/css/mdtimepicker.css')}}" rel="stylesheet"/>

<script src="{{asset('public/js/cropper.js')}}"></script>
<script src="{{asset('public/js/php_datetime.min.js')}}"></script>


<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
      <div class="card">
        @include('shared/nevigationdetails')
      </div>
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">General</h4>
        </div>
        <div class="card-body collapse in">
          <div class="proimg"></div>
          <div class="imgappend" style="display:none"></div>  
          <div class="card-block card-dashboard">
            <form class="form form-horizontal row-separator" enctype="multipart/form-data" method="POST" id="imageform"> 
              <div class="form-body">
                <div class="card-block text-xs-center">
                  <p>
                   
                    <div class="rounded-circle person-img" style="background-image: url('<?php echo asset($result['profile_pic'])?>')"> 
<!--                       <input id="file_upload" type="file" name="uploadimgfile" />
 -->
                      <input type="file"  id="file_upload" name="uploadimgfile"  accept=".png, .jpg, .jpeg"/>
                      <input type="hidden"  id="service_image_blob" value="">
                      <div class="img-upload" style="display: none"></div>
                      <div class="proimginfo"></div>
                      <a class="edit-img"><i class="fa fa-camera"></i></a>
                    </div>
                      <input type="hidden" name="id" value="<?php echo $result['studio_id'] ?>">
                      <input type="hidden" name="columname" value="profile_pic">
                      <input type="hidden" name="columname_big" value="profile_pic_big">
                       <input type="hidden" name="token" value="<?php echo session()->get('token'); ?>">
                       <input type="hidden" id="lat" value="" name="lat">
                        <input type="hidden" id="lng" value="" name="lng">
                      <!-- <input type="submit" value="Submit"> -->
                  </p>
                </div>  

                <input type="hidden" name="usertype" value="studio_user">
                <div class="form-group row">
                  <label class="col-md-2 label-control" for="studio">Studio Name <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="text" id="studio" class="form-control" value="<?php echo $result['Name'] ?>" placeholder="Studio Name" name="Studio_Name" required readonly>
                    <a href="javascript:void(0)" class=" btn btn-default info float-xs-left edit-btn" columname="Studio_Name"><i class="fa fa-pencil"></i></a>
                    <div class="form-control-position">
                      <i class="fa fa-building"></i>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 label-control" for="user-email">Email <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="email"  class="form-control" value="<?php echo $result['Email'] ?>" placeholder="Email Address" name="email" required readonly>
                    <a href="javascript:void(0)" class="btn btn-default info float-xs-left edit-btn" columname="email"><i class="fa fa-pencil"></i></a>
                    <div class="form-control-position">
                      <i class="fa fa-envelope"></i>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 label-control" for="user-Contact">Contact No <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="number" id="user-Contact" class="form-control" value="<?php echo $result['Contact'] ?>" placeholder="Contact No" name="contact_no" required readonly>
                    <a href="javascript:void(0)" class="btn btn-default info float-xs-left edit-btn" columname="contact_no" ><i class="fa fa-pencil"></i></a>
                    <div class="form-control-position">
                      <i class="fa fa-phone"></i>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 label-control" for="zipcode">Zip Code <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="text" id="zipcode" class="form-control" value="<?php echo $result['Zip_code'] ?>" placeholder="Zipcode" name="zipcode" required readonly>
                    <a href="javascript:void(0)" class="btn btn-default info float-xs-left edit-btn" columname="zipcode"><i class="fa fa-pencil"></i></a>
                    <div class="form-control-position">
                      <i class="fa fa-street-view"></i>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 label-control" for="country">Country <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="text" id="country" class="form-control"  value="<?php echo $result['Country'] ?>" placeholder="Country" name="zipcode" required readonly>
                    <a href="javascript:void(0)" class="btn btn-default info float-xs-left edit-btn" columname="country"><i class="fa fa-pencil"></i></a>
                    <div class="form-control-position">
                      <i class="fa fa-street-view"></i>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 label-control" for="searchTextField">Address <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="text" id="searchTextField" class="form-control" value="<?php echo $result['Address'] ?>" placeholder="Address" name="Address" required readonly>
                    <a href="javascript:void(0)" class="btn btn-default info float-xs-left edit-btn" columname="Address"><i class="fa fa-pencil"></i></a>
                    <div class="form-control-position">
                      <i class="fa fa-location-arrow"></i>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 label-control" for="currntpackage">Current Subscription <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                    <input type="hidden" name="usertype" value="studio_user">
                    <input type="text" id="currntpackage" class="form-control" value="<?php echo $result['Current_package'] ?>" placeholder="Subscription" name="Studio_Name" required readonly>
                    <div class="form-control-position">
                      <i class="fa fa-building"></i>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="usertype" value="<?php echo session()->get('user_type')?>">
                <div class="form-group row">
                  <label class="col-md-2 label-control" for="workingdays">Working Hour <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                   
                   <!-- <a  data-toggle="modal" data-target="#myModal" style="text-decoration:none">Click Here to Edit Working Hours </a> -->

                     <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">Add Working Hours</button>

                    <div class="form-control-position">
                     <!--  <i class="fa fa-building"></i> -->
                    </div>
                  </div>

                </div>
            <?php if(count($Bank_details)>0 && session()->get('user_type')!='freelancer'){ 
              ?>
                <div class="form-group row">
                  <label class="col-md-2 label-control" for="workingdays">Bank Details <span class="text-red">*</span></label>
                  <div class="col-md-9 has-icon-left">
                   <!-- <a  data-toggle="modal" data-target="#myModal" style="text-decoration:none">Click Here to Edit Working Hours </a> -->
                     <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#bankdetails">Click here To Display </button>
                    <div class="form-control-position">
                     <!--  <i class="fa fa-building"></i> -->
                    </div>
                  </div>

                </div>
            <?php } ?>
              </div><!-- ENDS .form-body -->
            </form> 
          </div>
        </div>
      </div><!-- Recent invoice with Statistics -->
    </div>
  </div>
</div>

<!-- Modal -->
</div> 

<button type="button" id="service_modal_open" class="btn btn-primary" data-target="#service_modal" data-toggle="modal" style="display:none">
                            </button>
<div class="modal fade" id="service_modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                                    <!-- <input type="file" onchange="readURL(this);"/> -->
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                                <div class="modal-body">
                                <div id="service_divimage1">
                                    <div id="service_divimage">
                                    <img id="serviceimg"   src=""  alt="Picture" height="500px" width="568px">
                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="crop_button" data-dismiss="modal" class="btn btn-default" >Crop</button>
                                    <!-- <button >Crop</button> -->
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Working Hours</h4>

        </div>
        <div class="modal-body">


        <form id ="editform">
          <div class="form-group">
            <label for="pwd">Days:</label>
            <select id="days" name="days" class="form-control">
              '<option value="">Select Day</option>
              <?php 
                $days = getDays();
                foreach ($days as $value) {
                  echo '<option value="'.$value.'">'.$value.'</option>';
                }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label for="pwd">Open From:</label>
            <input type="text" name="from" id="from" class="form-control basicExample" >
            <input type="hidden" id="studio_id" name='studio_id' value="<?php echo $result['studio_id'] ?>">
          </div>
          <div class="form-group">
            <label for="pwd">Close At:</label>
            <input type="text" name="to" id="to" class="form-control basicExample" >
          </div>
          <div class="form-group">
          <label class="col-md-5 label-control" style="">Happy Hours </label>
                    <!-- <div class="col-md-4 mb-15-resp">
                    </div> -->
                    <div class="col-md-2 mb-15-resp text-center">
                        <input type="checkbox" class="js-switch" name='switch'/>
                    </div>
                </div>
        <br>
        <!-- <input type="hidden" value="" id="rowid"> -->

       <div class="pull-right">

           <button class="btn btn-primary" id="updateworkinghours">Add working Hours</button>
         </div>
        
        </form>
        <br>
        <br>

        <table class="table table-bordered">
          <thead>
            <tr>
              <th > Working Days</th>
              <th> Timings</th>
              <th> Day Light Savings</th>
              <th> Action</th>
            </tr>
          </thead>
          <tbody id="append_data">
          <?php 
          $workingDays= $result['workingDays'];
          $workingDays= explode(',', $workingDays);
          $StartTime  = $result['StartTime'];
          $StartTime  = explode(',', $StartTime);
          $working_end= $result['working_end'];
          $working_end= explode(',', $working_end);
          $row_id= $result['row_id'];
          $row_id= explode(',', $row_id);
          $is_daylight_saving = explode(',',$result['is_daylight_saving']);
           for($i=0; $i<count($workingDays); $i++)
            { 
              
          ?>
            <tr >
              <td class='days_lenghth'><?php echo $workingDays[$i] ?></td>
              <td><?php 
              if($is_daylight_saving[$i]==1) {
                echo  date('h:i a',strtotime("+ 1 hours",strtotime($StartTime[$i]))); ?> -- <?php echo  date('h:i a',strtotime("+ 1 hours",strtotime($working_end[$i]))) ;
               } else {
                echo  date('h:i a',strtotime($StartTime[$i])); ?> -- <?php echo date('h:i a',strtotime($working_end[$i])) ;
               } ?> </td>
              <td><?php 
               if($is_daylight_saving[$i]==1){
                  echo "Yes" ;
                }else {
                  echo "No" ;
                }
              ?> </td>
              <td><a href="javascript:void(0)" id="editworking" style="text-decoration:none"><i class="fa fa-trash-o workingadd" <?php echo 'id=hours'.$row_id[$i] ?> rowid="<?php echo $row_id[$i] ?>"></i> </a></td>


            </tr>
      <?php } ?>
          </tbody>
        </table>
      
        </div>
        <div class="modal-footer">

          <button type="button" id="closepophours" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="bankdetails" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Bank Details</h4>
        </div>
        <div class="modal-body">
        <table class="table table-bordered">
    <!-- <thead> -->
      <!-- <tr>
        <th></th>
        <th>Lastname</th>
      </tr> -->
    <!-- </thead> -->
    <tbody>
    <?php if(count($Bank_details)>0){ ?>
      <tr>
        <td>First Name (Account Owner)</td>
        <td><?php echo $Bank_details->first_name ?></td>
      </tr>
      <tr>
        <td>Last Name (Account Owner) *</td>
        <td><?php echo $Bank_details->last_Name ?></td>
      </tr>
      <tr>
        <td>IBAN Number *</td>
        <td><?php echo $Bank_details->iban_num ?></td>
      </tr>
      <tr>
        <td>Bank Name *</td>
        <td><?php echo $Bank_details->bank_name ?></td>
      </tr>
      <tr>
        <td>Bank Zip *</td>
        <td><?php echo $Bank_details->bank_zip ?></td>
      </tr>
    <?php }  ?>
    </tbody>
  </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@include('shared/footer')

 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="{{asset('public/js/login/login.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
<script  src="{{asset('public/js/timepicker/jquery-ui-timepicker-addon.js')}}"></script>
<script src="{{asset('public/js/mdtimepicker.js')}}"></script>


<script type="text/javascript">
 //Not to conflict with other scripts
jQuery(document).ready(function($) {
//   $('#from').timepicker({
//   // hourGrid: 4,
//   // minuteGrid: 10,
//    timeFormat: 'hh:mm:ss'
// });
var elem = document.querySelector('.js-switch');
var switchery  = new Switchery(elem);
$( "#days" ).datepicker({
    'dateFormat':'yy-mm-dd',
    onSelect: function(dateText){
        var seldate = $(this).datepicker('getDate');
        seldate = seldate.toDateString();
        seldate = seldate.split(' ');
        var weekday=new Array();
            weekday['Mon']="Monday";
            weekday['Tue']="Tuesday";
            weekday['Wed']="Wednesday";
            weekday['Thu']="Thursday";
            weekday['Fri']="Friday";
            weekday['Sat']="Saturday";
            weekday['Sun']="Sunday";
        var dayOfWeek = weekday[seldate[0]];
        $('#days').val(dayOfWeek);
    }
});

//   $('#to').timepicker({
//   // hourGrid: 4,
//   // minuteGrid: 10,
//     timeFormat: 'hh:mm:ss'
// });

$('.basicExample').mdtimepicker({
        // format of the time value (data-time attribute)
        timeFormat: 'hh:mm:ss',
        // format of the input value
        format: 'h:mm tt',     
        // theme of the timepicker
        // 'red', 'purple', 'indigo', 'teal', 'green'
        theme: 'red',       
        // determines if input is readonly
        readOnly: true,      
        // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
        hourPadding: false    
        });
        $(document).on('click','#check_all',function(){
        if($(this).is(":checked")){
        $(".days_name").prop('checked',true);
        }else{
        $('.days_name').prop('checked',false);
        }

        });
});
  </script>






