<style type="text/css">
	#carousel-example{
		
		overflow: hidden;
		-webkit-box-shadow: 0px 5px 2px -4px #999;
		-moz-box-shadow: 0px 5px 2px -4px #999;
		box-shadow: 0px 5px 2px -4px #999;
	}

	#carousel-example  .auth-img img{
		height: 100%;
	}
	#carousel-example  .p-text{
		line-height: 1.45;
	}
	#carousel-example  .icon-chevron-left2,
	#carousel-example  .icon-chevron-right2{
		line-height: 95px;
	}
	.carousel-indicators li.active,
	.carousel-indicators-img{
		border-color: #f03b66 !important;
		opacity: .5;
	}
	#innerStudio .carousel-indicators li.active{
		opacity: 1
	}
	#innerStudio .carousel-indicators li{
		margin: 0px;
	}
	#innerStudio .carousel-inner{
		height: 270px;
		border: 1px solid #ccc;
	}
</style>
@include('shared/header')

@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <div class="card" id="innerStudio">
                <div class="card-header customTitle">
                    <h4 class="card-title"><?php echo $result[0]['category']?><small> <?php  echo count($result) ?> </small></h4>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        	<div id="carousel-example" class="carousel slide" data-ride="carousel">
								<div class="card-header p-0 mb-1 border0">
									<ol class="carousel-indicators">
										<?php 
										for($i=0;$i<count($result);$i++) 
										{ ?>
										<li data-target="#carousel-example" data-slide-to="<?php echo $i ?>" class="carousel-indicators-img active"><img src="<?php echo asset($result[$i]['thumbnail']); ?>" alt="element 04" class="float-xs-left"></li>

										<?php } ?>

									</ol>
								</div>
                            <div class="carousel-inner" role="listbox">
                                <?php 
                                    for($i=0;$i<count($result);$i++) 
                                { ?>
                                <div class="carousel-item active">                                                    
                                    <div class="card-body">
                                        <div class="auth-img col-md-3 col-sm-4 col-xs-4 pl-0"><img src="<?php echo asset($result[$i]['thumbnail']) ?>" alt="element 04"class="float-xs-left"></div>
                                        <div class=" col-md-9 col-sm-8 col-xs-8 pl-0 pb-1">
                                            <div class="card-header p-0 pt-1 border0">
                                                <a href="#" class="float-xs-left text-capitalize"><h3><?php echo $result[$i]['title'] ?>  </h3></a>
                                                <div class="subsbtn"> 
                                                    <span class=""><a href="#" class="btn btn-default btn-danger bg-danger bg-darken-1 btn-subscribe"><i class="fa fa-youtube-play"></i> &nbsp;Subscribe</a><span class="txt"><?php echo $result[$i]['likes'] ?></span></span>
                                                </div>
                                            </div>
                                            <p class="card-text"><?php echo $result[$i]['discribtion'] ?></p>
                                            <h4 class="card-title mt-1">Preview</h4>
                                            <img src="<?php echo asset($result[$i]['thumbnail']) ?>" alt="element 04" height="100" width="150" class="float-xs-left">
                                        </div>
                                    </div>
                                </div>        <?php } ?>
								<a class="left carousel-control" href="#carousel-example" role="button" data-slide="prev"><span class="icon-chevron-left2" aria-hidden="true"></span><span class="sr-only">Previous</span></a>
                            <a class="right carousel-control" href="#carousel-example" role="button" data-slide="next"><span class="icon-chevron-right2" aria-hidden="true"></span><span class="sr-only">Next</span></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div><!-- Recent invoice with Statistics -->
        </div>
    </div>
</div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
   