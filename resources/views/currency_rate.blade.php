@include('shared/header')

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;



}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;


}
.active{
color:red;

}
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card-body collapse in"> 
        <div class="card-block">
        <section id="basic-modals">
              <div class="row">
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Currency Rate</h4>
                    </div>
                    <div class="card-body collapse in">
                      <div class="card-block">
                        <form class="form" method='POST' action="{{url('/currency_rate')}}" >
                          <div class="form-body">                
                            <div class="form-group">
                                <table class="table table-bordered">
                                <thead>
                                <tr>
                                <th>Currency Name</th>
                                <th>Currency Code</th>
                                <th>Hotspot Rate</th>
                                <th>Google Rate</th>
                                </tr>
                                </thead>
                                <tbody>
                            <?php foreach($data as $value) { ?>
                                <tr>
                                <td><?php echo $value->currency_name ?></td>
                                <td><?php echo "1 ". $value->currency_code ?></td>
                                <td><input type='text' name='rate[]' value='<?php echo $value->Rate ?>' class='ratechange form-control' tocurrency='<?php echo $value->currency_code ?>'></td>
                                <td><input type='text' value='<?php echo $value->covertcurrency ?>' disabled class=" form-control"></td>
                                </tr>
                            <?php } ?>
                                </tbody>
                                </table>
                          </div>
                          </div>
                          <input type='submit' value='Update' class="btn btn-primary pull-right">
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      </div>

<!-- Recent invoice with Statistics -->
        </div>
      </div>
    </div>
    </div><!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>

<script src="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/vendors/js/editors/ckeditor/ckeditor.js" type="text/javascript"></script>

<script>
// $(document).on('keyup','.ratechange',function(){
//     selector = $(this);
//     tocurrency  = $(this).attr('tocurrency');
//     amount      = $(this).val();
//     $.ajax({
//         url: login.base_url+`convert_rate/USD/${tocurrency}/${amount}`,
//         type: 'GET',
//         data: '',
//         async: false,
//         success: function(data) {
//             // console.log(data);
//             selector.parent().parent().find('td:last').children().val(data);
//         },
//         cache: false,
//         contentType: false,
//         processData: false
//     });
// })
    

</script>