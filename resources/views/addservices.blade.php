@include('shared/header')
<style type="text/css">
 .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 08px;
}
.fileUpload input.upload_service {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}  

button.close {
    opacity: 1 !important;

}
 .proimg{
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin: auto;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 } 

/*a:hover{
  color:white;
  text-decoration:none;
}*/
</style>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.css" rel="stylesheet" type="text/css"/>
<link href=" https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/css/mdtimepicker.css')}}" rel="stylesheet"/>
<link  href="https://rawgit.com/fengyuanchen/cropperjs/master/dist/cropper.css" rel="stylesheet">
<script src="{{asset('public/js/cropper.js')}}"></script>
<script src="{{asset('public/js/php_datetime.min.js')}}"></script>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

<div class="imgappend" style="display:none"></div>  
<div class="app-content content container-fluid">
    <div class="content-wrapper">
    
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <div class="card">
            <form  id="formservice" enctype="multipart/form-data" method="POST" class="form form-horizontal row-separator fv-form fv-form-bootstrap">
            <?php
            // echo "<pre>";
            // print_r($services);

            // exit;
            ?>
            <div class="proimg" style="background-image:url('<?php echo asset('').@$services->services_image ?>')"></div>
            <input type="hidden"  id="service_image_blob" value="">
            <input type="hidden" name="Studio_id" value="<?php echo $studio_id ?>">
            <input type="hidden" name="service_id" value="<?php echo @$services->id ?>">

            <div class="form-body">
                <div class="form-group row">
                    <label class="col-md-2 label-control">Upload Logo <span class="text-red">*</span></label>
                    <div class="col-md-9 has-icon-left" style="padding-left: 35px;">
                    <label id="serviceimg" class="file center-block">
                        <div class="fileUpload btn btn-primary" style="margin:0;">
                        <span><i class="fa fa-upload"></i> Upload</span>
                        <input type="file" class="upload_service" id="serviceimg" name="services_image" accept=".png, .jpg, .jpeg"/>
                        </div>
                    </label>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 label-control">Select Service <span class="text-red">*</span></label>
                    <div class="col-md-9 has-icon-left">
                        <select id="selectservice" name="Category_id" class="form-control js-example-basic-single"  style="width: 100%" required>
                           
                            <option disabled selected value>Select Service</option>
                            <?php 
                            foreach ($video_category as $key => $value) {

                            ?>
                            <option value="<?php echo $value->id ?>"><?php echo $value->category_name ?></option>

                            <?php }  ?>

                        </select>
                     
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 label-control">Service For <span class="text-red">*</span></label>
                    <div class="col-md-9 has-icon-left">
                        <select id="gender" name="gender"  class="form-control js-example-basic-single"  data-fv-message="The user type is not valid"
                        required data-fv-notempty-message="The user type is required and cannot be empty">
                        <option disabled selected value>Service For</option>
                        <option value="Male"> Male  </option>
                        <option value="Female"> Female  </option>
                        <option value="Kids">  Kids</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 label-control">Select Duration <span class="text-red">*</span></label>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number"  style="line-height: 24px;" id="duration" class="form-control"   name="service_time" required data-fv-message="The Time is not valid" data-fv-notempty-message="The Time  is required and cannot be empty" pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Time can only consist of time like '3 min' , 4 hour" value="<?php 
                                    if(@$services->service_time_unit=='minutes'){
                                        echo @($services->service_time/1000)/60;
                                    } else {
                                        echo @(($services->service_time/(1000*60*60)) / 24);
                                    }
                                    ?>">
                            </div>
                            <div class="col-md-6">
                                <select id="time" name="Timeunit" class="form-control js-example-basic-single">
                                    <option value="hours">Hours</option>
                                    <option value="minutes">  Minutes</option>
                                </select>
                                <!-- <input id="time" name="timeunit"  class="form-control" value="Min"  required readonly> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                        <label class="col-md-2 label-control">Service Title <span class="text-red">*</span></label>
                        <div class="col-md-9">
                        <!-- <select id="selectservicetype" name="SubCategory_id" class="form-control js-example-basic-single"  style="width: 100%" required disabled>
                        <option  selected value readonly>Select Service Type</option>
                        </select> -->
                            <input type="text" name="service_title" class="form-control"  style="width: 100%" required placeholder="Service Title" id="servicetitle" value='<?php echo @$services->service_title ?>'; >
                        </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 label-control">Service Amount <span class="text-red">*</span></label>
                    <div class="col-md-9">
                        <input type="number"  id="Amount" class="form-control"   name="price" value="<?php echo @$services->Service_charges ?>" required  placeholder="Enter Amount"> 
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 label-control">Service Description <span class="text-red">*</span></label>
                    <div class="col-md-9" style="padding: 5px 0px 5px 20px;">
                        <textarea placeholder="Description" name="Description" class="form-control"  id="Description" required><?php echo @$services->Description ?></textarea>
                    </div>
                </div>
            </div>

                <div class="form-group row" style="border-bottom: none;">
                <label class="col-md-5 label-control" style="padding-top: 20px;">Happy Hours </label>
                    <!-- <div class="col-md-4 mb-15-resp">
                    </div> -->
                    <div class="col-md-2 mb-15-resp text-center">
                        <input type="checkbox" class="js-switch" name='switch'/>
                    </div>
                </div>
            </div>


            <div class="form-body" style="padding-bottom: 10px;">
                <div class="half mb-15 formRow" id="is_happy_hour" style="display:none">
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control radius-50 enable basicExample" name="happy_from" placeholder="from" style="background-color:white !important;" value ="<?php if(@$services->discount_amount>0){  
                                echo  @date('H:i:s a',strtotime($services->happy_hour_from));
                                }
                                ?>" readonly/>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control radius-50 enable basicExample" name="happy_to" placeholder="To" style="background-color:white !important;" value ="<?php
                                if(@$services->discount_amount>0){
                                echo  @date('H:i:s a',strtotime($services->happy_hour_to));
                                } ?>"
                                readonly/>
                            </div>
                            <span style="position:absolute;right:14%;font-size: 18px;bottom: 24%;color:red">  </span>
                            <div class="col-sm-3">
                                <input type="number" class="form-control radius-50 enable" name="price_discount" placeholder="Discount Price" style="background-color:white !important;" min="1"  value='<?php echo @$services->discount_amount ?>' readonly/>
                            </div>
                    </div>

                    <h4><p style="text-align:center; margin-top: 20px; margin-bottom: 20px;">Happy Hour Days </h4>
                        <div class="pretty p-svg p-curve" style="width: 100%; text-align:center; margin-bottom: 20px;">
                        <div style="position: relative; width: 100px; margin: 0px auto;">
                            <input type="checkbox" id="check_all"/>
                                <div class="state p-success">
                                <!-- svg path -->
                                    <svg class="svg svg-icon" viewBox="0 0 20 20">
                                    <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path>
                                    </svg>
                                    <label>Every Day</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="col-md-2">
                        </div>
                                <div class="col-md-8" style="padding-bottom: 20px;">
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Saturday"/>
                                            <div class="state p-primary-o">
                                            <label>Saturday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Sunday"/>
                                            <div class="state p-primary-o">
                                            <label>Sunday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Monday"/>
                                            <div class="state p-primary-o">
                                            <label>Monday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Tuesday"/>
                                            <div class="state p-primary-o">
                                            <label>Tuesday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Wednesday"/>
                                            <div class="state p-primary-o">
                                            <label>Wednesday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Thursday"/>
                                            <div class="state p-primary-o">
                                            <label>Thursday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Friday"/>
                                            <div class="state p-primary-o">
                                            <label>Friday</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-md-12 text-center">

                           <?php if(isset($edit)) { ?>
                            <button class="btn btn-primary" id="formsubmit">Edit Service</button>
                           <?php }  else { ?>
                            <button class="btn btn-primary" id="formsubmit">Add Service</button>
                            <?php }  ?>
                            </div>
            </form>
          </div>
        </div>
    </div>
</div>
</div>
<button type="button" id="service_modal_open" class="btn btn-primary" data-target="#service_modal" data-toggle="modal" style="display:none">
</button>
<div class="modal fade" id="service_modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                                    <!-- <input type="file" onchange="readURL(this);"/> -->
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                                <div class="modal-body">
                                <div id="service_divimage1">
                                    <div id="service_divimage">
                                    <img id="serviceimg"   src=""  alt="Picture" height="500px" width="568px">
                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="crop_button" data-dismiss="modal" class="btn btn-default" >Crop</button>
                                    <!-- <button >Crop</button> -->
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="{{asset('public/js/mdtimepicker.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.js"></script>
<script type="text/javascript">
   setTimeout(function(){
          $('[data-toggle="tooltip"]').tooltip();
          },1000);
        var elem = document.querySelector('.js-switch');
        var switchery  = new Switchery(elem);

        $(document).on('change','.js-switch',function(e){
        if(this.checked){
        $('.enable').removeAttr('readonly');
        $('#is_happy_hour').show();
        $('#days_display').show();

        } else {
        $('.enable').attr('readonly','readonly');
        $('#is_happy_hour').hide();
        $('#days_display').hide();
        }
        });
        $('.basicExample').mdtimepicker({
        // format of the time value (data-time attribute)
        timeFormat: 'hh:mm:ss.000',
        // format of the input value
        format: 'h:mm tt',     
        // theme of the timepicker
        // 'red', 'purple', 'indigo', 'teal', 'green'
        theme: 'red',       
        // determines if input is readonly
        readOnly: true,      
        // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
        hourPadding: false    
        });
        $(document).on('click','#check_all',function(){
        if($(this).is(":checked")){
        $(".days_name").prop('checked',true);
        }else{
        $('.days_name').prop('checked',false);
        }

        });
        
$('.upload_service').change(function(e) {
        // e.preventDefault();
        // e.stopPropagation();
        console.log(this);
        // alert(this);
        if ($(this).val()) {
        size_in_kb=this.files[0].size/1024;
        if(size_in_kb > 400){
        alert('The file size can not exceed 400Kb.');
        $('.upload').val('');
        error = false;
        } else{
        // error = false;
        // var filename = $(this).val();
        // $('.imgappend').append('<img>');
        // console.log(filename);
        // admin.readURL(this, 'proimg');
        $('.viral-loader').show();
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#service_divimage').remove();
        $("#service_divimage1").append(`<div id="service_divimage"><img id="serviceimg_2"   src=""  alt="Picture" height="500px" width="568px"> </div>`);
        $('#serviceimg_2').attr('src',e.target.result);
        setTimeout(function () {
        initCropper();
        $('.viral-loader').hide();
        }, 1000);
        $('#service_modal_open').trigger('click');
        };
        reader.readAsDataURL(this.files[0]);
        // $("#service_img_file").val('');
        }

        }
    });


    function initCropper(){
        console.log("Came here");
        var image = document.getElementById('serviceimg_2');
        var cropper = new Cropper(image, {
        aspectRatio: 1/1,
        center: true,
        highlight: true,
        background: true,
        minContainerWidth:500,
        minContainerHeight:500,
        getCroppedCanvas:{fillcolor: "#FFFFFF"},
        crop: function(e) {
        console.log(e.detail.x);
        console.log(e.detail.y);
    }
    });
    $(document).off("click", "#crop_button").on("click", "#crop_button", function(e) {  
        // $('.viral-loader').show();
        // console.log(cropper.getCroppedCanvas().toDataURL());
        // $("#proimg").attr('src',cropper.getCroppedCanvas().toDataURL());
        $('.proimg').css('background-image', 'url(' + cropper.getCroppedCanvas().toDataURL() + ')');
        $("#service_image_blob").val(cropper.getCroppedCanvas().toDataURL('image/jpeg', 1));
    });
}
<?php if(@isset($services->Category_id)){ ?>
$('[name=Category_id]').val('<?php echo $services->Category_id ?>').change();

<?php  }  ?>

<?php if(@isset($services->gender)) { ?>
    $('#gender').val('<?php echo $services->gender ?>').change();
<?php } ?>

<?php if(@isset($services->service_time_unit)) { ?>
    $('[name=Timeunit]').val('<?php echo $services->service_time_unit ?>').change();
<?php } ?>

<?php if(@$services->discount_amount>0) { ?>
    $('.switchery').trigger('click');
    array= '<?php print_r(@$services->happy_hour_days) ?>';
    console.log(JSON.parse(array));
    happy_hour_days=JSON.parse(array);
    $.each(happy_hour_days,function(i,val){
        console.log(val);
        $('[value='+val+']').prop('checked', true);
    });
    if(happy_hour_days.length==7){
        $('#check_all').prop('checked', true);
    }

<?php } ?>

</script>

