@include('shared/header')

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;



}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;


}
.active{
color:red;

}
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
.disabled {
  pointer-events: none;
  cursor: default;
  opacity: 0.6;
}
input.parsley-success,
select.parsley-success,
textarea.parsley-success {
  color: #468847;
  background-color: #DFF0D8;
  border: 1px solid #D6E9C6;
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
  color: #B94A48;
  background-color: #F2DEDE;
  border: 1px solid #EED3D7;
}

.parsley-errors-list {
  margin: 2px 0 3px;
  padding: 0;
  list-style-type: none;
  font-size: 0.9em;
  line-height: 0.9em;
  opacity: 0;

  transition: all .3s ease-in;
  -o-transition: all .3s ease-in;
  -moz-transition: all .3s ease-in;
  -webkit-transition: all .3s ease-in;
}

.parsley-errors-list.filled {
  opacity: 1;
}
</style>
 <?php 
  $text_en=array();
  $text_de=array();
  for($i=0;$i<count($result);$i++){
     array_push($text_en,json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','', $result[$i]->content_en), true ));
     array_push($text_de,json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','', $result[$i]->content_de), true ));
  }
    //  echo "<pre>";
     

    //  print_r(json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $result[$i]->content_de), true ));
    //  exit;
    // echo "<pre>";
    // print_r($text_en);
    // exit;
    function modify($str) {
        return ucwords(str_replace("_", " ", $str));
    }
 ?>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card-body collapse in"> 
        <div class="card-block">
        <section id="basic-modals">
              <div class="row">
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Translations</h4>
                    </div>
                    <div class="card-body collapse in">
                      <div class="card-block">
                        <form class="form ajaxForm nopopup" method='POST' action="{{url('Update_translations')}}" id="form_id">

                            <div class="col-xs-12" style="margin-bottom: 20px;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select class="form-control" id="get_val" name='update_index'>
                                        <option value="" disabled selected>Select your option</option>
                                            <?php  
                                            foreach($text_en[0] as $key=>$value) { ?>
                                            <option value='<?=$key?>' en_lng='<?php echo htmlspecialchars(json_encode($text_en[0][$key]), ENT_QUOTES)?>' de_lng='<?php echo htmlspecialchars(json_encode($text_de[0][$key]), ENT_QUOTES)?>'><?=modify($key)?></option>
                                            <?php  } ?>
                                        </select>
                                    </div>

                                    <div class="pull-right">
                                        <button type="button" id="add_translationbtn" class="rounded-circle add-studio-class" style="border: none; padding-top: 0; cursor: pointer"><i class="icon-plus-round"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12" style="margin-bottom: 20px; display:none;" id="add_translation">
                                <div class="col-md-5">
                                    <textarea class="form-control typing"  data-parsley-pattern="/^[a-z\d\-_\s]+$/i" name="eng" data-parsley-trigger="keyup"></textarea>
                                </div>

                                <div class="col-md-2">
                                    <a href="javascript:void(0)" id="translate">
                                    <img src="https://www.shareicon.net/data/256x256/2017/04/11/883747_text_512x512.png" class="img-responsive center-block" style="width: 80px; margin: 0 auto; display: block;">
                                    </a>
                                </div>

                                <div class="col-md-5">
                                    <textarea class="form-control typing"  name="de" data-parsley-pattern="/^[a-z\d\-_\s]+$/i" data-parsley-trigger="keyup"></textarea>
                                </div>

                                <div class="col-xs-12">
                                    <a href="javascript:void(0)" class="btn btn-danger pull-right ajax disabled">Add Translation</a>
                                </div>

                            </div>

                            <div class="col-xs-12">
                                <div class="form-body">                
                                    <div class="form-group">
                                        <table class="table tabel-responsive">
                                            <thead>
                                                <tr>
                                                    <th>English</th>
                                                    <th>German</th>
                                                </tr>
                                            </thead>
                                            <tbody id="append_lng">
                                            

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger pr-3 pl-3 float-xs-right"  id="update_button" style="display:none" rowid=""> Update</button>
                                    </div>
                                </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      </div>

<!-- Recent invoice with Statistics -->
        </div>
      </div>
    </div>
    </div><!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<!-- <script src="{{asset('public/js/admin/custom.js')}}" type="text/javascript"></script> -->
<script src="https://parsleyjs.org/dist/parsley.min.js"> </script>
<script>
$(document).on('change','#get_val',function(){
  eng = JSON.parse($('option:selected', this).attr('en_lng'));
  ger = JSON.parse($('option:selected', this).attr('de_lng'));
  console.log(eng);
  console.log(ger);
//   eng = JSON.parse(eng);
//   ger = JSON.parse(ger);
  $("#append_lng").html('');
  html ='';
  if(eng.lenght==0){
    $("#update_button").hide();
      return false;
  }
  $("#update_button").show();
  $.each(eng,function(i,val){
     html +=`<tr>
             <td><textarea class="form-control" name='${i}'>${val}</textarea></td>
             <td><textarea class="form-control" name='${i}_de'>${ger[i]}</textarea></td>
             </tr>`;
  })
  $("#append_lng").html(html);

// <tr>
//     <td><textarea class="form-control"></textarea></td>
//     <td><textarea class="form-control"></textarea></td>
// </tr>
//   $("#en_title").val(eng.title);
//   $("#de_title").val(ger.title);
//   $("#en_description").val(eng.description);
//   $("#de_description").val(ger.description);
//   $("#en_keywords").val(eng.keywords);
//   $("#de_keywords").val(ger.keywords);
});

$(document).ready(function(){
    $("#add_translationbtn").click(function(){
        $("#add_translation").hide();
        if($("#get_val").val()!=null){
            $("#add_translation").show();
        }
        
    });

    $(document).on('click','#translate',function(){
        data ={
        cate_name    : $("textarea[name='eng']").val(),
        description  : ''
        };
        data=JSON.stringify(data);
        var query_string=`data=${data}`;
        $.post("<?=asset('').'translate'?>",query_string,function(result){
            result = JSON.parse(result);
            $("textarea[name='de']").val(result.data.cate_name);
            base_url ='<?=url('')?>';
            eng   = $("textarea[name='eng']").val();
            de    = $("textarea[name='de']").val();
            index = $("#get_val").val();
            $('.ajax').attr('href',base_url+'/add_word/'+index+'/'+eng+'/'+de);
            if(eng!='' && eng!=null && eng!="" && de!='' && de!=null && de!=""){
                $('.ajax').removeClass('disabled');
            }
        });

    })
    $(document).on('keyup',".typing",function(){
        base_url ='<?=url('')?>';
        eng   = $("textarea[name='eng']").val();
        de    = $("textarea[name='de']").val();
        index = $("#get_val").val();
        $('.ajax').attr('href',base_url+'/add_word/'+index+'/'+eng+'/'+de);
        var ok = $('.parsley-error').length ;
        if(eng!='' && eng!=null && eng!="" && de!='' && de!=null && de!="" && ok == 0){
            $('.ajax').removeClass('disabled');
        } else {
            $('.ajax').addClass('disabled');
        }
       
    })
});
$("#form_id").parsley();
</script>