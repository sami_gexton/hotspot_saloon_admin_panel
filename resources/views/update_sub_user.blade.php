@include('shared/header')
@section('Content') 
<style type="text/css">
.help-block
{

 color: red;

}
table.dataTable {
width: 100%;
}
button.close {
    opacity: 1 !important;

}
/**table{*/
    /* margin: 0 auto;
     width: 100%;
     clear: both;
     border-collapse: collapse;
     table-layout: fixed;
     word-wrap:break-word;*/
/*}*/
</style> 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
         <div class="card">
<div class="card-header">
<h4 class="card-title">Sub Admins</h4>
<div class="heading-elements" style="top: 9px;">
<!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModalpackage">
<span class="ladda-label">Create New</span> -->
</button>
</div>
</div>
<div class="card-body collapse in">
<div class="card-block card-dashboard">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap4">
<div class="row">
<div class="col-md-12">

    <table id="packages" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
        <thead>
            <tr role="row">
                <th>Date</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Coutry</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // echo "<pre>";
            // print_r($result);
            // exit();
            if(count($result)>0){
            for($i=0;$i<count($result);$i++)
            { 
            ?>
            <tr role="row" class="odd">
                <td style="width:auto;"> <?php echo date('M d, Y',strtotime($result[$i]['created_date'])) ?></td>
                <td><?php echo $result[$i]['Studio_Name'] ?></td>
                <td><?php echo $result[$i]['email'] ?></td>
            
                <td><?php echo $result[$i]['country'] ?></td>
                <td> 
                <a class='btn btn-danger btn-sm deleteuser' title='Delete' aria-hidden='true' data-toggle='tooltip' href="#" userid="<?php echo $result[$i]['id'] ?>">
                <i class='fa fa-trash-o'></i>
                </a>
                </td>
            </tr>

        <?php  }  }  ?>

    
           
        </tbody>                
    </table>
</div>
</div>
</div>              
</div>
</div>
</div>  


           
        </div>
       
        
    </div>
</div>

      <!-- Modal content-->
   
      
    </div>
  </div>

   
@include('shared/footer')

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">

