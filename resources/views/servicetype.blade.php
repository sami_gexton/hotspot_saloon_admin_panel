@include('shared/header')

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;
}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;
}
.active{
color:red;

}
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
    .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 08px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.proimg {
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin: auto;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 } 
 .editimage{
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin: auto;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 } 

 .img-thumbnail {
    padding: .25rem;
    background-color: #F1F1F1;
    border: 1px solid #DDD;
    border-radius: .18rem;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    background-size: 100% 100%;
    height:40px;
    width:40px;
}
.help-block
{

 color: red;

}
body
{
    counter-reset: Serial;           /* Set the Serial counter to 0 */
}
tr td:first-child:before
{
  counter-increment: Serial;      /* Increment the Serial counter */
  content: counter(Serial); /* Display the counter */
}
button.close {
    opacity: 1 !important;

}
textarea {
    resize: none;
}
</style>
<link  href="https://rawgit.com/fengyuanchen/cropperjs/master/dist/cropper.css" rel="stylesheet">
<script src="{{asset('public/js/cropper.js')}}"></script>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
    <input type="hidden" id="imgprofile" value="{{asset('public/assets/images/placeholder.png')}}">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="imgappend" style="display:none"></div>  
        <div class="content-body"><!-- stats -->
                    <div class="card">
            <div class="card-header mb-3">
                <h4 class="card-title">Add Category</h4>
            </div>
            <div class="card-body collapse in">
                 <div class="proimg"></div>
                <div class="card-block card-dashboard" id="formContainer">
                    <form class="form form-horizontal row-separator fv-form fv-form-bootstrap" id="addcategory" data-fv-framework="bootstrap" data-fv-message="Image is requried" data-fv-feedbackicons-valid="glyphicon glyphicon-ok" data-fv-feedbackicons-invalid="glyphicon glyphicon-remove" data-fv-feedbackicons-validating="glyphicon glyphicon-refresh" enctype="multipart/form-data" method="POST" novalidate>
                          <div class="form-body">
                          <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>" name="token">

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="Category"> Category Image <span class="text-red">*</span></label>
                                <div class="col-md-4 has-icon-right">
                                  <!-- <input type="text" id="subcattegory" class="form-control" placeholder="Enter Amount" name="zipcode" required="" data-fv-field="zipcode"> -->

                                 <!--  <i class="form-control-feedback" data-fv-icon-for="zipcode" style="display: none;"></i> -->
                                  <!-- <div class="form-control-position">
                                      <i class="fa fa-plus"></i>
                                  </div> -->
                                  <label id="uploadimg" class="file center-block">
                                  <div class="fileUpload btn btn-primary" style="margin:0;">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input type="file" class="upload" id="uploadimg" name="profile_img"  accept=".png, .jpg, .jpeg" required/>
                                      <input type="hidden"  id="service_image_blob" value="">
                                  </div>
                              </label>
                               </div>
                              </div>

                              <div class="form-group row has-feedback">
                                <div class="col-xs-12">
                                  <div class="col-md-6 text-center">
                                    <h4>ENGLISH</h4>
                                  </div>

                                  <div class="col-md-6 text-center">
                                    <h4>GERMANY</h4>
                                  </div>

                                </div>
                              </div>

                               <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="newcategory">New Category <span class="text-red">*</span></label>
                                <div class="col-md-4 has-icon-right">
                                  <input type="text" id="newcategory" class="form-control" placeholder="Category Name" name="Category" data-fv-field="Category" 
                                  data-fv-message="The Category Name is not valid"
                                  required data-fv-notempty-message="The Category Name is required and cannot be empty"
                                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Category Name can only consist of alphabetical"><i class="form-control-feedback" data-fv-icon-for="Category" style="display: none;"></i>
                                  <!-- <div class="form-control-position">
                                      <i class="fa fa-location-arrow"></i>
                                  </div> -->
                                <!-- <small class="help-block" data-fv-validator="notEmpty" data-fv-for="Category" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small> -->
                                </div>


                                <div class="col-md-1">
                                  <a href="javascript:void(0)" id="translate" ><img src="https://www.shareicon.net/data/256x256/2017/04/11/883747_text_512x512.png" alt="" width="50" class="pull-left" style="margin-top: 5px; margin-left: 5px; position: absolute; top: 35px;">
                                  </a>
                                </div>
                                <div class="col-md-4 has-icon-right">
                                  <input type="text" id="newcategory_de" class="form-control" placeholder="Category Name" name="newcategory_de" data-fv-field="Category" 
                                  data-fv-message="The Category Name is not valid"
                                  required data-fv-notempty-message="The Category Name is required and cannot be empty"
                                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Category Name can only consist of alphabetical"><i class="form-control-feedback" data-fv-icon-for="Category" style="display: none;"></i>
                                  <!-- <div class="form-control-position">
                                      <i class="fa fa-location-arrow"></i>
                                  </div> -->
                                <!-- <small class="help-block" data-fv-validator="notEmpty" data-fv-for="Category" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small> -->
                                </div>
                              </div>

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="usertype">Description<span class="text-red">*</span></label>
                                <div class="col-md-4 has-icon-right">
                                <textarea class="form-control" name="description"></textarea>
                                  <!-- <div class="form-control-position">
                                      <i class="fa fa-location-arrow"></i>
                                  </div> -->
                                <!-- <small class="help-block" data-fv-validator="notEmpty" data-fv-for="Category" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small> -->
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-4 has-icon-right">
                                  <textarea class="form-control" name="description_de"></textarea>
                                </div>
                              </div>

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="usertype">User Type <span class="text-red">*</span></label>
                                  <div class="col-md-4 has-icon-right">
                                  <select id="usertype" name="status"  class="form-control js-example-basic-single"  data-fv-message="The user type is not valid"
                                    required data-fv-notempty-message="The user type is required and cannot be empty" required>
                                  <option disabled selected value>Select User Type</option>

                                  <option value="studio">
                                  Studio  
                                  </option>
                                  <option value="user">
                                  User
                                  </option>
                                  </select>
                                    <!-- <div class="form-control-position">
                                        <i class="fa fa-location-arrow"></i>
                                    </div> -->
                                  <!-- <small class="help-block" data-fv-validator="notEmpty" data-fv-for="Category" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small> -->
                                  </div>
                                </div>

                <div class="form-group row has-feedback" style="border-bottom: none;">
                  <div class="col-md-12">
                    <button  id="btncategory" class="btn btn-danger pr-3 pl-3">Create</button>
                  </div>
                </div>
                            </div><!-- ENDS FORM GROUP -->
                        </form>
                    </div>
          <div class="card-block card-dashboard">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap4">
              <div class="row">
                <div class="col-md-12">
                  <table id="Category" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                    <thead>
                      <tr role="row">
                        <th style="width: 35px;">S.No</th>
                        <th>Category Image</th>
                        <th>Category Name</th>
                        <th>Category Name (de)</th>
                        <th>Category For</th>
                        <th>Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="dataapend">
                    <?php 
                       $counts = 0;
                      for($i=0;$i<count($result);$i++)
                       {

                     ?> 
                     <tr>
                      <td ></td>
                      <td><div class='img-thumbnail' id="<?php echo  "catimageid".$result[$i]->id  ?>" style="max-height:40px;background-image:url(<?php echo asset($result[$i]->service_image) ?> )"/> </div></td>
                      <td id="<?php echo  "catnameid".$result[$i]->id ?>" style="width: 150px;">
                       <?php echo ucwords($result[$i]->category_name) ?></td>
                       <td id="" style="width: 150px;display: table-cell;    white-space: normal !important;" >
                       <?php echo ucwords($result[$i]->category_name_de) ?></td>
                       <td id="<?php echo  "status".$result[$i]->id ?>">
                       <?php echo ucfirst($result[$i]->status) ?></td>
                       <td id="<?php echo  "dis".$result[$i]->id  ?>" style="width:700px;display: table-cell;    white-space: normal !important;" text="{{$result[$i]->description}}" >
                       {{App\BasicModel::short_para($result[$i]->description,50,100)}}
                       </td>
                       <td> 
                          @if($result[$i]->slug != 'presentation-video')
                           <a   class="btn btn-info btn-sm editmodal" aria-hidden='true' data-toggle="modal" href="#editmodal" userid="<?php echo $result[$i]->id ?>" data='<?=json_encode($result[$i])?>' >  <i class='fa fa-pencil'></i></a>

                           <!-- <a class='btn btn-danger btn-sm deletcat' title='Delete' aria-hidden='true' data-toggle='tooltip' href="#" userid="<?php echo $result[$i]->id ?>">
                           <i class='fa fa-trash-o'></i>
                          </a> -->
                          @endif
                      </td>
                     
                     </tr>
                    

                   <?php  } ?>
                     
                      
                    </tbody>        
                  </table>
                </div>
              </div>
            </div>        
          </div>

                </div>
            </div>

      </div>

    <div class="modal fade" id="editmodal" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content col-xs-12">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Update Category</h4>

        </div>
        <div class="modal-body">


        <form id="editcat" enctype="multipart/form-data" method="POST">
           <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>" name="token">
        <div class="editimage"></div>

        <label id="editcatimage" class="col-xs-12 file center-block">
        <br>
        <div class="fileUpload btn btn-primary" style="margin:0;left:43%">
        <span><i class="fa fa-upload"></i> Upload</span>
        <input type="file" class="upload" id="editcatimage" name="updatecatimage" />
        </div>
        </label>

        <br>
        <div class="col-md-6">
          <label><b>Category Name</b></label>
          <br>
          <input type="text" name="catname" id="editcatname" class="form-control">
          <input type="hidden" value="" id="rowid" name="rowid">
          <br>
        </div>

        <div class="col-md-6">
          <label><b>Category Name (de)</b></label>
          <br>
          <input type="text" class="form-control" name="editcatname_de" id="editcatname_de" >
        </div>
        <br>

        <div class="col-xs-12">
        <div class="row">
        <div class="col-md-6">
        <label><b>Description</b></label> 
        <br>
        <textarea rows="4" cols="70" class="form-control" name="description" id="description">
        </textarea >
        </div>

          <div class="col-md-6">
          <label><b>Description (de)</b></label> 
          <br>
          <textarea rows="4" cols="70" class="form-control"  name="description_de" id="description_de">
          </textarea >
          </div>
          </div>
      </div>
            <br>
            

            <div class="col-md-6">
            <br>
            <label><b>User Type</b></label> 
            <br> 
          <select id="usertypecat" name="status"  class="form-control js-example-basic-single"  data-fv-message="The user type is not valid"
            required data-fv-notempty-message="The user type is required and cannot be empty" required>
          <option disabled selected value>Select User Type</option>
          <option value="studio">Studio</option>
          <option value="user">User</option>
          </select>
          </div>

        <div class="col-xs-12" style="padding-bottom: 10px;">
          <div class="pull-right">
              <button class="btn btn-primary" id="updatecat">Update</button>
            </div>
          </div>
        
        </form>
        <br>
        <br>
        </div>
       <!--  <div class="modal-footer">

          <button type="button" id="closepopedit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
    </div>

    <button type="button" id="service_modal_open" class="btn btn-primary" data-target="#service_modal" data-toggle="modal" style="display:none">
                            </button>
<div class="modal fade" id="service_modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                                    <!-- <input type="file" onchange="readURL(this);"/> -->
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                                <div class="modal-body">
                                <div id="service_divimage1">
                                    <div id="service_divimage">
                                    <img id="serviceimg"   src=""  alt="Picture" height="500px" width="568px">
                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="crop_button" data-dismiss="modal" class="btn btn-default" >Crop</button>
                                    <!-- <button >Crop</button> -->
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">

<script>
$(document).on('click','#translate',function(){
  data ={
  cate_name    : $("#newcategory").val(),
  description  : $("textarea[name='description']").val()
  };
  data=JSON.stringify(data);
  var query_string=`data=${data}`;
  $.post("<?=asset('').'translate'?>",query_string,function(result){
      console.log(JSON.parse(result));
      result = JSON.parse(result);
      $("#newcategory_de").val(result.data.cate_name);
      $("textarea[name='description_de']").val(result.data.description);
  });

})


</script>