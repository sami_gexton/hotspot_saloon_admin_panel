@include('shared/header')

<style type="text/css">
.border0{
        border: 0px;
       
    }
.center-block{
        display:  block;
    }

</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <?php 
             if(count($result)>0)
             {


            ?>  
      <section id="hover-effects" class="card">
              <div class="card-header">
                  <h4 class="card-title">Channel Video </h4>
              </div>
              <div class="card-body collapse in">
                  <div class="card-block my-gallery" itemscope="" itemtype="http://schema.org/ImageGallery" data-pswp-uid="1">
                      <div class="row">
                          <div class="grid-hover">
                          <?php

              for($i=0;$i<count($result);$i++)
              {

              
              ?>
                              <div class="col-md-4 col-sm-4 col-xs-12">
                                  <figure class="effect-bubba" id="#model1" style="min-height: 240px">
                                      <img src="<?php echo asset($result[$i]['thumbnail']) ?>" alt="img16" style="height: 100%">
                                      <figcaption>
                                          <h2><?php echo $result[$i]['title'] ?></h2>
                                          <p><?php echo $result[$i]['discribtion'] ?></p>
                                          <a class="p-0" data-toggle="modal" href="#"data-target="<?php echo '#default'.$i ?>">View more</a>
                                      </figcaption>
                                  </figure>
                                  <!-- Modal -->
                                  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true"  id="<?php echo 'default'.$i ?>">
                                    <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-body">
                                      <button type="button" class="close videoclose" data-dismiss="modal" aria-label="Close" videosrcid="<?php echo 'videosrc'.$i ?>"><span aria-hidden="true">×</span></button>
                                  <video width="100%" height="360" controls id="<?php echo 'videosrc'.$i ?>" >
                                  <source   src="<?php echo asset($result[$i]['Videolink'])  ?>" type="video/mp4">
                                  </video>
                                      </div>
                                    </div>
                                    </div>
                                  </div>


                                  </div>



                               
                          <?php } ?>
                              
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ Image grid -->
          </section>
   <?php } ?>


      
    </div>
      
  </div>
</div>



    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
