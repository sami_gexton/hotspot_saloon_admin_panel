<style>@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900');</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body bgcolor="#E3E3E3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: 'Roboto', sans-serif;font-size: 11pt;color: #777777; mso-line-height-rule: exactly;line-height: 1.5em;width: 100%; min-width: 600px;" cz-shortcut-listen="true">


    <table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="padding: 0;margin: 0;border-collapse: collapse; margin: auto;">
    <!-- START - HEAD LINK -->
      <tbody>
      <!-- START - HEADER -->
      <tr>
          <td width="100%" align="center" style="background: #ffffff; border-collapse: collapse; border-top: 3px solid #d21135;">
          <div style="width: 100%;margin: auto;">
            <table border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
              <tbody>
                <tr style="background: url({{asset('public')}}/email_assets/header-bg.jpg);">
                  <td width="270" style="text-align: center; padding-top: 15px;padding-bottom: 15px;border-collapse: collapse;" align="left" valign="top">
                    <a href="#" style="display: inline-block; text-align: left; width:60%; float: left;"><img mc:edit="logo_img" editable=""  src="{{asset('public')}}/assets/images/logo/studio-logo-white.png" alt="Hotspot Studio" width="260" style=" display: inline-block;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"></a>
                    <h4 style="text-align: right; padding-top: 20px; float: right; margin:0; width: 30%; color: #fff; font-weight: 300; font-size: 20px;"><span style="padding: 0 10px;">Contact Us</span></h4>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </td>
      </tr>
      <!-- END - HEADER -->

      <!-- **************** START LAYOUT ZONE **************** -->          
      <!-- START BLOCK - BIG BUTTON & PARAGRAPH -->
      <tr>
          <td colspan="2" align="center">
                @isset($data['Message'])
                <center> <h4 style="font-size: 1.6rem; margin: 0px;font-weight: 500;"><span style="color: #e54c6f;">Contact Us</h4></center>
                @endisset
                @isset($data['contact_no'])
                <center>  <h4 style="font-size: 1.6rem; text-align: left; margin: 0px;font-weight: 500;"><span style="color: #e54c6f;">{{@$data['Name']}} Create New Account Needs To Active its Account</h4></center>
                @endisset
          </td>
      </tr>
      <tr>
   
      	<td width="100%" align="center" style="border-collapse: collapse; padding: 35px 40px 20px;">

      		<div label="Content" style="line-height: 1.67">
                <table class="table">
                  
                    <tbody>
                      <tr>
                        <td>Name</td>
                        <td>{{@$data['Name']}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td>{{@$data['Email']}}</td>
                      </tr>
                      @isset($data['Subject'])
                      <tr>
                        <td>Subject</td>
                        <td>{{@$data['Subject']}}</td>
                      </tr>
                      @endisset
                      @isset($data['Message'])
                      <tr>
                        <td>Message</td>
                        <td>{{@$data['Message']}}</td>
                      </tr>
                      @endisset
                      @isset($data['platform'])
                      <tr>
                        <td>Platform</td>
                        <td>{{@$data['platform']}}</td>
                      </tr>
                      @endisset
                      @isset($data['contact_no'])
                      <tr>
                        <td>Contact No</td>
                        <td>{{@$data['contact_no']}}</td>
                      </tr>
                      @endisset
                    </tbody>
                  </table>
	        </div>
      	</td>
      </tr>
	<tr>
		<td style="background:#ffffff; border:solid 1px #ddd; border-width:1px 0 0 0; height:1px; width:85%; margin:15px auto; display:block; line-height: 10px;">&nbsp;</td>
	</tr>
      
      <!-- END BLOCK - BIG BUTTON AND PARAGRAPH -->
                
      <!-- START - FOOTER -->
      <tr>
        <td width="100%" align="center" style="border-collapse: collapse; font-size:9pt;">
            <table border="0" bgcolor="#fff" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
              <tbody>
              <tr>
                  <td width="100%" style=" color: #767676;border-collapse: collapse;font-family: 'Roboto', sans-serif;" align="center" valign="middle" mc:edit="footer_address">
                    <p label="Footer Address">© {{date('Y')}} Hotspot Studios. All rights reserved.
                    </p>
                  </td>
              </tr>
              <tr>
                  <td width="100%" colspan="3" style="padding-top: 20px;padding-bottom: 30px;border-collapse: collapse;" align="center" valign="middle">
                      <div mc:edit="social_link">
                        <a href="#" style="margin-left:5px;width:35px;color: #aaaaaa;text-decoration: none;outline: none; display: inline-block;"><img width="100%" src="{{asset('public')}}/email_assets/facebook-circle.png" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;" alt=""></a>
                        <a href="#" style=" width:35px;margin-left:5px;display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="100%" src="{{asset('public')}}/email_assets/twitter-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a>
                        <a href="#" style="width:35px;margin-left:3px; display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="100%" src="{{asset('public')}}/email_assets/googleplus-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;">
                        </a> <a href="#" style="width:35px;margin-left:3px; display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="100%" src="{{asset('public')}}/email_assets/youtube-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a>
                      </div>
                  </td>
              </tr>
          	</tbody>
          </table>
        </td>
      </tr>
      <!-- END - FOOTER -->

      <!-- START - FOOTER LINK -->
      <tr bgcolor="#E3E3E3">
        <td width="100%" style="padding-top: 15px;padding-bottom: 10px;color: #666666;border-collapse: collapse;font-family: 'Roboto', sans-serif;;font-size:8pt" align="center">
            <div>
              You're receiving this email because you signed up at hotspot4beauty.com<br>
            </div>
            If you are not signed up on hotspot4beauty.com, you may ignore this email.
      	</td>
      </tr>
      <!-- END - FOOTER LINK -->
  </tbody>
  </table>
</body>