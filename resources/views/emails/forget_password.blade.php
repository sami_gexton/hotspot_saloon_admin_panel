<style>@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900');</style>

<body bgcolor="#E3E3E3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: 'Roboto', sans-serif;font-size: 11pt;color: #777777; mso-line-height-rule: exactly;line-height: 1.5em;width: 100%; min-width: 600px;" cz-shortcut-listen="true">
    <table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="padding: 0;margin: 0;border-collapse: collapse; margin: auto;">
    <!-- START - HEAD LINK -->
      <tbody>
      <!-- START - HEADER -->
      <tr>
          <td width="100%" align="center" style="background: #ffffff; border-collapse: collapse; border-top: 3px solid #d21135;">
          <div style="width: 100%;margin: auto;">
            <table border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
              <tbody>
                <tr style="background: url({{asset('public')}}/email_assets/header-bg.jpg);">
                  <td width="270" style="text-align: center; padding-top: 15px;padding-bottom: 15px;border-collapse: collapse;" align="left" valign="top">
                    <a href="#" style="display: inline-block; text-align: left; width:60%; float: left;"><img mc:edit="logo_img" editable=""  src="{{asset('public')}}/assets/images/logo/studio-logo-white.png" alt="Hotspot Studio" width="260" style=" display: inline-block;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"></a>
                    <h4 style="text-align: right; padding-top: 20px; float: right; margin:0; width: 30%; color: #fff; font-weight: 300; font-size: 20px;"><span style="padding: 0 10px;">Reset Password</span></h4>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </td>
      </tr>
      <!-- END - HEADER -->

      <!-- **************** START LAYOUT ZONE **************** -->          
      <!-- START BLOCK - BIG BUTTON & PARAGRAPH -->
      <tr>
      	<td width="100%" align="center" style="border-collapse: collapse; padding: 35px 40px 20px;">
      		<h4 style="font-size: 1.6rem; text-align: left; margin: 0px;font-weight: 500;"><span style="color: #e54c6f;">Dear</span> {{@$name}},</h4>
      		<div label="Content" style="line-height: 1.67">
	          <p style="margin: 0; margin-top: 1em; margin-bottom: 0px;">
	           Your new Password </p>
	            <p style="background:#ddd; color:#666; padding:5px 15px; font-size:1.8rem; text-align: left; display: inline-block; margin:10px 0;"> 
	            {{@$password}}
	          </p>
	        </div>
      	</td>
      </tr>
	<tr>
		<td style="background:#ffffff; border:solid 1px #ddd; border-width:1px 0 0 0; height:1px; width:85%; margin:15px auto; display:block; line-height: 10px;">&nbsp;</td>
	</tr>
      
      <!-- END BLOCK - BIG BUTTON AND PARAGRAPH -->
                
      <!-- START - FOOTER -->
      <tr>
        <td width="100%" align="center" style="border-collapse: collapse; font-size:9pt;">
            <table border="0" bgcolor="#fff" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
              <tbody>
              <tr>
                  <td width="100%" style=" color: #767676;border-collapse: collapse;font-family: 'Roboto', sans-serif;" align="center" valign="middle" mc:edit="footer_address">
                    <p label="Footer Address">© {{date('Y')}} Hotspot Studios. All rights reserved.
                    </p>
                  </td>
              </tr>
              <tr>
                  <td width="100%" colspan="3" style="padding-top: 20px;padding-bottom: 30px;border-collapse: collapse;" align="center" valign="middle">
                      <div mc:edit="social_link">
                      <a href="https://www.facebook.com/Hotspot4Beauty/" style="margin-left:5px;width:35px;color: #aaaaaa;text-decoration: none;outline: none; display: inline-block;"><img width="30px" src="{{asset('public')}}/email_assets/facebook-circle.png" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;" alt=""></a>
                        <a href="https://twitter.com/hotspotbeauty" style=" width:35px;margin-left:5px;display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="30px" src="{{asset('public')}}/email_assets/twitter-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a>
                        <a href="https://plus.google.com/u/0/102709337787722846176" style="width:35px;margin-left:3px; display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="30px" src="{{asset('public')}}/email_assets/googleplus-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;">
                        </a>
                      </div>
                  </td>
              </tr>
          	</tbody>
          </table>
        </td>
      </tr>
      <!-- END - FOOTER -->

      <!-- START - FOOTER LINK -->
      <tr bgcolor="#E3E3E3">
        <td width="100%" style="padding-top: 15px;padding-bottom: 10px;color: #666666;border-collapse: collapse;font-family: 'Roboto', sans-serif;;font-size:8pt" align="center">
            <div>
              You're receiving this email because you signed up at hotspot4beauty.com<br>
            </div>
            If you are not signed up on hotspot4beauty.com, you may ignore this email.
      	</td>
      </tr>
      <!-- END - FOOTER LINK -->
  </tbody>
  </table>
</body>