@include('shared/header')
<style type="text/css">
    .Direction
{
font-size: 32px;

}
#studiolisting
{

    text-align: center;
   /* width: auto ! important;*/
}  

table.dataTable {
width: 100%;
}
</style>
@section('Content')
<body class="customBody vertical-layout vertical-menu 2-columns fixed-navbar" data-col="2-columns" data-menu="vertical-menu" data-open="click">
    <!-- navbar-fixed-top-->
    <input id="usertoken" type="hidden" value="<?php echo session()->get('token'); ?>">
        <input id="userid" type="hidden" value="<?php echo session()->get('userdetails')->id ?>">
            @include('shared/navbar')
            <div class="app-content content container-fluid">
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">
                        <!-- stats -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    All Studios/FreeLancer
                                </h4>
                                <a class="heading-elements-toggle">
                                    <i class="icon-ellipsis font-medium-3">
                                    </i>
                                </a>
                                <div class="heading-elements">
                                    <a class="rounded-circle add-studio-class" data-placement="top" data-toggle="tooltip" href="{{url('/addstudio')}}" title="Add New Studio">
                                        <i class="icon-plus-round">
                                        </i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <!--id="studiolisting"-->
                                    <table class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle" id="studiolisting">
                                        <thead>
                                            <tr>
                                                <th width="40">
                                                    S.No
                                                </th>
                                                <th>
                                                    Logo
                                                </th>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    User Name
                                                </th>
                                                <th>
                                                    User Type
                                                </th>
                                                <th>
                                                    Direction
                                                </th>
                                                <!-- <th>Contact</th> -->
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Recent invoice with Statistics -->
                    </div>
                </div>
            </div>
            <!-- ////////////////////////////////////////////////////////////////////////////-->
            <form action="{{url('/overview')}}" id="formid" method="get" style="display: none">
                <input id="usersid" name="id" type="hidden" value=""/>
                <!--   <input type="hidden" name="token"  id="token" value="" /> -->
                <!-- <input type="sumbit" style="display: none"> -->
            </form>
            @include('shared/footer')
            <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
            <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript">
            </script>
            <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript">
            </script>
            <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
                <script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript">
                </script>
            </link>
        </input>
    </input>
</body>