<style>
	.inputdateto:hover,
	.inputdatefrom:hover{
		cursor: pointer;
	}
    #mapdata{

    height: 100%;

    }
.table {
    border-bottom:0px !important;
}
.table th, .table td {
    border: 1px !important;
}
.fixed-table-container {
    border:0px !important;
}
/*.inboxes
{

    padding-right: 40px !important;
}*/
   
</style>
@include('shared/header')
<link rel="stylesheet" href="https://cdn.rawgit.com/arschmitz/jquery-mobile-datepicker-wrapper/v0.1.1/jquery.mobile.datepicker.css">
@section('Content') 
 
<!--  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar"> -->
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar  menu-expanded pace-done">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo @session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo @session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

<?php if($result!="") 

 {

 ?>
 <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Sales stats -->
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-sm-12 border-right-blue-grey border-right-lighten-5">
                            <div class="media px-1">
                                <div class="media-left media-middle">
                                    <i class="fa fa-bank font-large-2 info"></i>
                                </div>
                                <div class="media-body text-xs-right">
                                    <span class="font-large-2 text-bold-300 info"><?php echo $result['totalstudios'] ?></span>
                                </div>
                                <p class="text-muted">Total Studio<span class="info float-xs-right"><i class="icon-arrow-up4 info"></i> <?php  echo number_format((float)$result['totalstudios']*100/$result['registoruser'], 2, '.', '');
                                   ?>%</span></p>
                                <progress class="progress progress-sm progress-info" value="<?php  echo round(number_format((float)$result['totalstudios']*100/$result['registoruser'], 2, '.', ''));
                                   ?>" max="100"></progress>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-sm-12 border-right-blue-grey border-right-lighten-5">
                            <div class="media px-1">
                                <div class="media-left media-middle">
                                    <i class="icon-user4 font-large-2 danger"></i>
                                </div>
                                <div class="media-body text-xs-right">
                                    <span class="font-large-2 text-bold-300 danger"><?php echo $result['totalusers'] ?></span>
                                </div>
                                <p class="text-muted">Total User<span class="danger float-xs-right"><i class="icon-arrow-down4 danger"></i> <?php  echo number_format((float)$result['totalusers']*100/$result['registoruser'], 2, '.', '');
                                   ?>%</span></p>
                                <progress class="progress progress-sm progress-danger" value="<?php  echo round(number_format((float)$result['totalusers']*100/$result['registoruser'], 2, '.', ''));
                                   ?>" max="100"></progress>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-sm-12">
                            <div class="media px-1">
                                <div class="media-left media-middle">
                                    <i class="fa fa-money font-large-2 success"></i>
                                </div>
                                <div class="media-body text-xs-right">
                                    <span class="font-large-2 text-bold-300 success"><?php echo $result['freelancer'] ?></span>
                                </div>
                                <p class="text-muted">Total FreeLancer<span class="success float-xs-right"><i class="icon-arrow-up4 success"></i> <?php  echo number_format((float)$result['freelancer']*100/$result['registoruser'], 2, '.', '');
                                   ?>%</span></p>
                                <progress class="progress progress-sm progress-success" value="<?php  echo round(number_format((float)$result['freelancer']*100/$result['registoruser'], 2, '.', ''));
                                   ?>" max="100"></progress>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Sales stats -->
<!-- Sales by Campaigns & Year -->
<div class="row" style="display: none">
    <div class="col-xl-8 col-lg-12">
        <div class="card">
            <div class="card-header no-border-bottom">
                <h4 class="card-title pb-1">Sales by Campaigns</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="sales-campaigns" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-12">
        <div class="card card-inverse bg-danger">
            <div class="card-header no-border-bottom">
                <h4 class="card-title">Yearly Sales</h4>
            </div>
            <div class="card-body">
                <div class="chartjs">
                    <canvas id="yearly-sales" class="height-350 px-2 pt-2"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Sales by Campaigns & Year -->
<!-- Top Selling Phones & Customer Browser's Stats -->
<div class="row" style="display: none">
    <div class="col-md-6 col-sm-12">
        <div class="card card-inverse bg-gradient-y-danger">
            <div class="card-header no-border-bottom">
                <h4 class="card-title">Customer Browser's Stats</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="browser-stats" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-12">
        <div class="card card-inverse bg-success">
            <div class="card-header no-border-bottom">
                <h4 class="card-title">Top Selling Phones</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="top-selling-phones-doughnut" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Top Selling Phones & Customer Browser's Stats -->
<!-- Analytics charts -->
<?php
// echo "<pre>";
// print_r($result);

?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Packages Overview</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="row my-1">
                        <div class="col-lg-3 col-xs-12 p-1 border-right-blue-grey border-right-lighten-5 inboxes">
                            <div class="text-xs-center">
                                <h3><?php echo $result['packages']['totalsubs'][0] ?></h3>
                                <p class="text-muted"><?php echo $result['packages']['packagename'][0] ?> (Studios)<span class="success darken-2"></i><br><?php
                                      $get=$result['packages']['totalsubs'][0]*100;
                                      
                                      echo number_format((float) $get/array_sum($result['packages']['totalsubs']), 2, '.', '');
                                       ?> %</span></p>
                               
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-12 p-1 border-right-blue-grey border-right-lighten-5 inboxes">
                            <div class="text-xs-center">
                                <h3><?php echo $result['packages']['totalsubs'][1] ?></h3>
                                <p class="text-muted"><?php echo $result['packages']['packagename'][1]?> <span class="success darken-2"></i> <br><?php
                                      $get=$result['packages']['totalsubs'][1]*100;
                                      echo number_format((float) $get/array_sum($result['packages']['totalsubs']), 2, '.', '');
                                       ?> %</span></p>
                               
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-12 p-1  inboxes">
                            <div class="text-xs-center">
                                <h3><?php echo $result['packages']['totalsubs'][2] ?></h3>
                                <p class="text-muted"><?php echo $result['packages']['packagename'][2]?><span class="success darken-2"><br><?php
                                      $get=$result['packages']['totalsubs'][2]*100;
                                        echo number_format((float) $get/array_sum($result['packages']['totalsubs']), 2, '.', '');
                                       ?>  %</span></p>
                              
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-12 p-1 inboxes">
                            <div class="text-xs-center">
                                <h3><?php echo $result['packages']['totalsubs'][3] ?></h3>
                                <p class="text-muted"><?php echo $result['packages']['packagename'][3]?> <span class="success darken-2"> <?php
                                      $get=$result['packages']['totalsubs'][3]*100;
                                      echo number_format((float) $get/array_sum($result['packages']['totalsubs']), 2, '.', '');
                                       ?> %</span></p>
                               
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-12 p-1 inboxes">
                            <div class="text-xs-center">
                                <h3><?php echo $result['packages']['totalsubs'][4] ?></h3>
                                <p class="text-muted"><?php echo $result['packages']['packagename'][4]?> (Users) <span class="success darken-2"> <?php
                                      $get=$result['packages']['totalsubs'][4]*100;
                                      echo number_format((float) $get/array_sum($result['packages']['totalsubs']), 2, '.', '');
                                       ?> %</span></p>
                               
                            </div>
                        </div>
                    </div>

                    <div class="chartjs" style="height: 275px;"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                        <canvas id="visitors-graph" height="275" width="1015" style="display: block; width: 1015px; height: 275px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Analytics charts -->
            <!-- Basic Map -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Studios On Map</h4>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block" id="mapdata"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Map -->
        </div>
      </div>
    </div>

 <?php  }  ?>


 
   
@include('shared/footer')

<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
 -->  
  
<!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
 -->
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/js-marker-clusterer/1.0.0/markerclusterer.js"></script>

  <script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
  <!-- <script src="{{asset('public/assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>

<script src="{{asset('public/assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/menu/jquery.mmenu.all.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script> -->


<!-- <script src="{{asset('public/assets/vendors/js/extensions/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/extensions/underscore-min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/extensions/clndr.min.js')}}" type="text/javascript"></script> 
<script src="{{asset('/public/assets/vendors/js/charts/echarts/chart/line.js')}}" type="text/javascript"></script> -->
<!-- <script src="{{asset('public/assets/vendors/js/extensions/unslider-min.js')}}" type="text/javascript"></script>

<script src="{{asset('public/assets/vendors/js/dashboard-ecommerce.min.js')}}" type="text/javascript"></script>-->
   <script src="{{asset('public/assets/vendors/js/dashboard-analytics.min.js')}}" type="text/javascript"></script>
 
<script type="text/javascript">
  
    // $( "#dateid" ).datepicker();





//    $(".monthPicker").datepicker({
//         dateFormat: 'MM yy',
//         changeMonth: true,
//         changeYear: true,
//         showButtonPanel: true,

//         onClose: function(dateText, inst) {
//             var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
//             var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
//             $(this).val($.datepicker.formatDate('MM yy', new Date(year, month, 1)));
//         }
//     });

//     $(".monthPicker").focus(function () {
//         $(".ui-datepicker-calendar").hide();
//         $("#ui-datepicker-div").position({
//             my: "center top",
//             at: "center bottom",
//             of: $(this)
//         });
//     });
	
// $(document).on('click',".inputdateto",function(){


// $("#dateto").focus();

// })
// $(document).on('click',".inputdatefrom",function(){


// $("#datefrom").focus();

// })

 
</script>

