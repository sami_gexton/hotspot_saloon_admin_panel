@include('shared/header')
@section('Content') 
<style type="text/css">
.help-block
{

 color: red;

}
table.dataTable {
width: 100%;
}
button.close {
    opacity: 1 !important;

}
/**table{*/
    /* margin: 0 auto;
     width: 100%;
     clear: both;
     border-collapse: collapse;
     table-layout: fixed;
     word-wrap:break-word;*/
/*}*/
</style>
<link rel="stylesheet" href="https://abpetkov.github.io/switchery/dist/switchery.min.css">
<script src="https://abpetkov.github.io/switchery/dist/switchery.min.js"></script>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
         <div class="card">
<div class="card-header">
<h4 class="card-title">Create Package</h4>
<div class="heading-elements" style="top: 9px;">
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModalpackage">
<span class="ladda-label">Create New</span>
</button>
</div>
<div style="position: absolute; left: 23%; right: auto; top: 3%; z-index: 99;">
<h3 style="margin-bottom: 15px;">Refund Charges(In Percentage)</h3>
<form method="POST" action="<?php echo asset('/package') ?>">
<input type="number" style="float: left; width: 55%; margin-right: 5px;" class="form-control" name='refound' value='<?php echo $stripe_refund_charges->amount_percentage ?>'>
<input type="submit" class="btn btn-danger" value="Apply Charges">
</form>
</div>

<div style="position: absolute; left: 55%; right: auto; top: 3%; z-index: 99;">
<h3 style="margin-bottom: 5px;">Free Presentation Video <br/> <small style="margin-top: 12px; display: block; text-align: center;">(For Signup)</small></h3>
<form method="POST" action="<?php echo asset('/package') ?>" class="text-center" id="is_presention_video">
<input type="checkbox" name='is_presention_video' class="js-switch" <?php if($stripe_refund_charges->is_presentation_free==1)
{ echo  "checked"; } ?>/>
<input type="hidden" name='check_state' value="" id="check_state"/>
</form>
</div>

</div>
<div class="card-body collapse in">
<div class="card-block card-dashboard">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap4">
<div class="row">
<div class="col-md-12">

    <table id="packages" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
        <thead>
            <tr role="row">
                <th>Date</th>
                <th>Package Name</th>
                <th>Video</th>
                <th>Price</th>
                <th>Package For</th>
                <th>Time</th>
                <th>Size Limit</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            <?php
        
            // exit();
            for($i=0;$i<count($result);$i++)
            { 
            ?>
            <tr role="row" class="odd">
                <td style="width:auto;"> <?php echo date('M d, Y',strtotime($result[$i]->Date)) ?></td>
                <td><?php echo $result[$i]->package_name ?></td>
                <td><?php 
                  if($result[$i]->limit!=0)
                  { 
                  echo $result[$i]->limit; 
                  }
                  else{
                   
                  echo "Unlimited"; 

                  }?></td>

                <td><?php if($result[$i]->Price==0)
                            {
                              echo "Free";
                            }
                            else {
                                echo '$'.$result[$i]->Price; 
                            } ?> 
                </td>
                <td><?php if($result[$i]->user_type=='normal_user')
                            {
                                echo  "User";
                            }
                            else if($result[$i]->user_type=='freelancer'){
                                echo  "freelancer"; 
                            }
                            else{
                                echo  "Studio"; 

                            } ?> </td>
                <td><?php 
                if($result[$i]->time_limit==0)
                 {
                              echo "Unlimited";
                 }
                 else{

                              echo $result[$i]->time_limit;
                 }
                  ?> </td>
                <td><?php echo $result[$i]->size_limit ?></td>
                <td > 
                <a   class="btn btn-info btn-sm packeditmodel" aria-hidden='true' data-toggle="modal" href="#packeditmodel" rowid="<?php echo $result[$i]->id ?>" > <i class='fa fa-pencil'></i></a>
                <!-- <a class='btn btn-danger btn-sm deletpackage' title='Delete' aria-hidden='true' data-toggle='tooltip' href="#" data-is_presentation="<?= $result[$i]->is_presentation ?>"  rowid="<?php echo $result[$i]->id ?>">
                <i class='fa fa-trash-o'></i>
                </a> -->
                </td>
            </tr>

        <?php  }  ?>
           
        </tbody>                
    </table>
</div>
</div>
</div>              
</div>
</div>
</div>  


           
        </div>
       
        
    </div>
</div>

 <div class="modal fade" id="myModalpackage" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Create Package</h4>

        </div>
        <div class="modal-body">


        <form id ="packageform" enctype="multipart/form-data" method="POST">

        <div class="form-group row">
                <label class="col-md-3 label-control" for="user-Package-name">Package Name  <span class="text-red">*</span></label>
                <div class="col-md-8">
                
                  <input type="text" id="package_name" class="form-control"  name="package_name" required data-fv-message="The Studio Name is not valid"
                              required data-fv-notempty-message="The Package Name is required and cannot be empty"
                              pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Package Name can only consist of alphabetical, number">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 label-control" for="Limit">Videos Limit<span class="text-red">*</span></label>
                <div class="col-md-8">
                  <input type="number" id="videolimit" class="form-control"  name="limit" required 
                  data-fv-message="The Vedio Size is not valid" data-fv-notempty-message="The video limit is required and cannot be empty"
                   pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The video limit can only consist of number" value="0">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 label-control" for="Limit">Price<span class="text-red">*</span></label>
                <div class="col-md-8">
                
                  <input type="number" min="1" max="1000" id="Price" class="form-control"  name="Price" required step="any" value="0"> 
                  
                </div>
            </div>

             <div class="form-group row">
                <label class="col-md-3 label-control" for="size_limit">Package For<span class="text-red">*</span></label>
                <div class="col-md-8">
                
                <select id="usertype" name="user_type"  class="form-control js-example-basic-single"  style="width: 100%" required>
                            <option disabled selected value>Select User Type</option>
               
                            <option value="studio_user">
                             Studio  
                            </option>
                            <option value="normal_user">
                             User
                            </option>
                            <option value="freelancer">
                            Free Lancer
                            </option>
                          
                            </select>
                  
                </div>
            </div>

             <div class="form-group row">
                <label class="col-md-3 label-control" for="Limit">Time Limit<span class="text-red">*</span></label>
                <div class="col-md-5">
                
                  <input type="number" style="line-height: 24px" id="timelimit" class="form-control"   name="time_limit" required name="time_limit" required  
                   data-fv-message="The Time is not valid" data-fv-notempty-message="The Time  is required and cannot be empty"
                              pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Time can only consist of time like '3 min' , 4 hour" value="0">
                  
                </div>
                <div class="col-md-3">

                  <input id="time" name="timeunit"  class="form-control" value="Min"  readonly>

               <!--  <select id="time" name="timeunit"  class="form-control js-example-basic-single"  style="width: 100%" required>
                            <option  selected value="Min">
                             Minutes 
                            </option>
                            <option  value="Hours">
                             Hours
                            </option>
          
                            </select> -->
                  
                  
                </div>
            </div>

            <div class="form-group row">
                          <label class="col-md-3 label-control" for="size">Size Limit<span class="text-red">*</span></label>
                          <div class="col-md-5">
                          
                            <input type="number" style="line-height: 24px" id="sizelimit" class="form-control"   name="size_limit" required name="sizelimit" required  
                             data-fv-message="The Size is not valid" data-fv-notempty-message="The Size is required and cannot be empty"
                                        pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Size can only consist of number">
                            
                          </div>
                          <div class="col-md-3">
                        <!--   <select id="size" name="size"  class="form-control js-example-basic-single"  style="width: 100%" required>
                                      <option  selected value="Kb">
                                       Kb 
                                      </option>
                                      <option  disabled selected value="Mb">
                                       Mb
                                      </option>
                    
                                      </select> -->

                                      <input type="text" value="Mb" name="size" class="form-control"  readonly>
                            
                            
                          </div>
                      </div>

         
        <br>
        <input type="hidden" value="" id="rowid">

       <div class="pull-right">

           <button class="btn btn-primary" id="createpackagebtn">Create</button>
         </div>
        
        </form>
        <br>
        <br>

       
        </div>
        <!-- <div class="modal-footer">

        <button type="button" id="closepoppackage" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

   <div class="modal fade" id="packeditmodel" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Update Package</h4>

        </div>
        <div class="modal-body">


        <form id ="packageditmodel" enctype="multipart/form-data" method="POST">

        <div class="form-group row">
                <label class="col-md-3 label-control" for="user-Package-name">Package Name  <span class="text-red">*</span></label>
                <div class="col-md-8">
                
                  <input type="text" id="updatepackname" class="form-control"  name="package_name" required data-fv-message="The Studio Name is not valid"
                  required data-fv-notempty-message="The Package Name is required and cannot be empty"
                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Package Name can only consist of alphabetical, number">
                  
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 label-control" for="Limit">Videos Limit<span class="text-red">*</span></label>
                <div class="col-md-8">
                
                  <input type="number" id="editvideolimit" class="form-control"  name="limit" required 
                  data-fv-message="The Vedio Size is not valid" data-fv-notempty-message="The video limit is required and cannot be empty"
                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The video limit can only consist of number" value="0">
                  
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 label-control" for="Limit">Price<span class="text-red">*</span></label>
                <div class="col-md-8">
                
                  <input type="Number" id="editPrice" class="form-control"  name="Price" disabled  
                  > 
                  
                </div>
            </div>

             <div class="form-group row">
                <label class="col-md-3 label-control" for="size_limit">Package For<span class="text-red">*</span></label>
                <div class="col-md-8">
                
                <select id="editusertype" name="user_type"  class="form-control js-example-basic-single"  style="width: 100%" required>
                            <option disabled selected value>Select User Type</option>
                            
                            <option value="studio_user">
                             Studio  
                            </option>
                            <option value="normal_user">
                             User
                            </option>
                            <option value="freelancer">
                            Free Lancer
                            </option>
                          
                            </select>
                  
                </div>
            </div>

             <div class="form-group row">
                <label class="col-md-3 label-control" for="Limit">Time Limit<span class="text-red">*</span></label>
                <div class="col-md-5">
                
                  <input type="number" style="line-height: 24px" id="edittimelimit" class="form-control"   name="time_limit" required name="time_limit" required  
                   data-fv-message="The Time is not valid" data-fv-notempty-message="The Time  is required and cannot be empty"
                              pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Time can only consist of time like '3 min' , 4 hour" value="0">
                  
                </div>
                <div class="col-md-3">

                <input id="time" name="timeunit"  class="form-control" value="Min"  required readonly>
                
                  
                </div>
            </div>

            <div class="form-group row">
                          <label class="col-md-3 label-control" for="size">Size Limit<span class="text-red">*</span></label>
                          <div class="col-md-5">
                          
                            <input type="number" style="line-height: 24px" id="editsizelimit" class="form-control"   name="size_limit" required name="sizelimit" required  
                             data-fv-message="The Size is not valid" data-fv-notempty-message="The Size is required and cannot be empty"
                                        pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Size can only consist of number">
                            
                          </div>
                          <div class="col-md-3">
                      

                                      <input type="text" value="Mb" name="size" class="form-control"  readonly>
                            
                            
                          </div>
                      </div>

        <br>
        <input type="hidden"  name="editrowid" id="editrowid">

       <div class="pull-right">
           <button class="btn btn-primary" id="updatepackagebtn">Update</button>
         </div>
        
        </form>
        <br>
        <br>

       
        </div>
        <!-- <div class="modal-footer">

          <button type="button" id="" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
@include('shared/footer')

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
<script>
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);
elem.onchange=function(event){
console.log(init.isChecked());
form =$("#is_presention_video")[0];
$("#check_state").val(init.isChecked());
form.submit();
};

</script>