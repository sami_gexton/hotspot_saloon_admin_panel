
@include('shared/header')
@section('Content') 
  
 <body style="padding-top: 200px;" data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
		<div class="card border-grey border-lighten-3 px-2 py-2 m-0">
			<div class="card-header no-border">
				<div class="card-title text-xs-center">
					<img class="img-responsive" src="{{asset('assets/images/logo/studio-logo-pink.png')}}">
				</div>
				<h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Create Account</span></h6>
			</div>
			<div class="card-body collapse in">	
				<div class="card-block">
					<form class="form-horizontal form-simple" action="{{url('auth/register')}}" method="post">
					<input type="hidden" name="usertype" value="normal_user">
						<fieldset class="form-group position-relative has-icon-left mb-1">
							<input type="text" class="form-control form-control-lg input-lg" id="studio" placeholder="Studio Name" name="Studio_Name" required>
							<div class="form-control-position">
							    <i class="fa fa-building"></i>
							</div>
						</fieldset>
						<span id="emailerror" style="color:red;display:none">Email Already Present </span>
						<fieldset class="form-group position-relative has-icon-left mb-1">
							<input type="email" class="form-control form-control-lg input-lg" id="user-email" placeholder="Your Email Address" required name="email">

							<div class="form-control-position">
							    <i class="fa fa-user"></i>
							</div>
						</fieldset>
						<fieldset class="form-group position-relative has-icon-left">
							<input type="text" class="form-control form-control-lg input-lg" id="user-Contact" placeholder="Contact Number"  name="contact_no" required>
							<div class="form-control-position">
							    <i class="fa fa-phone"></i>
							</div>
						</fieldset>
						 <fieldset class="form-group position-relative has-icon-left">
						<input type="text" class="form-control form-control-lg input-lg" id="Address" placeholder="Address"  name="Address" required>
						<div class="form-control-position">
						<i class="fa fa-location-arrow"></i>
						</div>
						</fieldset>

						<fieldset class="form-group position-relative has-icon-left">
						<input type="text" class="form-control form-control-lg input-lg" id="country" placeholder="Country"  name="country" required>
						<div class="form-control-position">
						<i class="fa fa-globe"></i>
						</div>
						</fieldset>

						
						<fieldset class="form-group position-relative has-icon-left">
						<input type="text" class="form-control form-control-lg input-lg" id="zipcode" placeholder="zipcode"  name="zipcode" required>
						<div class="form-control-position">
						  <i class="fa fa-street-view"></i>
						</div>
						</fieldset>

						<fieldset class="form-group position-relative has-icon-left">
						<input type="text" size="50" class="form-control form-control-lg input-lg" id="searchTextField" placeholder="Enter Studio Location" required>
						<div class="form-control-position">
						<i class="fa fa-map-marker"></i>
						</div>
						</fieldset>

						<fieldset class="form-group position-relative has-icon-left">
							<input type="password" class="form-control form-control-lg input-lg" id="password" placeholder="Enter Password" name="password" required>
							<div class="form-control-position">
							    <i class="fa fa-asterisk"></i>
							</div>
						</fieldset>
						<fieldset class="form-group position-relative has-icon-left">
							<input type="password" class="form-control form-control-lg input-lg" id="confirm_password" placeholder="Retype Password" required>
							<div class="form-control-position">
							    <i class="fa fa-asterisk"></i>
							</div>
						</fieldset>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" id="showerror" value="<?php echo session()->get('register')?>">
						<input type="hidden" id="lat" value="" name="lat">
						<input type="hidden" id="lng" value="" name="lng">
						<button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Register</button>
					</form>
				</div>
				<p class="text-xs-center clearfix">Already have an account ? <a href="{{url('/login')}}" class="card-link">Login</a></p>
				<p id="error" class="success">Your are sucessfully Register </p>
			</div>
		</div>
	</div>
</section>
        </div>
      </div>
    </div>
   
@include('shared/footer')

<script type="text/javascript">
  	if($("#showerror").val()==1)
  	{
      $("#error").show();
      $("#showerror").val('');

      setTimeout(function(){ $("#error").hide(); }, 3000);


 <?php session()->forget('register'); ?>
	} 
</script> 


 