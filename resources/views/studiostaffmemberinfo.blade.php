@include('shared/header')

<style type="text/css">

/*.lightblue{
        background: #2492a5;
        color: #FFF;
    }
    p{
        margin-bottom:0; 
        line-height: 28px;
    }*/
    .check{
overflow: hidden;
clear: both;
margin-bottom: 15px; 
padding-top: 15px;
}
.imgbg{
height: 50px;
width: 50px;
border-radius: 50%;
background-size: 100% 100%;
float: left;
margin-right: 15px;

}
#outputbox{
    position: absolute;
    left: 15px;
    right: 15px;
    z-index: 9999999;
    background-color: white;
    padding: 15px;

}
.getstaffid
{
 font-size: 16px;

}
.customclass{
    float: right;
    display: inline;
    width: auto;
    margin-right: 150px;
    /* line-height: 18px; */
    /* height: 25px; */
    /* padding: 36px; */
    font-size: 47px;
    transform: scale(1.8);
    margin-top: 5px;
}

/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 250px;
    overflow-y: auto;
}
</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    
  
@include('shared/navbar')




<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Staff Member Details</h4>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table id="recent-orders" class="table table-bordered table-striped mb-0 ps-container ps-theme-default" data-ps-id="7ef9bdf9-e3b1-a757-11b3-3763218ce0b4">
                                <thead class="bgdefault">
                                    <tr>
                                        <th width="50%" class=" border-right-grey">General</th>
                                        <th width="50%">Chat Logs</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                         <th width="180" class="text-truncate"> Name:</th>
                                         <td width="180" class="text-truncate"><?php echo $result['membername']?></td>
                                    </tr>
                                    <tr>
                                         <th class="text-truncate">Email: </th>
                                          <td class="text-truncate"> <?php echo $result['memberemail']?></td>
                                    </tr>
                                    <tr>
                                        <th class="text-truncate"> Contact: </th>
                                        <td class="text-truncate"> <?php echo $result['membercontact']?></td>
                                    </tr>
                                    <tr>
                                          <th class="text-truncate">Services Category: </th>
                                           <td class="text-truncate"> <?php echo $result['member_category']?></td> 
                                          <!--  <td class="text-truncate"> <a href="#" data-toggle="modal" data-target="#myModal">Click Here To Update </a></td> -->
                                    </tr>
                                      
                                </tbody>
                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></table>
                        </div>
                    </div>
                </div>
            </div><!-- Recent invoice with Statistics -->
        </div>
    </div>
</div>
<!--   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <!-- <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Member Services</h4>
        </div>
        <div class="modal-body">
         <div>
            




        </div>
        <div class="modal-footer">
          <div class="pull-right">
                <button class="btn btn-primary" id="addstaffmemberbtn">Add</button>
         </div>
        </div>
      </div>
      
    </div>
  </div>
  </div> -->
    -->
@include('shared/footer')

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
