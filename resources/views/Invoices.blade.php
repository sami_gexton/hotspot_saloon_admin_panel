@include('shared/header')
@section('Content') 
<style type="text/css">
.help-block
{

 color: red;

}  

.ui-datepicker-calendar {
    display: none;
    }
</style> 
<link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery-ui-timepicker-addon.css')}}">

 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
         <div class="card">
<div class="card-header">
<h4 class="card-title">Invocies</h4>
 </div>
<div class="card-block">
                  <!-- Invoices List table -->
                   <div class="row">
                <div class="col-md-4 mt-1 mb-2">
                              <div class="form-group">
                                <label class="font-weight-bold col-md-2 col-sm-3" for="datefrom" style="margin-top: 5px;">Month </label>
                                <div class="col-md-10">
                                  <div class="input-group date">
                   <input type="text" name="to"   id="invociesto"   class="form-control" value="" >
                    <span class="input-group-addon inputdatefrom bgdefault white">
                      <span class="icon-calendar5"></span>
                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                 </div>

                  <table id="invoices-list" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                <thead>
                    <tr>
                       
                        <th>Date</th>
                        <th>Due Date</th>
                        <th>Customer Name</th>
                        <th>Package Name</th>
                        <th>Amount</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                  <!-- PAID -->
                  <?php 
                    if($data!="")
                    {
                      // print_r($data);
                      // exit();
                      for ($i=0; $i <count($data) ; $i++) { 
                
                  ?>
                    <tr>
                        
                        <td><?php  echo date('Y-m-d',strtotime($data[$i]->create_time));  ?></td>
                        <td><?php  echo date('Y-m-d',strtotime($data[$i]->end_date));  ?></td>
                        <td><?php  echo $data[$i]->customername;  ?></td>
                        <td><?php  echo $data[$i]->packagename;  ?></td>
                        <td><?php  echo $data[$i]->amount;  ?></td>
                        <td > <a  class="btn btn-danger btn-sm pdfpage" aria-hidden='true'  href="{{url('/invoicedetail/'.$data[$i]->rowid)}}" rowid="<?php echo $data[$i]->rowid ?>" > <i class='fa fa-file-pdf-o'></i></a></td>
                    </tr>
                    
                   <?php  }
                    }
                    ?>
                   
                </tbody>
               
          </table>
          </div>







 </div>  
</div>
          
    </div>
</div>



  
      
   
@include('shared/footer')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
<script  src="{{asset('public/js/timepicker/jquery-ui-timepicker-addon.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">

<script type="text/javascript">

$('#invociesto').datepicker({
changeMonth: true,
changeYear: true,
showButtonPanel: true,
dateFormat: 'MM yy',
onClose: function(dateText, inst) { 
$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
  // hourGrid: 4,
  // minuteGrid: 10,
},
});


</script>