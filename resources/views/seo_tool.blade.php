@include('shared/header')

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;



}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;


}
.active{
color:red;

}
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
</style>
 <?php 
 $text =array();
 for($i=0;$i<count($result);$i++){
    array_push($text,json_decode($result[$i]->text,true));
 }
    // echo "<pre>";
    // print_r($text);
    // exit;
 ?>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card-body collapse in"> 
        <div class="card-block">
        <section id="basic-modals">
              <div class="row">
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Seo Tool</h4>
                    </div>
                    <div class="card-body collapse in">
                      <div class="card-block">
                        <form class="form ajaxForm nopopup" method='POST' action="{{url('update_seo_info')}}">

                            <div class="col-xs-12" style="margin-bottom: 20px;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select class="form-control" id="get_val" name='update_index'>
                                        <option value="" disabled selected>Select your option</option>
                                            <?php  
                                            foreach($text[0] as $key=>$value) { ?>
                                            <option value='<?=$key?>' en_lng='<?php echo htmlspecialchars(json_encode($text[0][$key]), ENT_QUOTES)?>' de_lng='<?php echo htmlspecialchars(json_encode($text[1][$key]), ENT_QUOTES)?>'><?=$key?></option>
                                            <?php  } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="form-body">                
                                    <div class="form-group">
                                        <table class="table tabel-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Meta For</th>
                                                    <th>English</th>
                                                    <th>German</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><strong>Title</strong></td>
                                                    <td><input type="text" id='en_title' name="title[]" class="form-control"></td>
                                                    <td><input type="text"  id='de_title' name="title[]" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Description</strong></td>
                                                    <td><textarea id='en_description' name="description[]" class="form-control" style="min-height: 130px;"></textarea></td>
                                                    <td><textarea name="description[]" class="form-control" style="min-height: 130px;" id='de_description'></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Keywords</strong></td>
                                                    <td><textarea name="keywords[]" style="min-height: 150px;" class="form-control" id='en_keywords'></textarea></td>
                                                    <td><textarea name="keywords[]" style="min-height: 150px;" class="form-control" id='de_keywords'></textarea></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger pr-3 pl-3 float-xs-right"  rowid=""> Update</button>
                                    </div>
                                </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      </div>

<!-- Recent invoice with Statistics -->
        </div>
      </div>
    </div>
    </div><!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/custom.js')}}" type="text/javascript"></script>

<script>
$(document).on('change','#get_val',function(){
  eng = JSON.parse($('option:selected', this).attr('en_lng'));
  ger = JSON.parse($('option:selected', this).attr('de_lng'));
  $("#en_title").val(eng.title);
  $("#de_title").val(ger.title);
  $("#en_description").val(eng.description);
  $("#de_description").val(ger.description);
  $("#en_keywords").val(eng.keywords);
  $("#de_keywords").val(ger.keywords);
})
</script>