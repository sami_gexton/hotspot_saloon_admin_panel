@include('shared/header')


@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
@include('shared/navbar')





<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div id="user-profile">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">User Details</h4>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                          <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        @include('shared/usernevigationinfo')
                                       
                                        <div class="table-responsive">
                                            <table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default" data-ps-id="7ef9bdf9-e3b1-a757-11b3-3763218ce0b4"  >
                                                <tbody>
                                                    <tr>
                                                         <th class="text-truncate"> Name:</th>
                                                         <td class="text-truncate"><?php echo $result['Name']?></td>
                                                    </tr>
                                                    <tr>
                                                         <th class="text-truncate">Email: </th>
                                                          <td class="text-truncate"> <?php echo $result['Email']?></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-truncate"> Contact: </th>
                                                        <td class="text-truncate"> <?php echo $result['Contact']?></td>
                                                    </tr>
                                                    <tr>
                                                          <th class="text-truncate">Zip Code : </th>
                                                          <td class="text-truncate"> <?php echo $result['Zip_code']?></td>
                                                    </tr>
                                                    <tr>
                                                         <th class="text-truncate"> Country: </th>
                                                          <td class="text-truncate"> <?php echo $result['Country']?></td>
                                                    </tr>
                                                </tbody>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   
@include('shared/footer')

<script src="{{asset('js/admin/admin.js')}}" type="text/javascript"></script>
