<style>@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900');</style>

<body bgcolor="#E3E3E3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: 'Roboto', sans-serif;font-size: 11pt;color: #777777; mso-line-height-rule: exactly;line-height: 1.5em;width: 100%; min-width: 600px;" cz-shortcut-listen="true">
    <table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="padding: 0;margin: 0;border-collapse: collapse; margin: auto;">
    <!-- START - HEAD LINK -->
      <tbody>
      <!-- START - HEADER -->
      <tr>
          <td width="100%" align="center" style="background: #ffffff; border-collapse: collapse; border-top: 3px solid #d21135;">
          <div style="width: 100%;margin: auto;">
            <table border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; margin-top: 0px;">
              <tbody>
                <tr style="background: url({{asset('public')}}/email_assets/header-bg.jpg);">
                  <td style="text-align: center; padding-top: 15px;padding-bottom: 15px;border-collapse: collapse;" align="left" valign="top">
                    <a href="#" style="display: inline-block; text-align: left; width:60%; float: left;"><img mc:edit="logo_img" editable=""  src="{{asset('public')}}/assets/images/logo/studio-logo-white.png" alt="Hotspot Studio" width="260" style=" display: inline-block;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"></a>
                </td>
                <td>
                    <h4 style="text-align: right; padding-top: 20px; float: right; margin:0; width: 30%; color: #fff; font-weight: 300; font-size: 20px;"><span style="padding: 0 10px;">Payment Invoice</span></h4>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </td>
      </tr>
      <!-- END - HEADER -->

      <!-- **************** START LAYOUT ZONE **************** -->          
      <!-- START BLOCK - BIG BUTTON & PARAGRAPH -->
      <tr>
      	<td width="100%" align="center" style="border-collapse: collapse; padding: 35px 40px 20px;">
<style type="text/css">
  .help-block{color: red;}
  .position-top{
    position:relative;
    bottom:48px;
    font-weight:bold; 
  }
  .image{
    margin-left:40px;
  }
  table{
      margin-top: 20px;
  }
  table tr td{
    font-weight:normal;
    font-size:14px;}
  table tr th{font-size: 14px;}
  
  table.mtbl tr td,
  table.mtbl tr th{
      border: 1px solid #000;
  }
  
  .border-bottom{
    border-bottom:1px solid #000;
    width:100%;
  } 
  </style> 
   <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
      <!-- navbar-fixed-top-->
  <div class="app-content content container-fluid">
      <div class="content-wrapper">
          <div class="content-header row"></div>
          <div class="content-body"><!-- stats -->
           <div class="card">
  <div class="card-header">
  <!-- <h4 class="card-title">Invocies</h4>
   --> </div>
  <div class="card-block">
          <table border="0" width="100%" style="margin: 0px;" class="">
              <tr>
                  <td style="margin-bottom: 30px"><!--<img src="https://stripe.com/img/about/logos/logos/blue.png" alt="Logo" width="136">--></td>
                  <td align="right">
                      <!-- <h3 style="text-transform: uppercase; font-size: 18px;font-size: 30px;color: #9B9B9B;margin: 0px;">invoice</h3> -->
                    </td>
              </tr>
          </table>
  
          <table border="0" width="100%" style="margin:0px 0 0;">
              <tbody>
                  <tr>
                      <td style="text-align:left;width: 45%;">
                          <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px;" >
                              <tr>
                                  <th align="left"><h3 style="font-size: 22px;color: #4A4A4A;margin-top: 0"><?php echo $data->Userdeatails->Studio_Name  ?></h3></th>
                              </tr>
                              <tr>
                                  <td style="line-height: 24px;margin-bottom: 10px;display: block;"> Hotspot Payment Invoice
                                  </td>
                              </tr>
                              <tr>
                                  <td style="line-height: 24px"><strong>Phone:</strong> <?php echo $data->Userdeatails->contact_no  ?></td>
                              </tr>
                               <tr style="padding-top: 10px;">
                                  <th align="left"><br/><h3 style="font-size: 22px;color: #4A4A4A;margin: 10px 0px;">Bill To</h3></th>
                              </tr>
                              <tr>
                                  <td align="left"> <?php echo $data->Userdeatails->email  ?></td>
                              </tr>
                          </table>
                      </td>
                      <td style="text-align:right;vertical-align: top;width: 30%;">
                          <table border="0" cellpadding="8" cellspacing="3" width="100%" class="invoicedetail" style="margin-top: 0">
                              <tr>
                                  <td style="font-weight: bold;">Invoice Date</td>
                                  <td style="text-align: left;"><?php echo date('Y-m-d',strtotime($data->Userdeatails->created_date));  ?></td>
                              </tr>
                              <tr>
                                  <td style="font-weight: bold;">Currency</td>
                                  <td style="text-align: left;"> <?php echo strtoupper($data->payments[0]->currency_code) ?>  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
            </tbody>
          </table>   
          <div style="clear:both;"></div>
          <table border="0" width="100%" style="margin-top:20px;border-spacing:inherit;padding:0;" cellpadding="0" cellspacing="0" class="no-print mtbl">
            <tr>
                <td>
                  <table border="0" cellpadding="10" cellpadding="0" width="100%" style="border:0px;margin:0; border-spacing:inherit;" class="no-print">
                        <tr style="background:#D9F0FA;">
                              <td style="padding: 10px;"><h4 style="line-height:30px;margin:0;">Service Name</h4></td>
                              <td style="padding: 10px;" align="right"><h4 style="line-height:30px;margin:0;">Amount</h4></td>
                              <td style="padding: 10px;" align="right"><h4 style="line-height:30px;margin:0; ">Hotspot Charges (20%)</h4></td>
                            <td style="padding: 10px;" align="right"><h4 style="line-height:30px;margin:0;">Total Amount</h4></td>
                          </tr>
                          <?php 
                          $amount_total       =0;
                          $Hotspot_Charges    =0;
                          $Total_Amount       =0;
                          foreach($data->payments as $value) { ?>
                          <tr>
                            <td style="padding: 5px;"><?php echo $value->product_name  ?></td>
                              <td style="padding: 5px;" align="right"> <?php echo @$value->amount ?></td>
                              <td style="padding: 5px;" align="right"> <?php echo @$value->amount*(20/100) ?></td>
                               <td style="padding: 5px;" align="right"> <?php echo @$value->amount- ($value->amount)*(20/100)  ?></td>
                               <?php   
                               $amount_total      +=$value->amount;
                               $Hotspot_Charges   +=$value->amount*(20/100);
                               $Total_Amount      +=$value->amount- ($value->amount)*(20/100);
  
                               ?>
                          </tr>
                          <?php } ?>
                          <tr>
                              <td style="padding: 5px;"><h3><b>Total Amount</b></h3></td>
                              <td style="padding: 5px;" align="right"><b><?php echo $amount_total ?></b></td>
                              <td style="padding: 5px;" align="right"><b><?php echo $Hotspot_Charges ?></b></td>
                              <td style="padding: 5px;" align="right"><b><?php echo$Total_Amount ?></b></td>
                          </tr>
                          <!-- <tr>
                              <td >&nbsp;</td>
                              <td >&nbsp;</td>
                              <td >&nbsp;</td>
                              <td >&nbsp;</td>
                          </tr> -->
                          <tr>
                              <th align="left" colspan="2" style="padding: 5px;">Note to recipeitn(s)</th>
                              <th style="padding: 5px;" colspan="2" align="right"></th>
                          </tr>
                          <tr>
                              <td style="padding: 5px;" colspan="4">Thank you for your business</td>
                          </tr>
                      </table>
                </td>
              </tr>
          </table>    <!-- Invoices List table -->  
            </div>
   </div>  
  </div>
      </div>
  </div>
      	</td>
      </tr>
	<tr>
		<td style="background:#ffffff; border:solid 1px #ddd; border-width:1px 0 0 0; height:1px; width:85%; margin:15px auto; display:block; line-height: 10px;">&nbsp;</td>
	</tr>
      
      <!-- END BLOCK - BIG BUTTON AND PARAGRAPH -->
                
      <!-- START - FOOTER -->
      <tr>
        <td width="100%" align="center" style="border-collapse: collapse; font-size:9pt;">
            <table border="0" bgcolor="#fff" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
              <tbody>
              <tr>
                  <td width="100%" style=" color: #767676;border-collapse: collapse;font-family: 'Roboto', sans-serif;" align="center" valign="middle" mc:edit="footer_address" colspan="3">
                    <p label="Footer Address">© {{date('Y')}} Hotspot Studios. All rights reserved.
                    </p>
                  </td>
              </tr>
              <tr>
                  <td width="100%" colspan="3" style="padding-top: 20px;padding-bottom: 30px;border-collapse: collapse;" align="center" valign="middle">
                      <div mc:edit="social_link">
                        <a href="https://www.facebook.com/Hotspot4Beauty/" style="margin-left:5px;width:35px;color: #aaaaaa;text-decoration: none;outline: none; display: inline-block;"><img width="30px" src="{{asset('public')}}/email_assets/facebook-circle.png" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;" alt=""></a>
                        <a href="https://twitter.com/hotspotbeauty" style=" width:35px;margin-left:5px;display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="30px" src="{{asset('public')}}/email_assets/twitter-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a>
                        <a href="https://plus.google.com/u/0/102709337787722846176" style="width:35px;margin-left:3px; display: inline-block;color: #aaaaaa;text-decoration: none;outline: none;"><img width="30px" src="{{asset('public')}}/email_assets/googleplus-circle.png" alt="" style="display: block;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;">
                        </a>
                      </div>
                  </td>
              </tr>
          	</tbody>
          </table>
        </td>
      </tr>
      <!-- END - FOOTER -->

      <!-- START - FOOTER LINK -->
      <tr bgcolor="#E3E3E3">
        <td width="100%" style="padding-top: 15px;padding-bottom: 10px;color: #666666;border-collapse: collapse;font-family: 'Roboto', sans-serif;;font-size:8pt" align="center">
            <div>
              You're receiving this email because you signed up at hotspot4beauty.com<br>
            </div>
            If you are not signed up on hotspot4beauty.com, you may ignore this email.
      	</td>
      </tr>
      <!-- END - FOOTER LINK -->
  </tbody>
  </table>
</body>