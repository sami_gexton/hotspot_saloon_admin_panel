@include('shared/header')

<link rel="stylesheet" type="text/css" href="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/css/core/colors/palette-callout.min.css">


@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card-body collapse in"> 
        <div class="card-block">
        <section id="basic-modals">
              <div class="row">
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Notifications</h4>
                    </div>
                    <div class="card-body collapse in">
                      <div class="card-block">
                      <?php if(count($notifications)>0) { 
                            foreach($notifications as $value)  { ?>
                        <div class="bs-callout-success callout-border-right callout-round callout-transparent mt-1 pl-3 pr-2 py-1 col-xs-12">
                         
                            <div class="bg" style="background: url({{url('/public/assets/images/videos_profilescreen_enable.png')}}) no-repeat; background-size: cover; width: 50px; height: 60px; border-radius: 50%; float: left; margin-right: 20px;"></div>
                            <strong><?php echo  $value->title?></strong>
                            <p><?php echo  $value->body?>
                            </p>

                            <div class="btn-group pull-right" role="group" style="margin-top: 10px;">
                              <!-- <button type="button" class="btn btn-primary btn-round"><i class="fa fa-eye"></i> Details</button> -->
                              <?php if($value->type=='new_video' ||  $value->type=='video_report') { 
                                $data = json_decode($value->push_data);
                                if(isset($data->videoid)) {
                                  $id = $data->videoid;
                                } else {
                                  $id = $data->video_id;
                                }

                                ?>
                              <a class="btn btn-secondary btn-round" 
                              href="<?php echo 'https://www.hotspot4beauty.com/videos/id/'.videoId_encode($id) ?>" target='_blank'> <i class="fa fa-play"></i> Play Video</a>
                              <?php  
                            } ?>
                              <?php if($value->type=='Subscribed') { 
                                $data = json_decode($value->push_data);
                                ?>  
                              <a  class="btn btn-success btn-round" href="<?php echo url('overview/'.$data->studio_id); ?>" traget='_blank' ><i class="fa fa-heart" ></i>Subscribe</a>
                              <?php } ?>
                              <!-- <button type="button" class="btn btn-info btn-round"><i class="fa fa-check"></i> Account Credited</button> -->
                            </div>
                        </div>
                            <?php 
                            }
                          } ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      </div>

<!-- Recent invoice with Statistics -->
        </div>
      </div>
    </div>
    </div><!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   <?php  
    function videoId_encode($name) {
      $name = base64_encode($name);

      for ($b = 0; $b < 3; $b++) {
          $name = base64_encode($name);
      }
      return $name;
    }
  
   ?>
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>

<script src="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/vendors/js/editors/ckeditor/ckeditor.js" type="text/javascript"></script>