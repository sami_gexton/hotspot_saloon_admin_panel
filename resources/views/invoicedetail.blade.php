@include('shared/header')
@section('Content') 
<style type="text/css">
.help-block
{

 color: red;

} 

.position-top{
  position:relative;
  bottom:48px;
  font-weight:bold; 
}
.image{
  margin-left:40px;
}
table{
    margin-top: 20px;
}
table tr td{
  font-weight:normal;
  font-size:14px;
}
table tr th{
  font-size: 14px;
}
.border-bottom{
  border-bottom:1px solid #000;
  width:100%;
} 
</style> 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
         <div class="card">
<div class="card-header">
<h4 class="card-title">Invocies</h4>
<button class="btn btn-primary btn-md pull-right" id="downloadpdf">Download Pdf</button>
 </div>
<div class="card-block">
        <table border="0" width="100%" style="margin: 0px;" class="">
            <tr>
                <td style="margin-bottom: 30px"><img src="http://www.klc.org.pk/wp-content/uploads/2015/09/paypal-784404_640.png" alt="Logo" width="136"></td>
                <td align="right"><h3 style="text-transform: uppercase; font-size: 18px;font-size: 30px;color: #9B9B9B;margin: 0px;">invoice</h3></td>
            </tr>
        </table>

        <table border="0" width="100%" style="margin:0px 0 0;">
            <tbody>
                <tr>
                <input type="hidden" id="rowid" value="<?php echo $data->rowid  ?>">
                    <td style="text-align:left;width: 70%;">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px;" >
                            <tr>
                                <th align="left"><h3 style="font-size: 22px;color: #4A4A4A;margin-top: 0"><?php echo $data->customername  ?></h3></th>
                            </tr>
                            <tr>
                                <td style="line-height: 24px;margin-bottom: 10px;display: block;"><?php echo $data->short_description  ?></td>
                            </tr>
                            <tr>
                                <td style="line-height: 24px"><strong>Phone:</strong> <?php echo $data->contact_no  ?></td>
                            </tr>
                            <tr>
                                <td style="line-height: 24px"><strong>Email:</strong>  <?php echo $data->email  ?></td>
                            </tr>
                             <tr>
                                <th align="left"><h3 style="font-size: 22px;color: #4A4A4A;margin: 10px 0px;">Bill To</h3></th>
                            </tr>
                            <tr>
                                <td align="left"> <?php echo $data->email ?></td>
                            </tr>
                        </table>
                    </td>
                    <td style="text-align:right;vertical-align: top;width: 30%;">
                        <table border="0" cellpadding="8" cellspacing="3" width="100%" class="invoicedetail" style="margin-top: 0">
                            <tr>
                                <td width="50%" style="font-weight: bold;">Invoice Number</td>
                                <td width="50%"> <?php echo $data->paypalid ?></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Invoice Date</td>
                                <td><?php echo $data->create_time ?></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Currency</td>
                                <td><?php echo $data->currency_code ?></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Due Date</td>
                                <td><?php echo date('Y-m-d',strtotime("+30 days",strtotime($data->create_time))); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
          </tbody>
        </table>   
        <div style="clear:both;"></div>
        <table border="0" width="100%" style="margin-top:20px;border-spacing:inherit;padding:0;" cellpadding="0" cellspacing="0" class="no-print">
          <tr>
              <td>
                <table border="0" cellpadding="10" cellpadding="0" width="100%" style="border:0px;margin:0px 0px 20px;border-spacing:inherit;" class="no-print">
                      <tr style="background:#D9F0FA;">
                            <td><h3 style="line-height:30px;margin:0;">Description</h3></td>
                            <td align="right"><h3 style="line-height:30px;margin:0;">Quantity</h3></td>
                            <td align="right"><h3 style="line-height:30px;margin:0;">Unit Price</h3></td>
                          <td align="right"><h3 style="line-height:30px;margin:0;">Amount</h3></td>
                        </tr>
                        <tr>
                          <td ><?php echo $data->short_description  ?></td>
                            <td align="right">1</td>
                            <td align="right"> <?php echo $data->amount ?> <?php echo $data->currency_code ?></td>
                            <td align="right"> <?php echo $data->amount ?> <?php echo $data->currency_code ?></td>
                        </tr>
                        <tr>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <th align="left" colspan="2" style="border-top: 1px solid #CECECE;">Note to recipeitn(s)</th>
                            <th align="right" style="border-top: 1px solid #CECECE;">Subtotal</th>
                            <th align="right" style="border-top: 1px solid #CECECE;"> <?php echo $data->amount ?> <?php echo $data->currency_code ?></th>
                        </tr>
                        <tr>
                            <td colspan="2">Thank you for your business</td>
                         
                         
                        </tr>
                    </table>
              </td>
            </tr>
        </table>    <!-- Invoices List table -->
                  
                  
          </div>







 </div>  
</div>
          
    </div>
</div>



  
      
   
@include('shared/footer')

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">

