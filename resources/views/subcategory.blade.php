@include('shared/header')

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;
}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;
}
.active{
color:red;

}
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
    .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.proimg2 {
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin: auto;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 } 
 .editimage{
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin: auto;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 } 

 .img-thumbnail {
    padding: .25rem;
    background-color: #F1F1F1;
    border: 1px solid #DDD;
    border-radius: .18rem;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    background-size: 100% 100%;
    height:40px;
    width:40px;
}
.help-block
{

 color: red;

}
body
{
    counter-reset: Serial;           /* Set the Serial counter to 0 */
}
tr td:first-child:before
{
  counter-increment: Serial;      /* Increment the Serial counter */
  content: counter(Serial); /* Display the counter */
}
.errorline {

border-bottom: 2px solid red;

}
button.close {
    opacity: 1 !important;

}
</style>
@section('Content') 

 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
    <input type="hidden" id="imgprofile" value="{{asset('public/assets/images/placeholder.png')}}">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="imgappend" style="display:none"></div>  
        <div class="content-body"><!-- stats -->
                    <div class="card">
            <div class="card-header mb-3">
                <h4 class="card-title">Add Sub Category</h4>
            </div>
            <div class="card-body collapse in">
                <!--  <div class="proimg2"></div> -->
                <div class="card-block card-dashboard" id="formContainer">
                    <form class="form form-horizontal row-separator fv-form fv-form-bootstrap" id="addsubcat" data-fv-framework="bootstrap" data-fv-message="Select Category" data-fv-feedbackicons-valid="glyphicon glyphicon-ok" data-fv-feedbackicons-invalid="glyphicon glyphicon-remove" data-fv-feedbackicons-validating="glyphicon glyphicon-refresh" enctype="multipart/form-data" method="POST" novalidate>
                          <div class="form-body">
                          <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>" name="token">
                              <div class="form-group row has-feedback">
                              </div>

                               <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="newcategory">Select Category <span class="text-red">*</span></label>
                                <div class="col-md-4 has-icon-right">
                                  <select id="catselect" name="category"  class="form-control js-example-basic-single"  style="width: 100%" required>
                                  <option disabled selected value>Select Category</option>        
                                 <?php 

                                  $data2=array($result);
                                  $known = array();
                                  $filtered = array_filter($data2, function ($val) use (&$known) {
                                  for($i=0;$i<count($val);$i++) {
                                  if(!in_array($val[$i]->Name, $known))
                                  {
                                  $unique = $val[$i]->Name;
                                  $values = $val[$i]->parent_id;
                                  ?>
                                
                                    <option value="<?php  echo $values ?>">

                                    <?php echo $unique; ?>
                                    
                                    </option>


                                  <?php  }
                                     $known[] = $val[$i]->Name;
                                     

                                     }

                                  });  ?>


                               
                                 </select>
                                 
                                </div>
                              </div>
                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="newcategory">New Sub Category <span class="text-red">*</span></label>
                                <div class="col-md-4 has-icon-right">
                                  <input type="text" id="newcategory" class="form-control" placeholder="Sub Category Name" name="Category" data-fv-field="Category" 
                                  data-fv-message="The Category Name is not valid"
                                  required data-fv-notempty-message=" Sub Category Name is required and cannot be empty"
                                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Sub Category Name can only consist of alphabetical"><i class="form-control-feedback" data-fv-icon-for="Category" style="display: none;"></i>
                                 
                                </div>
                              </div>

                <div class="form-group row has-feedback" style="border-bottom: none;">
                  <div class="col-md-12">
                    <button type="sumbit"  id="btnsubcategory" class="btn btn-danger pr-3 pl-3">Create</button>
                  </div>
                </div>
                </div><!-- ENDS FORM GROUP -->
                </form>
                </div>
          <div class="card-block card-dashboard">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap4">
              <div class="row">
                <div class="col-md-12">
                  <table id="Category" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                    <thead>
                      <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 35px;">S.No</th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending">Category Image</th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending">Category Name</th>
                         <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending">Sub Category</th>
                         <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending">Action</th>
                      </tr>
                    </thead>
                    <tbody id="dataapend">
                    <?php 
                       $counts = 0;
                      for($i=0;$i<count($result);$i++)
                       {

                     ?> 
                     <tr role="row" class="odd">
                      <td ></td>
                      <td><div class='img-thumbnail' id="<?php echo  "subcatimageid".$result[$i]->id  ?>" style="max-height:40px;background-image:url(<?php echo asset($result[$i]->service_image) ?> )"/> </div></td>
                      <td id="<?php echo  "parentcatnameid".$result[$i]->id ?>">
                      <?php echo $result[$i]->Name ?></td>
                      <td id="<?php echo  "subcatname".$result[$i]->id ?>">
                      <?php echo $result[$i]->category_name ?></td>

                       <td> 

                       <a  class="btn btn-info btn-sm subeditcategory" aria-hidden='true' data-toggle="modal" href="#subeditcategory" parentid="<?php echo $result[$i]->parent_id ?>" rowid="<?php echo $result[$i]->id ?>" >  <i class='fa fa-pencil'></i></a>

                       <a class='btn btn-danger btn-sm deletsubcat' title='Delete' aria-hidden='true' data-toggle='tooltip' href="#" rowid="<?php echo $result[$i]->id ?>">
                       <i class='fa fa-trash-o'></i>
                    </a></td>
                     </tr>

                   <?php } ?>
                     
                      
                    </tbody>        
                  </table>
                </div>
              </div>
            </div>        
          </div>

                </div>
            </div>

      </div>

      <div class="modal fade" id="subeditcategory" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Update Sub Category</h4>

        </div>
        <div class="modal-body">
       
              <label  >Select Category <span class="text-red">*</span></label>

              <select id="subcategory" name="category"  class="form-control js-example-basic-single"  style="width: 100%" required>
              <option disabled selected value>Select Category</option>        
              <?php 

              $data2=array($result);
              $known = array();
              $filtered = array_filter($data2, function ($val) use (&$known) {
              for($i=0;$i<count($val);$i++) {
              if(!in_array($val[$i]->Name, $known))
              {
              $unique = $val[$i]->Name;
              $values = $val[$i]->parent_id;
              ?>

              <option value="<?php  echo $values ?>">

              <?php echo $unique; ?>

              </option>


              <?php  }
              $known[] = $val[$i]->Name;


              }

              });  ?>
              </select>

              <br>
              <br>
               <label>New Sub Category <span class="text-red">*</span></label>

               <input type="text" id="newsubcategory" class="form-control" placeholder="Sub Category Name" name="Category" data-fv-field="Category" 
                                  data-fv-message="The Category Name is not valid"
                                  required data-fv-notempty-message=" Sub Category Name is required and cannot be empty"
                                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Sub Category Name can only consist of alphabetical">

                                  <br>
                                  <br>
                  <div class="form-group row has-feedback" style="border-bottom: none;">
                  <div class="col-md-12">
                      <input type="hidden" id="parentid" value="<?php echo session()->get('userdetails')->id ?>">
                      <input type="hidden" id="rowid" value="<?php echo session()->get('userdetails')->id ?>">

                    <button type="sumbit"   id="updatesubcategory" class="btn btn-danger pr-3 pl-3 pull-right">Update</button>
                  </div>
                </div>
        
          </div>
          
      
      <!--   <div class="modal-footer">

          <button type="button" id="closepopedit" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">