@include('shared/header')

<style type="text/css">

.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
}
button.close {
    opacity: 1 !important;

}
.check{
overflow: hidden;
clear: both;
margin-bottom: 15px; 
padding-top: 15px;
}
.imgbg{
height: 50px;
width: 50px;
border-radius: 50%;
background-size: 100% 100%;
float: left;
margin-right: 15px;

}
#outputbox{
    position: absolute;
    left: 15px;
    right: 15px;
    z-index: 9999999;
    background-color: white;
    padding: 15px;

}
.getstaffid
{
 font-size: 16px;

}
.customclass {
    float: right;
    display: inline;
    width: auto;
    margin-right: 150px;
    /* line-height: 18px; */
    /* height: 25px; */
    /* padding: 36px; */
    font-size: 47px;
    transform: scale(1.8);
    margin-top: 5px;
}

.updatecustomclass{

    float: right;
    display: inline;
    width: auto;
    margin-right: 150px;
    /* line-height: 18px; */
    /* height: 25px; */
    /* padding: 36px; */
    font-size: 47px;
    transform: scale(1.8);
    margin-top: 5px;


}

/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 250px;
    overflow-y: auto;
}
#searchuser{

width: 90% !important;
/* right: 0; */
margin: auto;
}
#staffname{
    text-transform:capitalize;
    margin-left: 50px;

}
</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">

     <input type="hidden" id="studioid" value="<?php echo  session()->get('studio_id'); ?>">

@include('shared/navbar')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Staff Members</h4>
                 <div class="heading-elements">
                    <span data-toggle="modal" data-target="#addstaffmember" class="">
                    <a class="rounded-circle add-studio-class" data-toggle="tooltip" data-placement="top" title="Add Staff Member" href="#"><i class="icon-plus-round"></i></a>
                    </span>
                 </div>  
                </div>
                <div class="card-body collapse in ">
                    <div class="card-block card-dashboard dataTables_wrapper" style="overflow: hidden;">
                        <!--id="staffmembers"-->
                        <table id="staffmembers" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                            <thead>
                                <tr>
                                    <th width="40">S.No</th>
                                    <th>Logo</th>
                                    <th>Name</th>
                                    <th>Zip Code</th>
                                    <th>Contact</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><img class="img-thumbnail" style="max-height: 40px;" src="http://www.underconsideration.com/brandnew/archives/dell_2016_logo.png" alt="studio name" /></td>
                                    <td>Studio na,me</td>
                                    <td>123444</td>
                                    <td>03451234578</td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-sm  btn-info"><i class="fa fa-pencil"></i></a>
                                        <a href="#" class="btn btn-sm  btn-danger"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- Recent invoice with Statistics -->
        </div>
    </div>
</div>

<div class="modal fade" id="addstaffmember" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close closestaffbtn" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="headings2"> Add Staff Members</h4>
</div>
<form  id="formservice" enctype="multipart/form-data" method="POST">
<input type="hidden" name="setserviceid" value="" id="setserviceid">
<br> 
<input  type="text" class="form-control" placeholder="Type Staff Member Name/Email Here" id="searchuser">
<input  type="hidden" class="form-control"  id="staffid">
<div class="modal-body">
<div id="wrapper">
<!-- <div id="scroller"> -->

<ul id="outputbox" style="list-style-type:none"></ul>
<!--  </div>  -->
</div>

<br>
<div>
<?php  
$data_array=[];
for($i=0;$i<count($service);$i++){
    $data_array[$service[$i]->id] = $service[$i]->categoryName;
}
$categories_array=array_unique($data_array);
for($i=0;$i<count($service);$i++){
    if(isset($categories_array[$service[$i]->id])){
?>
<div  class="check"><div class='imgbg' style='width:70px;height:70px;background-image:url(<?php echo  $service[$i]->services_image ?>)'>

</div> 
<div>
<h5><?php echo  $service[$i]->categoryName ?></span></h5> 

<div>
<span><?php echo  $service[$i]->service_time ?></span>
<input type="checkbox" class="form-control customclass" serviceid="<?php  echo  $service[$i]->id ?>">
</div>
<div><?php  echo  $service[$i]->SubCategory ?> </div>
</div>

</div> 
<!-- <input type="checkbox" name=""> -->


<?php 
 }
  } ?>
</div>

</form>
<br>
<br>
</div>
<div class="modal-footer">
<div class="pull-right">
<button class="btn btn-primary" id="addstaffmemberbtn">Add</button>
</div>
</div>

</div>

</div>



</div>
 
<div class="modal fade" id="updatestaffmember" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close closestaffbtn" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="headings2"> Update Staff Members</h4>
</div>
<form  id="formservice" enctype="multipart/form-data" method="POST">
<input type="hidden" name="setserviceid" value="" id="setserviceid">
<br> 
<!-- <input  type="text" class="form-control" placeholder="Type Staff Member Name/Email Here" id="searchuser">
 -->
 <h4 id="staffname"> </h4>
 <input  type="hidden" class="form-control"  id="staffid">
<div class="modal-body">
<div id="wrapper">
<!-- <div id="scroller"> -->

<ul id="outputbox" style="list-style-type:none"></ul>
<!--  </div>  -->
</div>

<br>
<div>
<?php  
$data_array=[];
for($i=0;$i<count($service);$i++){
    $data_array[$service[$i]->id] = $service[$i]->categoryName;
}
$categories_array=array_unique($data_array);
// print_r($categories_array);
// exit;
for($i=0;$i<count($service);$i++){
    if(isset($categories_array[$service[$i]->id])){
?>
<div  class="check"><div class='imgbg' style='width:70px;height:70px;background-image:url(<?php echo  $service[$i]->services_image ?>)'>

</div> 
<div>
<h5><?php echo  $service[$i]->categoryName ?></h5> 
<div>
<span><?php echo  $service[$i]->service_time ?></span>
<input type="checkbox" class="form-control updatecustomclass" id="<?php  echo "serid".$service[$i]->id ?>" service-id="<?php  echo  $service[$i]->id ?>">
</div>
<div><?php  echo  $service[$i]->SubCategory ?> </div>
</div>

</div> 
<!-- <input type="checkbox" name=""> -->


<?php  }
 } ?>
</div>

</form>
<br>
<br>
</div>
<div class="modal-footer">
<div class="pull-right">
<button class="btn btn-primary" id="Updatestaff">Update</button>
</div>
</div>

</div>

</div>
@include('shared/footer')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="https://rawgit.com/cubiq/iscroll/master/build/iscroll.js
" type="text/javascript"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">