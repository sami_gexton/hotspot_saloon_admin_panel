@include('shared/header')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.5/css/select.dataTables.min.css">

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;
}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;
}
/* .active{
color:red;
} */
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card-body collapse in"> 
        <div class="card-block">
        <section id="basic-modals">
              <div class="row">
                <div class="col-xs-12">
                  <div class="card">
                  @include('shared/nevigationdetails')
                    <div class="card-header">
                        <div class="col-sm-5">
                          <h4 class="card-title">Studio/Freelancer Finance</h4>
                        </div>
                        <div class="col-sm-4 pull-right">
                          <!-- <h4 class="card-title">Invoice Total </h4> -->
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="col-xs-12" style="margin-bottom: 20px;">
                                <div class="row">
                                    <div class="col-sm-5">
                                        Total Amount (Due on HotSpot)      : 
                                        <?php echo number_format((float)$Dues->total_Dues, 2, '.', '').' '. strtoupper($Dues->currency_code); ?> <br/>
                                        Total Amount (Payment By HotSpot)  : 
                                        <?php echo number_format((float)$paid->total_paid, 2, '.', '') .' '. strtoupper($paid->currency_code); ?> <br/>
                                    </div>

                                    <div class="col-sm-4 pull-right" id='payable_amount'>
                                       
                                    </div>
                                </div>
                            </div>

                            <ul class="nav nav-tabs nav-topline" style="margin-top: 20px;"> 
                               <li class="nav-item">
                                    <a class="nav-link" id="profile-tab2" data-toggle="tab" href="javascript:void(0)" aria-controls="profile2" aria-expanded="false">UnPaid</a>
                                </li>
                                <li class="nav-item">
                                    <a class="mtabs nav-link" id="home-tab2" data-toggle="tab" href="javascript:void(0)" aria-controls="home2" aria-expanded="true">Paid</a>
                                </li>

                                 <li class="nav-item">
                                    <a class="mtabs nav-link" id="paid_inovices" data-toggle="tab" href="javascript:void(0)" aria-controls="" aria-expanded="true">Paid Invocies</a>
                                </li>
                                
                            </ul>


                            <div class="tab-content px-1 pt-1 border-grey border-lighten-2 no-border-top">
                                <div role="tabpanel" class="tab-pane mtabs fade active in" id="home2" aria-labelledby="home-tab2" aria-expanded="true">
                                    <table id="example" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Date</th>
                                                <?php if(session()->get('active_status')=='unpaid' || session()->get('active_status')=='paid' ) { ?>
                                                <th>Product name</th>
                                                <th>Amount (<?=strtoupper($paid->currency_code); ?>)</th>
                                                <th>HotSpot Charges (20%) (<?=strtoupper($paid->currency_code); ?>)</th>
                                                <?php } ?>
                                                <th>Payable Amount(<?=strtoupper($paid->currency_code); ?>)</th>
                                                <?php if(session()->get('active_status')=='Paid_invoices') { ?>
                                                <th>Actions</th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <!-- <th></th>
                                                <th>Date</th>
                                                <th>Product name</th>
                                                <th>Amount</th>
                                                <th>HotSpot Charges</th>
                                                <th>Payable Amount</th>
                                                <th>Status</th> -->
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php  foreach($studio_finance as $value) { 
                                               if(session()->get('active_status')=='unpaid' || session()->get('active_status')=='paid' ) {
                                            ?>
                                            <tr>
                                                <td row_data='<?=json_encode($value);?>'></td>
                                                <td><?php echo date('Y-m-d',strtotime($value->create_time)) ?></td>
                                                <td><?php echo $value->product_name ?></td>
                                                <td><?php echo $value->amount  ?></td>
                                                <td><?php echo ($value->amount)*(20/100)  ?></td>
                                                <td><?php echo $value->amount - ($value->amount)*(20/100) ?></td>
                                            </tr>
                                            <?php } else if(session()->get('active_status')=='Paid_invoices') { ?>
                                                <tr>
                                                <td></td>
                                                <td><?php echo date('Y-m-d',strtotime($value->created_date)) ?></td>
                                                <td><?php echo $value->Amount ?></td>
                                                <td row_data='<?=json_encode($value);?>'> <a class="btn btn-danger btn-sm" aria-hidden="true" href="{{url('/genrate_invoice/'.$value->id.'/genrate_pdf')}}" rowid="1" data-toggle="tooltip" data-placement="top" title="Download PDF"> <i class="fa fa-file-pdf-o"></i></a>
                                                <a class="btn btn-danger btn-sm" aria-hidden="true" href="{{url('/genrate_invoice/'.$value->id.'/send_mail')}}" rowid="1" data-toggle="tooltip" data-placement="top" title="Send Email"> <i class="fa fa-paper-plane-o"></i></a></td>
                                            </tr>

                                            <?php }  ?>

                                            <?php  }  ?>
                                        </tbody>
                                    </table>
                                </div>
						    </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      </div>

<!-- Recent invoice with Statistics -->
        </div>
      </div>
    </div>
    </div><!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>

<script src="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/vendors/js/editors/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
<script>
$(document).ready(function(){
    $('.mtabs').addClass('active');
    $('#example').DataTable( {
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    });

});
$('#home-tab2').on('click',function(){
  window.open('<?php echo url('/studio_finance').'/'.session()->get('studioid').'/paid'?>','_self');
})


$('#profile-tab2').on('click',function(){
  window.open('<?php echo url('/studio_finance').'/'.session()->get('studioid').'/unpaid'?>','_self');
})

$('#paid_inovices').on('click',function(){
  window.open('<?php echo url('/studio_finance').'/'.session()->get('studioid').'/Paid_invoices'?>','_self');
})

Popup = '<?php echo session()->get('Popup');?>';
if(Popup=='Send_Email'){
alert('Send By Email');
<?php session()->forget('Popup');?>
}


activated = '<?php echo session()->get('active_status');?>';
setTimeout(() => {
    if(activated=='paid'){
    $('.nav-link').removeClass('active');
    // $('#profile-tab2').removeClass('active');
    // $('#paid_inovices').removeClass('active');
    $('#home-tab2').addClass('active');

    
} else if(activated=='unpaid'){
    $('.nav-link').removeClass('active');
    // $('#home-tab2').removeClass('active');
    // $('#paid_inovices').removeClass('active');
    $('#profile-tab2').addClass('active');
    }
  else if(activated=='Paid_invoices'){
    $('.nav-link').removeClass('active');
    // $('#home-tab2').removeClass('active');
    // $('#profile-tab2').removeClass('active');
    $('#paid_inovices').addClass('active');
    }
},100);
$(document).on('click','.select-checkbox',function(){
    data=$(this).attr('row_data');
    // if($(this).parent('tr').hasClass('selected')){
        data = JSON.parse(data);
        // console.log(data);
        if(data.payment_status=="unpaid"){
          if($('#Amount_display').length==0){ 
             $('#payable_amount').append(`<span id='Amount_display'>Invoices Total Amount <span><br/><h5 id='total_amount'> </h5><button class="btn btn-primary pull-right" id="paid_button" style="margin-top: 20px">Paid</button>`);
          }
             Ammount = 0;
          $('.select-checkbox').each(function(i,val){
            if($(this).parent('tr').hasClass('selected')){ 
              data_ammount=JSON.parse($(this).attr('row_data'));
            //   console.log(data_ammount.amount);
              Ammount += eval(data_ammount.amount);
            }
          })
          $('#total_amount').text(((Ammount/100)*80)+' '+data.currency_code.toUpperCase());
            if(Ammount==0){
              $("#Amount_display").remove();
            }
          
        }
    // } 
    // else{
    //     console.log(JSON.parse(data));
    // }  
})

$(document).on('click','#paid_button',function(){
   paid_amount = $('#total_amount').text().trim();
//    console.log(paid_amount); 
//    loc = window.location.href;
//    userid=loc.replace( /^\D+/g, ''); 
//    userid= userid.split('/');
//    userid= userid[0];
//    console.log(userid);
     userid = '<?php echo $studio_id ?>';
     invoices_array=[];
    $('.select-checkbox').each(function(i,val){
        if($(this).parent('tr').hasClass('selected')){ 
            data_ammount=JSON.parse($(this).attr('row_data'));
            invoices_array.push(data_ammount.id);
        }
    })
    formData = 'user_id='+userid+'&paid_amount='+paid_amount+'&inovices_id='+JSON.stringify(invoices_array);
    $.post(login.base_url+'Paid_invocies',formData ,function( data ) {
        if(data.result>0){
            alert('Amount Paid');
            location.reload();
        } else {
            alert('something went wrong');
        }
    });

})

</script>