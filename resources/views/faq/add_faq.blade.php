
@include('shared/header')
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= (isset($data['update']) && $data['update'])? 'Update FAQ' : 'Add FAQ'; ?></h4>
            </div>
            <div class="card-body collapse in">
              <div class="card-block card-dashboard" id="formContainer">
                <form class="form form-horizontal row-separator" id="add_form" action="{{url('/add_faq')}}" enctype="multipart/form-data" method="POST" novalidate>
                    <input type="hidden" name="token" value="<?php echo session()->get('token'); ?>">
                      <div class="alert" id="msg" style="display: none;">
                        <strong>Error! </strong> Something went wrong
                      </div>
                      <div class="form-body">
                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="question">Question<span class="text-red">*</span></label>
                            <div class="col-md-9">
                              <input type="text" id="question" class="form-control js-input" placeholder="Question" value="{{@$data['faq']->question}}" name="question" required/>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="answer">Answer<span class="text-red">*</span></label>
                            <div class="col-md-10">
                              <textarea name="answer" rows="10" class="form-control summernote" id="answer">{{@$data['faq']->answer}}</textarea>
                            </div>
                          </div>

                           <div class="form-group row">
                            <label class="col-md-2 label-control" for="category">Category<span class="text-red">*</span></label>
                            <div class="col-md-9">
                              <select id="category" name="category" class="form-control"  style="width: 100%" required>
                                <option disabled selected value="">Select Category</option>
                                @foreach($data['categories'] as $category)
                                  <option <?= (@$data['faq']->category_id == $category->id)? 'selected': '' ?> value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="type">Question Type<span class="text-red">*</span></label>
                            <div class="col-md-9">
                              <select id="type" name="type" class="form-control"  style="width: 100%" required>
                                <option selected value="both">Both</option>
                                <option <?= (@$data['faq']->type == 'user')? 'selected': '' ?> value="user">User</option>
                                <option <?= (@$data['faq']->type == 'studio')? 'selected': '' ?> value="studio">Studio</option>
                              </select>
                            </div>
                          </div>

                          <!-- <div class="form-group row">
                            <label class="col-md-2 label-control" for="type">Show On<span class="text-red">*</span></label>
                            <div class="col-md-9">
                              <select id="type" name="type" class="form-control"  style="width: 100%" required>
                                <option selected value="both">Both</option>
                                <option value="app">Mobile Apps</option>
                                <option value="web">Web App</option>
                              </select>
                            </div>
                          </div> -->
                          <div class="form-group row mb-1" style="border:none; margin-top: 10px;">
                            <div class="col-md-12">
                              @if(isset($data['update']) && @$data['update'])
                                <input type="hidden" id="faq_id" value="{{@$data['faq']->faq_id}}">
                              @endif
                              <button type="submit" class="btn btn-primary pull-right">
                                  <i class="icon-plus"></i> Submit
                              </button>
                            </div>
                          </div>
                        </div><!-- ENDS FORM GROUP -->
                    </form>
                </div>
            </div>
        </div><!-- Recent invoice with Statistics -->
        </div>
      </div>
</div>
   
@include('shared/footer')

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script>
  $(document).ready(function() {
    $('.summernote').summernote({
      tabsize: 2,
      height: 150
    });
  });

  $("#add_form").submit(function(e){
    e.preventDefault();
    var update = <?= (isset($data['update'])) ? @$data['update'] : 0; ?>;
    var msg = $("#msg");
    var answer = $('.summernote').val();
    var question = $('#question').val();
    var category = $('#category').val();
    var type = $('#type').val();

    msg.removeClass('alert-success').removeClass('alert-danger').hide();
    if(question == ''){
      msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> Please enter question').show();
      return false;
    }else if(answer == ''){
      msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> Please enter answer').show();
      return false;
    }else if(category == '' || category === null || category === undefined){
      msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> Please select category').show();
      return false;
    }else if(type == '' || type === null || type === undefined){
      msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> Please select type').show();
      return false;
    }else{
      var data = {
        'question': question,
        'answer': answer,
        'category_id': category,
        'type': type,
      };
      if(update){
        data['faq_id'] = $('#faq_id').val();
      }
      $.ajax({
        url: $(this).attr('action'),
        data: data,
        dataType: "json",
        method: "post",
        success: function(data){
          if(data.status == 'success'){
            msg.removeClass('alert-danger').addClass('alert-success').html('<strong>Success </strong> '+data.msg).show();
            setTimeout(function(){
              window.location.href=data.redirect;
            },1500);
            
          }else{
            msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> '+data.msg).show();
          }
        },
        fail: function(data){
          alert(data);
        }
      })
    }
  });
</script>