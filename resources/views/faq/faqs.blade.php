@include('shared/header')
@section('Content') 

<style type="text/css">
.help-block
{

 color: red;

}  
.ui-datepicker-calendar {
    display: none;
    }
</style> 
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
         <div class="card">
            <div class="card-header">
                <h4 class="card-title pull-left">FAQs</h4>
                <a href="{{url('/add_faq')}}" class="white btn btn-danger pull-right" style="text-decoration:none">Add FAQ</a>
            </div>
            <div class="card-block">
                <table id="faqs" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                  <thead>
                      <tr> 
                          <th>S.No</th>
                          <th>Date</th>
                          <th>Question</th>
                          <th>Answer</th>
                          <th>Category</th>
                          <th>Type</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                    @if($data != '')
                      @for($i = 0; $i < count($data); $i++)
                      <tr>
                          <td>{{$i+1}}</td>
                          <td>{{date('m/d/Y', strtotime($data[$i]->created_on))}}</td>
                          <td>{{ str_limit($data[$i]->question, 20,'...?') }}</td>
                          <td>{{ strip_tags(str_limit($data[$i]->answer, 20,'...')) }}</td>
                          <td>{{ $data[$i]->category }}</td>
                          <td>{{ ucfirst($data[$i]->type) }}</td>
                          <td>
                              <a href="{{url('/faq_edit')}}/{{$data[$i]->id}}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                              <a href="#" onclick="deletes(event, this)" data-id="{{$data[$i]->id}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                          </td>
                      </tr>
                      @endfor
                    @endif
                  </tbody>
                </table>
            </div>
           </div>  
          </div>
    </div>
</div>
   
@include('shared/footer')
<script src="{{asset('public/assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<link rel="stylesheet" href="{{asset('public/assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<script type="text/javascript">
  $("#faqs").dataTable();
  function deletes(e,ele){
    e.preventDefault();
    var c = confirm('Are you sure you want delete?');
    if(c){
      var url = "{{url('faq/remove/')}}"+'/'+$(ele).attr('data-id');
      $.get(url, function(data){
        location.reload();
      });  
    };    
  }
</script>