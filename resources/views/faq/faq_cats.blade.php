
@include('shared/header')
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">FAQ Categories</h4>
            </div>
            <div class="card-body collapse in">
              <div class="card-block card-dashboard" id="formContainer">
                <form class="form form-horizontal row-separator" id="add_form" action="{{url('/faq_cat_add')}}" method="POST" novalidate>
                    <input type="hidden" name="token" value="<?php echo session()->get('token'); ?>">
                      <div class="alert" id="msg" style="display: none;">
                        <strong>Error! </strong> Something went wrong
                      </div>
                      <div class="form-body">
                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="category">Category<span class="text-red">*</span></label>
                            <div class="col-md-8">
                              <input type="text" id="category" class="form-control js-input" placeholder="Category Name" value="{{@$data['category']->category}}" name="category" required/>
                            </div>
                            <div class="col-md-2">
                              @if(isset($data['update']) && @$data['update'])
                                <input type="hidden" id="category_id" value="{{@$data['category']->id}}">
                              @endif
                              <button type="submit" class="btn btn-primary">
                                  <i class="icon-plus"></i> Submit
                              </button>
                            </div>
                          </div>
                        </div><!-- ENDS FORM GROUP -->
                    </form>
                </div>
            </div>
            <div class="clearfix">
              <div class="col-md-12">
                <table id="faqs" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                    <thead>
                        <tr> 
                            <th width="100">S.No</th>
                            <th>Category Name</th>
                            <th width="120">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @if($data['categories'] != '')
                        @foreach($data['categories'] as $k => $category)
                        <tr>
                            <td>{{$k+1}}</td>
                            <td>{{ ucwords($category->category) }}</td>
                            <td>
                                <a href="{{url('faq_categories/edit/')}}/{{$category->id}}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-id="{{$category->id}}" class="btn btn-sm btn-danger" onclick="deletes(event, this)"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                      @endif
                    </tbody>
                  </table>
              </div>
            </div>
        </div><!-- Recent invoice with Statistics -->
        </div>
      </div>
</div>
   
@include('shared/footer')
<script src="{{asset('public/assets/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<link rel="stylesheet" href="{{asset('public/assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<script>
  $(document).ready(function() {
    $('table').dataTable();
  });

  function deletes(e,ele){
    e.preventDefault();
    var c = confirm('Are you sure you want delete?');
    if(c){
      var url = "{{url('faq_categories/remove/')}}"+'/'+$(ele).attr('data-id');
      $.get(url, function(data){
        location.reload();
      });  
    };    
  }

  $("#add_form").submit(function(e){
    e.preventDefault();
    var update = <?= (isset($data['update'])) ? @$data['update'] : 0; ?>;
    var msg = $("#msg");
    var category = $('#category').val();

    msg.removeClass('alert-success').removeClass('alert-danger').hide();

    if(category == ''){
      msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> Please enter category name').show();
      return false;
    }else{
      var data = {
        'category': category,
      };
      if(update){
        data['category_id'] = $('#category_id').val();
      }
      $.ajax({
        url: $(this).attr('action'),
        data: data,
        dataType: "json",
        method: "post",
        success: function(data){
          if(data.status == 'success'){
            msg.removeClass('alert-danger').addClass('alert-success').html('<strong>Success </strong> '+data.msg).show();
            setTimeout(function(){
              window.location.href=data.redirect;
            },1500);
            
          }else{
            msg.removeClass('alert-success').addClass('alert-danger').html('<strong>Error! </strong> '+data.msg).show();
          }
        },
        fail: function(data){
          alert(data);
        }
      })
    }
  });
</script>