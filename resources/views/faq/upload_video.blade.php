@include('shared/header')
@section('Content') 
<style>
.progress-bar-success {
    background-color: #5cb85c;
}
.progress-bar {
    float: left;
    width: 0;
    height: 100%;
    font-size: 12px;
    line-height: 20px;
    color: #fff;
    text-align: center;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
    box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
    -webkit-transition: width .6s ease;
    -o-transition: width .6s ease;
    transition: width .6s ease;
}
.progress {
    height: 1.5rem !important;
}
</style>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- Generic page styles -->
<link rel="stylesheet" href="{{asset('public/assets/upload_video/css/style.css')}}">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{asset('public/assets/upload_video/css/jquery.fileupload.css')}}">

 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Add Faqs Video</h4>
            </div>
            <div class="card-body collapse in">
              <div class="card-block card-dashboard" id="formContainer">
                <form class="form form-horizontal row-separator" id="add_form" action="" enctype="multipart/form-data" method="POST">
                    <!-- <input type="hidden" name="token" value="<?php echo session()->get('token'); ?>"> -->
                      <div class="alert" id="msg" style="display: none;">
                        <strong>Error! </strong> Something went wrong
                      </div>
                      <div class="form-body">
                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="question">Video Title<span class="text-red">*</span></label>
                            <div class="col-md-9">
                              <input type="text" id="Video_title" class="form-control js-input" placeholder="Video_title" value="" name="Video_title" required/>
                            </div>
                          </div>

                      

                         <div class="form-group row">
                            <label class="col-md-2 label-control" for="type">Upload Video<span class="text-red">*</span></label>
                            <div class="col-md-9">
                          <span class="btn btn-success fileinput-button">
                          <i class="glyphicon glyphicon-plus"></i>
                          <span>Select file</span>
                          <!-- The file input field used as target for the file upload widget -->
                          <input id="fileupload" type="file" name="files" accept="video/mp4,video/x-m4v,video/*">
                          </span>
                          <br>
                          <br>
                          <div id="progress" class="progress">
                          <div class="progress-bar progress-bar-success">
                          <span id="dialog_title_span"></span>
                          </div>
                          </div>
                          <!-- The global progress bar -->
                         
                          <!-- The container for the uploaded files -->
                          <!-- <div id="files" class="files"></div> 
                            </div> -->
                          </div> 
                          <div class="form-group row mb-1" style="border:none; margin-top: 10px;">
                            <div class="col-md-12">
                              <!-- @if(isset($data['update']) && @$data['update'])
                                <input type="hidden" id="faq_id" value="{{@$data['faq']->faq_id}}">
                              @endif -->
                              <button type="submit" id="submit_button" class="btn btn-primary pull-right" disabled>
                                  <i class="icon-plus"></i> Save Video
                              </button>
                            </div>
                          </div>
                        </div><!-- ENDS FORM GROUP -->
                    </form>

<div class="card-body collapse in">
<div class="card-block card-dashboard">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap4">
<div class="row">
<div class="col-md-12">

    <table id="videotable" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
        <thead>
            <tr role="row">
                <th>Sr.</th>
                <th>Date</th>
                <th>video_title</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            <?php
        
            // exit();
            for($i=0;$i<count($result);$i++)
            { 
            ?>
            <tr role="row" class="odd">
                <td><?php echo $result[$i]->id ?></td>
                <td style="width:auto;"> <?php echo date('M d, Y',strtotime($result[$i]->date)) ?></td>
                <td><?php echo $result[$i]->video_title ?></td>
                <td> 
                <a class='btn btn-danger btn-sm delet_video' title='Delete' aria-hidden='true' data-toggle='tooltip' href="javascript:void(0)"  rowid="<?php echo $result[$i]->id?>">
                <i class='fa fa-trash-o'></i>
                </a>
                </td>
            </tr>

        <?php  }  ?>
           
        </tbody>                
    </table>
</div>
</div>
</div>              
</div>
</div>
</div>  
                </div>
            </div>
        </div><!-- Recent invoice with Statistics -->
        </div>
      </div>
</div>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->


@include('shared/footer')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
<script>
Video_link = '';
basepath = window.location.pathname;
localpath = "";
basepath = basepath.split('/');
localpath = '/' + basepath[1] + '/';
var url = localpath+'public/upload_video_faqs';
$(document).ready(function() {    
    $('#fileupload').fileupload({
    url: url,
    dataType: 'json',
    done: function (e, data) {
        // $.each(data.result.files, function (index, file) {
        //     $('<p/>').text(file.name).appendTo('#files');
        // });
        $("#submit_button").removeAttr('disabled');
        console.log(data.result.link);
        Video_link = data.result.link;
    },
    progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100,10);
        $('.progress-bar').css(
            'width',
            progress + '%'
        );
        $('#dialog_title_span').text(progress);
       
    }
});
    $('#add_form').submit(function(evt) {
        evt.preventDefault();
        query_string =`file=${Video_link}&title=${$('#Video_title').val()}`;
        $.post(url,query_string, function( data ) {
            console.log(data);
            if(data){
               location.reload();
            }
        });
    })
    $('#videotable').DataTable();
        $(document).on('click','.delet_video',function(){
        id=$(this).attr('rowid');
        check = confirm('Are You Sure!');
        if(check){
            window.open(`${localpath}delete_faq_video/${id}`, `_self`);
        }
    })
})
</script>
<script src="{{asset('public/assets/upload_video/js/vendor/jquery.ui.widget.js')}}"></script>
<script src="{{asset('public/assets/upload_video/js/jquery.iframe-transport.js')}}"></script>
<script src="{{asset('public/assets/upload_video/js/jquery.fileupload.js')}}"></script>
<script>
// Change this to the location of your server-side upload handler:




 
