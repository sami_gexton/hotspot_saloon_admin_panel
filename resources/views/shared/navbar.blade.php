<style type="text/css">
   .disabled {
        pointer-events: none;
        cursor: default;
        opacity: 0.6;
    }

    .notifyicon-scroll {
      overflow-y: scroll;
      max-height: 325px;
    }

    .notifyicon-scroll::-webkit-scrollbar {
    width: 5px;
}
 
.notifyicon-scroll::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
 
.notifyicon-scroll::-webkit-scrollbar-thumb {
  background-color: rgba(0,0,0,.4);
  outline: 1px solid slategrey;
}

</style>
<?php 
$persmission_array = json_decode(Auth::User()->permissions,true);
?>
 <div class="sideNavbar">
  <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
            <li class="nav-item"><a href="javascript:void(0)" class="img-responsive navbar-brand nav-link"><img alt="branding logo" src="{{asset('public/assets/images/logo/studio-logo-white.png')}}" data-expand="{{asset('public/assets/images/logo/studio-logo-white.png')}}" data-collapse="{{asset('public/assets/images/logo/studio-logo-white.png')}}" class="brand-logo img-responsive"></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="javascript:void(0)navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5"></i></a></li>
              <li class="nav-item hidden-sm-down"><a href="javascript:void(0)" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
             
            </ul>
            <ul class="nav navbar-nav float-xs-right">
            <li class="dropdown dropdown-notification nav-item">
                  <a href="#" data-toggle="dropdown" class="nav-link nav-link-label">
                    <i class="ficon icon-bell4" id='noti_badge'></i>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                      <h6 class="dropdown-header m-0">
                        <span class="grey darken-2">Notifications</span>
                        <span class="notification-tag tag tag-default tag-danger float-xs-right m-0" id='notification_count'> </span>
                      </h6>
                    </li>

                    <div id="append_notifactions" class="notifyicon-scroll">
                    </div>

                  <li class="dropdown-menu-footer"><a href="{{url('notifications')}}" class="dropdown-item text-muted text-xs-center">Read all notifications</a></li>

                </ul>
              </li>
              <li class="dropdown dropdown-user nav-item"><a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online">
              
              <img src="<?php if(!empty(Auth::User()->profile_pic)){
                  echo asset('').Auth::User()->profile_pic;
              } else {
                echo asset('').'public/assets/uploads/placeholder.png';
                
              } ?>" alt="avatar"><i></i></span><span class="user-name"><?php echo @session()->get('userdetails')->Studio_Name ?></span></a>
                <div class="dropdown-menu dropdown-menu-right">
                 <a href="{{url('/logout')}}" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <div class="main-menu-header">
        <input type="text" placeholder="Search" class="menu-search form-control round"/>
      </div>
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class="nav-item"><a  href="{{url('/dashboard')}}"><i class="icon-home3 "></i><span data-i18n="nav.dash.main" class="menu-title">Dashboard</span></a></li>
          <?php if($persmission_array['Studio']==1) { ?>
           <li class="nav-item"><a href="{{url('/studios')}}"><i class="fa fa-bank"></i><span  class="menu-title">Studio</span></a></li>
          <?php } ?>
           <?php if($persmission_array['Users']==1) { ?>
           <li class="nav-item"><a href="{{url('/users')}}"><i class="icon-user4"></i><span  class="menu-title">Users</span></a></li>
           <?php } ?>
           <?php if($persmission_array['Categories']==1) { ?>
           <li class="nav-item"><a href="javascript:void(0)"><i class="icon-price-tags"></i><span  class="menu-title">Video Categories</span></a>
            <ul class="menu-content">
              <li class=""><a href="{{url('/AddCategory')}}"><!-- <i class="icon-tags"></i> --><span  class="menu-title">Categories</span></a></li>
              <li class=""><a href="{{url('/subcategories')}}"><!-- <i class="icon-tags"></i> --><span  class="menu-title">Sub Categories</span></a></li>
            </ul>
           </li>
           <?php } ?>
           <?php if($persmission_array['Packages']==1) { ?>
           <li class="nav-item"><a href="{{url('/package')}}"><i class="icon-adjust"></i><span  class="menu-title" >Create Packages</span></a></li>
           <?php } ?>
           <?php if($persmission_array['About']==1) { ?>
            <li class="nav-item"><a href="{{url('/aboutus')}}"><i class="icon-users"></i><span  class="menu-title">About Us</span></a></li>
          <?php } ?>
          <?php if($persmission_array['Finance']==1) { ?>
           <li class="nav-item"><a href="javascript:void(0)"><i class="icon-price-tags"></i><span  class="menu-title">Finance</span></a>
            <ul class="menu-content">
            <li class="nav-item"><a href="{{url('/invoices')}}"><span  class="menu-title">Invoices</span></a></li>
              <li class=""><a href="{{url('/dues')}}"><!-- <i class="icon-line-chart"></i> --><span  class="menu-title">Dues</span></a></li>
            </ul>
          </li>
          <?php } ?>
          <?php if($persmission_array['FAQ']==1) { ?>
          <li class="nav-item"><a href="javascript:void(0)"><i class="fa fa-question-circle"></i><span  class="menu-title">FAQ</span></a>
            <ul class="menu-content">
              <li class="nav-item"><a href="{{url('/add_faq')}}"><span  class="menu-title">Add FAQ</span></a></li>
              <li class=""><a href="{{url('/faq_categories')}}"><!-- <i class="icon-line-chart"></i> --><span  class="menu-title">Add FAQ Category</span></a></li>
              <li class=""><a href="{{url('/faq')}}"><!-- <i class="icon-line-chart"></i> --><span  class="menu-title">All FAQ</span></a></li>
              <li class=""><a href="{{url('/upload')}}"><!-- <i class="icon-line-chart"></i> --><span  class="menu-title">Upload Videos</span></a></li>
            </ul>
          </li>
          <?php } ?>
         <?php if(Auth::User()->user_type=='admin') { ?>
          <li class="nav-item"><a href="javascript:void(0)"><i class="icon-price-tags"></i><span  class="menu-title">Create Admin User</span></a>
            <ul class="menu-content">
              <li class=""><a href="{{url('/add_Sub_Admin')}}"><!-- <i class="icon-tags"></i> --><span  class="menu-title">Add</span></a></li>
              <li class=""><a href="{{url('/sub_admin_listing')}}"><!-- <i class="icon-tags"></i> --><span  class="menu-title">Delete</span></a></li>
            </ul>
           </li>
         <?php } ?>

         <?php if($persmission_array['Contact_Us']==1) { ?>
          <li class="nav-item"><a href="{{url('/contact_list')}}"><i class="icon-user4"></i><span  class="menu-title">Contact Us</span></a></li>
           </li>
         <?php } ?>

          <?php if($persmission_array['currency_rate']==1) { ?>
            <li class="nav-item"><a href="{{url('/currency_rate')}}"><i class="fa fa-money"></i><span  class="menu-title">Currency Rate</span></a></li>
           </li>
         <?php } ?>

           <?php if($persmission_array['About']==1) { ?>
            <li class="nav-item"><a href="{{url('/seo_tool')}}"><i class="fa fa-line-chart"></i><span  class="menu-title">Seo Tool</span></a></li>
           </li>
         <?php } ?>

          <?php if($persmission_array['About']==1) { ?>
            <li class="nav-item"><a href="{{url('/translate')}}"><i class="fa fa-language"></i><span  class="menu-title">WebSite Translations</span></a></li>
           </li>
         <?php } ?>
         
        </ul>
      </div>
    </div>
</div>

<script>


</script>