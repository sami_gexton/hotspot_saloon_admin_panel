<footer class="footer navbar-fixed-bottom footer-dark navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  © 2017 <a href="#" target="_blank" class="text-bold-800 grey darken-2">hotspot4beauty </a>, All rights reserved. </span></p>
    </footer>
    <script src="{{asset('public/assets/vendors/js/core/libraries/jquery.min.js')}}" type="text/javascript"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.js"></script>
    <script src="{{asset('public/assets/vendors/js/ui/tether.min.js')}}" type="text/javascript"></script>   
    <script src="{{asset('public/js/formValidation.min.js')}}"></script>
    <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/js/core/libraries/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/ui/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/ui/unison.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/ui/blockUI.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/ui/jquery.matchHeight-min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/ui/jquery-sliding-menu.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/sliders/slick/slick.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/ui/screenfull.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/extensions/pace.min.js')}}" type="text/javascript"></script>
	<!-- Error in hese files -->
    <script src="{{asset('public/assets/vendors/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/core/app.min.js')}}" type="text/javascript"></script>
	<!-- <script src="{{asset('public/assets/vendors/js/scripts/extensions/unslider.min.js')}}" type="text/javascript"></script>  -->
    <!-- Error in hese files -->    
    <script src="{{asset('public/assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/js/charts/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<!--     <script src="{{asset('public/assets/vendors/js/dashboard-analytics.min.js')}}" type="text/javascript"></script>
 -->    
    
    <!-- /build-->
    <!-- build:js app-assets/js/app.min.js-->
    <script src="{{asset('public/assets/vendors/js/scripts/ui/fullscreenSearch.min.js')}}" type="text/javascript"></script>
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD9oB86fWndMReplwi1c9EdPp8vfp40ptM'></script>
    <script src="{{asset('public/js/login/login.js')}}" type="text/javascript"></script>

    <script src="{{asset('public/app-assets/js/scripts/ui/scrollable.min.js')}}" type="text/javascript"></script>
   
    
  <!--    
     <script src="{{asset('public/assets/vendors/js/dashboard-analytics.min.js')}}" type="text/javascript"></script>
     <script src="{{asset('public/assets/vendors/js/dashboard-crm.min.js')}}" type="text/javascript"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.2.1/bootstrap-filestyle.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.0/sweetalert2.min.js"></script> 

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.0/sweetalert2.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script src="{{asset('public/js/admin/jquery.form.js')}}" type="text/javascript"></script>
<script src="{{asset('public/js/admin/toastr.min.js')}}" type="text/javascript"></script>
<!-- <script src="{{asset('public/js/admin/jquery.validationEngine.js')}}" type="text/javascript"></script> -->
<script src="{{asset('public/js/admin/custom.js')}}" type="text/javascript"></script>
<script>
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

function notification(test=null){
        $.get('/admin/notifications',function(data){
            if(data.data==null || data.data==[] || data.data==''){
                return false;
            }  
            count = 0;    
        $.each(data.data,function(i,val){
            // pictures='https://hotspot4beauty.com/admin/public/assets/uploads/images/propic/2017/11/studioapp_up_1510577861.jpeg';
            if(val.type=="new_video" || val.type=="video_report" ||             val.type=="Subscribed"){
                    pictures ="{{url('/public/assets/images/videos_profilescreen_enable.png')}}";
            }
            html= `<li class="list-group scrollable-container ps-container ps-theme-dark ps-active-y">
                <a href="{{url('notifications')}}" class="list-group-item">
                <div class="media">
                <div class="media-left valign-middle" style="background: url('${pictures}') no-repeat; background-size: cover; width: 35px !important; height: 40px; float: left; margin-right: 10px; border-radius: 50%;">
                </div>
                <div class="media-body">
                <h6 class="media-heading">${val.title}</h6>
                <p class="notification-text font-small-3 text-muted">${val.body}</p><small>
                </div>
                </div>
                </a>
                </li>`;
            if(test==null){
                $('#append_notifactions').append(html);
            }   
            if(val.is_admin_read==0){
                count++;
                if(test==true && $('#append_notifactions').children('li').length<count){
                    html= `<li class="list-group scrollable-container ps-container ps-theme-dark ps-active-y">
                    <a href="{{url('notifications')}}" class="list-group-item">
                    <div class="media">
                    <div class="media-left valign-middle" style="background: url('${pictures}') no-repeat; background-size: cover; width: 35px !important; height: 40px; float: left; margin-right: 10px; border-radius: 50%;">
                    </div>
                    <div class="media-body">
                    <h6 class="media-heading">${val.title}</h6>
                    <p class="notification-text font-small-3 text-muted">${val.body}</p><small>
                    </div>
                    </div>
                    </a>
                    </li>`;
                    $('#append_notifactions').prepend(html);
                }
            }
        });   
        if(count>0){
            $('#notification_count').text(count +" new notifications");
            $('.tag-pill').remove();
            $('#noti_badge').after('<span class="tag tag-pill tag-default tag-danger tag-default tag-up">'+ count +'</span>');
        }else {
            $('#notification_count').text('no new notifications');
        }    
        //  setTimeout(function(){ notification(true); },10000);
        });
    }
// $(function(){
   
        window.onload = function () {
        string=window.location.href;
        if(string.indexOf("login")==-1){
             notification(); 
         }
            // setTimeout(function(){ notification(); },100);
        }
// })
</script>
</body>
</html>