<style>
.fontsize
{
  font-size: 25px;  

}
.cover-bg{


  background-image: url(<?php echo asset(session()->get('cover_pic'))?>);
  background-size: cover;
}
</style>

 <input type="hidden"   id="usersid" value="<?php echo session()->get('studioid')?>" />
  <input type="hidden"  id="activeclass" value="<?php echo session()->get('activeclass')?>" />

    <div class="headerWrap card-primary">
      <div class="card-header border0 cover-bg">
        <div class="row">
          <div class="col-md-2 col-sm-2 col-lg-2 col-xs-4">
            <div class="rounded-circle logo-bg" style="height: 100px;width: 100px;background: url(<?php echo asset(session()->get('pro_pic'))?>) no-repeat center center;
    background-size: cover;"></div>
          </div>
          <div class="col-sm-4 col-md-3 col-lg-4 col-xs-8 white pl-0 pr-0 pt-1 col-p-Top">
            <h1 class="content-header-title mb-0"><?php echo session()->get('StudioName')?></h1>
            <p class="text-muted white mb-0"><?php echo session()->get('username')?></p>
            <h6 class="text-muted white"><?php echo session()->get('country')?></h6>
          </div>
          <div class="col-xl-6 col-lg-6 col-sm-6 col-md-7 col-xs-12 pr-0 pl-0 col-xs-12 mt-2 pt-1 mb-1 text-xs-right float-xs-right col-m-Top col-m-bottom">
              <div class="media list-inline-item align-middle">
                  <div class="text-xs-center">
                    <a href="<?php echo session()->get('facebooklink'); ?>" class="btn btn-social-icon btn-facebook rounded-circle" target="_blank"><span class="icon-facebook3"></span></a>
                    <a href="<?php echo session()->get('twitterlink'); ?> " class="btn btn-social-icon btn-twitter rounded-circle" target="_blank"><span class="icon-twitter3"></span></a>
                    <a href="<?php echo session()->get('linkedinlink'); ?>" class="btn btn-social-icon btn-linkedin rounded-circle" target="_blank"><span class="icon-linkedin3"></span></a>
                </div>
              </div>
              <div class="border-left-black pl-1 list-inline-item align-middle mobile-display-none">
                <div class="media px-1 bgdefault rounded-circle circleBg p-1">
                    <div class="vidColl white text-xs-center" data-toggle="tooltip" data-placement="top" title="Subscribe">
                        <p><i class="icon-ei-pointer font-large-1"></i></p>
                        <h6 class="card-subtitle"><?php echo session()->get('totalsubscribers') ?></h6>
                    </div>
                </div>
              </div>
              <div class="border-left-black pl-1 list-inline-item align-middle mobile-display-none">
                <div class="media bg-blue rounded-circle circleBg p-1">
                    <div class="vidColl white text-xs-center" data-toggle="tooltip" data-placement="top" title="Video">
                        <p><i class="icon-play font-large-1"></i></p>
                        <h6 class="card-subtitle"><?php echo session()->get('totalvideo') ?></h6>
                    </div>
                </div> 
              </div>
             
          </div>
          </div>
        </div>
        <nav class="navbar navbar-default navbarTabs">
            <div class="navbar-wrapper">
              <div class="navbar-header nav-head desktop-display-none">        
                <button data-toggle="collapse" data-target="#navbar-toggle" class="bgdefault btn btn-default open-navbar-container collapsed" aria-expanded="false">Menu</button>
              </div>
              <div class="navbar-container content bg-white pb-0 pt-0" style="margin-left: 0px">
                <div id="navbar-toggle" class="collapse navbar-toggleable-sm">
                  <ul class="nav navbar-nav fontsize text-xs-center mb-0">
                    <li class="nav-item " id="Overveiw"> <a  href="<?php echo url('/overview').'/'.session()->get('studioid')?>"><p><i class="fa fa-file-text"></i></p> Overveiw </a></li>
                    <li class="nav-item" id="Services"><a  id="servicesclick" href="<?php echo url('/services').'/'.session()->get('studioid')?>"><p><i class="fa fa-cog"></i></p> Services</a></li>
                   <?php if(session()->get('user_type')!='freelancer') { ?>
                    <li class="nav-item" id="StaffMembers"><a  href="<?php echo url('/staff').'/'.session()->get('studioid')?>"><p><i class="fa fa-users"></i></p>Staff Members</a></li>
                    <?php } 
                     else { ?>
                     <li class="nav-item" id="StaffMembers"><a  href="<?php echo url('/bank_deatails').'/'.session()->get('studioid')?>"><p><i class="fa fa-users"></i></p>Bank Account</a></li>
                     <?php } ?>
                    <li class="nav-item" id="Videos"> <a href="<?php echo url('/video'),'/'.session()->get('studioid')?>"><p><i class="icon-play"></i></p> Videos</a></li>
                    <li class="nav-item" id="Reviews"><a href="<?php echo url('/reviews').'/'.session()->get('studioid')?>"><p><i class="icon-wechat"></i></p> Reviews</a></li>
                    <li class="nav-item" id="General"><a href="<?php echo url('/genral').'/'.session()->get('studioid')?>"><p><i class="fa fa-user"></i></p> General</a></li>
                    <li class="nav-item" id="finance" style="width: 14.2% !important;"><a href="<?php echo url('/studio_finance').'/'.session()->get('studioid').'/unpaid'?>"><p><i class="fa fa-line-chart"></i></p> Finance</a></li>
                  </ul>
                </div>
              </div>
            </div>
        </nav>
     </div> <!-- end .headerWrap -->

