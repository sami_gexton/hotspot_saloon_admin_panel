@include('shared/header')

<style type="text/css">

.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php   echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div class="card border0">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <div class="card bgcoll border0">
                <div class="card-body" id="overviewWrap">
                    <div class="card-block p-0">
                        <div class="row">
                            <div class="col-xl-4 col-md-6 col-lg-6 col-sm-12">
                                <div class="media bg-twitter white p-1 border-grey border-lighten-3">
                                    <div class="media-body">
                                        
                                       <?php if(session()->get('user_type')!='freelancer') { ?>
                                        <span class="font-medium-3 text-bold-600"><?php  echo $result['total_member']?></span>
                                        <p class="font-medium-1">Staff Member</p>

                                        <?php }  else { ?>
                                <span class="font-medium-3 text-bold-600"><?php  echo $result['totalservice']?></span>
              
                                        <p class="font-medium-1">Total Service</p>
                                        <?php  }  ?>
                                    </div>
                                    <div class="media-left media-middle card-outline-white white rounded-circle p-1">
                                        <i class="icon-users font-large-1 white"></i>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-xl-4 col-md-6 col-md-6 col-lg-6 col-sm-12">
                                <div class="media px-1 bg-danger white p-1 border-grey border-lighten-3">
                                    <div class="media-body">
                                        <span class="font-medium-3 text-bold-600"><?php  echo $result['totalvideo']?></span>
                                        <p class="font-medium-1">Total Videos</p>
                                    </div>
                                    <div class="media-left media-middle white card-outline-white rounded-circle p-1">
                                        <i class="icon-play font-large-1 white"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-lg-6 col-sm-12">
                                <div class="media bg-success white p-1 border-grey border-lighten-3">
                                    <div class="media-body">
                                        <span class="font-medium-3 text-bold-600">Package Name</span>
                                        <p class="font-medium-1"><?php  echo $result['package_name']?></p>
                                    </div>
                                    <div class="media-left media-middle white card-outline-white rounded-circle p-1">
                                        <i class="icon-box font-large-1 white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Recent invoice with Statistics -->
        
        <div class="row">
          <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-title">Overview</h4>
                  </div>
                  <div class="card-body collapse in">
                      <div class="card-block">
                      <div id="container"> </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>


@include('shared/footer')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
  

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
            // colors: ['#0000FF', '#0066FF', '#00CCFF'],

    title: {
        text: 'Studio Montly Basics Chart'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
    // },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Subscribers'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> Subscribers<br/>'
    },

    series: [{
        name: 'Subscribers',
        colorByPoint: true,
        data: [{
            name: 'Monthly',
            y: <?php echo $result['Monthlysubscribers'] ?>
        }, {
            name: 'Weekly',
            y: <?php echo $result['weeklysubscribers'] ?>
        }, {
            name: 'Daily',
            y: <?php echo $result['daily'] ?>
        }],
         
    }]
});


  
 
});
</script>