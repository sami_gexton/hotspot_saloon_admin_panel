@include('shared/header')

<style type="text/css">
.border0{
        border: 0px;
       
    }
.center-block{
        display:  block;
    }
body
{
    counter-reset: Serial;           /* Set the Serial counter to 0 */
}
tr td:first-child:before
{
counter-increment: Serial;      /* Increment the Serial counter */
content: counter(Serial); /* Display the counter */
}

</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
            <?php 
             if(count($result)>0)
             {


            ?>  
      <section id="hover-effects" class="card">
              <div class="card-header">
                  <h4 class="card-title">Studio Video </h4>
              </div>
              <div class="card-body collapse in">
                  <div class="card-block my-gallery" itemscope="" itemtype="http://schema.org/ImageGallery" data-pswp-uid="1">
                      <div class="row">
                    <div class="grid-hover">
                    <table id="videotable" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                    <thead>
                      <tr >
                        <th width="40">S.No</th>
                        <th>Video Thumbnail</th>
                        <th>Title</th>
                        <th>Duration</th>
                         <th>Category</th>
                         <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="dataapend">
                          <?php

                            for($i=0;$i<count($result);$i++){
                            ?>

                            <tr role="row" class="odd">
                            <td ></td>
                            <td>
                            <img class='img-thumbnail' style='max-height: 40px' src="<?php echo asset($result[$i]->thumbnail) ?>" />  
                            </td>
                             <td ><span style="width:200px;display: table-cell;    white-space: normal !important;"><?php echo $result[$i]->title ?></span></td>
                            <td ><?php echo $result[$i]->Duration ?></td>
                            <td ><?php echo $result[$i]->category_name ?></td>
                            <td >
                            <a class="btn btn-info" data-toggle="modal" href="#" data-target="<?php echo '#default'.$i ?>" style="    font-size: 12px;"> <i class="fa fa-play" aria-hidden="true"></i> </a>
                            <a class="btn btn-danger deletevideo" style="font-size: 12px;" href="#" rowid="<?php echo $result[$i]->id ?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
                            </td>

                            </tr>
                      <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true"  id="<?php echo 'default'.$i ?>">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <div class="modal-body">
                      <button type="button" class="close videoclose" data-dismiss="modal" aria-label="Close" videosrcid="<?php echo 'videosrc'.$i ?>"><span aria-hidden="true">×</span></button>
                      <video width="100%" height="360" controls id="<?php echo 'videosrc'.$i ?>" >
                      <source   src="<?php echo $result[$i]->Video_link ?>" type="video/mp4">
                      </video>
                      </div>
                      </div>
                      </div>
                      </div>
                  
                          <?php } ?>
                          

                          </tbody>        
                  </table>
                              
                          </div>
                      </div>
                  </div>
              </div>
              <!--/ Image grid -->
          </section>


   <?php } ?>


      
    </div>
      
  </div>
</div>
   <!-- ////////////////////////////////////////////////////////////////////////////-->
   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">