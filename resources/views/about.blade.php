@include('shared/header')

<style type="text/css">
.lists{
    display: inline-flex;
    list-style-type: none;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    padding-left: 0;
    font-size: 27px;



}
.lists li{
    list-style: none;
    display: inline-bl;
    width: calc(50% / 3);
    text-align: center;


}
.active{
color:red;

}
.details{
    margin-left: 50px;
    background-color: #4dd0e1;
    width: 258px;
    text-align: left;
    font-size: 20px;
    border-radius: 2%;
    }
</style>
@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')

   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card-body collapse in"> 
        <div class="card-block">
        <section id="basic-modals">
              <div class="row">
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Personal Info</h4>
                    </div>
                    <div class="card-body collapse in">
                      <div class="card-block">
                        <form class="form">
                          <div class="form-body">                
                            <div class="form-group">
                              <label for="aboutus">About Us</label>
                              
                              <?php  if(isset($data->description)!="" && $data!=null) 
                                 {
                                    $varibale   ='readonly';
                                    $description=$data->description;
                                    $buttontext ='Edit';
                                    $rowid      =$data->id;
                                    $id         = "editaboutus";
                                 }
                                 else
                                 {

                                    $varibale   ='';
                                    $description='';
                                    $buttontext ='Save';
                                    $rowid      ='';
                                    $id         = "updateaboutus";


                                 }
                                ?>
                              
                                <textarea id="aboutus" rows="10" class="form-control bg-grey bg-lighten-4" name="comment" placeholder="Message"<?php echo $varibale  ?> ><?php  echo $description ?>
                                </textarea>

                              
                            </div>
                            <div class="form-group">
                              <button type="submit" class="btn btn-danger pr-3 pl-3 float-xs-right" id="<?php  echo $id?>" rowid="<?php echo $rowid  ?>"> <?php echo $buttontext  ?></button>



                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      </div>

<!-- Recent invoice with Statistics -->
        </div>
      </div>
    </div>
    </div><!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>

<script src="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/vendors/js/editors/ckeditor/ckeditor.js" type="text/javascript"></script>