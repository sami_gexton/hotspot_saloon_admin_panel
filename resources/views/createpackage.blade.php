@include('shared/header')
@section('Content') 
 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
        <div class="card">
            <div class="card-header mb-3">
                <h4 class="card-title">Create New Subscriptions</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard" id="formContainer">
                    <form class="form form-horizontal row-separator fv-form fv-form-bootstrap" id="addstudio" data-fv-framework="bootstrap" data-fv-message="This value is not valid" data-fv-feedbackicons-valid="glyphicon glyphicon-ok" data-fv-feedbackicons-invalid="glyphicon glyphicon-remove" data-fv-feedbackicons-validating="glyphicon glyphicon-refresh" enctype="multipart/form-data" method="POST" novalidate>
                          <div class="form-body">
                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="studio">Date <span class="text-red">*</span></label>
                                <div class="col-md-9 has-icon-left">
                                  <input type="hidden" name="usertype" value="studio_user">
                                  <input type="text" id="studio" class="form-control" placeholder="Enter Date" name="Studio_Name" required="" data-fv-message="The Studio Name is not valid" data-fv-notempty-message="The Studio Name is required and cannot be empty" pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Studio Name can only consist of alphabetical, number" data-fv-field="Studio_Name"><i class="form-control-feedback" data-fv-icon-for="Studio_Name" style="display: none;"></i>
                                  <div class="form-control-position">
                                      <i class="fa fa-building"></i>
                                  </div>
                                <small class="help-block" data-fv-validator="notEmpty" data-fv-for="Studio_Name" data-fv-result="NOT_VALIDATED" style="display: none;">The Studio Name is required and cannot be empty</small><small class="help-block" data-fv-validator="regexp" data-fv-for="Studio_Name" data-fv-result="NOT_VALIDATED" style="display: none;">The Studio Name can only consist of alphabetical, number</small></div>
                              </div>                          

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="username">Package Name <span class="text-red">*</span></label>
                                <div class="col-md-9 has-icon-left">
                                <span id="packagerror" style="color:red;display:none">User Name Already Present </span>
                                  <input type="text" id="packagename" class="form-control js-input" placeholder="Package" name="username" required="" data-fv-message="The User Name is not valid" data-fv-notempty-message="The  User Name is required and cannot be empty" pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The  User Name can only consist of alphabetical, number" data-fv-field="username"><i class="form-control-feedback" data-fv-icon-for="username" style="display: none;"></i>
                                  <div class="form-control-position">
                                      <i class="fa fa-user"></i>
                                  </div>
                                <small class="help-block" data-fv-validator="notEmpty" data-fv-for="username" data-fv-result="NOT_VALIDATED" style="display: none;">The  User Name is required and cannot be empty</small><small class="help-block" data-fv-validator="regexp" data-fv-for="username" data-fv-result="NOT_VALIDATED" style="display: none;">The  User Name can only consist of alphabetical, number</small></div>
                              </div>

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="user-Contact">Number of Videos <span class="text-red">*</span></label>
                                <div class="col-md-9 has-icon-left">
                                  <input type="number" id="user-Contact" class="form-control" placeholder="Videos" name="contact_no" required="" data-fv-field="contact_no"><i class="form-control-feedback" data-fv-icon-for="contact_no" style="display: none;"></i>
                                  <div class="form-control-position">
                                      <i class="fa fa-phone"></i>
                                  </div>
                                <small class="help-block" data-fv-validator="integer" data-fv-for="contact_no" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small><small class="help-block" data-fv-validator="notEmpty" data-fv-for="contact_no" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small></div>
                              </div>

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="Address">Time <span class="text-red">*</span></label>
                                <div class="col-md-9 has-icon-left">
                                  <input type="text" id="Address" class="form-control" placeholder="Time" name="Address" required="" data-fv-field="Address"><i class="form-control-feedback" data-fv-icon-for="Address" style="display: none;"></i>
                                  <div class="form-control-position">
                                      <i class="fa fa-location-arrow"></i>
                                  </div>
                                <small class="help-block" data-fv-validator="notEmpty" data-fv-for="Address" data-fv-result="NOT_VALIDATED" style="display: none;">This value is not valid</small></div>
                              </div>

                              <div class="form-group row">
                                <label class="col-md-2 label-control" for="country">Select Country <span class="text-red">*</span></label>
                                <div class="col-md-9 has-icon-left">
                                    <select id="country" name="country" class="form-control js-example-basic-single select2-hidden-accessible" style="width: 100%" tabindex="-1" aria-hidden="true"> required&gt;
                                        <option disabled="" selected="" value="">Select Country</option>
                                        <option value="Viet Nam">Viet Nam</option>
                                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                        <option value="Wallis And Futuna">Wallis And Futuna</option>
                                        <option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                   <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;">
                                       <span class="selection">
                                           <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-country-container">
                                               <span class="select2-selection__rendered" id="select2-country-container" title="Select Country">Select Country</span>
                                               <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                                           </span>
                                       </span>
                                       <span class="dropdown-wrapper" aria-hidden="true"></span>
                                   </span>
                                    <div class="form-control-position">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                  </div>
                              </div>

                              <div class="form-group row has-feedback">
                                <label class="col-md-2 label-control" for="zipcode">Amount <span class="text-red">*</span></label>
                                <div class="col-md-9 has-icon-left">
                                  <input type="text" id="zipcode" class="form-control" placeholder="Enter Amount" name="zipcode" required="" data-fv-field="zipcode"><i class="form-control-feedback" data-fv-icon-for="zipcode" style="display: none;"></i>
                                  <div class="form-control-position">
                                      <i class="fa fa-street-view"></i>
                                  </div>
                                <small class="help-block" data-fv-validator="notEmpty" data-fv-for="zipcode" data-fv-result="NOT_VALIDATED" style="display:none;">This value is not valid</small></div>
                              </div>


                            </div><!-- ENDS FORM GROUP -->
                            <div class="form-actions right no-margin">
                                <button type="submit" class="btn btn-primary pr-3 pl-3"><i class="icon-plus"></i> Create</button>
                            </div>
                            <input type="hidden" id="lat" value="" name="lat">
                            <input type="hidden" id="lng" value="" name="lng">
                        </form>
                    </div>
                </div>
            </div>





           
        </div>
       
        
    </div>
</div>

 
   
@include('shared/footer')

