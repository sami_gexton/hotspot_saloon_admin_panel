
@include('shared/header')
@section('Content') 
 <style>
 .proimg{
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin-left: 45%;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 } 
 /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.success {
    color: #A2D200;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    display: none;
    padding: 15px;
    border-radius: 5px;
}
form.iconSize i{
    font-size: 18px;
    /*vertical-align: super;*/
}

#select2-country-container
{

padding-left: 35px;
}
.help-block
{
  color: red;
}
#select2-usertype-container
{
 padding-left: 35px;
}

 /*sasdsa*/
 </style>
<link  href="https://rawgit.com/fengyuanchen/cropperjs/master/dist/cropper.css" rel="stylesheet">
<script src="{{asset('public/js/cropper.js')}}"></script>
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
    <input type="hidden" id="imgprofile" value="{{asset('public/assets/images/placeholder.png')}}">
@include('shared/navbar')
   <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
        <div class="card">
            <div class="card-header mb-3">
                <h4 class="card-title">Add User/Studio/Freelancer</h4>
            </div>
            <div class="card-body collapse in">
                <div class="proimg"></div>
                <div class="imgappend" style="display:none"></div>  
                <div class="card-block card-dashboard" id="formContainer">
                <form class="form form-horizontal row-separator" id="addstudio"   data-fv-framework="bootstrap"
                data-fv-message="This value is not valid"
                data-fv-feedbackicons-valid="glyphicon glyphicon-ok"
                data-fv-feedbackicons-invalid="glyphicon glyphicon-remove"
                data-fv-feedbackicons-validating="glyphicon glyphicon-refresh" enctype="multipart/form-data" method="POST">
                    <input type="hidden" name="token" value="<?php echo session()->get('token'); ?>">
                      <div class="form-body">
                          <div class="form-group row">
                            <label class="col-md-2 label-control">Upload Logo <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <label id="uploadimg2" class="file center-block">
                                  <div class="fileUpload btn btn-primary" style="margin:0;">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input type="file" class="upload" id="uploadimg2" name="profile_img" accept=".png, .jpg, .jpeg" />
                                    <input type="hidden"  id="service_image_blob" value="">
                                  </div>
                              </label>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="studio">Studio Name <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <input type="hidden" name="usertype" value="studio_user">
                              <input type="text" id="studio" class="form-control" placeholder="Studio Name" name="Studio_Name" required data-fv-message="The Studio Name is not valid"
                              required data-fv-notempty-message="The Studio Name is required and cannot be empty"
                              pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Studio Name can only consist of alphabetical, number" >
                              <div class="form-control-position">
                                  <i class="fa fa-building"></i>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="username">User Name <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                            <span id="usererror" style="color:red;display:none">User Name Already Present </span>
                              <input type="text" id="username" class="form-control js-input" placeholder="User Name" name="username" required data-fv-message="The User Name is not valid"
                          required data-fv-notempty-message="The  User Name is required and cannot be empty"
                          pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The  User Name can only consist of alphabetical, number">
                              <div class="form-control-position">
                                  <i class="fa fa-user"></i>
                              </div>
                            </div>
                          </div>

                           <div class="form-group row">
                            <label class="col-md-2 label-control" for="UserType">User Add As <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                            <select id="usertype" name="usertype"  class="form-control js-example-basic-single"  style="width:100%">
                            <option disabled selected value>Select User Type</option>
                            <!-- <select id="usertype" name="usertype"  class="form-control js-example-basic-single"  style="width: 100%">
                            <option disabled selected value>Select User Type</option> -->
                            <option value="studio_user">
                             Studio  
                            </option>
                            <option value="normal_user">
                             User
                            </option>
                            <option value="freelancer">
                            Free Lancer
                            </option>
                          
                            </select>
                              <div class="form-control-position">
                                  <i class="fa fa-user"></i>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="user-email">Email <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <span id="emailerror" style="color:red;display:none">Email Already Present </span>
                              <input type="email" id="user-email" class="form-control" placeholder="Email Address" name="email" required>
                              <div class="form-control-position">
                                  <i class="fa fa-envelope"></i>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="user-Contact">Contact No <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <input type="number" id="user-Contact" class="form-control" placeholder="Contact No" name="contact_no" required>
                              <div class="form-control-position">
                                  <i class="fa fa-phone"></i>
                              </div>
                            </div>
                          </div>

                          <!-- <div class="form-group row">
                            <label class="col-md-2 label-control" for="Address">Address <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <input type="text" id="Address" class="form-control" placeholder="Address" name="Address" required>
                              <div class="form-control-position">
                                  <i class="fa fa-location-arrow"></i>
                              </div>
                            </div>
                          </div> -->
                        
                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="country">Country <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                                <select id="country" name="country"  class="form-control js-example-basic-single"  style="width: 100%">
                                    <option disabled selected value>Select Country</option>
                                    <?php
                                      $keys = array_keys($countries);
                                      for($i=0; $i < count($keys); ++$i) {?>
                                        <option value="<?= $countries[$keys[$i]]; ?>"><?= $countries[$keys[$i]] ;?></option>
                                    <?php } ?>
                                </select>
                                <div class="form-control-position">
                                    <i class="fa fa-globe"></i>
                                </div>
                              </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="timezone">timezone <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                                <select id="country" name="timezone"  class="form-control js-example-basic-single"  style="width: 100%" required>
                                    <option disabled selected value>Select timezone</option>
                                    <?php
                                    foreach(timezone_abbreviations_list() as $abbr => $timezone){
                                      foreach($timezone as $val){
                                      if(isset($val['timezone_id'])){ ?>
                                          
                                           <option value="<?= $val['timezone_id']; ?>"><?= $val['timezone_id'] ;?></option>
                                     <?php }
                                    }
                                    } ?>
                                    
                                </select>
                                <div class="form-control-position">
                                    <i class="fa fa-globe"></i>
                                </div>
                              </div>
                          </div>

                
          
            
                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="zipcode">Zip Code <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <input type="number" id="zipcode" class="form-control" placeholder="Zipcode" name="zipcode" required>
                              <div class="form-control-position">
                                  <i class="fa fa-street-view"></i>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="searchTextField">
                            Address <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <input type="text" size="50" id="searchTextField" class="form-control" placeholder="Enter Studio Location" name="Address" required>
                              <div class="form-control-position">
                                  <i class="fa fa-map-marker"></i>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="password">Password <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">
                              <input type="password" size="50" id="password" class="form-control" placeholder="Enter Password" name="password"  pattern=".{8}" required title="8 characters">
                              <div class="form-control-position">
                                  <i class="fa fa-asterisk"></i>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 label-control" for="password">Confirm Password <span class="text-red">*</span></label>
                            <div class="col-md-9 has-icon-left">

                              <input type="password" size="50" id="confirm_password" class="form-control" placeholder="Retype Password"  pattern=".{8}" required title="8 characters" name="confirmPassword" />
                              <div class="form-control-position">
                                  <i class="fa fa-asterisk"></i>
                              </div>
                            </div>
                          </div>

                          <!-- <div class="form-group row last">
                            <label class="col-md-2 label-control" for="status">Status<span class="text-red">*</span></label>
                            <div class="col-md-9">
                              <label class="switch"> 
                                <input type="hidden" name="user_status" value="" id="user_status">
                                <input type="checkbox">
                                <div class="slider round"></div>
                              </label>
                            </div>
                          </div> -->

                        </div><!-- ENDS FORM GROUP -->
                        <div class="form-actions right no-margin">
                           
                          <!--  <div class="success">Add Successfully</div> -->
                         
                        
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-plus"></i> Add Studio
                            </button>
                        </div>
                        <input type="hidden" id="lat" value="" name="lat">
                        <input type="hidden" id="lng" value="" name="lng">
                    </form>
                </div>
            </div>
        </div><!-- Recent invoice with Statistics -->
        </div>
      </div>
</div>
<button type="button" id="service_modal_open" class="btn btn-primary" data-target="#service_modal" data-toggle="modal" style="display:none">
                            </button>
<div class="modal fade" id="service_modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                                    <!-- <input type="file" onchange="readURL(this);"/> -->
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                                <div class="modal-body">
                                <div id="service_divimage1">
                                    <div id="service_divimage">
                                    <img id="serviceimg"   src=""  alt="Picture" height="500px" width="568px">
                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="crop_button" data-dismiss="modal" class="btn btn-default" >Crop</button>
                                    <!-- <button >Crop</button> -->
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
