@include('shared/header')

<style type="text/css">

body .chatbox .timeline li:nth-child(odd){
float: left !important;
}

#timeline.timeline-center .timeline-item:nth-child(odd):after{

border-right: 12px solid #fff !important;
}

.timeline-badge
{
   
    background-size: 100% 100%;
    display: block;
    height: 40px;
    width: 40px;
    border-radius: 50%;
    border: 1px solid #1d2b36;


}
</style>
@section('Content') 
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
<!-- navbar-fixed-top-->
<input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
<input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">

@include('shared/navbar')

<div class="app-content content container-fluid">
	<div class="content-wrapper">
	<div class="content-header row"></div>
		<div class="content-body"><!-- stats -->
			<div class="card">
			@include('shared/nevigationdetails')
			</div><!-- Recent invoice with Statistics -->
			<?php if(count($result)>0) { ?>
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Reviews</h4>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-Wrap">
						<?php if($result[0]['username']!="") { ?>
						<div class="row">
							<?php for($i=0;$i<count($result);$i++) { ?>
							<div class="card bg-grey bg-lighten-5">
							    <div class="card-header bg-grey bg-lighten-3">
							    	<div class="media-left pull-left userThumb">
						                <img class="media-object avatar avatar-md rounded-circle" src="<?php echo asset($result[$i]['coverpic']) ?>" alt="Auth">
						            </div>
						            <div class="media-body">
						                <p class="text-bold-600 m-0"><?php echo $result[$i]['username'] ?></p>
						                <p class="text-muted m-0">Marketing Manager</p>
						            </div>
						            <div class="heading-elements">
						            	<p class="text-muted mb-0"><span class="font-small-3"><?php echo $result[$i]['date'] ?></span></p>
						            	<p class=" text-xs-right text-muted mb-0">
						            		<span class="font-small-3">
							            		<?php
								            		for($j=0;$j<5;$j++){
													if($j<$result[$i]['ratings']){
													echo "<i class=\"fa fa-star\"></i>";            
													} else {
													echo "<i class=\"fa fa-star-o\"></i>";            
													}
													}
												?>
						            		</span>
						            	</p>
						           </div>
							    </div>
							    <div class="mt-1">
							        <div class="card-text">
							            <section class="cd-horizontal-timeline m-0 loaded">
							                <!-- .timeline -->
							                <div class="events-content">
							                    <ol class="list-unstyled">
							                        <li class="selected rev-wrap">
							                            <p><?php echo $result[$i]['comments']  ?></p>
							                        </li>
							                    </ol>
							                </div>
							                <!-- .events-content -->
							            </section>
							        </div>
							    </div>
							</div> <?php } ?>
						</div> <?php  }  ?>
					</div>
				</div><!-- Recent invoice with Statistics -->
			</div> <?php } ?>
		</div>
	</div>
</div>
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
  


