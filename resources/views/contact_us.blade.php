@include('shared/header')
@section('Content') 
<style type="text/css">
.help-block
{

 color: red;

}
table.dataTable {
width: 100%;
}
button.close {
    opacity: 1 !important;

}
/**table{*/
    /* margin: 0 auto;
     width: 100%;
     clear: both;
     border-collapse: collapse;
     table-layout: fixed;
     word-wrap:break-word;*/
/*}*/
</style> 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
         <div class="card">
<div class="card-header">
<h4 class="card-title">Contact Details </h4>
<div class="heading-elements" style="top: 9px;">
<!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModalpackage">
<span class="ladda-label">Create New</span>
</button> -->
</div>
</div>
<div class="card-body collapse in">
<div class="card-block card-dashboard">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap4">
<div class="row">
<div class="col-md-12">

    <table id="packages" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
        <thead>
            <tr role="row">
                <th>Date</th>
                <th>Name</th>
                <th>Email</th>
                <th>Subject</th>
                <th>Info</th>
            </tr>
        </thead>
        <tbody>

            <?php
        
            // exit();
            for($i=0;$i<count($data);$i++)
            { 
            ?>
            <tr role="row" class="odd">
                <td style="width:auto;"> <?php echo date('M d, Y',strtotime($data[$i]->created_date)) ?></td>
                <td><?php echo $data[$i]->Name ?></td>
                <td><?php 
                 
                  echo $data[$i]->Email; 
                 
                  ?></td>

                <td><?php echo $data[$i]->Subject; ?> 
                </td>
                <td>
                <a   class="btn btn-info btn-sm contact_detail" aria-hidden='true' data-toggle="modal" href="#contact_detail" rowid="<?php echo $data[$i]->id ?>"  data="<?php echo htmlspecialchars(json_encode($data[$i]), ENT_QUOTES) ?>"> <i class='fa fa-eye' ></i></a>
                </td>
            </tr>

        <?php  }  ?>
           
        </tbody>                
    </table>
</div>
</div>
</div>              
</div>
</div>
</div>  


           
        </div>
       
        
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Details</h4>
        </div>
        <div class="modal-body">
        <table class="table table-bordered table-striped" style="background-color: transparent;">
                  
                  <tbody id="table_info">
                    
                  </tbody>
                </table>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
</div>

@include('shared/footer')

<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">

<script>
$(document).on('click','.contact_detail',function(){
  $('#myModal').modal('toggle');
  console.log(JSON.parse($(this).attr('data')));
  data = JSON.parse($(this).attr('data'));
  $('#table_info').html(' ');
  html=`<tr>
            <td>Name   </td>
            <td> ${data.Name} </td>
            </tr>
            <tr>
            <td>Email   </td>
            <td>${data.Email}</td>
            </tr>
            <tr>
            <td>Subject </td>
            <td>${data.Subject}</td>
            </tr>
            <tr>
            <td>Message </td>
            <td> ${data.Message}</td>
        </tr>`;
    $('#table_info').html(html);
})

</script>