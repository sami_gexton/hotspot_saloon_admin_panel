@include('shared/header')
<style type="text/css">
 .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 08px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}  

button.close {
    opacity: 1 !important;

}
 .proimg{
  background-image: url("{{asset('public/assets/uploads/placeholder.png')}}"); 
  background-repeat: no-repeat;
  background-position: 50%;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  margin: auto;
  border-style: solid;
  border-color:gray;
  background-repeat: no-repeat; 
  background-size: 100% 100%;
 }

.ribbon{
    background: #3ac627;
    padding: 5px 10px;
    color: #fff;
    display: inline-block;
    margin: 0px auto;
    position: absolute;
    z-index: 9;
    line-height: 0.9;
}

/*a:hover{
  color:white;
  text-decoration:none;
}*/
</style>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.css" rel="stylesheet" type="text/css"/>
<link href=" https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/css/mdtimepicker.css')}}" rel="stylesheet"/>

@section('Content') 
 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="customBody vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <input type="hidden" id="usertoken" value="<?php echo session()->get('token'); ?>">
    <input type="hidden" id="userid" value="<?php echo session()->get('userdetails')->id ?>">
  
@include('shared/navbar')
<div class="imgappend" style="display:none"></div>  
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body"><!-- stats -->
            <div class="card">
                @include('shared/nevigationdetails')
            </div><!-- Recent invoice with Statistics -->
           
            <div id="scroll-types" class="card" style="overflow:hidden;">
                <div class="card-header mb-1">
                    <h4 class="card-title">Services</h4>
                <div class="heading-elements">
                <span data-toggle="modal" btnupdate="Add">
                <a class="rounded-circle add-studio-class" data-toggle="tooltip" data-placement="top" title="Add Services" href="<?php echo url('/AddStudioservice').'/'.session()->get('studioid')?>"><i class="icon-plus-round"></i></a>
                </span>
                </div>  
                </div>
                <?php if(count($data)>0) 
                { ?> 
                <div class="card-body collapse in">
                <div class="col-md-12 col-sm-12">
                <div class="bgcoll m-0">
                <div class="media card-body collapse in">
                  <?php 
                      for($i=0;$i<count($data);$i++)
                      {   
                   ?>          
                    <div class="col-xl-4 col-md-6 col-xs-12">
                    <?php if($data[$i]['discount_amount']>0) {   ?> 
                    <div class="ribbon"><i class="fa fa-clock-o"></i> Happy Hours </div>
                    <?php } ?>
                    <div class="card profile-card-with-stats box-shadow-1 service-card-box">
                        <div class="text-xs-center minHeight">
                            <div class="card-block icon_box">
                                <span class="float-xs-left mr-1 icon icon-circled icon-border-effect effect-circle icon-xl">
                                    <div class="img-responsive" style="height: 100px;width: 100px;background-size: 100% 100%;background-image:url(<?php  echo asset($data[$i]['serviceimage'])?>)">
                                    </div>
                                </span>
                                <h4 class="card-title text-xs-left mt-0 "><?php  echo $data[$i]['categoryName']; ?></h4>
                                <h6 class="card-subtitle text-muted"  style="font-style: italic;text-align:left; "> <?php echo $data[$i]['service_title']?></h6>
                                <h4 class="card-title text-xs-left danger mt-1 font-large-2 text-bold-700" style="min-height: 114px;"><?php echo $data[$i]['currency_code']?>  <?php echo $data[$i]['Service_charges']?></h4>
                            </div>

                            <div class="card-block text-xs-left line-height-1 vertical-scroll scroll-example no-border">
                                <p><?php echo $data[$i]['Description']?></p>
                               
                            </div>
                        </div>
                        <div class="editDeleteBtn">
                            <a href="<?php echo url('/Editservice').'/'.session()->get('studioid').'/'. $data[$i]['id']?>" class="btn bg-white togglebutton"  btnupdate="Update" serviceid="<?php echo $data[$i]['id'] ?>"><i class="fa fa-pencil info"></i> Edit</a>
                            <a class="btn bg-white btn-xs deleteservice" title="" aria-hidden="true" href="#" rowid="<?php echo $data[$i]['id'] ?>" ><i class="fa fa-trash-o danger"></i> Delete</a>
                        </div>
                    </div>
                  </div>       
                              
                        <?php } ?>
                        </div>
                      </div>
                          </div>
                    </div>    
                </div>
                <?php } ?>

            <div class="modal fade" id="addservices" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close closestaffbtn" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="headings"></h4>
            </div>
            <div class="modal-body">


            <form  id="formservice" enctype="multipart/form-data" method="POST">

            <div class="proimg"></div>

            <input type="hidden" name="Studio_id" value="<?php echo session()->get('studioid') ?>">
            <input type="hidden" name="setserviceid" value="" id="setserviceid">

            <label id="serviceimg" class="file center-block">
            <br>
            <div class="fileUpload btn btn-primary" style="margin:0;left:235px">
            <span><i class="fa fa-upload"></i> Upload</span>
            <input type="file" class="upload" id="serviceimg" name="services_image" accept=".png, .jpg, .jpeg"/>
            </div>
            </label>
            <br>
            <br>
            <select id="selectservice" name="Category_id" class="form-control js-example-basic-single"  style="width: 100%" required>
            <option disabled selected value>Select Service</option>
            <?php 
            foreach ($video_category as $key => $value) {

            ?>
            <option value="<?php echo $value->id ?>"><?php echo $value->category_name ?></option>

            <?php }  ?>


            </select>
            <br>
            <br>       
            <select id="gender" name="gender"  class="form-control js-example-basic-single"  data-fv-message="The user type is not valid"
            required data-fv-notempty-message="The user type is required and cannot be empty">
            <option disabled selected value>Service For</option>
            <option value="Male"> Male  </option>
            <option value="Female"> Female  </option>
            <option value="Kids">  Kids</option>

            </select>
            <br>
            <br>

            <div class="form-group row">
            <div class="col-md-6">

            <input type="number"  style="line-height: 24px;" id="duration" class="form-control"   name="service_time"  required  
            data-fv-message="The Time is not valid" data-fv-notempty-message="The Time  is required and cannot be empty"
                  pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Time can only consist of time like '3 min' , 4 hour" value="0">

            </div>
            <div class="col-md-6">


            <select id="time" name="Timeunit" class="form-control js-example-basic-single">
            <option value="Hours">Hours</option>

            <option value="Minutes">  Minutes</option>

            </select>
            <!-- <input id="time" name="timeunit"  class="form-control" value="Min"  required readonly> -->


            </div>
            </div>
            <div class="form-group row">
            <div class="col-md-6">
            <!-- <select id="selectservicetype" name="SubCategory_id" class="form-control js-example-basic-single"  style="width: 100%" required disabled>
            <option  selected value readonly>Select Service Type</option>

            </select> -->
            <input type="text" name="service_title" class="form-control"  style="width: 100%" required placeholder="Service Title" id="servicetitle">

            </div>
            <div class="col-md-6">

            <input type="text"  id="Amount" class="form-control"   name="Service_charges" required name="time_limit" required  
            data-fv-message="The Time is not valid" data-fv-notempty-message="The Time  is required and cannot be empty"
            pattern="^[a-zA-Z0-9_ ]*$" data-fv-regexp-message="The Time can only consist of time like '3 min' , 4 hour" placeholder="Enter Amount"> 
            </div>
            </div>
            <br>
            <div class="form-group row">
            <div class="col-md-12">
            <textarea rows="6" placeholder="Description" name="Description" class="form-control" id="Description" required></textarea>
            </div>
            </div>

            <div class="col-md-4 mb-15-resp">
            <h4> Happy Hours </h4>
            </div>
            <div class="col-md-4 mb-15-resp">
            <input type="checkbox" class="js-switch" name='switch'/>
            </div>
            <br>
            <div class="half mb-15 formRow" id="is_happy_hour" style="display:none">
                                  
                <input type="text" class="form-control radius-50 enable basicExample" name="happy_from" placeholder="from" style="background-color:white !important;" readonly/>
                
            
                <input type="text" class="form-control radius-50 enable basicExample" name="happy_to" placeholder="To" style="background-color:white !important;" readonly/>
            
                <span style="position:absolute;right:14%;font-size: 18px;bottom: 24%;color:red">  </span>
                <input type="number" class="form-control radius-50 enable" name="price_discount" placeholder="Discount Price" style="background-color:white !important;" min="1"  readonly/>

                <h4><p style="text-align:center">  Happy Hour Days </h4>
                                    <div class="pretty p-svg p-curve" style="margin-left:42%;margin-bottom: 20px;">
                                        <input type="checkbox" id="check_all"/>
                                            <div class="state p-success">
                                            <!-- svg path -->
                                                <svg class="svg svg-icon" viewBox="0 0 20 20">
                                                <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path>
                                                </svg>
                                            <label>Every Day</label>
                                        </div>
                                    </div>
                                <br>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Saturday"/>
                                            <div class="state p-primary-o">
                                            <label>Saturday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Sunday"/>
                                            <div class="state p-primary-o">
                                            <label>Sunday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Monday"/>
                                            <div class="state p-primary-o">
                                            <label>Monday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Tuesday"/>
                                            <div class="state p-primary-o">
                                            <label>Tuesday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Wednesday"/>
                                            <div class="state p-primary-o">
                                            <label>Wednesday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Thursday"/>
                                            <div class="state p-primary-o">
                                            <label>Thursday</label>
                                        </div>
                                    </div>
                                    <div class="pretty p-default">
                                        <input type="checkbox" name="happy_days[]" class="days_name" value="Friday"/>
                                            <div class="state p-primary-o">
                                            <label>Friday</label>
                                        </div>
            
         </div>

            <div class="pull-right">
            <button class="btn btn-primary" id="formsubmit"></button>
            </div>


            </form>
            <br>
            <br>
            </div>

            </div>

            </div>
            </div>
            </div>
        </div>
    </div>
</div>


    <!-- ////////////////////////////////////////////////////////////////////////////-->


   <!--  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer> 
     -->
   
@include('shared/footer')
<script src="{{asset('public/js/admin/admin.js')}}" type="text/javascript"></script>
<script src="{{asset('public/js/mdtimepicker.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.js"></script>
<script type="text/javascript">
   setTimeout(function(){
          $('[data-toggle="tooltip"]').tooltip();
          },1000);
        var elem = document.querySelector('.js-switch');
        var switchery  = new Switchery(elem);

        $(document).on('change','.js-switch',function(e){
        if(this.checked){
        $('.enable').removeAttr('readonly');
        $('#is_happy_hour').show();
        $('#days_display').show();

        } else {
        $('.enable').attr('readonly','readonly');
        $('#is_happy_hour').hide();
        $('#days_display').hide();
        }
        });
        $('.basicExample').mdtimepicker({
        // format of the time value (data-time attribute)
        timeFormat: 'hh:mm:ss.000',
        // format of the input value
        format: 'h:mm tt',     
        // theme of the timepicker
        // 'red', 'purple', 'indigo', 'teal', 'green'
        theme: 'red',       
        // determines if input is readonly
        readOnly: true,      
        // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
        hourPadding: false    
        });        
</script>

