<?php
namespace App;
use Aws\CloudFront\UrlSigner as Awsserver;
use DateTime;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Storage;
use \Mailjet\Resources;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;


class BasicModel extends Model
{

    public $s3_baseurl = "https://hotspot-videos-bucket.s3-us-west-2.amazonaws.com/";
    public $lng         = '';
    // public $local_baseurl = asset('');

    public function __construct()
    {
        $allheader=getallheaders();
        // print_r($allheader);
        // exit;
       if(isset($allheader['lng'])){
            if($allheader['lng']!='en' && !empty($allheader['lng'])){
                $this->lng='_de';
            }
       }
        

       
    }
    public function getVideos($params, $whereRaw, $single = false, $limit = 0,$offset = 0)
    {
        $dataarray = array();
        $select    = array(
            'videos.id as videoid',
            'videos.studio_id as studioid',
            'videos.title',
            'videos.discribtion',
            'videos.is_pro',
            "video_category.category_name$this->lng as category",
            'videos.Video_category_id as Video_category_id',
            'videos.likes',
            'users.Studio_Name as StudioName',
            'users.Average_ratings as Averageratings',
            DB::raw('IF(users.cover_pic IS NOT NULL, CONCAT("' . asset('') . '",users.cover_pic), "") AS cover_pic'),
            DB::raw('IF(users.cover_pic_big IS NOT NULL, CONCAT("' . asset('') . '",users.cover_pic_big), "") AS cover_pic_big'),
            'users.user_type as user_type',
            'videos.views',
            'videos.date',
            'videos.tags',
            'video_category.id as videocategoryid',
            'video_category.status',
            'video_category.slug',
            'videos.is_visible',
            // DB::raw('IF(videos.Video_link IS NOT NULL ,CONCAT("'.$this->s3_baseurl.'",videos.Video_link), "") AS Video_link'),
            'videos.Video_link AS Video_link',
            DB::raw('IF(videos.thumbnail != "", CONCAT("' . $this->s3_baseurl . '",videos.thumbnail), "") AS thumbnail'),
            DB::raw('IF(users.profile_pic IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic), "") AS profilepic'),
            DB::raw('IF(users.profile_pic_big IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic_big), "") AS profile_pic_big'),
            DB::raw('COUNT(studio_subscribers.`subscribers_id`) AS studio_subscribers'),
            'videos.duration',
        );

        // DB::enableQueryLog();

        $data = DB::table("videos")->select($select)->leftjoin('users', 'users.id', '=', 'videos.studio_id')->leftjoin('video_category', 'videos.Video_category_id', '=', 'video_category.id')->leftjoin('studio_subscribers', 'studio_subscribers.studio_id', '=', 'videos.studio_id')->whereRaw($whereRaw)->groupby('videos.id')->orderBy('videos.id', 'desc');

        if ($single) {
            $data = $data->first();
        } else {
            // ->offset($offset)->limit($limit);
            if ($limit > 0) {
                $data = $data->skip($offset)->limit($limit)->get();
            } else {
                $data = $data->get();
            }
        }

        // dd(DB::getQueryLog());exit;

        return $this->removeNull($data);
    }

    public function removeNull($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $a => $b) {
                        if ($b == null || $b == "NULL" || $b == "null" || $b == null) {
                            $arr[$key][$a] = "";
                        }
                    }
                } else {
                    if ($value == null || $value == "NULL" || $value == "null" || $value == null) {
                        $arr[$key] = "";
                    }
                }
            }
        } else {
            if ($arr == null || $arr == "NULL" || $arr == "null" || $arr == null) {
                $arr = "";
            }
        }
        return $arr;
    }

    public static function time_elapsed_string($datetime, $full = false)
    {
        $now  = new DateTime;
        $ago  = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function sendEmail($view, $data = array(), $to = array(), $subject, $from = null, $attachment = null)
    {
        // $mj = Mailjet::getClient();
        $mj = new \Mailjet\Client(getenv('MAILJET_APIKEY'), getenv('MAILJET_APISECRET'),true,['version' => 'v3.1']);
        // if ($from === null) {
        //     $from['name']  = "Hotspot 4 Beauty";
        //     $from['email'] = "noreply@hotspot4beauty.com";
        // }
        $sending_data['to']      = $to['email'];
        $sending_data['subject'] = $subject;
        // $view = View::make($view);
        // $contents = (string) $view;
        
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "info@p-b-v.ch",
                        'Name' => "hotspot4beauty.com"
                    ],
                    'To' => [
                        [
                            'Email' => $sending_data['to'],
                            'Name' => ""
                        ]
                    ],
                    'Subject' => $sending_data['subject'],
                    'TextPart' => "",
                    'HTMLPart' => $view,
                ]
            ]
        ];
        $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);
        if($response->success()){ 
          return 1;
        }
        return 0;
     
        // $sending_data['to']      = $to ? (object) $to : null;
        // $sending_data['from']    = $from ? (object) $from : null;
        // $sending_data['subject'] = $subject;

        // Mail::send($view, $data, function ($message) use ($sending_data) {
        //     $message->from($sending_data['from']->email, $sending_data['from']->name);

        //     $message->to($sending_data['to']->email, $sending_data['to']->name)->subject($sending_data['subject']);
        //     // $message->priority(1);
        //     // $message->attach($pathToFile, array $options = []);
        //      $message->getSwiftMessage();
        // });
        // if (Mail::failures()) {
        //     return 0;
        // }
        // return 1;
    }

    public static function checkParams($array = null)
    {
        if (!$array) {
            echo json_encode(array("status" => "error", "error" => "Parameters Required"));
            exit;
        } else {
            $check      = false;
            $param_name = "";
            foreach ($array as $key => $value) {
                if (!array_key_exists($value, $_REQUEST) && trim(@$_REQUEST[$value]) == "") {
                    $param_name = $value;
                    $check      = true;
                }
            }
            if ($check) {
                echo json_encode(array("status" => "error", "error" => $param_name . " parameter is missing OR missing value"));
                exit;
            }
        }
    }

    public static function InsertData($tablename, $array)
    {

        $id = DB::table($tablename)->insertGetId($array);
        return $id;

    }

    public static function DeleteData($tablename, $id, $columname)
    {

        $id = DB::table($tablename)->where($columname, $id)->delete();
        return $id;

    }
    public static function UpdateData($tablename, $id, $columname, $array)
    {

        $id = DB::table($tablename)->where($columname, $id)->update($array);
        return $id;

    }

    public static function insertRecord($arr, $tablename)
    {

        $arr_length = count($arr);
        if ($arr_length > 0) {
            $id = DB::table($tablename)->insertGetId(
                $arr
            );
            return $id;
        }
    }
    public static function getRows($table_name, $column, $criteria, $order = "ASC", $multiple = false)
    {
        if ($multiple) {
            $query = "select  * from  `" . $table_name . "` where `" . $column . "`  IN   (" . implode(",", $criteria) . ") ORDER BY  `id` " . $order;

            $result = DB::select(DB::raw($query));
            return $result;

        } else {
            $query  = "select  * from  `" . $table_name . "` where `" . $column . "`= :column ORDER BY `id` " . $order;
            $result = DB::select(DB::raw($query), array("column" => $criteria));
            if (is_array($result) && count($result) > 0) {
                return $result[0];
            }
        }

    }
    public static function fileupload($file, $path, $fileName, $saveas)
    {

        $f = Storage::disk('s3')->putFileAs($path, $file, $fileName, $saveas);
        if ($f) {
            return Storage::disk('s3')->url($path . $fileName);
        }

    }

    public static function signedUrl($filepath)
    {
        // $is_local = env('IS_LOCAL');
        // if ($is_local = 'true') {
        //     $key = './s3private_key.pem';
            
        // } else {
            $key = '/var/www/html/s3private_key.pem';
        // }
        // $key = '/var/www/html/s3private_key.pem';
        $urlsigned = new Awsserver('APKAIZZMGFU6MIPDQR3Q', $key);
        $expiry    = new DateTime("+300 minutes");
        $signedUrl = $urlsigned->getSignedUrl('https://d88uc4h2u8uwv.cloudfront.net/' . $filepath, $expiry->getTimeStamp());
        return $signedUrl;

    }

    public static function api_logs($request)
    {
        $headerarray=$_SERVER['REMOTE_ADDR'];
        $allheader=getallheaders(); 
        if($request->header('platform')=='web'){
            $headerarray=$allheader['client_ip'];
        }
        if ($request->path()!='get_notifications') {
            $dataarray = ['ip_address' =>$headerarray,'data' =>$request,'method_name'=>$request->path(),'platform'=>$request->header('platform')];
            $result    = BasicModel::InsertData('api_logs', $dataarray);
            
        }
    }

    public static function mulity_currency_with_rate()
    {
        $allheader=getallheaders();
        $headerarray=$_SERVER['REMOTE_ADDR'];
        if($allheader['platform']=='web'){
            $headerarray=$allheader['client_ip'];
        }
        $geoPlugin_array = file_get_contents('http://www.geoplugin.net/json.gp?ip='.$headerarray);
        $geoPlugin_array = json_decode($geoPlugin_array,true);
        $to_Currency=DB::table('currency_code_new')->select('CurrencyCode')->where('CountryCode', $geoPlugin_array['geoplugin_countryCode'])->first();
        $currencyCode="";  
        if(!empty($to_Currency->CurrencyCode)){
            $currencyCode = $to_Currency->CurrencyCode;
        } else {
            $currencyCode='USD';

        }
        // $converted_currency='';
        // $get = file_get_contents("https://finance.google.com/finance/converter?a=1&from=USD&to=$currencyCode");
        // $get = explode("<span class=bld>",$get);
        // if(isset($get[1])){
        //   $get = explode("</span>",$get[1]);
        //   $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
        // }
        $converted_currency = 1;
        $result=DB::table('currency_rate')->select('Rate')->where('currency_code', $currencyCode)->first();
        if(count($result)>0){
            $converted_currency = $result->Rate;
        }
        $data        = new \stdClass();
        $data->coverted_currency=$converted_currency;
        $data->currencyCode=$currencyCode;
        return $data;

    }

    public static function pushNotification($title, $body, $deviceToken, $type = 'video', $push_data = array())
    {
        $key = "AAAA5HTfB6k:APA91bGnv4o034DAB6LRCDf6MIMCDJ1Iauv0QBiG2XajLf9vk-n32lyqhKXT2nl4DctCkTcW2IaWvMAp1Q_qTuoXC9O2AengfuGKe1T6PnZgO8L3UhXmEZj8fOdZ6hpoiuC_iGvhsz1S";
        
        $ch  = curl_init("http://fcm.googleapis.com/fcm/send");

        //The device token.
        $token = $deviceToken; //token here is an array

        //Creating the notification array.
        $notification = array('title' => $title, 'text' => $body, 'type' => $type, 'push_data' => $push_data);
        //This array contains, the token and the notification. The 'to' attribute stores the token.
        $arrayToSend = array('registration_ids' => $token, 'notification' => $notification, 'priority' => 'high');
         
        // $allheader=getallheaders();
        // if($allheader['platform']=='android'){
        //     $arrayToSend = array('registration_ids' => $token, 'data' => $notification, 'priority' => 'high');
        // }

        //Generating JSON encoded string form the above array.
        $json = json_encode($arrayToSend);
        //Setup headers:
        $headers   = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key= ' . $key; // key here

        //Setup curl, add headers and post parameters.
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Send the request
        $response = curl_exec($ch);
        $err      = curl_error($ch);

        //Close request
        curl_close($ch);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }

    public function currencyConverter($from_Currency,$to_Currency,$amount) {
        // $from_Currency = urlencode($from_Currency);
        // $to_Currency = urlencode($to_Currency);
        // $encode_amount = 1;
        $get = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
        $get = explode("<span class=bld>",$get);
        if(isset($get[1])){
          $get = explode("</span>",$get[1]);
          $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
          return $converted_currency;
        }
    }

    public static function get_currencies_symbol($currency_code) {
        $currencies =  array(
            'USD' => array('name'=>'USD - U.S. Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'GBP' => array('name'=>'GBP - British Pounds', 'symbol'=>'£','symbol_html'=>'&pound;'),
            'EUR' => array('name'=>'EUR - Euros', 'symbol'=>'€','symbol_html'=>'&euro;'),
            'AUD' => array('name'=>'AUD - Australian Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'BRL' => array('name'=>'BRL - Brazilian Real', 'symbol'=>'R$','symbol_html'=>'R$'),
            'CAD' => array('name'=>'CAD - Canadian Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'CZK' => array('name'=>'CZK - Czech koruny', 'symbol'=>'Kč','symbol_html'=>''),
            'DKK' => array('name'=>'DKK - Danish Kroner', 'symbol'=>'kr','symbol_html'=>'kr'),
            'HKD' => array('name'=>'HKD - Hong Kong Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'HUF' => array('name'=>'HUF - Hungarian Forints', 'symbol'=>'Ft','symbol_html'=>'Ft'),
            'ILS' => array('name'=>'ILS - Israeli Shekels', 'symbol'=>'₪','symbol_html'=>'&#8362;'),
            'JPY' => array('name'=>'JPY - Japanese Yen', 'symbol'=>'¥','symbol_html'=>'&#165;'),
            'MYR' => array('name'=>'MYR - Malaysian Ringgits', 'symbol'=>'RM','symbol_html'=>'RM'),
            'MXN' => array('name'=>'MXN - Mexican Pesos', 'symbol'=>'$','symbol_html'=>'$'),
            'NZD' => array('name'=>'NZD - New Zealand Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'NOK' => array('name'=>'NOK - Norwegian Kroner', 'symbol'=>'kr','symbol_html'=>'kr'),
            'PHP' => array('name'=>'PHP - Philippine Pesos', 'symbol'=>'Php','symbol_html'=>'Php'),
            'PLN' => array('name'=>'PLN - Polish zloty', 'symbol'=>'zł','symbol_html'=>''),
            'SGD' => array('name'=>'SGD - Singapore Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'SEK' => array('name'=>'SEK - Swedish Kronor', 'symbol'=>'kr','symbol_html'=>'kr'),
            'CHF' => array('name'=>'CHF - Swiss Francs', 'symbol'=>'CHF','symbol_html'=>'CHF'),
            'TWD' => array('name'=>'TWD - Taiwan New Dollars', 'symbol'=>'$','symbol_html'=>'$'),
            'THB' => array('name'=>'THB - Thai Baht', 'symbol'=>'฿','symbol_html'=>' &#3647;'),
            'TRY' => array('name'=>'TRY - Turkish Liras', 'symbol'=>'TL','symbol_html'=>' &#3647;'),
        );  
             if(isset($currencies[$currency_code])){
                 $currencies_array = $currencies[$currency_code];
                 return $currencies_array['symbol'];
             }else {
                 return  '$' ;
             }
      }

      public static function Location(){
        $allheader=getallheaders();
        $headerarray=$_SERVER['REMOTE_ADDR'];
        if($allheader['platform']=='web'){
            $headerarray=$allheader['client_ip'];
        }
        $geoPlugin_array = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=adf0531f133f5110bc8212687ff6017139d190484fc286343b07f4778effaf0b&format=json&ip='.$headerarray);
        $geoPlugin_array =json_decode($geoPlugin_array,true);
        $location =  $geoPlugin_array['countryName'];
        $data_array=array('location'=>$location,'lat'=>$geoPlugin_array['latitude'],'lng'=>$geoPlugin_array['longitude']);
        return  $data_array;
    }
    public static function short_para($string,$lenght=10,$max=15){
        $string = (strlen($string) > $max) ? substr($string,0,$lenght).'...' : $string;
        return  $string;
    }


    protected static function requestTranslation($source, $target, $text) {

        // Google translate URL
      $url = "https://translate.google.com/translate_a/single?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&hl=es-ES&ie=UTF-8&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e";

      $fields = array(
        'sl' => urlencode($source),
        'tl' => urlencode($target),
        'q' =>  urlencode($text)
        );

        // URL-ify the data for the POST
      $fields_string = "";
      foreach($fields as $key=>$value) {
        $fields_string .= $key.'='.$value.'&';
      }

      rtrim($fields_string, '&');

        // Open connection
      $ch = curl_init();

        // Set the url, number of POST vars, POST data
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, count($fields));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_USERAGENT, 'AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1');

        // Execute post
      $result = curl_exec($ch);

        // Close connection
      curl_close($ch);
    //   return  $result;
      return self::getSentencesFromJSON($result);
    }

    protected static function getSentencesFromJSON($json) {
        error_reporting(0);
        $sentencesArray = json_decode($json, true);
        $sentences = "";
  
        foreach ($sentencesArray["sentences"] as $s) {
          $sentences .= utf8_decode($s["trans"]);
        }
  
        return $sentences;
      }

    public static function get_messages_lng($index,$type) {

        $allheader=getallheaders();
        // print_r($allheader);
        // exit;
        $lng = '';
        if(isset($allheader['lng'])){
                if($allheader['lng']!='en' && !empty($allheader['lng'])){
                    $lng='_de';
                }
        }
        if($type=='success'){
            $message_array_sucess= [
             'Your_account_has_created'   =>'Your account has created, Enjoy using the app',
             'Your_account_has_created_de'=>'Ihr Konto wurde erstellt, Genießen Sie die Verwendung der App',
             'Email_Send'   =>'Email Send',
             'Email_Send_de'=>'E-Mail senden',
             'verified'     =>'verified',
             'verified_de'  =>'verifiziert',
             'Your_Password_Has_Been_Changed' => 'Your Password Has Been Changed And Sent To Your Email Adress',
             'Your_Password_Has_Been_Changed_de' =>'Ihr Passwort wurde geändert und an Ihre E-Mail-Adresse gesendet',

            ]; 
            return $message_array_sucess[$index.$lng];
        } else {
            $message_array_error= [
                'Email_account_already_exist'      =>'Email account already exist, please login',
                'Email_account_already_exist_de'   =>'E-Mail-Account existiert bereits, bitte loggen Sie sich ein',
                'Invalid_Email_or_Password'        =>'Invalid Email or Password, Please try again',
                'Invalid_Email_or_Password_de'     =>'ungültige E-Mail oder Passwort, bitte versuchen Sie es erneut',
                'failed_to_create_token'           =>'failed to create token',
                'failed_to_create_token_de'        =>'Token konnte nicht erstellt werden',
                'Your_account_doesnt_exist'        =>"Your account doesn't exist, please signup",
                'Your_account_doesnt_exist_de'     =>'Ihr Konto existiert nicht, bitte registrieren Sie sich',
                'This_account_is_linked_with_user' => 'This account is linked with user, please use appropriate app to login with this account',
                'This_account_is_linked_with_user_de' => 'Dieses Konto ist mit dem Nutzer verknüpft. Bitte verwenden Sie die entsprechende App, um sich mit diesem Konto anzumelden',
                'This_account_is_linked_with_studio' => 'This account is linked with studio, please use appropriate app to login with this account',
                'This_account_is_linked_with_studio_de' => 'Dieses Konto ist mit Studio verknüpft. Bitte verwenden Sie die entsprechende App, um sich mit diesem Konto anzumelden',
                'Email_not_Send'   =>'Email Not Send',
                'Email_not_Send_de'=>'E-Mail nicht senden',
                'not_verified'     =>'Not verified',
                'not_verified_de'  =>'nicht verifiziert',

               ]; 
               return $message_array_error[$index.$lng];
        }
        
    
      }

}
