<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class StudioAppointments extends Model
{
  
      protected $table = 'service_appointments';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','studio_id','service_id','status','time','message','payment_id' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = false;

}
