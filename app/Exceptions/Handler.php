<?php

namespace App\Exceptions;

use Exception as ex;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Paypalpayment;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(ex $exception)
    {   
        
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, ex $e)

    {

        if (strpos($e->getMessage(),'token')) {
            return response()->json(['status'=>'error','message'=>$e->getMessage()]);
            exit;
        }

        if (strpos($e->getMessage(), 'Token') !== false) {
            return response()->json(['status'=>'error','message'=>$e->getMessage()]);
            exit;
            }

        if ($e instanceof \PayPal\Exception\PayPalConnectionException) {
            // dd($exception->getStatusCode());
            return response()->json(['status'=>'error','message'=>"Service is Busy Try Again After 30 Seconds"]);
            }

        if ($e instanceof \PayPal\Exception\PayPalConfigurationException) {
            // dd($exception->getStatusCode());
            return response()->json(['status'=>'error','message'=>"Service is Busy Try Again After 30 Seconds"]);
            }

        if ($e instanceof \PayPal\Exception\PayPalInvalidCredentialException) {
        // dd($exception->getStatusCode());
        return response()->json(['status'=>'error','message'=>"Service is Busy Try Again After 30 Seconds"]);
        }

        if ($e instanceof \PayPal\Exception\PayPalMissingCredentialException) {
        // dd($exception->getStatusCode());
        return response()->json(['status'=>'error','message'=>"Service is Busy Try Again After 30 Seconds"]);
        }
     

        // if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
        //     // dd($exception->getStatusCode());
        //     return response()->json(['status'=>'error','message'=>$e->getMessage()]);
        //     }
        //      if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
        //         // return response()->json(['token_expired'], $e->getStatusCode());
        //         return response()->json(['status'=>'error','message'=>"token_expired"], $e->getStatusCode());

        //     }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
        //          return response()->json(['status'=>'error','message'=>"token_invalid"], $e->getStatusCode());
        //         // return response()->json(['token_invalid'], $e->getStatusCode());
        //     }else{
        //         // return response()->json(['error'=>'Token is required']);

        //          return response()->json(['status'=>'error','message'=>"Token is required"]);
        //     }
       
       
        
        return parent::render($request,$e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->intended('login');
    }
}
