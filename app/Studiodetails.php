<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Studiodetails extends Model
{
    
    protected $table = 'studio_details';
    /**
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Studio_id','Category_id','service_time','Service_charges','SubCategory_id','Date','gender','bodypart','Description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = false;

}
