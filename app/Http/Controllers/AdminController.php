<?php

namespace App\Http\Controllers;

use App\BasicModel;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use Image;
use Illuminate\Support\Facades\View;
use JWTAuth;
use PDF;
use Auth;
use Session;
use \Mailjet\Resources;
// use PayPal\Rest\ApiContext;
// use PayPal\Auth\OAuthTokenCredential;
use Cartalyst\Stripe\Stripe as Stripe;
use Exception;

class AdminController extends Controller
{

    public function __construct(Request $request)
    {   
        // $mailfromname ='meetandeatapp';
        // $mailfrom     ='info@meetandeatapp.com';
        // $toname       ='sami';
        // $to           ='sami199017@gmail.com';
        // $subject      ='Hello';
        // $html         = '<b> Hello</b>';
        // $text         = 'hi';
        // $replyto      ='sami@gexton.com';

        // $array_data = array(
        //     'from'=> $mailfromname .'<'.$mailfrom.'>',
        //     'to'=>$toname.'<'.$to.'>',
        //     'subject'=>$subject,
        //     'html'=>$html,
        //     'text'=>$text,
        //     'o:tracking'=>'yes',
        //     'o:tracking-clicks'=>'yes',
        //     'o:tracking-opens'=>'yes',
        //     'o:tag'=>'',
        //     'h:Reply-To'=>$replyto
        // );
        // $session = curl_init('https://api.mailgun.net/v3/meetandeatapp.com/messages');
        // curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($session, CURLOPT_USERPWD, 'api:key-894553d2cb826e0e23351a7da5bee2a8');
        // curl_setopt($session, CURLOPT_POST, true);
        // curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
        // curl_setopt($session, CURLOPT_HEADER, false);
        // curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
        // curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        // $response = curl_exec($session);
        // curl_close($session);
        // $results = json_decode($response, true);
        // dd($results);
        // dd(Auth::User()->permissions);
        // $stripe = new Stripe;
        // $customers = $stripe->customers()->all();
        // $token = $stripe->tokens()->create([
        //     'card' => [
        //         'number'    => '5555555555554444',
        //         'exp_month' => 10,
        //         'cvc'       => 314,
        //         'exp_year'  => 2020,
        //     ],
        // ]);
        // echo $token['id'];
        // exit;
        // $response='';
        // try {
        //   $resposne = $stripe->charges()->capture('ch_1BYrYdEKdAhdWvy0FEH9sDFz',20);
        // } catch (Exception $ex) { 
        //     dd($ex);
        // } 
      
        // dd($resposne);
        $this->middleware('auth');
        $this->stripe_object  = new Stripe;
    //     $merchant_reference = str_random(30);
    //     $redirectUrl = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';
    //     $return_url = 'https://www.google.com.pk/';
        // $merchant_reference = str_random(30);
        // $requestParams =  array(
        //     "service_command"=> "SDK_TOKEN",
        //     "access_code"=> "gjSYUcOS0UmS2xUEmb7k",
        //     "merchant_identifier"=>"gVpftyIL",
        //     "language"=> "en",
        //     "device_id"=>"00000000-7006-a292-ffff-ffff99d603a9",
            // 'amount '           =>10000,
            // 'currency ' =>'AED',
            // 'customer_email '=>'sami@gexton.com',
            // // 'token_name '=>'Op9Vmp',
            // 'card_number'=>4005550000000001,
            // 'card_security_code'=>123, 
            // 'expiry_date'=>1705,
            // 'remember_me'=>'YES',
            // 'card_holder_name'=>'John Smith',
            // );

            // $requestParams = array(
            // 'service_command' => 'TOKENIZATION',
            // 'language' => 'en',
            // 'merchant_identifier' => 'gVpftyIL',
            // 'access_code' => 'gjSYUcOS0UmS2xUEmb7k',
            // 'merchant_reference' => $merchant_reference,
            // 'card_security_code' => 123,
            // 'card_number' => 4005550000000001,
            // 'expiry_date' => 2105,
            // 'remember_me' => 'YES',
            // 'card_holder_name' => 'John Smith');

            // $shaString = '';
            // $signType ='request';
            // ksort($requestParams);
            // $SHARequestPhrase   = 'PASS';
            // $SHAResponsePhrase   = 'PASS';
            // $SHAType       = 'sha256';
            // foreach ($requestParams as $k => $v) {
            //     $shaString .= "$k=$v";
            // }
          

            // if ($signType == 'request') 
            //     $shaString = $SHARequestPhrase . $shaString . $SHARequestPhrase;
            // else 
            //     $shaString = $SHAResponsePhrase . $shaString . $SHAResponsePhrase;
               
            // $signature = hash($SHAType, $shaString);
            // $requestParams['signature'] = $signature;
            // // dd($requestParams);
            // $requestParams=json_encode($requestParams);
            // // dd($requestParams);

            // $result = file_get_contents('https://sbpaymentservices.payfort.com/FortAPI/paymentApi', null, stream_context_create(array(
            //         'http' => array(
            //         'method' => 'POST',
            //         'header' => 'Content-Type: application/json' . "\r\n"
            //         . 'Content-Length: ' . strlen($requestParams) . "\r\n",
            //         'content' => $requestParams,
            //         ),
            //     )
            // ));

            // $result=json_decode($result);
            // dd($result);

            //     $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';
            //     $arrData = array(
            //         'command '=>"TOKENIZATION",
            //         'access_code'=>"gjSYUcOS0UmS2xUEmb7k",
            //         'merchant_identifier'=>"gVpftyIL",
            //         'merchant_reference '=>"MyReference0001",
            //         'amount '           =>10000,
            //         'currency ' =>'AED',
            //         'language'=>"en",
            //         'customer_email '=>'sami@gexton.com',
            //         // 'token_name '=>'Op9Vmp',
            //         'card_number'=>4005550000000001,
            //         'card_security_code'=>123, 
            //         'expiry_date'=>1705,
            //         'remember_me'=>'YES',
            //         'card_holder_name'=>'John Smith',
                

            //     );
            //     $shaString = '';
            //     $signType ='request';
            //     ksort($arrData);
            //     $SHARequestPhrase   = 'GLAM';
            //     $SHAResponsePhrase   = 'GLAM';
            //     $SHAType       = 'sha256';
            //     foreach ($arrData as $k => $v) {
            //         $shaString .= "$k=$v";
            //     }

            //     if ($signType == 'request') 
            //         $shaString = $SHARequestPhrase . $shaString . $SHARequestPhrase;
            //     else 
            //         $shaString = $SHAResponsePhrase . $shaString . $SHAResponsePhrase;

            //     $signature = hash($SHAType, $shaString);

            //     $arrData['signature'] =$signature;
            //     dd($arrData);    
            //     $ch = curl_init( $url );
            //     # Setup request to send json via POST.
            //     $data = json_encode($arrData);
            //     curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
            //     curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            //     # Return response instead of printing.
            //     curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            //     # Send request.
            //     $result = curl_exec($ch);
            //     curl_close($ch);
            //     # Print response.
            //     dd($result);
            //     exit;

    }

    public function systemsetup()
    {
        // dd(Input::get('check_state'));
        if(Input::has('refound')){
            DB::table('stripe_refund_charges')->where('id','1')->update(['amount_percentage'=>Input::get('refound')]);
          }
          if(Input::has('check_state')){
              if(Input::get('check_state')!='false'){
                  DB::table('stripe_refund_charges')->where('id','1')->update(['is_presentation_free'=>1]);
              } else {
                  $result =DB::table('stripe_refund_charges')->where('id','1')->update(['is_presentation_free'=>0]);
                //   dd($result);
              }   
          }
          
        $data = DB::table('packages')->orderby('id', 'DESC')->get();
        $stripe_refund_charges = DB::table('stripe_refund_charges')->first();
        
        // dd($data);
        for ($i = 0; $i < count($data); $i++) {

            $data[$i]->time_limit = $this->convertTime($data[$i]->time_limit);

        }
        for ($i = 0; $i < count($data); $i++) {

            $data[$i]->size_limit = $data[$i]->size_limit / 1024 . " MB";

        }
        
        return view('systemsetup')->with('result', $data)->with('stripe_refund_charges', $stripe_refund_charges);

    }

    public function sub_admin_listing()
    {

        $whereRaw   = "users.user_type='sub_admin'";
        $totalusers = User::whereRaw($whereRaw)->get()->toArray();
       
        return view('update_sub_user')->with('result',$totalusers);

    }

    public function about()
    {

        $data = DB::table('about_us')->orderby('id', 'DESC')->first();
        return view('about')->with('data', $data);

    }

    public function currency_rate()
    {   
        $data   = DB::table('currency_rate')->get();
        if(Input::has('rate')){
            $data_array = Input::all();
            foreach($data_array['rate'] as $key=>$value){
               DB::table('currency_rate')->where('currency_code',$data[$key]->currency_code)->update(['Rate'=>$value]);
            //    DB::Select('UPDATE `currency_rate` SET `Rate`='.$value);
            }
        }
        $data   = DB::table('currency_rate')->get();
        for($i=0;$i<count($data);$i++){
            $data[$i]->covertcurrency =  $this->currencyConverter('USD',$data[$i]->currency_code,1);
        } 
        return view('currency_rate')->with('data', $data);
    }

    public function currencyConverter($from_Currency,$to_Currency,$amount) {
        $from_Currency = urlencode($from_Currency);
        $to_Currency = urlencode($to_Currency);
        $encode_amount = $amount;
        $get = file_get_contents("http://free.currencyconverterapi.com/api/v3/convert?q=".$from_Currency."_".$to_Currency."&compact=ultra");
        if(!empty($get)){
         $get = json_decode($get,true);
         $get = number_format((float)$get[$from_Currency."_".$to_Currency], 2, '.', '');
          return $get;
        }
    }

    public function getDashboard()
    {
        $registoruser = User::count();
        $whereRaw     = "users.user_type='studio_user'";
        $totalstudios = User::whereRaw($whereRaw)->count();

        $whereRaw   = "users.user_type='normal_user'";
        $totalusers = User::whereRaw($whereRaw)->count();

        $whereRaw   = "users.user_type='freelancer'";
        $freelancer = User::whereRaw($whereRaw)->count();

        $Totalsubscribtions = DB::table('subscription')->count();

        $dataarray['totalstudios'] = $totalstudios;

        $dataarray['totalusers'] = $totalusers;

        $dataarray['Totalsubscribtions'] = $Totalsubscribtions;

        $dataarray['freelancer'] = $freelancer;

        $dataarray['registoruser'] = $registoruser - 1;

        $package_name = array();
        $total        = array();
        $id_array     = array();
        $checking     = array();
        $data         = DB::table("packages")->select('id', 'package_name')->get();
        foreach ($data as $key => $value) {
            array_push($id_array, $value->id);

            $checking[$value->id] = $value->package_name;
        }

        $selected_sizes_comma_seprated = implode(',', $id_array);
        $whereRaw                      = "subscription.package_id IN ($selected_sizes_comma_seprated)";

        $packages = DB::table("subscription")->selectRaw('subscription.`package_id` AS package_ids')->whereRaw($whereRaw)->get();

        foreach ($packages as $package) {

            array_push($total, $package->package_ids);
        }

        $totalhotspots = array();
        $totalname     = array();
        $vals          = array_count_values($total);
        foreach (array_keys($checking) as $key => $paramName) {
            if (array_key_exists($paramName, $vals)) {
                array_push($totalhotspots, $vals[$key + 1]);
                array_push($totalname, $checking[$key + 1]);
            } else {
                array_push($totalhotspots, 0);
                array_push($totalname, $checking[$key + 1]);
            }
        }
        $a_cat[] = array(
            'packagename' => $totalname,
            'totalsubs'   => $totalhotspots,
        );
        $dataarray['packages'] = $a_cat[0];
        return view('dashboard')->with('result', $dataarray);
    }

    public function DatewiseChart()
    {
        $dataarray = array();
        $enddate   = date('Y-m-d');

        $startdate = date('Y-m-d', strtotime("- 30 days", strtotime($enddate)));

        $Monthlysubscribers = DB::select("SELECT * FROM subscription WHERE subscription.start_time BETWEEN '$startdate%' AND '$enddate%'");

        $startdate = date('Y-m-d', strtotime("- 7 days", strtotime($enddate)));

        $weeklysubscribers = DB::select("SELECT * FROM subscription WHERE subscription.start_time BETWEEN '$startdate%' AND '$enddate%'");

        $daily = DB::select("SELECT * FROM subscription WHERE subscription.start_time like '$enddate%'");

        $dataarray[0] = count($Monthlysubscribers);

        $dataarray[1] = count($weeklysubscribers);

        $dataarray[2] = count($daily);

        $months = array('Monthly', 'Weekly', 'Daily');

        $data = "";
        foreach ($dataarray as $key => $value) {

            $a_cat[] = array(
                'name' => $months[$key],
                'y'    => $value,

            );
        }

        return response()->json(["result" => $a_cat]);

    }

    public function DatewiseChartdasboard()
    {

        $dataarray = array();
        $enddate   = Input::get('to');
        $startdate = Input::get('from');

        $datewisechart = DB::select("SELECT * FROM subscription WHERE subscription.start_time BETWEEN '$startdate%' AND '$enddate%'");

        $datewisechart = $this->convertStdToArray($datewisechart);
        foreach ($datewisechart as $key => $value) {

            array_push($dataarray, date('Y-m-d', strtotime($value['start_time'])));
        }

        $chartdataarray = array_count_values($dataarray);

        foreach ($chartdataarray as $key => $value) {

            $dates = array_keys($chartdataarray, $value);

            $a_cat[] = array(
                'name' => $dates[0],
                'y'    => $value,

            );

        }

        return response()->json(["result" => $a_cat]);

    }

    public function getAddstudioview(Request $request)
    {

        $countries = array('AF' => 'Afghanistan', 'AX' => 'Aland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua And Barbuda', 'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan', 'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium', 'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia', 'BA' => 'Bosnia And Herzegovina', 'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam', 'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada', 'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China', 'CX' => 'Christmas Island', 'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo', 'CD' => 'Congo, Democratic Republic', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => 'Cote D\'Ivoire', 'C_I' => 'Canary Island', 'HR' => 'Croatia', 'CU' => 'Cuba', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia', 'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana', 'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam', 'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti', 'HM' => 'Heard Island & Mcdonald Islands', 'VA' => 'Holy See (Vatican City State)', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic Of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle Of Man', 'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'Kenya', 'KI' => 'Kiribati', 'KR' => 'Korea', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia', 'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libyan Arab Jamahiriya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States Of', 'MD' => 'Moldova', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro', 'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands', 'AN' => 'Netherlands Antilles', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NIR' => 'Northern Ireland', 'NI' => 'Nicaragua', 'NE' => 'Niger', 'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman', 'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar', 'RE' => 'Reunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'BL' => 'Saint Barthelemy', 'SH' => 'Saint Helena', 'KN' => 'Saint Kitts And Nevis', 'LC' => 'Saint Lucia', 'MF' => 'Saint Martin', 'PM' => 'Saint Pierre And Miquelon', 'VC' => 'Saint Vincent And Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome And Principe', 'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia And Sandwich Isl.', 'ES' => 'Spain', 'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard And Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania', 'TH' => 'Thailand', 'TL' => 'Timor-Leste', 'TG' => 'Togo', 'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad And Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan', 'TC' => 'Turks And Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom', 'US' => 'United States', 'UM' => 'United States Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela', 'VN' => 'Viet Nam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.', 'WF' => 'Wallis And Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe');

        return view('addstudio')->with('countries', $countries);

    }

    public function getAdd_Sub_Admin(Request $request)
    {

        $countries = array('AF' => 'Afghanistan', 'AX' => 'Aland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua And Barbuda', 'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan', 'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium', 'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia', 'BA' => 'Bosnia And Herzegovina', 'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam', 'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada', 'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China', 'CX' => 'Christmas Island', 'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo', 'CD' => 'Congo, Democratic Republic', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => 'Cote D\'Ivoire', 'C_I' => 'Canary Island', 'HR' => 'Croatia', 'CU' => 'Cuba', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia', 'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana', 'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam', 'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti', 'HM' => 'Heard Island & Mcdonald Islands', 'VA' => 'Holy See (Vatican City State)', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic Of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle Of Man', 'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'Kenya', 'KI' => 'Kiribati', 'KR' => 'Korea', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia', 'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libyan Arab Jamahiriya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States Of', 'MD' => 'Moldova', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro', 'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands', 'AN' => 'Netherlands Antilles', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NIR' => 'Northern Ireland', 'NI' => 'Nicaragua', 'NE' => 'Niger', 'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman', 'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar', 'RE' => 'Reunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'BL' => 'Saint Barthelemy', 'SH' => 'Saint Helena', 'KN' => 'Saint Kitts And Nevis', 'LC' => 'Saint Lucia', 'MF' => 'Saint Martin', 'PM' => 'Saint Pierre And Miquelon', 'VC' => 'Saint Vincent And Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome And Principe', 'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia And Sandwich Isl.', 'ES' => 'Spain', 'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard And Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania', 'TH' => 'Thailand', 'TL' => 'Timor-Leste', 'TG' => 'Togo', 'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad And Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan', 'TC' => 'Turks And Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom', 'US' => 'United States', 'UM' => 'United States Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela', 'VN' => 'Viet Nam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.', 'WF' => 'Wallis And Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe');

        return view('add_sub_admin')->with('countries', $countries);

    }

    public function postAddstudio(Request $request)
    {
        $data = Input::all();
        $path = public_path() . '/assets/uploads/images/';
        $path = $path . date('Y') . '/' . date('m') . '/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $pic    = Input::file('profile_img');
        $propic = Input::file('propic');
        $filename_big ='';
        if (isset($pic)) {

            //$fileName = 'studioapp'.'_up_'.time().'.png';
            $fileName = 'studioapp' . '_up_' . time();

            $fileName = "public/assets/uploads/images/" . date('Y') . '/' . date('m') . '/' . $fileName;
            // Input::file('profile_img')->move($path,$fileName);
            $image    = Input::file('propic');
            // $filename = $fileName . '.' . $image->getClientOriginalExtension();
            $image_mime=getimagesize($_FILES["propic"]["tmp_name"]);
            $path_parts=image_type_to_extension($image_mime[2]);
            $extension='.png';
            if(isset($path_parts)){
                $extension= $path_parts;
            }
            $fileName =  $fileName . $extension;
            Input::file('propic')->move($path,$fileName);
            // $path =public_path($filename);
            // Image::make($image->getRealPath())->resize(400, 400)->save($path);
            // echo  $filename;

            // exit();
            
                $path = public_path() . '/assets/uploads/images/profile_pic_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $filename_big = 'studioapp' . '_up_' . time();
                $filename_big = "public/assets/uploads/images/profile_pic_big/" . date('Y') . '/' . date('m') . '/' .  $filename_big;
                $image_mime=getimagesize($_FILES["profile_img"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
            
                $extension='.png';
                if(isset($path_parts)){
                    $extension= $path_parts;
                }
                $filename_big = $filename_big . $extension;
                Input::file('profile_img')->move($path,$filename_big);
              
            }

        else {

            $filename     = "public/assets/images/placeholder.png";
            $filename_big = "public/assets/images/placeholder.png";
        }
        // print_r($filename_big);
        // exit;
        $user = user::create([
            'Studio_Name'    => $data['Studio_Name'],
            'username'       => $data['username'],
            'email'          => $data['email'],
            'contact_no'     => $data['contact_no'],
            'password'       => bcrypt($data['password']),
            'user_type'      => $data['usertype'],
            'profile_pic'    => $fileName,
            'profile_pic_big'=> $filename_big,
            'Address'        => $data['Address'],
            'country'        => $data['country'],
            'zipcode'        => $data['zipcode'],
            'user_status'    => 1,
            'lat'            => $data['lng'],
            'lng'            => $data['lng'],
            'permissions'    => '',
            'time_zone'      => $data['timezone'],
            'created_date'   => date("Y-m-d H:i:s"),
            'platform'       => 'web'
        ]);
        DB::table('code_verified')->insert(['email'=>$data['email'],'status' => 1]);
        $credentials = array(
            'email'    => $data['email'],
            'password' => $data['password'],
        );
        $token = JWTAuth::attempt($credentials);
        $token = compact('token');
        $user  = JWTAuth::toUser($token['token']);
        if ($data['usertype'] != 'User') {
            $packageid = 1;
        } else {

            $packageid = 5;
        }
       
        DB::table('subscription')->insert(['user_id' => $user['id'], 'package_id' => $packageid]);

        return response()->json(["result" => "Added"]);

    }

    public function postRegister_sub_admin(Request $request)
    {
        $data = Input::all();
        $permisions_array=['Studio','Users','Categories','Packages','About','Finance','FAQ','Contact_Us','currency_rate'];
        $permisions = $request->radio19;
        $permisions_insert=[];
        foreach($permisions_array as $value){
            if(in_array($value, $permisions)){
                $permisions_insert[$value] = 1;
            }else{
                $permisions_insert[$value] = 0;
            }

        }
        $path = public_path() . '/assets/uploads/images/';
        $path = $path . date('Y') . '/' . date('m') . '/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $pic    = Input::file('profile_img');
        $propic = Input::file('propic');
        $fileName ='';
        $filename_big ='';
        if (isset($pic)) {

            //$fileName = 'studioapp'.'_up_'.time().'.png';
            $fileName = 'studioapp' . '_up_' . time();

            $fileName = "public/assets/uploads/images/" . date('Y') . '/' . date('m') . '/' . $fileName;
            // Input::file('profile_img')->move($path,$fileName);
            $image    = Input::file('propic');
            // $filename = $fileName . '.' . $image->getClientOriginalExtension();
            $image_mime=getimagesize($_FILES["propic"]["tmp_name"]);
            $path_parts=image_type_to_extension($image_mime[2]);
            $extension='.png';
            if(isset($path_parts)){
                $extension= $path_parts;
            }
            $fileName =  $fileName . $extension;
            Input::file('propic')->move($path,$fileName);
            // $path =public_path($filename);
            // Image::make($image->getRealPath())->resize(400, 400)->save($path);
            // echo  $filename;

            // exit();
            
                $path = public_path() . '/assets/uploads/images/profile_pic_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $filename_big = 'studioapp' . '_up_' . time();
                $filename_big = "public/assets/uploads/images/profile_pic_big/" . date('Y') . '/' . date('m') . '/' .  $filename_big;
                $image_mime=getimagesize($_FILES["profile_img"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
            
                $extension='.png';
                if(isset($path_parts)){
                    $extension= $path_parts;
                }
                $filename_big = $filename_big . $extension;
                Input::file('profile_img')->move($path,$filename_big);
              
            }

        else {

            $fileName     = "public/assets/images/placeholder.png";
            $filename_big = "public/assets/images/placeholder.png";
        }
        $user = user::create([
            'Studio_Name'    => $data['Studio_Name'],
            'username'       => '',
            'email'          => $data['email'],
            'contact_no'     => $data['contact_no'],
            'password'       => bcrypt($data['password']),
            'user_type'      => 'sub_admin',
            'profile_pic'    => $fileName,
            'profile_pic_big'=> $filename_big,
            'Address'        => $data['Address'],
            'country'        => $data['country'],
            'zipcode'        => $data['zipcode'],
            'user_status'    => 0,
            'lat'            => $data['lng'],
            'lng'            => $data['lng'],
            'permissions'    => json_encode($permisions_insert),
            'created_date'   => date("Y-m-d H:i:s"),
            'platform'       => 'web',
            
        ]);

        return response()->json(["result" => "Added"]);

    }

    public function postDeleteuser()
    {

        $id         = Input::get('id');
        $data = User::where('id', $id)->delete();
        return response()->json(["status" => $data]);

    }
    public function postActiveUser()
    {
        $id   = Input::get('id');
        $useractive = Input::get('useractive');
        $data = User::where('id', $id)->update(['user_status'=>$useractive]);
        $title            = "";
        $body             = "";
        $studio           = DB::table('users')->select('Studio_Name as name','device_token')->where('id', '=', $id)->first();
        if($useractive==1){
            $title            = "Your Account has been  Actived Now You Can Upload Videos";
            $body             = $studio->name . " you can upload videos now";
        }else {

            $title            = "Your Account has been  Deactived by Admin";
            $body             = $studio->name . " you can't upload videos now";
        }
            $deviceToken      = array($studio->device_token);
            $data2              = new \stdClass();
            $data2->studio_id = $id;
            $p_arr            = array(
                'title'     => $title,
                'body'      => $body,
                'push_data' => '',
                'type'      => 'Activation',
            );
            $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
            $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' =>  $id, 'device_token' => $studio->device_token, 'is_sent' => 1]);
            BasicModel::pushNotification($title, $body, $deviceToken, 'Activation', $data2);
    
        if($useractive == 1){

            $totalvideos = DB::table('videos')->where('studio_id',$id)->count();
            $package_id  = DB::table('subscription')->select('package_id')->where('user_id', $id)->first();
            // print_r($package_id);
            // exit;
            $videolimit  = DB::table('packages')->select('limit')->where('id',$package_id->package_id)->first();
            // dd($totalvideos);
            $video = (array) $videolimit->limit;
            $check = DB::table('presentation_subscription')->select('*')->where('user_id',$id)->count();
            if ($check > 0) {
                $video_category=DB::table('video_category')->where('status','custom')->first();
                DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$id' and Video_category_id='$video_category->id'");
            }
            // dd($video[0]);
                if ($totalvideos > $video[0]) {
                    $limit = $totalvideos - $video[0];
                    DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$id' and is_pro=0 ORDER BY id ASC LIMIT $limit ");
                    
                }else if($video[0]> $totalvideos) {
                    
                    DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$id' and is_pro=0");

                }
            } else {
                    
                    DB::select("UPDATE videos SET is_visible=0 WHERE studio_id = '$id'");
            }
                    return response()->json(["status" => $data]);
    }

    public function postDeletecat()
    {

        $id   = Input::get('id');
        $data = DB::table("video_category")->where('id', $id)->delete();

        BasicModel::DeleteData('sub_category', $id, 'parent_id');
        BasicModel::DeleteData('studio_services', $id, 'Category_id');
        BasicModel::DeleteData('videos', $id, 'Video_category_id');

        return response()->json(["status" => $data]);

    }

    public function postDeletestaffuser()
    {

        $id   = Input::get('id');
        $data = DB::table("staff_member")->where('parent_id', $id)->delete();
        return response()->json(["status" => $data]);

    }

    public function getStudios($start = 0, $length = 10, $search = null)
    {

        $start    = Input::get('start');
        $length   = Input::get('length');
        $search   = Input::get('search');
        $order    = Input::get('order');
        $orderby  = "";
        $whereRaw = "users.user_type !='normal_user' and users.user_type !='admin' and users.user_type !='sub_admin'";
        if ($order[0]['column'] == "0") {
            $orderby = "id";
        }
        if ($order[0]['column'] == "2") {
            $orderby = "studio_name";

        }

        if ($order[0]['column'] == "3") {
            $orderby = "zipcode";

        }
        if ($order[0]['column'] == "4") {
            $orderby = "contact_no";

        }
        $fetchsinglesellerlogin = User::select("*")->whereRaw($whereRaw)->get()->toArray();
        $TOTAL                  = count($fetchsinglesellerlogin);
        $users                  = User::select("users")->select('id', 'studio_name', 'contact_no', 'zipcode', 'profile_pic', 'username', 'lat', 'lng', 'user_type','user_status')->whereRaw($whereRaw)->skip($start)->limit($length)->orderby($orderby, $order[0]['dir'])->get();

        if ($search['value'] != "") {

            // return $search['value'];
            $whereRaw2 = "(";
            $whereRaw2 .= "`users`.`Studio_name`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`contact_no`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`username`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`user_type` like '%" . $search['value'] . "%'";
            $whereRaw2 .= " )";
            $users = User::select("users")->select('id', 'studio_name', 'contact_no', 'zipcode', 'profile_pic', 'username', 'lat', 'lng', 'user_type','user_status')->whereRaw($whereRaw)->whereRaw($whereRaw2)->skip($start)->limit($length)->orderby($orderby, $order[0]['dir'])->get();
            $TOTAL = count($users);
        }

        foreach ($users as $key => $value) {

            if ($users[$key]['profile_pic'] != null) {
                $users[$key]['profile_pic'] = "<img class='img-thumbnail' style='max-height: 40px' src='" . asset($users[$key]['profile_pic']) . "' alt='" . $users[$key]['studio_name'] . "' />";
            } else {
                $users[$key]['profile_pic'] = "<img class='img-thumbnail' style='max-height: 40px' src='" . asset('public/assets/images/placeholder.png') . "' alt='" . $users[$key]['studio_name'] . "' />";

            }

            if ($users[$key]['user_type'] == 'studio_user') {
                $users[$key]['user_type'] = 'Studio';
            } else {

                $users[$key]['user_type'] = 'Free Lancer';
            }
            
        }    

        return array("recordsTotal" => $TOTAL, "recordsFiltered" => $TOTAL, 'data' => $users);
    }

    public function getStudioservicedetails($id)
    {

        session()->put('activeclass', 'Services');
        $dataarray = array();
        $select    = array(
            'studio_services.service_time',
            'studio_services.Service_charges',
            'studio_services.gender',
            'studio_services.Description',
            'video_category.category_name as categoryName',
            'studio_services.service_title as service_title',
            'studio_services.services_image as serviceimage',
            'studio_services.id as id',
            'studio_services.currency_code',
            'studio_services.discount_amount',
            

        );
        $studio_id = $id;
        session()->put('studioid', $studio_id);
        $whereRaw = 'studio_services.Studio_id=' . $studio_id;
        $data     = DB::table("studio_services")->select($select)
            ->leftjoin('video_category', 'video_category.id', '=', 'studio_services.Category_id')->leftjoin('sub_category', 'studio_services.SubCategory_id', '=', 'sub_category.id')->whereRaw($whereRaw)->orderBy('service_index','asc')->get();
            
        $data = $this->convertStdToArray($data);
        if ($data != "") {
            foreach ($data as $key => $value) {
                $dataarray[$key]['id']              = $data[$key]['id'];
                $dataarray[$key]['service_time']    = $data[$key]['service_time'];
                $dataarray[$key]['Service_charges'] = $data[$key]['Service_charges'];
                $dataarray[$key]['gender']          = $data[$key]['gender'];
                $dataarray[$key]['Description']     = $data[$key]['Description'];
                $dataarray[$key]['categoryName']    = $data[$key]['categoryName'];
                $dataarray[$key]['service_title']   = $data[$key]['service_title'];
                $dataarray[$key]['serviceimage']    = $data[$key]['serviceimage'];
                $dataarray[$key]['currency_code']   = $data[$key]['currency_code'];
                $dataarray[$key]['discount_amount'] = $data[$key]['discount_amount'];

            }
        }
       
        $video_category = DB::table('video_category')->whereRaw("video_category.status ='studio'")->orderby('id', 'desc')->get();

        return view('studioservices')->with('data', $dataarray)->with('video_category', $video_category);

    }

    public function AddStudioservice($id)
    {
        session()->put('activeclass', 'Services');
        $studio_id = $id;
        session()->put('studioid', $studio_id);
        $video_category = DB::table('video_category')->whereRaw("video_category.status ='studio'")->orderby('id', 'desc')->get();
        return view('addservices')->with('studio_id',$studio_id)->with('video_category', $video_category);
    }

    public function Editservice_by_id($id,$serviceid)
    {
        session()->put('activeclass', 'Services');
        $studio_id = $id;
        session()->put('studioid', $studio_id);
        
        $video_category = DB::table('video_category')->whereRaw("video_category.status ='studio'")->orderby('id', 'desc')->get();
        $whereRaw  = 'studio_services.id=' . $serviceid;
        $data      = DB::table("studio_services")->select('*')->whereRaw($whereRaw)->first();
        return view('addservices')->with('studio_id',$studio_id)->with('video_category', $video_category)->with('services',$data)->with('edit',1);
    }

    public function getStudioservicedetailsbyid()
    {

        $serviceid = Input::get('serviceid');
        $whereRaw  = 'studio_services.id=' . $serviceid;
        $data      = DB::table("studio_services")->select('*')->whereRaw($whereRaw)->get();
        // for ($i=0; $i < count($data) ; $i++) {
        //   $data[$i]->services_image=asset($data[$i]->services_image);
        // }
        // $video_category=DB::table('video_category')->whereRaw("video_category.status ='studio'")->orderby('id','desc')->get();
        return response()->json(['status' => 'success', 'data' => $data[0]]);

    }

    public function getStudioOverview($id)
    {

        session()->put('activeclass', 'Overveiw');
        $dataarray = array("Studio_Name" => '', "userid" => "");
        // $id        = Input::get('id');
        $data      = User::select('Studio_Name', 'users.id', 'username', 'profile_pic', 'country', 'cover_pic', 'subscribers_id', 'user_type')->leftjoin('studio_subscribers', 'studio_subscribers.studio_id', '=', 'users.id')->where('users.id', $id)->get()->toArray();

        $socaillinks = DB::table("social_links")->select('*')->where('studio_id', $id)->get()->toArray();
        $videos      = DB::table("videos")->where('studio_id', $id)->get();
        $staff       = DB::table("staff_member")->where('studio_id', $id)->groupby('parent_id')->get();
        $staff       = count($staff);
        $package     = DB::table('subscription')->select('packages.package_name')->leftjoin('packages', 'packages.id', '=', 'subscription.package_id')->where('subscription.user_id', $id)->get();

        $package = $this->convertStdToArray($package);

        $totalvideo = count($videos);
        // dd($data);
        if ($data != []) {
            $dataarray['Studio_Name'] = $data[0]['Studio_Name'];

            $dataarray['total_member'] = $staff;
            $dataarray['totalvideo']   = $totalvideo;
            $dataarray['package_name'] = "Free User";
            if ($package != null) {
                $dataarray['package_name'] = $package[0]['package_name'];
            }

            session()->put('studioid', $id);
            session()->put('StudioName', $dataarray['Studio_Name']);
            session()->put('username', $data[0]['username']);
            session()->put('user_type', $data[0]['user_type']);
            if ($data[0]['profile_pic'] != null) {
                session()->put('pro_pic', $data[0]['profile_pic']);
            } else {
                session()->put('pro_pic', '/public/assets/images/placeholder.png');

            }

            session()->put('country', $data[0]['country']);
            if ($data[0]['cover_pic'] != null) {
                session()->put('cover_pic', $data[0]['cover_pic']);
            } else {
                session()->put('cover_pic', '/public/assets/uploads/coverplaceholder.jpg');

            }

            session()->put('totalvideo', $totalvideo);

            $totalsubs = count($data);
            session()->put('totalsubscribers', $totalsubs);
            $socaillinks = $this->convertStdToArray($socaillinks);

            if ($socaillinks != []) {
                foreach ($socaillinks as $key => $value) {

                    if ($value['site_name'] == "facebook") {

                        session()->put('facebooklink', $value['sharing_link']);

                    }
                    if ($value['site_name'] == "twitter") {

                        session()->put('twitterlink', $value['sharing_link']);

                    }

                    if ($value['site_name'] == "linkedin") {

                        session()->put('linkedinlink', $value['sharing_link']);

                    }

                }

            } else {
                session()->put('facebooklink', "#");
                session()->put('twitterlink', "#");
                session()->put('linkedinlink', "#");

            }
            $enddate            = date('Y-m-d');
            $startdate          = date('Y-m-d', strtotime("- 30 days", strtotime($enddate)));
            $Monthlysubscribers = DB::select("SELECT * FROM studio_subscribers WHERE studio_subscribers.date BETWEEN '$startdate%' AND '$enddate%' AND studio_subscribers.studio_id='$id'");

            $startdate = date('Y-m-d', strtotime("- 7 days", strtotime($enddate)));

            $weeklysubscribers = DB::select("SELECT * FROM studio_subscribers WHERE studio_subscribers.date BETWEEN '$startdate%' AND '$enddate%' AND studio_subscribers.studio_id='$id'");

            $daily = DB::select("SELECT * FROM studio_subscribers WHERE studio_subscribers.date like '$enddate%' AND studio_subscribers.studio_id='$id'");

            $totalservice = DB::table('studio_services')->where('Studio_id', $id)->count();

            $dataarray['Monthlysubscribers'] = count($Monthlysubscribers);

            $dataarray['weeklysubscribers'] = count($weeklysubscribers);

            $dataarray['daily'] = count($daily);

            $dataarray['totalservice'] = $totalservice;

        }

        return view('studiooverview')->with('result', $dataarray);

    }

    public function getstaff($id)
    {

        session()->put('activeclass', 'StaffMembers');
        $dataarray = array("Studio_Name" => '', "userid" => "");
        session()->put('studio_id', $id);
        $data   = User::select('Studio_Name', 'id')->where('id', $id)->get()->toArray();
        $select = array(
            'studio_services.id as id',
            'studio_services.service_time',
            'studio_services.Service_charges',
            'studio_services.gender',
            'studio_services.Description',
            'video_category.category_name as categoryName',
            'sub_category.category_name as SubCategory',
            'video_category.id as categoryid',
            'sub_category.id as subcategoryid',
            'studio_services.id as rowid',
            DB::raw('IF(studio_services.services_image IS NOT NULL, CONCAT("' . asset('') . '",studio_services.services_image), "") AS services_image'),

        );

        $whereRaw = 'studio_services.Studio_id=' . $id;

        $service = DB::table("studio_services")->select($select)
            ->leftjoin('video_category', 'video_category.id', '=', 'studio_services.Category_id')->leftjoin('sub_category', 'studio_services.SubCategory_id', '=', 'sub_category.id')->whereRaw($whereRaw)->get()->toArray();

        //      for ($i=0; $i <count($service) ; $i++) {
        //        if($service[$i]->services_image=="" || $service [$i]->services_image==null )
        //        {
        //           $catimage=DB::table('video_category')->select('service_image')->where('id',$data[$i]->categoryid)->first();
        //            $service[$i]->services_image  =  asset($catimage->service_image);
        //        }
        //      }

        // dd($service);
        return view('studiostaffmember')->with('result', $dataarray)->with('service', $service);

    }

    public function getStaffmembers($start = 0, $length = 10, $search = null)
    {

        $start  = Input::get('start');
        $length = Input::get('length');
        $search = Input::get('search');
        $order  = Input::get('order');
        $id     = Input::get('id');

        $orderby = "";
        if ($order[0]['column'] == "0") {
            $orderby = "users.id";

        }
        // if($order[0]['column']=="1")
        // {
        //     $orderby="users.Studio_Name";

        // }
        if ($order[0]['column'] == "2") {
            $orderby = "users.Studio_Name";

        }
        if ($order[0]['column'] == "3") {
            $orderby = "users.zipcode";

        }
        if ($order[0]['column'] == "4") {
            $orderby = "users.contact_no";

        }

        $staff = DB::table("staff_member")->select("*")->where('staff_member.studio_id', $id)->groupby('parent_id')->get()->toArray();
        $TOTAL = count($staff);

        $select = array('users.Studio_Name as Name',
            'users.contact_no as membercontact',
            'users.profile_pic',
            'users.zipcode',
            'users.id',
            'staff_member.id as staffrowid',
            'staff_member.parent_id as parent_id',

        );
        $staff = DB::table("users")->select($select)->leftjoin('staff_member', 'staff_member.parent_id', '=', 'users.id')->where('staff_member.studio_id', $id)->groupby('parent_id')->skip($start)->limit($length)->orderby($orderby, $order[0]['dir'])->get();

        if ($search['value'] != "") {

            $whereRaw2 = "(";
            $whereRaw2 .= "`users`.`Studio_Name`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`contact_no`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`zipcode`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " )";

            $staff = DB::table("users")->select($select)->leftjoin('staff_member', 'staff_member.parent_id', '=', 'users.id')->where('staff_member.studio_id', $id)->whereRaw($whereRaw2)->skip($start)->limit($length)->orderby($orderby, $order[0]['dir'])->get();
            $TOTAL = count($staff);
        }

        $staff = $this->convertStdToArray($staff);
        // dd($staff);
        foreach ($staff as $key => $value) {
            $staff[$key]['profile_pic'] = "<img class='img-thumbnail' style='max-height: 40px' src='" . asset($staff[$key]['profile_pic']) . "' alt='" . $staff[$key]['Name'] . "' />";

        }
        return array("recordsTotal" => $TOTAL, "recordsFiltered" => $TOTAL, 'data' => $staff);

    }

    public function getstaffinfo()
    {

        $dataarray = array("membername" => '', "membercontact" => "", "memberemail" => "", "member_category" => "", "member_subcategory" => "");

        $select = array('users.Studio_Name as membername',
            'users.contact_no as membercontact',
            'users.email',
            'users.id',
            'video_category.category_name as categoryName',

        );
        $id = Input::get('id');

        $data = DB::table("users")->select($select)->selectRaw('GROUP_CONCAT(video_category.category_name) as member_category')->selectRaw('GROUP_CONCAT(sub_category.category_name) as member_subcategory')->leftjoin('staff_member', 'staff_member.parent_id', '=', 'users.id')->leftjoin('studio_services', 'staff_member.service_id', '=', 'studio_services.id')->leftjoin('video_category', 'studio_services.Category_id', '=', 'video_category.id')->leftjoin('sub_category', 'studio_services.SubCategory_id', '=', 'sub_category.id')->where('users.id', $id)->get();

        // $data=DB::table("users")->select($select)->leftjoin('staff_member','staff_member.parent_id','=','users.id')->leftjoin('categories','categories.id','=','staff_member.service_category')->leftjoin('sub_category','sub_category.parent_id','=','staff_member.service_subcategory')->where('staff_member.parent_id', $id)->get();

        $data = $this->convertStdToArray($data);

        // dd($data);

        if ($data != []) {
            $dataarray['membername']    = $data[0]['membername'];
            $dataarray['membercontact'] = $data[0]['membercontact'];
            $dataarray['memberemail']   = $data[0]['email'];
            // $dataarray['categoryName']  = $data[0]['categoryName'];
            $dataarray['member_category']    = $data[0]['member_category'];
          
            // session()->put('studioid',$data[0]['id']);
        }

        // dd($dataarray);

        return view('studiostaffmemberinfo')->with('result', $dataarray);

    }

    public function getStudioreviews($id)
    {

        $dataarray = array();
        $select    = array('users.Studio_Name',
            'users.profile_pic',
            'studio_reviews.comments',
            'studio_reviews.date',
            'studio_reviews.ratings',
            'studio_reviews.user_id',

        );
        $data = DB::table("studio_reviews")->select($select)->leftjoin('users', 'users.id', '=', 'studio_reviews.user_id')->where('studio_reviews.studio_id', $id)->groupby('studio_reviews.user_id')->get();
        $data = $this->convertStdToArray($data);
        $data = array_reverse($data);

        foreach ($data as $key => $value) {

            $dataarray[$key]['comments'] = $data[$key]['comments'];
            $dataarray[$key]['date']     = date('d-M-Y , h:i a', strtotime($data[$key]['date']));
            $dataarray[$key]['ratings']  = $data[$key]['ratings'];

            // $data2=User::select('Studio_Name','profile_pic')->where('id', $data[$key]['user_id'])->get()->toArray();

            //  echo $data[$key]['user_id'];

            $dataarray[$key]['username'] = $data[$key]['Studio_Name'];
            $dataarray[$key]['coverpic'] = $data[$key]['profile_pic'];

        }

        // dd($dataarray);

        session()->put('studioid', $id);
        session()->put('activeclass', 'Reviews');
        return view('studioreviews')->with('result', $dataarray);

    }

    public function getGenraldetails($id)
    {
        $dataarray = array(
            'row_id'          => '',
            'studio_id'       => '',
            "Name"            => '',
            "Email"           => '',
            "Contact"         => '',
            "Zip_code"        => '',
            'Country'         => '',
            'Address'         => '',
            'Current_package' => '',
            'workingDays'     => '',
            "profile_pic"     => '',
            'working_end'     => '',
        );
        $data = DB::table("users")->select('Studio_Name', 'email', 'contact_no', 'zipcode', 'country', 'Address', 'profile_pic', 'package_name')->selectRaw('GROUP_CONCAT(working_hours.id) as rowid')->selectRaw('GROUP_CONCAT(working_hours.working_Days) as workingDays')->selectRaw('GROUP_CONCAT(working_hours.working_start) as StartTime')->selectRaw('GROUP_CONCAT(working_hours.working_end) as EndTime')->selectRaw('GROUP_CONCAT(working_hours.is_daylight_saving) as is_daylight_saving')->leftjoin('subscription', 'subscription.user_id', '=', 'users.id')->leftjoin('packages', 'subscription.package_id', '=', 'packages.id')->leftjoin('working_hours', 'working_hours.studio_id', '=', 'users.id')->where('users.id', $id)->groupby('working_hours.studio_id')->get();
        $data = $this->convertStdToArray($data);
        //dd($data);

        if ($data != []) {
            $dataarray['row_id']             = $data[0]['rowid'];
            $dataarray['studio_id']          = $id;
            $dataarray['Name']               = $data[0]['Studio_Name'];
            $dataarray['Email']              = $data[0]['email'];
            $dataarray['Contact']            = $data[0]['contact_no'];
            $dataarray['Zip_code']           = $data[0]['zipcode'];
            $dataarray['Country']            = $data[0]['country'];
            $dataarray['Address']            = $data[0]['Address'];
            $dataarray['Current_package']    = $data[0]['package_name'];
            $dataarray['workingDays']        = $data[0]['workingDays'];
            $dataarray['StartTime']          = $data[0]['StartTime'];
            $dataarray['working_end']        = $data[0]['EndTime'];
            $dataarray['profile_pic']        = $data[0]['profile_pic'];
            $dataarray['is_daylight_saving'] = $data[0]['is_daylight_saving'];
            

        }
        $Bank_details = DB::table('bank_account_details')->where('studio_id',$id)->first();
        // dd($Bank_details);
        session()->put('studioid',$id);
        session()->put('activeclass', 'General');
     
        return view('studiogenralinfo')->with('result', $dataarray)->with('Bank_details',$Bank_details);

    }

    public function getvideos($id)
    {
        $dataarray = array();
        $select    = array('videos.id',
            'videos.Video_link',
            'videos.is_pro',
            'videos.discribtion',
            'videos.thumbnail',
            'videos.title',
            'videos.Duration',
            'video_category.category_name',
        );
        $data = DB::table("videos")->select($select)->leftjoin('video_category', 'videos.Video_category_id', '=', 'video_category.id')->where('videos.studio_id', $id)->get();
        // dd($data);
        // $data=$this->convertStdToArray($data);
        $BasicModel = new BasicModel();
        if ($data != []) {
            foreach ($data as $key => $value) {

                $data[$key]->Video_link = $BasicModel::signedUrl($data[$key]->Video_link);

                $data[$key]->thumbnail = $BasicModel->s3_baseurl . $data[$key]->thumbnail;

                $data[$key]->Duration = $this->convertTimevideo($data[$key]->Duration);

            }

        }
        session()->put('studioid', $id);
        session()->put('activeclass', 'Videos');

        return view('studiovideos')->with('result', $data);
    }

    // public function  getvideosbyid(){

    //      $dataarray=array();
    //      $id=Input::get('id');
    //      $select=array('videos.Video_link');
    //      $data = DB::table("videos")->select($select)->where('videos.id', $id)->first();
    //     return response()->json(['status'=>'sucess','video_link'=>$data->Video_link]);

    //  }

    public function getchannel()
    {

        $dataarray = array(array("Videolink" => "", "is_pro" => ""));
        $id        = Input::get('id');
        $select    = array('videos.title',
            'videos.thumbnail',
            'videos.discribtion',
            'videos.is_pro',
            'videos.Video_link',
            'video_category.category_name',
            'videos.likes',

        );

        $data = DB::table("videos")->select($select)->leftjoin('video_category', 'videos.Video_category_id', '=', 'video_category.id')->where('videos.studio_id', $id)->get();

        $data = $this->convertStdToArray($data);
        // dd($data);

        if ($data != []) {
            foreach ($data as $key => $value) {

                $dataarray[$key]['discribtion'] = $data[$key]['discribtion'];
                $dataarray[$key]['title']       = $data[$key]['title'];
                $dataarray[$key]['category']    = $data[$key]['category_name'];
                $dataarray[$key]['thumbnail']   = $data[$key]['thumbnail'];
                $dataarray[$key]['likes']       = $this->restyle_text($data[$key]['likes']);

            }

        }
        session()->put('studioid', $id);
        session()->put('activeclass', 'Videos');

        return view('studiochannel')->with('result', $dataarray);

    }

    public function getUsers($start = 0, $length = 10, $search = null)
    {

        $start    = Input::get('start');
        $length   = Input::get('length');
        $search   = Input::get('search');
        $order    = Input::get('order');
        $orderby  = "";
        $whereRaw = "users.user_type='normal_user'";
        if ($order[0]['column'] == "0") {

            $orderby = "id";

        }
        if ($order[0]['column'] == "2") {
            $orderby = "studio_name";

        }

        if ($order[0]['column'] == "3") {
            $orderby = "zipcode";

        }

        if ($order[0]['column'] == "4") {
            $orderby = "contact_no";

        }

        $fetchsinglesellerlogin = User::select("*")->whereRaw($whereRaw)->get()->toArray();
        $TOTAL                  = count($fetchsinglesellerlogin);

        $users = User::select("users")->select('id', 'studio_name', 'contact_no', 'zipcode', 'email','user_status')->whereRaw($whereRaw)->skip($start)->limit($length)->orderby($orderby, $order[0]['dir'])->get();

        if ($search['value'] != "") {

            $whereRaw2 = "(";
            $whereRaw2 .= "`users`.`studio_name`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`contact_no`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`zipcode`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`Address`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " OR `users`.`email`  like '%" . $search['value'] . "%'";
            $whereRaw2 .= " )";

            $users = User::select("users")->select('id', 'studio_name', 'contact_no', 'zipcode', 'Address', 'email')->whereRaw($whereRaw)->whereRaw($whereRaw2)->skip($start)->limit($length)->orderby($orderby, $order[0]['dir'])->get();
            $TOTAL = count($users);

        }

        return array("recordsTotal" => $TOTAL, "recordsFiltered" => $TOTAL, 'data' => $users);
    }

    public function getUserdetails($id)
    {

        $dataarray = array("id" => "", "Name" => '', "Email" => "", "Contact" => '', "Zip_code" => '', 'Country' => '', 'profile_pic' => "");
        // $id        = Input::get('id');
        $data      = DB::table("users")->select('Studio_Name', 'email', 'contact_no', 'zipcode', 'country', 'profile_pic')->where('users.id', $id)->get();
        $data      = $this->convertStdToArray($data);

        if ($data != []) {
            $dataarray['Name']        = $data[0]['Studio_Name'];
            $dataarray['Email']       = $data[0]['email'];
            $dataarray['Contact']     = $data[0]['contact_no'];
            $dataarray['Zip_code']    = $data[0]['zipcode'];
            $dataarray['Country']     = $data[0]['country'];
            $dataarray['profile_pic'] = $data[0]['profile_pic'];
            $dataarray['id']          = $id;

        }
        session()->put('useridforusers', $id);

        return view('studiouserinfo')->with('result', $dataarray);

    }

    public function getuserchannel($id)
    {

        // $id     = Input::get('id');
        $select = array('videos.id',
            'videos.Video_link',
            'videos.is_pro',
            'videos.discribtion',
            'videos.thumbnail',
            'videos.title',
            'videos.Duration',
            'video_category.category_name',
        );
        $data = DB::table("videos")->select($select)->leftjoin('video_category', 'videos.Video_category_id', '=', 'video_category.id')->where('videos.studio_id', $id)->get();
        // dd($data);
        // $data=$this->convertStdToArray($data);
        $BasicModel = new BasicModel();

        if ($data != []) {
            foreach ($data as $key => $value) {

                $data[$key]->Video_link = $BasicModel::signedUrl($data[$key]->Video_link);

                $data[$key]->thumbnail = $BasicModel->s3_baseurl . $data[$key]->thumbnail;

                $data[$key]->Duration = $this->convertTimevideo($data[$key]->Duration);

            }

        }
        // session()->put('studioid',$id);
        // session()->put('activeclass','Videos');

        return view('studiouserchannel')->with('result', $data);

        // return view('studiouserchannel')->with('result',$data);

    }
    public function postupdategeninfo()
    {

        $id            = Input::get('id');
        $columname     = Input::get('columname');
        $columname_big = Input::get('columname_big');
        $value         = Input::get('value');
        $propic        = Input::file('propic');
        $data_array=[$columname => $value];
        if (Input::hasFile('uploadimgfile')) {

            $previouimg = User::where('id', $id)->first();
            if ($previouimg->profile_pic != "" && $previouimg->profile_pic != null) {
                @unlink($previouimg->profile_pic);
            }
            if(isset($propic)){
            $path = public_path() . '/assets/uploads/images/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $fileName = 'studioapp' . '_up_' . time();
            $fileName = "public/assets/uploads/images/" . date('Y') . '/' . date('m') . '/' . $fileName;
            $image    = Input::file('propic');
            $filename = $fileName . '.' . $image->getClientOriginalExtension();
            Input::file('propic')->move($path, $filename);
            $value = $filename;
            $data_array=[$columname => $value];
           
           
                $path = public_path() . '/assets/uploads/images/profile_pic_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $filename_big = 'studioapp' . '_up_' . time();
                $filename_big = "public/assets/uploads/images/profile_pic_big/" . date('Y') . '/' . date('m') . '/' .  $filename_big;
                $image_mime=getimagesize($_FILES["uploadimgfile"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
            
                $extension='.png';
                if(isset($path_parts)){
                    $extension= $path_parts;
                }
                $filename_big = $filename_big . $extension;
                Input::file('uploadimgfile')->move($path,$filename_big);
                $data_array[$columname_big]=$filename_big;
              
            }

        }
        if($columname=="Address"){
            $data_array['lat']     =  Input::get('lat');
            $data_array['lng']     =  Input::get('lng');
        }
        $data = DB::table('users')->where('id', $id)->update($data_array);
        return $data;
    }

    public function AddCategories()
    {

        $data = Input::all();
        $path = public_path() . '/assets/uploads/categoryimages/';
        $path = $path . date('Y') . '/' . date('m') . '/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }   
        $fileName = 'studioapp' . '_up_' . time();
        $fileName = "public/assets/uploads/categoryimages/" . date('Y') . '/' . date('m') . '/' . $fileName;
        $image_mime=getimagesize($_FILES["propic"]["tmp_name"]);
        $path_parts=image_type_to_extension($image_mime[2]);
        $extension='.png';
        if(isset($path_parts)){
            $extension=$path_parts;
        }
        $filename = $fileName.$extension;
        Input::file('propic')->move($path, $filename);

        $fileName_big = 'studioapp' . '_up_' . time();
        $fileName_big = "public/assets/uploads/categoryimages_big/" . date('Y') . '/' . date('m') . '/' . $fileName;
        $profile_img    = Input::file('profile_img');
        $fileName_big = $fileName_big . '.' . $profile_img->getClientOriginalExtension();
        Input::file('profile_img')->move($path, $fileName_big);

        $slug = str_replace('&', 'and', strtolower(preg_replace('/\s+/', '_', $data['Category'])));

        $result = DB::table('video_category')->insertGetId(['category_name' => $data['Category'], 'slug' => $slug, 'service_image' => $filename, 'status' => $data['status'],'description'=>$data['description'],'service_image_big' => $fileName_big,'category_name_de'=>$data['newcategory_de'],'description_de'=>$data['description_de']]);

        if (count($result) > 0) {

            return response()->json(array(
                'status' => "Added",
                'rowid'  => $result,
            ));

        } else {
            return response()->json(array(
                'status' => "error",
            ));

        }

    }
    public function getcategories()
    {

        $data = DB::table('video_category')->orderby('id', 'desc')->get();
        // dd($data);
        return view('servicetype')->with('result', $data);
    }

    public function Bank_deatails($id)
    {
        $Bank_details = DB::table('bank_account_details')->where('studio_id',$id)->first();
        // dd($Bank_details);
        session()->put('studioid',$id);
        session()->put('activeclass', 'StaffMembers');
        return view('bank_deatails')->with('Bank_details', $Bank_details);
    }

    public function studio_finance($id,$status)
    {
        $studio_finance = DB::table('payment_invoices')->where('userid',$id)->where('payment_type','buy_service');
        if($status=='unpaid'){
            $studio_finance = $studio_finance->where('payment_status','unpaid');
            session()->put('active_status', 'unpaid');
        } 
        if($status=='paid') {
            $studio_finance = $studio_finance->where('payment_status','paid');
            session()->put('active_status', 'paid');
          
        }
        if($status=='Paid_invoices') {
            $studio_finance = DB::table('paid_invoices')->where('Studio_id',$id);
            session()->put('active_status', 'Paid_invoices');
            
        }
        
        $studio_finance = $studio_finance->get();
        // dd($studio_finance);
        $Due            = DB::table('payment_invoices')->select(DB::raw('SUM(amount) as total_Dues'),'currency_code')->where('userid',$id)->where('payment_type','buy_service')->where('payment_status','unpaid')->first();
        $Dues_data = new \stdClass();
        $Dues_data->total_Dues    =($Due->total_Dues/100)*80;
        $Dues_data->currency_code = $Due->currency_code;
        $paid_amount  = DB::table('payment_invoices')->select(DB::raw('SUM(amount) as total_paid'),'currency_code')->where('userid',$id)->where('payment_type','buy_service')->where('payment_status','paid')->first();
        $paid_data = new \stdClass();
        $paid_data->total_paid    =($paid_amount->total_paid/100)*80;
        $paid_data->currency_code = $paid_amount->currency_code;
        session()->put('studioid',$id);
        session()->put('activeclass', 'finance');
        return view('studio_finance')->with('studio_finance',$studio_finance)->with('Dues',$Dues_data)->with('paid',$paid_data)->with('studio_id',$id);
    }

    public function Paid_invocies()
    {  
        $all_inputs = Input::all();
        // print_r();
        // exit;
        $data=[
            'created_date'  =>date('Y-m-d H:i:s'),
            'Amount'        =>$all_inputs['paid_amount'],
            'Studio_id'     =>$all_inputs['user_id'],
            'invoices_id'   =>$all_inputs['inovices_id']
        ];
        $result= DB::table('paid_invoices')->insertGetId($data);
        if($result>0){
          foreach(json_decode($all_inputs['inovices_id']) as $value){
            DB::table('payment_invoices')->where('id',$value)->update(['payment_status'=>'paid']);
          }
        }
        return response()->json(array(
            'result' => $result,
        ));
    }

    public function genrate_Hotpot_inovice($id,$send_mail)
    {
            $invoice_object = new  \stdClass();
            $data  = DB::table('paid_invoices')->where('id',$id)->first();
            $data->invoices_id=json_decode($data->invoices_id);
            $user_deatils = DB::table('users')->where('id',$data->Studio_id)->first();
            $invoice_object->Userdeatails = $user_deatils;
            $payment_invoices = DB::table('payment_invoices')->whereIn('id',$data->invoices_id)->get();
            $invoice_object->payments = $payment_invoices;
            $data         = [
                'data' => $invoice_object,
            ];
            if($send_mail=='send_mail'){
                $to = ['email'=>$user_deatils->email];
                $view     = View::make('invoice.hotspot_invoice',$data);
                $contents = (string) $view;
                $emailing = BasicModel::sendEmail($contents,array(),$to,"Hotspot Payment Invoice");
                session()->put('Popup','Send_Email');
                return back();
            }
            // dd($invoice_object);
            //return view('invoice.hotspot_invoice')->with('data',$invoice_object);
            $pdf   = PDF::loadView('invoice.hotspot_invoice',$data);
            return $pdf->download($user_deatils->Studio_Name. '.pdf');
    

    }


    public function convertStdToArray($data)
    {
        return json_decode(json_encode($data), true);
    }

    public function restyle_text($input)
    {

        $input       = number_format($input);
        $input_count = substr_count($input, ',');
        if ($input_count != '0') {
            if ($input_count == '1') {
                return substr($input, 0, -4) . 'k';
            } else if ($input_count == '2') {
                return substr($input, 0, -8) . 'mil';
            } else if ($input_count == '3') {
                return substr($input, 0, -12) . 'bil';
            } else {
                return;
            }
        } else {
            return $input;
        }
    }

    public function compress($source, $destination, $quality)
    {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);
        }

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    public function Checkusername()
    {

        $username = Input::get('username');
        $username = user::where("username", $username)->get();
        $username = count($username);
        return response()->json(array(
            'username' => $username,
        ));

    }

    public function postworkinghours()
    {
        $day        = Input::get('day');
        $from       = Input::get('from');
        $to         = Input::get('to');
        $studio_id  = Input::get('studio_id');
        $is_daylight_saving=0;
        if(Input::get('switch')=='on'){
            $is_daylight_saving=1;
        }
        $result = DB::table('working_hours')->insertGetId(['studio_id'=>$studio_id,'working_Days' => $day, 'working_start' => $from, 'working_end' => $to,'is_daylight_saving'=>$is_daylight_saving]);
        // $dataarray=array('rowid'=>$result,'day'=>$day,'from'=>$from,'to'=>$to);
        if ($result >1) {

            return response()->json(['status' => 'success', 'message' => 'updated','id'=>$result]);

        } else if ($result == 0) {

            return response()->json(['status' => 'success', 'message' => 'samedata']);

        }

    }

    public function editcategory()
    {

        $rowid          = Input::get('rowid');
        $catname        = Input::get('catname');
        $status         = Input::get('status');
        $description    = Input::get('description');
        $editcatname_de = Input::get('editcatname_de');
        $description_de = Input::get('description_de');
        // dd(Input::all());
        $slug      = str_replace('&', 'and', strtolower(preg_replace('/\s+/', '_', $catname)));
        $data      = DB::table('video_category')->where('id', $rowid)->update(['category_name' => $catname, 'slug' => $slug, 'status' => $status,'description'=>$description,'category_name_de'=>$editcatname_de,'description_de'=>$description_de]);
        $dataarray = array('catname' => $catname, 'catimage' => "", 'usertype' => $status,'description'=>$description,'category_name_de'=>$editcatname_de,'description_de'=>$description_de);

        if (Input::hasFile('updatecatimage')) {

            $previouimg = DB::table('video_category')->where('id', $rowid)->first();
            if ($previouimg->service_image != "" && $previouimg->service_image != null) {
                // unlink($previouimg->service_image);
            }
            $path = public_path() . '/assets/uploads/service-img/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $fileName = 'studioapp' . '_up_' . time();
            $fileName = "public/assets/uploads/service-img/" . date('Y') . '/' . date('m') . '/' . $fileName;
            $image    = Input::file('updatecatimage');
            $filename = $fileName . '.' . $image->getClientOriginalExtension();
            Input::file('updatecatimage')->move($path, $filename);
            $data      = DB::table('video_category')->where('id', $rowid)->update(['service_image' => $filename]);
            $dataarray = array('catname' => $catname, 'catimage' => asset($filename), 'usertype' => $status);

        }
        return response()->json(['status' => 'success', 'data' => $dataarray]);
    }

    public function getsubcategories()
    {

        $select = array(
            'video_category.service_image',
            'video_category.id as parent_id',
            'video_category.category_name as Name',
            'sub_category.id as id',
            'sub_category.category_name',
        );
        $data = DB::table('sub_category')->select($select)->leftjoin('video_category', 'video_category.id', '=', 'sub_category.parent_id')->orderby('sub_category.id', 'DESC')->get();

        return view('subcategory')->with('result', $data);

    }

    public function getsubcategorybyid()
    {

        $id   = Input::get('id');
        $data = DB::table('sub_category')->select('id', 'category_name')->where('parent_id', $id)->orderby('sub_category.id', 'DESC')->get();
        return response()->json(['status' => 'success', 'data' => $data]);

    }

    public function AddService()
    {
        // print_r(Input::all());
        // exit;
        $Studio_id       = Input::get('Studio_id');
        $Category_id     = Input::get('Category_id');
        $Service_charges = Input::get('price');
        $service_title   = Input::get('service_title');
        $Date            = Date('Y-m-d H:i:s');
        $gender          = Input::get('gender');
        $services_image  = Input::get('services_image');
        $Description     = Input::get('Description');
        $price_discount  = Input::get('price_discount');
        $switch          = Input::get('switch');
        $Timeunit        = Input::get('Timeunit');
        $happy_from      = Input::get('happy_from');
        $happy_to        = Input::get('happy_to');
        $price_discount  = Input::get('price_discount');
        $happy_days      = Input::get('happy_days');

        if ($Timeunit == "hours") {

            $service_time = Input::get('service_time') * 60 * 60 * 1000;

        } else if ($Timeunit == "minutes") {

            $service_time = Input::get('service_time') * 60 * 1000;

        }
        $fileName = "";
        if (Input::hasFile('propic')) {

            $path = public_path() . '/assets/uploads/serviceimg/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $fileName = 'studioapp' . '_up_' . time() . '.png';
            Input::file('propic')->move($path, $fileName);
            $fileName = "public/assets/uploads/serviceimg/" . date('Y') . '/' . date('m') . '/' . $fileName;

            $path = public_path() . '/assets/uploads/serviceimg_big/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $fileName_big = 'studioapp' . '_up_' . time() . '.png';
            Input::file('services_image')->move($path, $fileName_big);
            $fileName_big = "public/assets/uploads/serviceimg_big/" . date('Y') . '/' . date('m') . '/' . $fileName_big;

        } else {
            $catimage = DB::table("video_category")->select('service_image')->where('id', $Category_id)->first();

            $fileName     = $catimage->service_image;
            $fileName_big = $catimage->service_image;

        }
        if($switch=='on'){
          $happy_from      = date('Y-m-d H:i:s',strtotime($happy_from));
          $happy_to        = date('Y-m-d H:i:s',strtotime($happy_to));
          $array_days = [];
          foreach($happy_days as $value){
            array_push($array_days,$value);
          }
          $array_days =json_encode($array_days);
        } else {
            $happy_from      = '0000-00-00 00:00:00';
            $happy_to        = '0000-00-00 00:00:00';
            $array_days      = '';
            $price_discount  = 0;
        }
        $user = DB::table('users')->select('country')->where('id',$Studio_id)->first();

        $currency_code=DB::table('currency_code_new')->select('CurrencyCode')->where('Country',$user->country)->orWhere('Country', 'like', '%' .$user->country . '%')->first();
        $symbol= '$';
        $code  = 'USD';
        if(count($currency_code)>0){
          $CurrencyCode=BasicModel::get_currencies_symbol($currency_code->CurrencyCode);
          $symbol = $CurrencyCode;
          if($symbol!='$')
            $code   = $currency_code->CurrencyCode;
        }
        DB::table('studio_services')->whereRaw('Studio_id ='.$Studio_id)->increment('service_index', 1);
        $result = DB::table('studio_services')->insertGetId(
            [   'Studio_id'           => $Studio_id,
                'Category_id'         => $Category_id,
                'service_time'        => $service_time,
                'Service_charges'     => $Service_charges,
                'service_title'       => $service_title,
                'gender'              => $gender,
                'Description'         => $Description,
                'services_image'      => $fileName,
                'service_image_big'   => $fileName_big,
                'happy_hour_from'     => $happy_from,
                'happy_hour_to'       => $happy_to,
                'service_time_unit'   => $Timeunit ,
                'discount_amount'     => $price_discount,
                'happy_hour_days'     => $array_days,
                // 'currency_code'       => $currency_code,
                'Date'                => $Date,
                'currency_code'       => $code,
                'service_index'       => 0
            ]);
        if ($result > 0) {
            return response()->json(['status' => 'success', 'message' => 'Added']);
        }

    }

    public function editService()
    {
        $service_id      = Input::get('service_id');
        $Category_id     = Input::get('Category_id');
        $Service_charges = Input::get('price');
        $service_title   = Input::get('service_title');
        $Date            = Date('Y-m-d H:i:s');
        $gender          = Input::get('gender');
        $services_image  = Input::get('services_image');
        $Description     = Input::get('Description');
        $price_discount  = Input::get('price_discount');
        $switch          = Input::get('switch');
        $Timeunit        = Input::get('Timeunit');
        $happy_from      = Input::get('happy_from');
        $happy_to        = Input::get('happy_to');
        $price_discount  = Input::get('price_discount');
        $happy_days      = Input::get('happy_days');

        if ($Timeunit == "hours") {

            $service_time = Input::get('service_time') * 60 * 60 * 1000;

        } else if ($Timeunit == "minutes") {

            $service_time = Input::get('service_time') * 60 * 1000;

        }
        if (Input::hasFile('propic')) {

            $path = public_path() . '/assets/uploads/serviceimg/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $fileName = 'studioapp' . '_up_' . time() . '.png';
            Input::file('propic')->move($path, $fileName);
            $fileName = "public/assets/uploads/serviceimg/" . date('Y') . '/' . date('m') . '/' . $fileName;

            $path = public_path() . '/assets/uploads/serviceimg_big/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $fileName_big = 'studioapp' . '_up_' . time() . '.png';
            Input::file('services_image')->move($path, $fileName_big);
            $fileName_big = "public/assets/uploads/serviceimg_big/" . date('Y') . '/' . date('m') . '/' . $fileName_big;

        } else {
            // $catimage = DB::table("video_category")->select('service_image')->where('id', $Category_id)->first();

            // $fileName     = $catimage->service_image;
            // $fileName_big = $catimage->service_image;

        }
        if($switch=='on'){
          $happy_from      = date('Y-m-d H:i:s',strtotime($happy_from));
          $happy_to        = date('Y-m-d H:i:s',strtotime($happy_to));
          $array_days = [];
          foreach($happy_days as $value){
            array_push($array_days,$value);
          }
          $array_days =json_encode($array_days);
        } else{
          $happy_from      = '0000-00-00 00:00:00';
          $happy_to        = '0000-00-00 00:00:00';
          $array_days      = '';
          $price_discount  = 0;
        }
        $data = [
                'Category_id'         => $Category_id,
                'service_time'        => $service_time,
                'Service_charges'     => $Service_charges,
                'service_title'       => $service_title,
                'gender'              => $gender,
                'Description'         => $Description,
                'happy_hour_from'     => $happy_from,
                'happy_hour_to'       => $happy_to,
                'service_time_unit'   => $Timeunit ,
                'discount_amount'     => $price_discount,
                'happy_hour_days'     => $array_days,
                'Date'                => $Date,
                // 'currency_code'       => $code,
                'service_index'       => 0
                ];
         if(isset($fileName)){
            $data['services_image']    =   $fileName;
            $data['service_image_big'] =   $fileName_big;
         }
        // $user = DB::table('users')->select('country')->where('id',$Studio_id)->first();
        // $currency_code=DB::table('currency_code_new')->select('CurrencyCode')->where('Country',$user->country)->orWhere('Country', 'like', '%' .$user->country . '%')->first();
        // $symbol= '$';
        // $code  = 'USD';
        // if(count($currency_code)>0){
        //   $CurrencyCode=BasicModel::get_currencies_symbol($currency_code->CurrencyCode);
        //   $symbol = $CurrencyCode;
        //   if($symbol!='$')
        //     $code   = $currency_code->CurrencyCode;
        // }
        // DB::table('studio_services')->whereRaw('Studio_id ='.$Studio_id)->increment('service_index', 1);
        $result = DB::table('studio_services')->where('id',$service_id)->update($data);
        if ($result > 0) {
            return response()->json(['status' => 'success', 'message' => 'Updated']);
        }

    }

    public function Addsubcategory()
    {

        $catid      = Input::get('cat');
        $subcatname = Input::get('subcatename');
        $dataarray  = ['parent_id' => $catid, 'category_name' => $subcatname];
        $result     = BasicModel::InsertData('sub_category', $dataarray);
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function Deletesubcategory()
    {

        $catid  = Input::get('catid');
        $result = BasicModel::DeleteData('sub_category', $catid, 'id');
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function Deleteworkinghours()
    {

        $id  = Input::get('id');
        $result = BasicModel::DeleteData('working_hours', $id, 'id');
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function Deleteservice()
    {

        $serviceid = Input::get('serviceid');
        $result    = BasicModel::DeleteData('studio_services', $serviceid, 'id');
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function Deletevideo()
    {

        $id     = Input::get('id');
        $result = BasicModel::DeleteData('videos', $id, 'id');
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function updatesubcategory()
    {

        $rowid       = Input::get('rowid');
        $subcatename = Input::get('subcatname');
        $parentid    = Input::get('parentid');
        $array       = ['parent_id' => $parentid, 'category_name' => $subcatename];
        $result      = BasicModel::UpdateData('sub_category', $rowid, 'id', $array);
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function createpackage()
    {

        $data = Input::all();

        if ($data['timeunit'] == 'Hours') {
            $data['time_limit'] = $data['time_limit'] * 3600000;
        }
        if ($data['timeunit'] == 'Min') {
            $data['time_limit'] = $data['time_limit'] * 60000;
        }
        $data['size_limit'] = $data['size_limit'] * 1024;
        $data['Date'] = Date('Y-m-d');
        unset($data['timeunit']);
        unset($data['size']);
        $name = str_replace(' ', '_', $data['package_name']);
        $stripe_package_id =  $name;
        $test_package_name=DB::table('packages')->where('stripe_plan_name',$stripe_package_id)->count();
        if($test_package_name>0){
            return response()->json(['status' => 'error', 'message' =>'Package Name Alreday Exits']);
        }
        $plan = $this->stripe_object->plans()->create([
            'id'                   => $stripe_package_id,
            'name'                 => $data['package_name'],
            'amount'               => $data['Price'],
            'currency'             => 'USD',
            'interval'             => 'month',
            'statement_descriptor' => 'Monthly Subscription',
        ]);
        $data['stripe_plan_name'] = $plan['id'];
        $result = BasicModel::InsertData('packages', $data);
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function deletepackage()
    {

        $catid  = Input::get('id');
        $result = BasicModel::DeleteData('packages', $catid, 'id');
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function updatepackage()
    {

        $data  = Input::all();
        $rowid = Input::get('editrowid');
        if ($data['timeunit'] == 'Hours') {
            $data['time_limit'] = $data['time_limit'] * 3600000;
        }
        if ($data['timeunit'] == 'Min') {
            $data['time_limit'] = $data['time_limit'] * 60000;
        }
        $data['size_limit'] = $data['size_limit'] * 1024;
        // $data['Date'] =Date('Y-m-d');
        unset($data['timeunit']);
        unset($data['size']);
        unset($data['editrowid']);
        $strip_package_name_id=DB::table('packages')->select('stripe_plan_name')->where('id',$rowid)->first();
        $name = str_replace(' ', '_', $data['package_name']);
        $stripe_package_id =  $name;
        $plan =$this->stripe_object->plans()->update($strip_package_name_id->stripe_plan_name, [
            'name'                 => $data['package_name'],
        ]);
        // $data['stripe_plan_name'] = $plan['id'];
        $result = BasicModel::UpdateData('packages', $rowid, 'id', $data);
        return response()->json(['status' => 'success', 'data' => $result]);
    }

    public function convertTime($ms)
    {

        $minutes = floor(($ms % 3600000) / 60000);
        return $minutes . " Min";

    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1024000) {
            $bytes = floor(number_format($bytes / 1048576, 2)) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function Addaboutus()
    {

        $text   = Input::get('text');
        $rowid  = Input::get('rowid');
        $array  = ['description' => $text];
        $result = BasicModel::UpdateData('about_us', $rowid, 'id', $array);
        return response()->json(['status' => 'success', 'data' => $result]);

    }

    public function studiolisting()
    {

        return view('studiolisting');

    }

    public function users()
    {

        return view('studiousers');

    }

    public function invoicedetails($id, $genratepdf = false)
    {

        $select = array(
            'payment_invoices.id as rowid',
            'payment_invoices.product_name as packagename',
            'payment_invoices.create_time',
            'payment_invoices.paypalid',
            'payment_invoices.amount',
            'payment_invoices.short_description',
            'users.Studio_Name as customername',
            'users.email',
            'users.contact_no',
            'payment_invoices.currency_code',
            'payment_invoices.short_description',
            'payment_invoices.currency_code',
        );
        $data              = DB::table('payment_invoices')->select($select)->leftjoin('users', 'payment_invoices.userid', '=', 'users.id')->where('payment_invoices.id', $id)->orderby('payment_invoices.id', 'DESC')->first();
        $data->create_time = explode('T', $data->create_time);
        $data->create_time = $data->create_time[0];

        if ($genratepdf != false) {
            $customername = $data->customername;
            $data         = [
                'data' => $data,
            ];
            $pdf = PDF::loadView('invoice.invoice', $data);
            return $pdf->download($customername . '.pdf');
        }

        return view('invoicedetail')->with('data', $data);

    }

    public function genratepdf($id)
    {
        // $id=Input::get('id');
        $this->invoicedetails($id, 'genratepdf');

    }

   
    public function Invoices()
    {

        $select = array(
            'payment_invoices.id as rowid',
            'payment_invoices.product_name as packagename',
            'payment_invoices.create_time',
            'payment_invoices.end_date',
            'payment_invoices.paypalid',
            'payment_invoices.amount',
            'payment_invoices.short_description',
            'users.Studio_Name as customername',

        );

        $data = DB::table('payment_invoices')->select($select)->leftjoin('users', 'payment_invoices.userid', '=', 'users.id')->orderby('payment_invoices.id', 'DESC')->get();

        // for ($i=0; $i <count($data) ; $i++) {

        //   $data[$i]->create_time =explode('T',  $data[$i]->create_time);
        //   $data[$i]->create_time = $data[$i]->create_time[0];

        //  // $data[$i]->create_time = explode(' ',  $data[$i]->create_time);

        //  // $data[$i]->create_time = $data[$i]->create_time[0];

        // }
        // return redirect('Invoices')->with('data',$data);
        return view('Invoices')->with('data', $data);

    }

    public function DueInvoices()
    {

        $startdate = date('Y-m-d');
        $query     = "SELECT * FROM(SELECT payment_invoices.id AS rowid ,payment_invoices.product_name as packagename,payment_invoices.create_time,payment_invoices.paypalid,payment_invoices.amount,payment_invoices.short_description,payment_invoices.end_date,payment_invoices.userid,users.Studio_Name as customername, 12 * (YEAR('$startdate')
          - YEAR(payment_invoices.create_time))
          + (MONTH('$startdate')
          - MONTH(payment_invoices.create_time)) AS months
          FROM payment_invoices LEFT JOIN users  ON users.id = payment_invoices.userid
           GROUP BY payment_invoices.userid
           ORDER BY `payment_invoices`.`create_time` DESC) AS payment_invoices
          WHERE months > 1";
        $data = DB::select($query);

        // dd($data);

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]->currentmonth = date('M-Y', strtotime($data[$i]->create_time));

        }

        return view('DueInvoices')->with('data', $data);

    }

    public function Invoiceswithdates($Month)
    {

        $Period = explode(' ', $Month);

        $month  = date('m', strtotime($Period[0]));
        $year   = $Period[1];
        $select = array(
            'payment_invoices.id as rowid',
            'payment_invoices.product_name as packagename',
            'payment_invoices.create_time',
            'payment_invoices.end_date',
            'payment_invoices.paypalid',
            'payment_invoices.amount',
            'payment_invoices.short_description',
            'users.Studio_Name as customername',

        );

        $whereRaw = "YEAR(payment_invoices.create_time) = " . $year . " AND Month(payment_invoices.create_time) =" . $month;

        $data = DB::table('payment_invoices')->select($select)->leftjoin('users', 'payment_invoices.userid', '=', 'users.id')->whereRaw($whereRaw)->orderby('payment_invoices.id', 'DESC')->get();

        return view('Invoices')->with('data', $data);

    }

    public function StudioMapData()
    {

        $data = User::select('Address', 'lat', 'lng', 'id', 'Studio_Name', 'contact_no', 'Average_ratings')->whereNotNull('lat')->whereRaw("users.Address !='' and users.user_type='studio_user'")->whereNotNull('Address')->get();
        return response()->json(['status' => 'success', 'data' => $data]);

    }

    public function getSearchstaffmember()
    {

        $keyword  = Input::get('keyword');
        $whereRaw = "users.user_type='normal_user'";
        if (isset($keyword)) {

            $whereRaw2 = "(";
            $whereRaw2 .= "`users`.`username`  like '%" . $keyword . "%'";
            $whereRaw2 .= " OR `users`.`email`  like '%" . $keyword . "%'";
            $whereRaw2 .= " OR `users`.`Studio_Name`  like '%" . $keyword . "%'";
            $whereRaw2 .= " )";

            $users = User::select("users")->select('id', 'Studio_Name', 'username', 'contact_no', DB::raw('IF(profile_pic IS NOT NULL, CONCAT("' . asset('') . '", profile_pic), "") AS profile_pic'), 'email')->whereRaw($whereRaw)->whereRaw($whereRaw2)->get()->toArray();

            for ($i = 0; $i < count($users); $i++) {

                if ($users[$i]['profile_pic'] == "") {

                    $users[$i]['profile_pic'] = asset('/public/assets/images/placeholder.png');

                }

            }
            return response()->json(['status' => 'success', 'data' => $users]);

        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);

        }

    }
    public function checkemail()
    {

        $email = User::where("email", Input::get('email'))->get();
        $email = count($email);

        if ($email > 0) {
            return response()->json(['email' => 1]);

        }

    }

    public function Addstaffmember()
    {

        $studio_id = Input::get('studio_id');
        $serviceid = Input::get('service_id');
        $userid    = Input::get('userid');
        $serviceid = explode(',', $serviceid);

        if (Input::has('update')) {

            $whereRaw = "staff_member.parent_id=" . $userid;
            $result   = DB::table("staff_member")->whereRaw($whereRaw)->delete();

        }

        // $whereRaw = "staff_member.parent_id=" . $userid;
        // $result   = DB::table("staff_member")->whereRaw($whereRaw)->get();

        // if (count($result) > 0) {

        //     return response()->json(['status' => 'error', 'data' => 'Alreay Added']);

        // }

        foreach ($serviceid as $key => $value) {

            $data = DB::table("staff_member")->insertGetId(['studio_id' => $studio_id, 'service_id' => $serviceid[$key], 'parent_id' => $userid]);

        }

        return response()->json(['status' => 'success']);

    }

    public function getstaffmemberinfobyid()
    {

        $parentid = Input::get('id');
        $whereRaw = 'staff_member.parent_id=' . $parentid;
        $service  = DB::table("staff_member")->select('service_id')->whereRaw($whereRaw)->get();
        return response()->json(['status' => 'success', 'service' => $service]);

    }

    public function convertTimevideo($ms)
    {
        // $sign = $ms < 0 ? "-" : "";
        $ms  = abs($ms);
        $sec = floor($ms / 1000);
        $ms  = $ms % 1000;
        $min = floor($sec / 60);
        $sec = $sec % 60;
        $hr  = floor($min / 60);
        $min = $min % 60;
        $day = floor($hr / 60);
        $hr  = $hr % 60;
        $hr  = sprintf("%02d", $hr);
        $min = sprintf("%02d", $min);
        $sec = sprintf("%02d", $sec);
        if ($hr > 0) {
            return "$hr hour $min min $sec sec";
        } elseif ($min > 0) {
            return "$min min $sec sec";
        } else {
            return "$sec sec";
        }
        // $sec = number_format($ms, 2, '.', '');

    }

    public function faq(Request $request)
    {
        $data = DB::table('faq')->select('faq.*', 'fc.category')->leftjoin('faq_categories as fc', 'fc.id', '=', 'faq.category_id')->orderby('faq.id', 'desc')->get();
        return view('faq.faqs')->with('data', $data);
    }

    public function faq_edit($id)
    {
        $data['update']     = true;
        $data['faq']        = DB::table('faq')->select('faq.*', 'fc.category', 'faq.id as faq_id')->leftjoin('faq_categories as fc', 'fc.id', '=', 'faq.category_id')->where(['faq.id' => $id])->first();
        $data['categories'] = DB::table('faq_categories')->get();
        return view('faq.add_faq')->with('data', $data);
    }

    public function faq_add_page()
    {
        $data['categories'] = DB::table('faq_categories')->orderby('id', 'desc')->get();
        return view('faq.add_faq')->with('data', $data);
    }

    public function faq_add(Request $request)
    {
        BasicModel::checkParams(['question', 'answer', 'category_id', 'type']);
        // $faq_id = $request->faq_id;
        $arr = array(
            'question'    => $request->question,
            'answer'      => $request->answer,
            'category_id' => $request->category_id,
            'type'        => $request->type,
            // 'show' => $request->show,
            'created_on'  => date('Y-m-d H:i:s'),
        );

        if ($request->faq_id) {
            $id  = DB::table('faq')->where(['id' => $request->faq_id])->update($arr);
            $msg = 'Updated';
        } else {
            $id  = DB::table('faq')->insert($arr);
            $msg = 'Added';
        }

        if ($id) {
            return response()->json(['status' => 'success', 'msg' => "FAQ $msg successfully", 'redirect' => url('/faq')]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Something went wrong']);
        }
    }

    public function faq_delete($faq_id)
    {
        $id = DB::table('faq')->where(['id' => $faq_id])->delete();
        // $id = 0;
        if ($id) {
            return response()->json(['status' => 'success', 'msg' => "Deleted Successfully", 'redirect' => url('/faq')]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Something went wrong']);
        }
    }

    public function faq_cats()
    {
        $data['categories'] = DB::table('faq_categories')->orderby('id', 'desc')->get();
        return view('faq.faq_cats')->with('data', $data);
    }

    public function faq_edit_cats($id)
    {
        $data['category']   = DB::table('faq_categories')->where('id', $id)->first();
        $data['categories'] = DB::table('faq_categories')->orderby('id', 'desc')->get();
        $data['update']     = true;
        return view('faq.faq_cats')->with('data', $data);
    }

    public function faq_cat_add(Request $request)
    {
        BasicModel::checkParams(['category']);
        $category = $request->category;
        if ($request->category_id) {
            $id  = DB::table('faq_categories')->where(['id' => $request->category_id])->update(['category' => $category]);
            $msg = 'Updated';
        } else {
            $check = DB::table('faq_categories')->where(['category' => $request->category])->first();
            if ($check) {
                return response()->json(['status' => 'error', 'msg' => 'Category already exists']);
            }
            $id  = DB::table('faq_categories')->insert(['category' => $category]);
            $msg = 'Added';
        }

        if ($id) {
            return response()->json(['status' => 'success', 'msg' => "FAQ Categories $msg successfully", 'redirect' => url('/faq_categories')]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Something went wrong']);
        }
    }
    public function remove_faq_category($category_id)
    {
        $id = DB::table('faq_categories')->where(['id' => $category_id])->delete();
        // $id = 0;
        if ($id) {
            return response()->json(['status' => 'success', 'msg' => "Deleted Successfully", 'redirect' => url('/faq_categories')]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Something went wrong']);
        }
    }

    public function cantact_us()
    {
        $data = DB::table('contact_us')->orderby('id', 'desc')->get();
        return view('contact_us')->with('data', $data);
    }

    public function notifications(Request $request)
    {  
        $notifications = DB::table('push_notifications as pn')->select('pn.*', 'nd.*', 'nd.id as notification_id')->leftjoin('notification_details as nd', 'nd.push_id', '=', 'pn.id')->whereIn('pn.type', ['video_report','new_video','Subscribed'])->orderBy('nd.id', 'desc')->get();
        // dd($notifications);
        if($request->ajax()){
            return response()->json(['status' => 'success', 'data' =>$notifications]);
        } else {
            DB::table('notification_details')->update(['is_admin_read' => 1]);
        }
        return view('notification')->with('notifications',$notifications);
    }
    
    public function upload_video_faqs(Request $request)
    {   $title       = $request->title;
        // print_r($title);
        // exit;
        if(empty($title)){
            $file        = Input::file('files');
            $fileName    = 'studioapp' . '_up_' . time() . '.' . $file->getClientOriginalExtension();
            $path        = "faqs/" . date('Y') . "/" . date('m') . '/';
            $retrivepath = BasicModel::fileupload($file, $path, $fileName, 'private');
            $fileName    = $path . $fileName;
            return response()->json(['status' => 'success', 'link' => $fileName]);
        } else {

            $file        = Input::get('file');
            $data = [
             'video_title'=> $title,
             'file'       => $file,
             'date'       =>date('Y-m-d h:i:s')
            ] ;
            $result= DB::table('faqs_videos')->Insert($data);
            return response()->json(['status' => 'success', 'data' => $result]);

        }
    }

    public function upload_video(Request $request){
        $result= DB::table('faqs_videos')->get();
        return view('faq.upload_video')->with('result',$result);
    }
    
    public function delete_faq_video($id){
        BasicModel::DeleteData('faqs_videos', $id, 'id');
        return back();
    }

    public function seo_tool(Request $request){
        $result = DB::table('pages_seo_info')->get()->toArray();
        // echo "<pre>";
        // print_r($result);
        // exit;
        return view('seo_tool')->with('result',$result);
    }

    public function update_seo_info(Request $request){

        $result = DB::table('pages_seo_info')->get()->toArray();
        $update_index=$_POST['update_index'];
        $text=array();
        foreach($result as $key=>$val){
            $val->text = json_decode($val->text,true);
            array_push($text,$val->text);
        }
        $counter=0;
        foreach($result as $key=>$val){
            $text[$counter][$update_index]=[
               'title'      =>$_POST['title'][$counter],
               'description'=>$_POST['description'][$counter],
               'keywords'   =>$_POST['keywords'][$counter],
            ];
            $counter++;
        }
            
        foreach($result as $key=>$val){
            $string =json_encode($text[$key]);
            $string = str_replace(array('[',']'),'',$string);
            $array =[
                'text'=>$string
            ];
            BasicModel::UpdateData('pages_seo_info',$val->id, 'id', $array);
        }
        echo json_encode(array("success" =>"Updated",'reload'=>true));
        exit;

    }

    public function translate(Request $request){
            $data = Input::get('data');
            $data = json_decode($data);
            $arr =[
                'cate_name'=>'',
                'description'=>''
            ];
            foreach($data as $key=>$val){
                if($val!=""){
                    $val= BasicModel::requestTranslation('en','de',$val);
                    $arr[$key]=utf8_encode($val);
                }
            }
            echo json_encode(array("status" =>"success",'data'=>$arr));
            exit;
      
    }

    public function translations(Request $request){
        $result = DB::table('translations')->orderby('last_updated_date', 'desc')->limit(1)->get()->toArray();
        // echo "<pre>";
        // print_r($result);
        // exit;
        return view('translations')->with('result',$result);
    }

    public function Update_translations(Request $request){
        $result = DB::table('translations')->orderby('last_updated_date', 'desc')->limit(1)->get()->toArray();
        $update_index=$_POST['update_index'];
        $text_en=array();
        $text_de=array();
        for($i=0;$i<count($result);$i++){
            array_push($text_en,json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','', $result[$i]->content_en), true ));
            array_push($text_de,json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','', $result[$i]->content_de), true )); 
         }
        $counter=0;
        foreach($text_en[0][$update_index] as $key=>$val){
        //     foreach($val as $keys=>$value){
                // if(isset($_POST[$keys])){
                //     $text_en[$counter][$update_index][$keys]=$_POST[$keys];
                //     $text_de[$counter][$update_index][$keys]=$_POST[$keys.'_de'];
                // }
                // $counter++;
            //     if(isset($_POST[$keys])){
            //       echo $text_en[$counter][$update_index][$keys]."<br>";
            //     }
            // }
            // echo "<pre>";
            // print_r($val);
            // echo $key.'->'.$val.'<br>';
                $dast_key = str_replace(" ","_",$key); 
                $text_en[$counter][$update_index][$key]=$_POST[$dast_key];
                $text_de[$counter][$update_index][$key]=$_POST[$dast_key.'_de'];
        }
        // // echo "<pre>";
        // // print_r($_POST);
        // echo "<pre>";
        // print_r($text_en[0]);
        // exit;
        
        $array = [
           'last_updated_date'=>date('Y-m-d H:i:s'),
           'content_en'       =>json_encode($text_en[0],JSON_PRETTY_PRINT),
           'content_de'       =>json_encode($text_de[0],JSON_PRETTY_PRINT)
        ];
        BasicModel::InsertData('translations',$array);
        file_put_contents('./translations/content_de.json',json_encode($text_de[0],JSON_PRETTY_PRINT));
        file_put_contents('./translations/content_en.json',json_encode($text_en[0],JSON_PRETTY_PRINT));
        echo json_encode(array("success" =>"Updated",'reload'=>true));
        exit;
    }

    public function add_word($index,$eng_word,$de_word){
     
        $result = DB::table('translations')->orderby('last_updated_date','desc')->limit(1)->get()->toArray();
        $text_en=array();
        $text_de=array();
        for($i=0;$i<count($result);$i++){
            array_push($text_en,json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','', $result[$i]->content_en), true ));
            array_push($text_de,json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','', $result[$i]->content_de), true )); 
         }
        $counter=0;
        $eng_word_index=str_replace(' ', '_', $eng_word);
        $text_en[0][$index][$eng_word_index] = $eng_word;
        $text_de[0][$index][$eng_word_index] = $de_word;
        $array = [
           'last_updated_date'=>date('Y-m-d H:i:s'),
           'content_en'       =>json_encode($text_en[0],JSON_PRETTY_PRINT),
           'content_de'       =>json_encode($text_de[0],JSON_PRETTY_PRINT)
        ];
        BasicModel::InsertData('translations',$array);
        echo json_encode(array("success" =>"Added",'reload'=>true));
        exit;

    }

    
}
