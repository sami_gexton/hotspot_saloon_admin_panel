<?php

namespace App\Http\Controllers;

use App\BasicModel;
use App\Http\Controllers\Controller;
use App\User;
use App\StudioAppointments;
use DateTime;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UserprofileController extends Controller
{

    public $inputs      = array();
    public $headerarray = array();
    public $lng         = '';

    public function __construct(Request $request)
    {   
       
            @BasicModel::api_logs($request);
            $this->headerarray['method'] = $request->path();
         if ($this->headerarray['method']!='subscribed_channels' && $this->headerarray['method']!='getuservideos' && $this->headerarray['method']!='liked_videos_by_user') {
            $query ="DELETE FROM `api_logs` WHERE _id NOT IN ( SELECT _id FROM (SELECT _id FROM `api_logs` ORDER BY _id DESC LIMIT 1000 ) foo )";
             @DB::select($query);
         }
        
        // $this->headerarray['method'] = $request->path();
        $this->headerarray['REMOTE_ADDR']=$_SERVER['REMOTE_ADDR'];
       
        // $this->headerarray['method'] = $request->path();
        
        //  if ($this->headerarray['method']!='get_notifications') {
        //      $dataarray = ['ip_address' =>$_SERVER['REMOTE_ADDR'],'data' =>$request];
        //      $result    = BasicModel::InsertData('api_logs', $dataarray);
             
        //  }
        // print_r();
        // exit;
        $this->headerarray['lng']      = $request->header('lng');
        $this->headerarray['username'] = $request->header('PHP_AUTH_USER');
        $this->headerarray['password'] = $request->header('PHP_AUTH_PW');

        if($this->headerarray['lng']!='en' && !empty($this->headerarray['lng'])){
            $this->lng='_de';
        }

        if ($request->header('usertype') != "") {

            $this->inputs['usertype'] = $request->header('usertype');
        } else {

            echo json_encode(['status' => 'error', 'message' => 'Unauthorized action']);
            exit;
        }

        $this->inputs['registration_platform']  = "";
        $this->inputs['appname']                = "";
        $this->inputs['registration_device_id'] = "";

        $platform    = $request->header('platform');
        $appname     = $request->header('appname');
        $deviceToken = $request->header('deviceToken');
        if ($deviceToken != "") {

            $this->inputs['registration_device_id'] = $request->header('deviceToken');
        }

        if ($platform != "") {
            $this->inputs['registration_platform'] = $request->header('platform');
        }

        if ($appname != "") {
            $this->inputs['appname'] = $request->header('appname');
        }

        if ($request->header('token') != "") {

            Input::merge(['token' => $request->header('token')]);

            $this->middleware('jwt.auth', ['except' => ['authenticate']]);
        } else {
            if ($this->headerarray['username'] != "hotspot" || $this->headerarray['password'] != "admin123!@#.") {

                echo json_encode(['status' => 'error', 'message' => 'Unauthorized action']);
                exit;
            }
        }
    }

    public function postupdateprofile()
    {
        $id       = Input::get('id');
        $name     = Input::get('name');
        $phone    = Input::get('phone');
        $address  = Input::get('address');
        $lat      = Input::get('lat');
        $lng      = Input::get('lng');
        $username = Input::get('username');
        $country  = Input::get('country');
        BasicModel::checkParams(['id']);
        $inserdata=[];
        if (isset($username) && !empty($username)) {
            $checkusername = User::where("username", $username)->get();
            $checkusername = count($checkusername);
            if ($checkusername > 0) {
                return response()->json(['status' => 'error', 'message' => 'Username already exist, try another name.']);
            } else {

                DB::table('users')->where('id', $id)->update(['username' => $username]);
            }
        }
        if (!empty($phone)) {
            $inserdata['contact_no'] = $phone;
        }
        if (!empty($lat) && !empty($lng)) {
            $inserdata['lat'] = $lat;
            $inserdata['lng'] = $lng;
        }
        if (!empty($name)) {
            $inserdata['Studio_Name'] = $name;
        }
        if (!empty($address)) {
            $inserdata['Address'] = $address;
            
        }
        if(!empty($country)){
            $inserdata['country']     =  $country;
        } else {
            $locationdata = BasicModel::location();
            $inserdata['country']     =  $locationdata['location'];
        }
            $filename='';
            if (Input::hasfile('propic')) {
                $path = public_path() . '/assets/uploads/images/propic/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $fileName = 'studioapp' . '_up_' . time();
     
                $fileName = "public/assets/uploads/images/propic/" . date('Y') . '/' . date('m') . '/' . $fileName;
                $image_mime=getimagesize($_FILES["propic"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);

                $extension='.png';
                if(isset($path_parts)){
                    $extension=$path_parts;
                }
                $filename = $fileName.$extension;
                Input::file('propic')->move($path,$filename);
                $inserdata['profile_pic'] = $filename;
            }
            $filename_big='';
            if (Input::hasfile('propic_big')) {
                $path = public_path() . '/assets/uploads/images/profile_pic_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $filename_big = 'studioapp' . '_up_' . time();
                $filename_big = "public/assets/uploads/images/profile_pic_big/" . date('Y') . '/' . date('m') . '/' .  $filename_big;
                $image_mime=getimagesize($_FILES["propic_big"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
            
                $extension='.png';
                if(isset($path_parts)){
                    $extension= $path_parts;
                }
                $filename_big = $filename_big . $extension;
                Input::file('propic_big')->move($path,  $filename_big);
                $inserdata['profile_pic_big'] = $filename_big;
            }
            $data = DB::table('users')->where('id',$id)->update($inserdata);

            if ($data > 0) {
                if($filename!=""){
                    $filename = asset($filename);
                }
                if($filename_big!=""){
                    $filename_big = asset($filename_big);
                }
                return response()->json(['status' => 'success', 'message' => 'updated', 'imagepath' =>$filename,'imagepath_big' =>$filename_big]);
            } else {
                return response()->json(['status' => 'error', 'message' => "You don't make any changes"]);
            }
       
    }

    public function postupdateworkinghours()
    {
        $day  = Input::get('day');
        $from = Input::get('from');
        $to   = Input::get('to');
        $id   = Input::get('id');
        $is_daylight_saving   = Input::get('is_daylight_saving');
        $lunch_time_to        = Input::get('lunch_time_to');
        $lunch_time_from      = Input::get('lunch_time_from');
        if (isset($day) && isset($from) && isset($to) && isset($id)) {
            $data = ['studio_id' => $id, 'working_Days' => $day, 'working_start' => $from, 'working_end' => $to];
            if(isset($is_daylight_saving)){

                $data['is_daylight_saving'] = $is_daylight_saving;
            }
                $data['lunch_time_to']   = '00:00:00';
                $data['lunch_time_from'] = '00:00:00';
        if (isset($lunch_time_to) && isset($lunch_time_from)) {
                $data['lunch_time_to']    = $lunch_time_to;
                $data['lunch_time_from']  = $lunch_time_from;
           } 
           
            $result = DB::table('working_hours')->insertGetId($data);
            if ($result > 0) {
                $dataarray = array('rowid' => $result, 'day' => $day, 'from' => $from, 'to' => $to,'lunch_time_to'=>$data['lunch_time_to'],'lunch_time_from'=>$data['lunch_time_from']);
                if(isset($is_daylight_saving)){
                    if($is_daylight_saving>0){
                    $dataarray['is_daylight_saving'] = '1';
                } else {
                    $dataarray['is_daylight_saving'] = '0';
                    }
                } 

                return response()->json(['status' => 'success', 'message' => 'updated', 'data' => $dataarray]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function postdeleteworkinghours()
    {

        $id = Input::get('id');
        if (isset($id)) {

            $result = DB::table('working_hours')->where('id', $id)->delete();

            if ($result > 0) {

                return response()->json(['status' => 'success', 'message' => 'Deleted']);
            } else {

                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function postcategories()
    {

        $usertype         = 'studio';
        $dataarray        = array();
        $subcatgoriesids  = array();
        $subcatgoriesName = array();
      
        $data = DB::select("SELECT `video_category`.id AS catogory_id,`video_category`.`category_name$this->lng` AS catogory_name,GROUP_CONCAT(`sub_category`.`id`) AS `sub_cat_ids`,
            GROUP_CONCAT(`sub_category`.`category_name` ) AS `sub_cat_names` FROM `video_category`
            LEFT JOIN `sub_category` ON `sub_category`.`parent_id`= `video_category`.`id`  where video_category.status = '$usertype'
            GROUP BY `video_category`.`id`");

        // $data = $this->convertStdToArray($data);

        foreach ($data as $key => $value) {

            $data[$key]->sub_cat_ids   = explode(",",  $value->sub_cat_ids);
            $data[$key]->sub_cat_names = explode(",",  $value->sub_cat_names);
        }

        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function getStudiosmap()
    {
        $origLat = Input::get('lat');
        $origLon = Input::get('lng');
        if (!isset($origLat) && !isset($origLon)) {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }

        $dist      = 31.0686;
        $tableName = "users";
        // This is the maximum distance (in miles) away from $origLat, $origLon in which to search
        $query = "SELECT id as studio_id,Studio_Name, profile_pic, lat, lng,Average_ratings, 3956 * 2 *
            ASIN(SQRT( POWER(SIN(($origLat - lat)*pi()/180/2),2)
            +COS($origLat*pi()/180 )*COS(lat*pi()/180)
            *POWER(SIN(($origLon-lng)*pi()/180/2),2)))
            as distance FROM $tableName WHERE
            lng between ($origLon-$dist/cos(radians($origLat))*69)
            and ($origLon+$dist/cos(radians($origLat))*69)
            and lat between ($origLat-($dist/69))
            and ($origLat+($dist/69))  and users.user_type='studio_user'
            having distance < $dist ORDER BY distance";

        $data = DB::select($query);
        $data = $this->convertStdToArray($data);

        if ($data != []) {

            foreach ($data as $key => $value) {

                $dataarray[$key]['studio_id']   = $data[$key]['studio_id'];
                $dataarray[$key]['Studio_Name'] = $data[$key]['Studio_Name'];
                $dataarray[$key]['ratings']     = $data[$key]['Average_ratings'];
                $dataarray[$key]['lat']         = $data[$key]['lat'];
                $dataarray[$key]['lng']         = $data[$key]['lng'];

                $services = DB::table('studio_services')->select('service_time', 'Service_charges', 'Service_charges', 'gender', 'Category_id', 'SubCategory_id')->where('Studio_id', $data[$key]['studio_id'])->get();

                $services = $this->convertStdToArray($services);

                if (isset($services[0]['service_time'])) {

                    $dataarray[$key]['service_time']    = $services[0]['service_time'];
                    $dataarray[$key]['Service_charges'] = $services[0]['Service_charges'];
                    $dataarray[$key]['gender']          = $services[0]['gender'];
                } else {

                    $dataarray[$key]['service_time']    = "";
                    $dataarray[$key]['Service_charges'] = "";
                    $dataarray[$key]['gender']          = "";
                }

                if (isset($services[0]['Category_id'])) {
                    $categories                      = DB::table('video_category')->select('id', "category_name$this->lng as category_name")->where('id', $services[0]['Category_id'])->get();
                    $categories                      = $this->convertStdToArray($categories);
                    $dataarray[$key]['categoryName'] = $categories[0]['category_name'];
                } else {

                    $dataarray[$key]['categoryName'] = "";
                }

                if (isset($services[0]['SubCategory_id'])) {
                    $sub_category = DB::table('sub_category')->select('id', 'category_name')->where('id', $services[0]['SubCategory_id'])->get();
                    $sub_category = $this->convertStdToArray($sub_category);

                    $dataarray[$key]['bodypart'] = $sub_category[0]['category_name'];
                } else {

                    $dataarray[$key]['bodypart'] = "";
                }
                $whereraw = "is_pro=1";

                $videos = DB::table('videos')->leftjoin('video_category', 'video_category.id', '=', 'videos.Video_category_id')->select('videos.*', "video_category.category_name$this->lng as category")->whereRaw($whereraw)->where('studio_id', $data[$key]['studio_id'])->first();

                // $videos=$this->convertStdToArray($videos);
                $BasicModel = new BasicModel();

                if (isset($videos->Video_link)) {

                    $dataarray[$key]['video']          = $videos;
                    $dataarray[$key]['video']->videoid = $dataarray[$key]['video']->id;
                    // $dataarray[$key]['video']->Video_link = $dataarray[$key]['video']->Video_link;
                    $dataarray[$key]['video']->Video_link     = "";
                    $dataarray[$key]['video']->thumbnail      = $BasicModel->s3_baseurl . $dataarray[$key]['video']->thumbnail;
                    $dataarray[$key]['video']->profilepic     = asset('') . $data[$key]['profile_pic'];
                    $dataarray[$key]['video']->Averageratings = $data[$key]['Average_ratings'];
                } else {

                    $dataarray[$key]['video'] = "";
                }
            }
            return response()->json(['status' => 'success', 'data' => $dataarray]);
        } else {

            return response()->json(['status' => 'error', 'message' => 'data not found']);
        }
    }

    public function postupdateservices()
    {
        BasicModel::checkParams(['categoryid', 'gender', 'price', 'studioid', 'duration', 'service_title']);
        $category          = Input::get('categoryid');
        $subcategory       = null;
        $time              = Input::get('duration');
        $gender            = Input::get('gender');
        $price             = Input::get('price');
        $decribtion        = Input::get('decribtion');
        $id                = Input::get('studioid');
        $service_title     = Input::get('service_title');
        $happy_hour_from   = Input::get('happy_hour_from');
        $happy_hour_to     = Input::get('happy_hour_to');
        $discount_amount   = Input::get('discount_amount');
        $service_time_unit = Input::get('service_time_unit');
        $happy_hour_days   = Input::get('happy_hour_days');
        $currency_code     = Input::get('currency_code');
        $fileName = "";
        $fileName_big="";
        $arr = array(
            'Studio_id'       => $id,
            'Category_id'     => $category,
            'service_time'    => $time,
            'Service_charges' => $price,
            'SubCategory_id'  => $subcategory,
            'gender'          => $gender,
            'Description'     => $decribtion,
            'service_title'   => $service_title,
            'service_index'   => 0,
        );
        if(!empty($happy_hour_from) && !empty($happy_hour_to) && !empty($discount_amount)){
            $arr['happy_hour_from']      = $happy_hour_from;
            $arr['happy_hour_to']        = $happy_hour_to;
            $arr['discount_amount']      = $discount_amount;
            $data_array=[];
            if(!empty($happy_hour_days)){
                foreach($happy_hour_days as $value) {
                    array_push($data_array,$value);
                }
            $arr['happy_hour_days']      = json_encode($data_array);   
            }
        }
        $arr['currency_code'] ='USD';
        if(!empty($currency_code)){
            $arr['currency_code']          =  $currency_code;
        }
        if(!empty($service_time_unit)){
            $arr['service_time_unit']      =  $service_time_unit;
        }
        
        if(Input::hasFile('service_image')){
        if ($_FILES["service_image"]["size"]>0) {

            $path = public_path() . '/assets/uploads/serviceimg/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            // $path_parts =pathinfo($_FILES["service_image"]["name"]);
            $image_mime=getimagesize($_FILES["service_image"]["tmp_name"]);
            $path_parts=image_type_to_extension($image_mime[2]);
            $extension='.png';
            if(isset($path_parts)){
                $extension=$path_parts;
            }
            $fileName = 'studioapp' . '_up_' . time().$extension;
            Input::file('service_image')->move($path, $fileName);
            $fileName = "public/assets/uploads/serviceimg/" . date('Y') . '/' . date('m') . '/' . $fileName;
            $arr['services_image']  = $fileName;
            if(Input::hasFile('service_image_big')){
                $path = public_path() . '/assets/uploads/service_image_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                // $path_parts =pathinfo($_FILES["service_image"]["name"]);
                $image_mime=getimagesize($_FILES["service_image_big"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
                $extension='.png';
                if(isset($path_parts)){
                    $extension=$path_parts;
                }
                $fileName_big = 'studioapp' . '_up_' . time().$extension;
                Input::file('service_image_big')->move($path, $fileName_big);
                $fileName_big = "public/assets/uploads/service_image_big/" . date('Y') . '/' . date('m') . '/' . $fileName_big;
                $arr['service_image_big']=$fileName_big;
         }
        }
        } else {
                $catimage = DB::table("video_category")->select('service_image')->where('id', $category)->first();
                $arr['services_image'] = $catimage->service_image;
                $arr['service_image_big'] = $catimage->service_image;
        }
        DB::table('studio_services')->whereRaw('Studio_id ='.$id)->increment('service_index', 1);
        $result = DB::table('studio_services')->insertGetId($arr);
        if ($result > 0) {
            $arr['rowid'] = $result;
                if(isset($arr['services_image'])){
                    $arr['services_image']     =  asset('').$arr['services_image'];
                }else{

                    $arr['services_image']  = "";
                }
                if(isset($arr['service_image_big'])){
                    $arr['services_image_big'] = asset('').$arr['service_image_big'];
                }else {

                    $arr['services_image_big'] = "";
                }
            unset($arr['service_image_big']);
            $data = DB::table('video_category')->select('category_name'.$this->lng .' as category_name')->where('id',$arr['Category_id'])->first();
            
            $arr['categoryName'] = $data->category_name;
            return response()->json(['status' => 'success', 'message' => 'updated','data'=> $arr]);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
        }
    }

    public function postdeleteservices()
    {
        $id = Input::get('id');

        if (isset($id)) {

            $result = DB::table('studio_services')->where('id', $id)->delete();

            if ($result > 0) {

                return response()->json(['status' => 'success', 'message' => 'Deleted']);
            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function getStudioDetails()
    {
        $id     = Input::get('studioid');
        $userid = Input::get('userid');

        if (isset($id)) {
            $data = DB::table("users")->select('contact_no', 'Address', 'cover_pic', 'working_Days', 'working_start', 'working_end', 'Average_ratings', 'profile_pic')->leftjoin('working_hours', 'working_hours.studio_id', '=', 'users.id')->where('users.id', $id)->get();
            $data = $this->convertStdToArray($data);
            $result = DB::table('studio_reviews')->select('comments', 'ratings')->where('user_id', $userid)->get();

            $result = $this->convertStdToArray($result);

            if ($data != []) {

                $dataarray['studio_id']     = $id;
                $dataarray['Contact']       = $data[0]['contact_no'];
                $dataarray['Address']       = $data[0]['Address'];
                $dataarray['working_Days']  = $data[0]['working_Days'];
                $dataarray['working_start'] = $data[0]['working_start'];
                $dataarray['working_end']   = $data[0]['working_end'];
                if ($data[0]['cover_pic'] != "") {
                    $dataarray['cover_pic'] = asset($data[0]['cover_pic']);
                } else {
                    $dataarray['cover_pic'] = "";
                }

                if ($data[0]['profile_pic'] != "") {
                    $dataarray['profile_pic'] = asset($data[0]['profile_pic']);
                } else {
                    $dataarray['profile_pic'] = "";
                }
                $dataarray['Average_ratings'] = $data[0]['Average_ratings'];

                if (isset($result[0]['comments'])) {
                    $dataarray['usercomment'] = $result[0]['comments'];
                } else {

                    $dataarray['usercomment'] = "";
                }

                if (isset($result[0]['ratings'])) {
                    $dataarray['userratings'] = $result[0]['ratings'];
                } else {

                    $dataarray['userratings'] = "";
                }
            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }

            return response()->json(['status' => 'success', 'data' => $dataarray]);
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function getSearchUser()
    {

        $keyword  = Input::get('keyword');
        $whereRaw = "users.user_type='normal_user'";
        if (isset($keyword)) {

            $whereRaw2 = "(";
            $whereRaw2 .= "`users`.`username`  like '%" . $keyword . "%'";
            $whereRaw2 .= " OR `users`.`email`  like '%" . $keyword . "%'";
            $whereRaw2 .= " OR `users`.`Studio_Name`  like '%" . $keyword . "%'";
            $whereRaw2 .= " )";

            $users = User::select("users")->select('id', 'Studio_Name', 'username', 'contact_no', DB::raw('IF(profile_pic IS NOT NULL, CONCAT("' . asset('') . '", profile_pic), "") AS profile_pic'),DB::raw('IF(profile_pic_big IS NOT NULL, CONCAT("' . asset('') . '", profile_pic_big), "") AS profile_pic_big'), 'email')->whereRaw($whereRaw)->whereRaw($whereRaw2)->get()->toArray();
            return response()->json(['status' => 'success', 'data' => $users]);
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function getAddstaffmember()
    {
        BasicModel::checkParams(['studio_id', 'service_id', 'userid', 'remove']);
        $studio_id = Input::get('studio_id');
        $serviceid = Input::get('service_id');
        $userid    = Input::get('userid');
        $remove    = Input::get('remove');

        // if ($remove == "1") {
        $deleted_data = DB::table("staff_member")->where(['studio_id' => $studio_id, 'parent_id' => $userid])->delete();
        // }
        foreach ($serviceid as $key => $value) {

            $data = DB::table("staff_member")->insertGetId(['studio_id' => $studio_id, 'service_id' => $serviceid[$key], 'parent_id' => $userid]);
        }
        $dataarray = array();

        if ($data > 0) {
            $select = array(
                'users.contact_no as contact',
                'users.Studio_Name as name',
                DB::raw('CONCAT("' . asset('') . '/",users.profile_pic) as profile_pic'),
                'staff_member.id as member_staffrowid',
                'staff_member.parent_id as parent_id',
                'users.device_token',
                'staff_member.status',
                DB::raw('GROUP_CONCAT(DISTINCT video_category.id) as member_category_id'),
            );
            

            $data2 = DB::table("users")->select($select)->selectRaw("GROUP_CONCAT(DISTINCT video_category.category_name$this->lng) as categoryName")->selectRaw('GROUP_CONCAT(DISTINCT sub_category.category_name) as subcategory')->leftjoin('staff_member', 'staff_member.parent_id', '=', 'users.id')->leftjoin('studio_services', 'staff_member.service_id', '=', 'studio_services.id')->leftjoin('video_category', 'studio_services.Category_id', '=', 'video_category.id')->leftjoin('sub_category', 'studio_services.SubCategory_id', '=', 'sub_category.id')->where('staff_member.parent_id', $userid)->groupby('staff_member.parent_id')->first();
            if (count($data2) > 0) {
                // if ($remove == "0") {
                $studio           = DB::table('users')->select('Studio_Name as name')->where('id', '=', $studio_id)->first();
                $title            = "Request For Being Staff Member";
                $body             = $studio->name . " has added you as their staff member do you accept that ?";
                $deviceToken      = array($data2->device_token);
                $data2->studio_id = $studio_id;
                $p_arr            = array(
                    'title'     => $title,
                    'body'      => $body,
                    'push_data' => json_encode($data2),
                    'type'      => 'new_staff',
                );
                $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
                $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $userid, 'device_token' => $data2->device_token, 'is_sent' => 1]);
                BasicModel::pushNotification($title, $body, $deviceToken, 'new_staff', $data2);
                // }
                return response()->json(['status' => 'success', 'data' => $data2]);
            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {
            return response()->json(['status' => 'error', 'message' => "data not found"]);
        }
    }

    public function acceptStaffRequest()
    {
        BasicModel::checkParams(['studio_id', 'userid', 'status', 'push_id']);
        $studio_id = Input::get('studio_id');
        $status    = Input::get('status');
        $userid    = Input::get('userid');
        $push_id   = Input::get('push_id');

        $user       = DB::table('users')->where('id', '=', $userid)->first();
        $studio     = DB::table('users')->select('device_token')->where('id', '=', $studio_id)->first();
        $checkStaff = DB::table("staff_member")->where(['studio_id' => $studio_id, 'parent_id' => $userid, 'status' => 1])->get();
        if (is_object($checkStaff) && count($checkStaff) > 0) {
            DB::table('notification_details')->where('id', $push_id)->update(['is_deleted' => 1]);
            return response()->json(['status' => 'error', 'message' => "You are already activated"]);
        } else {
            DB::table('notification_details')->where('id', $push_id)->update(['is_deleted' => 1]);
            if ($status == "1") {
                $data  = DB::table("staff_member")->where(['studio_id' => $studio_id, 'parent_id' => $userid])->update(['status' => 1]);
                $title = $user->Studio_Name . " added as your Staff Member";
                $body  = $user->Studio_Name . " has accepted being your staff member";

                $p_arr = array(
                    'title'     => $title,
                    'body'      => $body,
                    'push_data' => '',
                    'type'      => 'staff_response',
                );
                $push_id = DB::table('push_notifications')->insertGetId($p_arr);
                DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $studio_id, 'device_token' => $studio->device_token, 'is_sent' => 1]);

                BasicModel::pushNotification($title, $body, array($studio->device_token), $type = 'staff_response');
                return response()->json(['status' => 'success', 'message' => "You are activated successfully"]);
            } else {
                $data = DB::table("staff_member")->where(['studio_id' => $studio_id, 'parent_id' => $userid])->delete();

                $title = $user->Studio_Name . " removed as your Staff Member";
                $body  = $user->Studio_Name . " has rejected being your staff member";

                $p_arr = array(
                    'title'     => $title,
                    'body'      => $body,
                    'push_data' => '',
                    'type'      => 'staff_response',
                );
                $push_id = DB::table('push_notifications')->insertGetId($p_arr);
                DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $studio_id, 'device_token' => $studio->device_token, 'is_sent' => 1]);

                BasicModel::pushNotification($title, $body, array($studio->device_token), $type = 'staff_response');

                return response()->json(['status' => 'success', 'message' => "You have rejected successfully"]);
            }
        }

    }

    public function getStudioworkinghours()
    {
        $studio_id = Input::get('studio_id');
        $whereRaw  = "working_hours.studio_id=" . $studio_id;
        if (isset($studio_id)) {
            $dataarray = array();
            $result = DB::table("working_hours")->select('id as rowid','working_Days as day','working_start as from','working_end as to','is_daylight_saving','lunch_time_to','lunch_time_from')->whereRaw($whereRaw)->get();
            // $result = $this->convertStdToArray($result);

            // if (count($result) > 0) {
            //     foreach ($result as $key => $value) {

            //         $dataarray[$key]['rowid']                 = $value['id'];
            //         $dataarray[$key]['day']                   = $value['working_Days'];
            //         $dataarray[$key]['from']                  = $value['working_start'];
            //         $dataarray[$key]['to']                    = $value['working_end'];
            //         $dataarray[$key]['is_daylight_saving']    = $value['is_daylight_saving'];
            //     }

                return response()->json(['status' => 'success', 'data' => $result]);
            // } else {

            //     return response()->json(['status' => 'error', 'message' => "data not found"]);
            // }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function getStudioServices(Request $request)
    {
        $studio_id = Input::get('studio_id');
        $select = array(
            'studio_services.service_time',
            'studio_services.Service_charges',
            'studio_services.gender',
            'studio_services.happy_hour_to',
            'studio_services.happy_hour_from',
            'studio_services.discount_amount',
            'studio_services.Studio_id',
            'studio_services.Description',
            'studio_services.service_title',
            'studio_services.happy_hour_days',
            'studio_services.currency_code',
            "video_category.category_name$this->lng as categoryName",
            'studio_services.id as rowid',
            DB::raw('IF(studio_services.services_image IS NOT NULL, CONCAT("' . asset('') . '",studio_services.services_image), "") AS services_image'),
            DB::raw('IF(studio_services.service_image_big IS NOT NULL, CONCAT("' . asset('') . '",studio_services.service_image_big), "") AS services_image_big'),
        );
        $whereRaw = 'studio_services.Studio_id=' . $studio_id;
        if (isset($studio_id)) {
            $data = DB::table("studio_services")->select($select)
                ->leftjoin('video_category', 'video_category.id', '=', 'studio_services.Category_id')->whereRaw($whereRaw)->orderByRaw('service_index ASC')->get();
            $user_data= User::select('time_zone')->where('id',$studio_id)->first();
            foreach($data as $key => $value){
                if(!empty($user_data->time_zone)){
                    date_default_timezone_set($user_data->time_zone);
                  }
                $days = [];  
                if(!empty($value->happy_hour_days)){
                    $days  = json_decode($value->happy_hour_days);
                }
                $today = Date('l');
                $start_date_hour_minutes    = date('H:i:s', strtotime($value->happy_hour_from));
                $end_date_hour_minutes      = date('H:i:s', strtotime($value->happy_hour_to));
                $now_date_hour_minutes      = date('H:i:s');
                $data[$key]->is_discount_show = 'false';
                if($start_date_hour_minutes<=$now_date_hour_minutes && $end_date_hour_minutes>=$now_date_hour_minutes && in_array($today,$days)) {
                    $data[$key]->is_discount_show = 'true';
                } 
            
             }
                // $currency=BasicModel::mulity_currency_with_rate();
                // $currency_code  = $currency->currencyCode;
                // $currency_rate = $currency->coverted_currency;
                // if($currency_code!="USD" && $currency_code!="AUD" && $currency_code!="CHF" &&  $currency_code!="EUR" && $currency_code!="GBP" ){
                $currency_rate = 1;
                $currency_code = '';
                if(isset($data[0]->currency_code)){
                    $currency_code=  $data[0]->currency_code;
                }

                // }
            $user_data= User::select('time_zone')->where('id',$studio_id)->first();
            if(!empty($user_data->time_zone)){
                date_default_timezone_set($user_data->time_zone);
                }
            $is_day_light_service = DB::table('working_hours')->where('working_Days',date('l'))->whereRaw('studio_id ='. $studio_id)->count();

            $check_bank_account_details =  DB::table('bank_account_details')->where('studio_id',$studio_id)->count();
            $bank_account_details ='hide';
            if($check_bank_account_details==0){
                $bank_account_details ='show';
            }
            if ($data != "") {
                return response()->json(['status' => 'success', 'data' => $data,'currency_rate'=>$currency_rate,'currency_code'=>$currency_code,'is_day_light_service'=>$is_day_light_service,'bank_account_details'=>$bank_account_details]);
            } else {
                return response()->json(['status' => 'error', 'message' => "Data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Studio ID Missing']);
        }
    }

    public function deleteStaffMember()
    {

        $parenrid = Input::get('parentid');

        if (isset($parenrid)) {

            $result = DB::table('staff_member')->where('parent_id', $parenrid)->delete();

            if ($result > 0) {

                return response()->json(['status' => 'success', 'message' => 'Deleted']);
            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function getStaffMember()
    {

        $id = Input::get('studio_id');
        if (isset($id)) {
            $dataarray = array();
            $select    = array(
                'users.contact_no as contact',
                'users.Studio_Name as name',
                DB::raw('CONCAT("' . asset('') . '/",users.profile_pic) as profile_pic'),
                DB::raw('CONCAT("' . asset('') . '/",users.profile_pic_big) as profile_pic_big'),
                'staff_member.id as member_staffrowid',
                'staff_member.status',
                'staff_member.parent_id as parent_id',
                DB::raw('GROUP_CONCAT(video_category.id) as member_category_id'),
            );

            $data = DB::table("users")->select($select)->selectRaw("GROUP_CONCAT(DISTINCT video_category.category_name$this->lng) as categoryName")->leftjoin('staff_member', 'staff_member.parent_id', '=', 'users.id')->leftjoin('studio_services', 'staff_member.service_id', '=', 'studio_services.id')->leftjoin('video_category', 'studio_services.Category_id', '=', 'video_category.id')->leftjoin('sub_category', 'studio_services.SubCategory_id', '=', 'sub_category.id')->where('staff_member.studio_id', $id)->groupby('staff_member.parent_id')->get();

            // $data=DB::table("users")->select($select)->selectRaw('GROUP_CONCAT(categories.Name) as member_category')->selectRaw('GROUP_CONCAT(sub_category.category_name) as member_subcategory')->leftjoin('staff_member','staff_member.parent_id','=','users.id')->leftjoin('categories','staff_member.service_category','=','categories.id')->leftjoin('sub_category','staff_member.service_subcategory','=','sub_category.id')->where('staff_member.studio_id', $id)->groupby('staff_member.parent_id')->get();

            // $data = $this->convertStdToArray($data);

            if (is_object($data) && count($data) > 0) {
                return response()->json(['status' => 'success', 'data' => $data]);
            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => "Parameter Missing"]);
        }
    }

    public function postupdatecover()
    {
        $userid   = Input::get('userid');
        $coverpic = Input::file('coverpic');
        $fileName="";
        $fileName_big="";
        if (isset($userid) && isset($coverpic)) {
         
            $insertdata= [];
            $path = public_path() . '/assets/uploads/coverimages/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            // $path_parts =pathinfo($_FILES["coverpic"]["name"]);
            $image_mime=getimagesize($_FILES["coverpic"]["tmp_name"]);
            $path_parts=image_type_to_extension($image_mime[2]);
            $extension='.png';
            if(isset($path_parts)){
                $extension=$path_parts;
            }
            $fileName = 'studioapp' . '_up_' .time().$extension;
            Input::file('coverpic')->move($path, $fileName);

            $fileName = "public/assets/uploads/coverimages/" . date('Y') . '/' . date('m') . '/' . $fileName;
            $insertdata['cover_pic'] = $fileName;

            $fileName_big="";
            if(Input::hasFile('coverpic_big')){
                $path = public_path() . '/assets/uploads/coverpic_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                // $path_parts =pathinfo($_FILES["coverpic_big"]["name"]);
                $image_mime=getimagesize($_FILES["coverpic_big"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
                $extension='.png';
                if(isset($path_parts)){
                    $extension=$path_parts;
                }

                $fileName_big = 'studioapp' . '_up_' . time().$extension;
                Input::file('coverpic_big')->move($path,$fileName_big);
                $fileName_big= "public/assets/uploads/coverpic_big/" . date('Y') . '/' . date('m') . '/' . $fileName_big;
                $insertdata['cover_pic_big']=$fileName_big;
            }

                $result = DB::table('users')->where('id', $userid)->update($insertdata);
                if ($result > 0) {

                    if($fileName!=""){
                       $fileName = asset($fileName);
                    }
                    if($fileName_big!=""){
                        $fileName_big = asset($fileName_big);
                    }

                return response()->json(['status' => 'success', 'message' => 'updated', 'data' => $fileName,'data_big' => $fileName_big]);
            } else {

                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function getstudiouserpackagesinfo(Request $request)
    {
        $result      = array();
        $studioid    = Input::get('id');
        $is_active   = DB::table('users')->select('user_status')->where('id', $studioid)->first();
        $whereRaw    = "packages.user_type='studio_user' AND packages.is_presentation = 0";
        $allpackages = DB::table('packages')->select('*')->whereRaw($whereRaw)->get();

        $subscription = DB::table('subscription')->select('*')->where('user_id', $studioid)->first();

        $selectedpackage = DB::table('packages')->select('*', 'id as packageid', 'limit as video_limit')->where('id', $subscription->package_id)->first();
        $totalvideos                     = DB::table('videos')->select('*')->where('studio_id', $studioid)->whereRaw('is_pro!=1')->count();
        $selectedpackage->uploadedvideos = $totalvideos;
        $selectedpackage->is_active      = $is_active->user_status;
        $selectedpackage->message        = "You can't upload the video until your account get activated please contact admin for more information";

        $result['packages']             = $allpackages;
        $result['selectedpackages']     = $selectedpackage;
        $result['presentation_package'] = DB::table('packages')->where('is_presentation', 1)->first();
        $checkPP = DB::table('presentation_subscription')->where('user_id',$studioid)->first();

        $subscriptioncard = DB::table('subscription')->select('start_time','end_time','credit_card_last')->where('user_id', $studioid)->orderBy('id','desc')->first();
        $presentation_subscription = DB::table('presentation_subscription')->select('start_time','end_time','credit_card_last')->where('user_id', $studioid)->orderBy('id','desc')->first();
        $subscription_card_info = array('subscription_card'=>$subscriptioncard,'presentation_subscription'=>$presentation_subscription);
        $currency=BasicModel::mulity_currency_with_rate();
        $currencyCode      = $currency->currencyCode;
        $coverted_currency = $currency->coverted_currency;
        if($currencyCode!="USD" && $currencyCode!="AUD" && $currencyCode!="CHF" && $currencyCode!="EUR" && $currencyCode!="GBP" ){
        $result['presentation_package']->coverted_currency = $result['presentation_package']->Price;
        $result['presentation_package']->currencyCode= "USD";
        $result['selectedpackages']->coverted_currency = $result['selectedpackages']->Price;
        $result['selectedpackages']->currencyCode= "USD";
        foreach($allpackages as $key=>$value) {
            $allpackages[$key]->coverted_currency = $value->Price;
            $allpackages[$key]->currencyCode      = "USD";

            }
        }else{
            foreach($allpackages as $key=>$value) {
                $allpackages[$key]->coverted_currency = number_format((float)(($coverted_currency*$value->Price)), 2, '.', '');
                $allpackages[$key]->currencyCode      = $currencyCode;
            }
            $result['presentation_package']->coverted_currency = number_format((float)( ($coverted_currency*$result['presentation_package']->Price)), 2, '.', '');
            $result['presentation_package']->currencyCode = $currencyCode;
            $result['selectedpackages']->coverted_currency = number_format((float)(($coverted_currency*$result['selectedpackages']->Price)), 2, '.', '');
            $result['selectedpackages']->currencyCode= $currencyCode;

        }
        if ($checkPP) {
            $result['is_presentation_video_allowed'] = 1;
        } else {
            $result['is_presentation_video_allowed'] = 0;
        }
        $text='';
        if(count($checkPP)>0){
            if(empty($checkPP->stripe_subscriptions_id)){
                $text='For the first six month we are offering free presentation video on signup. After 6 months you have to subscribe presentation video from subscription menu.';
            } 
        } else {

                $text=' For the first six month we are offering free presentation video on signup. After 6 months you have to subscribe presentation video from subscription menu.';
        }
        $is_presentation_free =  DB::table('stripe_refund_charges')->where('id','1')->first();
        if($is_presentation_free->is_presentation_free==0) {
                $text = '';
        }
        
        if (count($result) > 0 && isset($studioid)) {
            return response()->json(['status' => 'success', 'data' => $result,'subscription_card_info'=>$subscription_card_info,'text'=>$text,'stripe_key'=>env('STRIPE_KEY')]);
        } else {
            return response()->json(['status' => 'error', 'message' => "data not found"]);
        }
    }

    public function getuserpackagesinfo()
    {
        $dataarray   = array();
        $dataarray2  = array();
        $result      = array();
        $studioid    = Input::get('id');
        $is_active   = DB::table('users')->select('user_status')->where('id', $studioid)->first();
        $whereRaw    = "packages.user_type='normal_user'";
        $allpackages = DB::table('packages')->select('*')->whereRaw($whereRaw)->get();
        // $allpackages = $this->convertStdToArray($allpackages);
        // foreach ($allpackages as $key => $value) {
        //  $dataarray[$key]['id']            =  $value['id'];
        //  $dataarray[$key]['package_name']  =  $value['package_name'];
        //  $dataarray[$key]['limit']         =  $value['limit'];
        //  $dataarray[$key]['Price']         =  $value['Price'];
        //  $dataarray[$key]['size_limit']    =  $value['size_limit'];
        //  $dataarray[$key]['time_limit']    =  $value['time_limit'];
        //  $dataarray[$key]['user_type']     =  $value['user_type'];
        // }
        $subscription    = DB::table('subscription')->select('*')->where('user_id', $studioid)->first();
        // $subscription    = $this->convertStdToArray($subscription);
        $selectedpackage = DB::table('packages')->select('*')->where('id',$subscription->package_id)->where( 'is_presentation' , 0)->first();

        // $selectedpackage = $this->convertStdToArray($selectedpackage);

        $totalvideos = DB::table('videos')->select('*')->where('studio_id', $studioid)->count();

        // $videos      = $this->convertStdToArray($videos);
        // $totalvideos = count($videos);

        $dataarray2['packageid']      = $selectedpackage->id;
        $dataarray2['package_name']   = $selectedpackage->package_name;
        $dataarray2['video_limit']    = $selectedpackage->limit;
        $dataarray2['size_limit']     = $selectedpackage->size_limit;
        $dataarray2['time_limit']     = $selectedpackage->time_limit;
        $dataarray2['uploadedvideos'] = $totalvideos;

        $result['packages']         = $allpackages;
        $result['selectedpackages'] = $dataarray2;
        $result['is_active']        = $is_active->user_status;

        if (count($result) > 0 && isset($studioid)) {

            return response()->json(['status' => 'success', 'data' => $result]);
        } else {

            return response()->json(['status' => 'error', 'message' => "data not found"]);
        }
    }

    public function getvideocategory()
    {
        
        $result = DB::table('video_category')->select('id',"category_name$this->lng as category_name",'status','slug','service_image','service_image_big',"description$this->lng as description")->where('status', 'studio')->get();

        // $result = $this->convertStdToArray($result);

        if (count($result) > 0) {

            return response()->json(['status' => 'success', 'data' => $result]);
        } else {

            return response()->json(['status' => 'error', 'message' => "data not found"]);
        }
    }

    public function getuservideocategory()
    {

        $result = DB::table('video_category')->select('id',"category_name$this->lng as category_name",'status','slug','service_image','service_image_big',"description$this->lng as description")->where('status', 'user')->get();

        // $result = $this->convertStdToArray($result);

        if (count($result) > 0) {

            return response()->json(['status' => 'success', 'data' => $result]);
        } else {

            return response()->json(['status' => 'error', 'message' => "data not found"]);
        }
    }

    public function getstudiovideos(Request $request)
    {
        $studio_id = Input::get('id');
        $user_id   = Input::get('user_id');
        $cat_slug  = $request->cat_slug;
        $subscribe = "0";
        $offset = Input::get('offset');
        $limit  = Input::get('limit');
        if(!isset($offset) || $offset=="" && !isset($limit) || $limit==""){
            $offset = 0;
            $limit  = 15;
        }
        $BasicModel = new BasicModel();
        $whereRaw2 = "videos.is_pro =1 and video_category.status='custom' and videos.studio_id = " . $studio_id;
        $presentation_video="";
        if($this->inputs['usertype']=="normal_user"){
            $whereRaw2 .=" And videos.is_visible =1";
        }
        if (!isset($cat_slug)){
         $presentation_video = $BasicModel->getVideos('studio_videos',$whereRaw2,true);
        }
        // dd($presentation_video);
        $whereRaw   = "videos.is_pro = 0 and video_category.status!='custom' and videos.studio_id = " . $studio_id;
        
        if (isset($cat_slug) && $cat_slug!="single") {
            $whereRaw = "videos.studio_id = $studio_id AND video_category.slug = '$cat_slug'";
        }
        if($this->inputs['usertype']=="normal_user"){
            $whereRaw .=" And videos.is_visible =1";
        }
         $data = $BasicModel->getVideos('studio_videos', $whereRaw,false,$limit,$offset);
         
        if(count($presentation_video)>0 && $presentation_video!=""){
          $count  = count($data);
          $data[$count]=$presentation_video;
        }
        $data=json_encode($data);
        $data=json_decode($data,true);
        // $data=array_reverse($data); 
        if ($user_id != 0 && $studio_id != 0) {
            $whereRaw         = "studio_subscribers.studio_id =" . $studio_id . " AND studio_subscribers.subscribers_id = " . $user_id;
            $subscriptiondata = DB::table("studio_subscribers")->whereRaw($whereRaw)->get();
            if (count($subscriptiondata) > 0) {
                $subscribe = "1";
            }
        }

        $subscribers = DB::table('studio_subscribers')->where('studio_id', $studio_id)->count();
        $ratings     = User::select('Average_ratings')->where('id', $studio_id)->first();

        if (count($data) > 0) {
            return response()->json(['status' => 'success', 'data' => $data, 'totalvideos' => count($data), 'is_subscribe' => $subscribe, 'total_subscribers' => $subscribers, 'Average_ratings' => $ratings->Average_ratings]);
        } else {
            return response()->json(['status' => 'success', 'data' => []]);
        }
    }

    public function getallstudiovideos()
    {
        $offset = Input::get('offset');
        $limit  = Input::get('limit');
        if(!isset($offset) || $offset=="" && !isset($limit) || $limit==""){
            $offset = 0;
            $limit  = 15;
        }
        $whereRaw   = "users.user_type='studio_user' AND  video_category.slug !='presentation-video'";
        if($this->inputs['usertype']=="normal_user"){
            $whereRaw .=" And videos.is_visible =1";
        }
        $BasicModel = new BasicModel();
        $data       = $BasicModel->getVideos('studio_videos', $whereRaw,false, $limit,$offset);

        if (count($data) > 0) {
            return response()->json(['status' => 'success', 'data' => $data, 'totalvideos' => count($data)]);
        } else {
            return response()->json(['status' => 'success', 'data' => []]);
        }
    }

    public function getalluservideos()
    {   
        $offset = Input::get('offset');
        $limit  = Input::get('limit');
        if(!isset($offset) || $offset=="" && !isset($limit) || $limit==""){
            $offset = 0;
            $limit  = 15;
        }
        $whereRaw   = "users.user_type='normal_user'";
        $BasicModel = new BasicModel();
        if($this->inputs['usertype']=="normal_user"){
            $whereRaw .=" And videos.is_visible =1";
        }
        $data       = $BasicModel->getVideos('studio_videos', $whereRaw,false, $limit,$offset);

        if (count($data) > 0) {
            return response()->json(['status' => 'success', 'data' => $data, 'totalvideos' => count($data)]);
        } else {
            return response()->json(['status' => 'success', 'data' => []]);
        }
    }

    public function getvideostatus()
    {
        $dataarray = array();
        BasicModel::checkParams(['studioid']);
        $studioid        = Input::get('studioid');
        $is_presentation = Input::get('is_presentation');
        if(!isset($is_presentation) && empty($is_presentation)){
            $is_presentation =0;
        }
        $packages; 
        // $this->down_grade_subscribtion($studioid);
        $text='';
        $test_package = DB::table('presentation_subscription')->where('user_id',$studioid)->first();
      
        if(count($test_package)>0){
            if(empty($test_package->stripe_subscriptions_id)){
                $text='<p> For the first six month we are offering free presentation video on signup. <br>After 6 months you have to subscribe presentation video from subscription menu.
                </p>';
            } 
        } else {

                $text='<p> For the first six month we are offering free presentation video on signup. <br>After 6 months you have to subscribe presentation video from subscription menu.
                </p>';
        }
        $is_presentation_free =  DB::table('stripe_refund_charges')->where('id','1')->first();
        if($is_presentation_free->is_presentation_free==0) {
                $text = '';
        }
        $check_Active=DB::table('users')->select('user_status')->where('id',$studioid)->first();
        if($check_Active->user_status==0){
            return response()->json(['status' => 'error', 'message' => "You can't upload the video until your account get activated please contact admin for more information"]); 
        }

        if (isset($studioid)) {

            $subscription = DB::table('subscription')->select('*')->where('user_id', $studioid)->first();

            if (count($subscription) > 0) {
                // $subscription = $this->convertStdToArray($subscription);

                $packages = DB::table('packages')->select('*', 'limit as video_limit')->where('id', $subscription->package_id)->first();

                $videos = DB::table('videos')->select('*')->where('studio_id', $studioid)->whereRaw('is_pro!=1')->count();

                $presentation_package = DB::table('packages')->where('is_presentation', 1)->first();
                // $videos = $this->convertStdToArray($videos);
                // $totalvideos = count($videos);
            
                if ($videos == $packages->limit && $packages->video_limit != 0 && $is_presentation!=1) {
                    return response()->json(['status' => 'error', 'message' => "You have reached your limit please Delete Some Videos to upload more videos"]);
                } else {
                    $dataarray                       = $packages;
                    $dataarray->uploadedvideos       = $videos;
                    $dataarray->presentation_package = $presentation_package;
                    return response()->json(['status' => 'success', 'data' => $dataarray,'text'=>$text]);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => "Parameter Missing"]);
        }
    }

    public function postAddvideo(request $req)
    {
        // dd($req);
        $dataarray = array();
        $studioid  = Input::get('studioid');
        $check_Active=DB::table('users')->select('user_status')->where('id',$studioid)->first();
        if($check_Active->user_status==0){
            return response()->json(['status' => 'error', 'message' => "You can't upload the video until your account get activated please contact admin for more information"]); 
        }
        if ($this->inputs['registration_platform'] == 'web') {
            $videofile = Input::get('videofile');
            if(!isset($videofile)){
                return response()->json(['status' => 'error', 'message' => "videofile parameter is missing OR missing value"]);           
            }
        } else {
            $videofile = Input::file('videofile');
            if(!isset($videofile)) {
                return response()->json(['status' => 'error', 'message' => "videofile parameter is missing OR missing value"]);
            }
        }
            $title             = Input::get('title');
            $discribtion       = Input::get('discribtion');
            $ispro             = Input::get('ispro');
            $Video_category_id = Input::get('videocategoryid');
            $duration          = Input::get('duration');
            $size              = Input::get('size');
            $tags              = Input::get('tags');
            $likes             = '0';
          BasicModel::checkParams(array('studioid','title','discribtion','ispro','duration','size'));
        // if (isset($studioid) && isset($videofile) && isset($title) && isset($discribtion) && isset($ispro) && isset($duration) && isset($size)) {

            // $path=public_path().'/assets/uploads/videos/';
            // $path = $path . date('Y') . '/' . date('m').'/';
            // if (!is_dir($path)) {
            // mkdir($path, 0777, TRUE);
            // }
            $thumbnailfileName = '';
            $fileName          = '';

            if ($this->inputs['registration_platform'] == 'web') {
                $fileName          = $videofile;
                $thumbnailfileName = Input::get('thumbnail');
            } else {

                $file        = Input::file('videofile');
                $fileName    = 'studioapp' . '_up_' . time() . '.' . $file->getClientOriginalExtension();
                $path        = "videos/" . date('Y') . "/" . date('m') . '/';
                $retrivepath = BasicModel::fileupload($file, $path, $fileName, 'private');
                $fileName    = $path . $fileName;

            if (Input::hasFile('thumbnail')) {

                $thumbnail         = Input::file('thumbnail');
                $thumbnailfileName = 'studioapp' . '_up_' . time() . '.' . $thumbnail->getClientOriginalExtension();
                $thumbnailpath     = "thumbnails/" . date('Y') . "/" . date('m') . '/';
                $retrivepath       = BasicModel::fileupload($thumbnail, $thumbnailpath, $thumbnailfileName, 'public');
                $thumbnailfileName = $thumbnailpath . $thumbnailfileName;
            }
         }
            // $fileFlv='/'.$fileName;
            // $fileFlv='test.mp4';
            // $Flvjpg= $fileName2.'.jpg';
            // $command1 =('ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=width,height '.$fileFlv);
            // exec($command1,$ret);
            // var_dump($ret);
            // exit();
            // exit();
            // exit();
            // $width = str_replace( 'streams_stream_0_width=', '', $ret['0']);
            // $height = str_replace( 'streams_stream_0_height=', '', $ret['1']);
            // $width = ($width >= 1000) ? round($width/2) : $width;
            // $height = ($width >= 1000) ? round($height/2) : $height;
            // $command2 = ('ffmpeg -i '.$fileFlv.' -vframes 1 -ss 00:00:11 -s '.$width.'x'.$height.' -f image2 '.$Flvjpg);
            // exec($command2);
            if ($ispro == 1) {
                DB::select("DELETE FROM videos WHERE studio_id = $studioid and is_pro = 1");
                // $array             = ['is_pro' => 0];
                $cat_id            = DB::table('video_category')->select('id')->where('slug', 'presentation-video')->first();
                $Video_category_id = ($cat_id) ? $cat_id->id : 0;
                // $result            = BasicModel::UpdateData('videos', $studioid, 'studio_id', $array);
            } else {
                $Video_category_id = $Video_category_id;
            }
            
            $dataarray = ['studio_id' => $studioid,
                'Video_link'              => $fileName,
                'title'                   => $title,
                'discribtion'             => $discribtion,
                'thumbnail'               => $thumbnailfileName,
                'is_pro'                  => $ispro,
                'Video_category_id'       => $Video_category_id,
                "duration"                => $duration,
                "size"                    => $size,
                "tags"                    => $tags,
                "likes"                   => $likes,
                'date'                    => Date('y-m-d H:i:s'),
            ];

            $data = DB::table('videos')->insertGetId($dataarray);

            if ($data > 0) {

                $raw           = "LENGTH(device_token) > 15 AND id IN (select subscribers_id from studio_subscribers where studio_id = $studioid)";
                $subscribers   = DB::table('users')->select(DB::raw('GROUP_CONCAT(`device_token`) AS device_tokens, GROUP_CONCAT(`id`) AS user_ids'))->whereRaw($raw)->first();
                $device_tokens = explode(',', $subscribers->device_tokens);
                $user_ids      = explode(',', $subscribers->user_ids);
                $studio        = DB::table('users')->select('Studio_Name')->where('id', '=', $studioid)->first();
                $title         = 'New Video Uploaded By ' . $studio->Studio_Name;
                $body          = "$studio->Studio_Name has upload their new video $title";
                $BasicModel    =  new BasicModel();
                $video         = $BasicModel->getVideos(null, 'videos.id = ' . $data, true);

                $p_array = array(
                    'title'     => $title,
                    'body'      => $body,
                    'push_data' => json_encode($video),
                    'type'      => 'new_video',
                );
                $push_id = DB::table('push_notifications')->insertGetId($p_array);
                $arr     = [];
                foreach ($user_ids as $k => $uid) {
                    $arr[] = ['push_id' => $push_id, 'sent_to' => $uid, 'device_token' => $device_tokens[$k], 'is_sent' => 1];
                }
                // var_dump($arr);exit;

                DB::table('notification_details')->insert($arr);

                BasicModel::pushNotification($title, $body, $device_tokens, $type = 'new_video', array('video' => $video));

                return response()->json(['status' => 'success', 'data' => "Video Uploaded SucessFully"]);
            } else {
                return response()->json(['status' => 'success', 'data' => "Something went wrong!"]);
            }
        // } else {
        //     return response()->json(['status' => 'error', 'message' => "Parameter Missing"]);
        // }
    }

    public function postupdatevideo()
    {
        $dataarray         = array();
        $videoid           = Input::get('videoid');
        $title             = Input::get('title');
        $discribtion       = Input::get('discribtion');
        $Video_category_id = Input::get('videocategoryid');
        $tags              = Input::get('tags');

        if (isset($videoid) && isset($title) && isset($discribtion) && isset($Video_category_id)) {

            $dataarray = [
                'title'             => $title,
                'discribtion'       => $discribtion,
                'Video_category_id' => $Video_category_id,
                "tags"              => $tags,
            ];
            $data = DB::table('videos')->where('id', $videoid)->update($dataarray);
            if (count($data) > 0) {

                return response()->json(['status' => 'success', 'data' => "updated"]);
            }
        } else {

                return response()->json(['status' => 'error', 'message' => "Parameter Missing"]);
        }
    }

    public function postupdatepassword()
    {

        $id          = Input::get('id');
        $oldpassword = Input::get('oldpassword');
        $newpassword = Input::get('newpassword');
        
        if (isset($id) && isset($oldpassword) && isset($newpassword)) {

            $user = User::select('username', 'password')->where('id', Input::get('id'))->first();

            if ($user->count()) {

                if (Hash::check($oldpassword, $user->password)) {

                    $data = DB::table('users')->where('id', $id)->update(['password' => bcrypt($newpassword)]);

                    if (count($data) > 0) {
                        return response()->json(['status' => 'success', 'message' => 'updated']);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'record not found']);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'wrong password']);
                }
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function postAddreviews()
    {

        $id       = Input::get('studio_id');
        $userid   = Input::get('userid');
        $comments = Input::get('comments');
        $ratings  = Input::get('ratings');

        if (isset($id) && isset($userid) && isset($comments) && isset($ratings)) {

            $already = DB::table('studio_reviews')->whereRaw('studio_id= ' . $id . ' and user_id=' . $userid)->get();

            // dd($already);

            if (count($already) > 0) {

                return response()->json(['status' => 'error', 'message' => 'You Already Made a Review']);
            }

            $result = DB::table('studio_reviews')->insertGetId(['studio_id' => $id, 'user_id' => $userid, 'comments' => $comments, 'ratings' => $ratings]);

            $averagerating = DB::select('SELECT round(avg(ratings)) as average_review, count(ratings) as number_of_reviews from studio_reviews where studio_id=' . $id);

            $averagerating = $this->convertStdToArray($averagerating);

            $averagerating = $averagerating[0]['average_review'];

            DB::table('users')->where('id', $id)->update(['Average_ratings' => $averagerating]);

            if (count($result) > 0) {
                return response()->json(['status' => 'success', 'message' => 'Added']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'record not found']);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function postDeleteVideos()
    {

        $rowid = Input::get('rowid');

        if (isset($rowid)) {

            $result = DB::table('videos')->where('id', $rowid)->delete();
            // Storage::disk('s3')->delete();
            // ::delete('file.jpg');

            if ($result > 0) {

                return response()->json(['status' => 'success', 'message' => 'Deleted']);
            } else {

                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function videosbycategoryid(Request $request)
    {

        $video_category_id = $request->Video_category_id;
        $cat_slug          = $request->cat_slug;
        $search_term       = $request->search;
        $whereRaw          = '';
        if ($search_term) {
            $whereRaw = " (videos.title LIKE '%$search_term%' OR videos.discribtion LIKE '%$search_term%' OR videos.tags LIKE '%$search_term%') AND  video_category.slug !='presentation-video'";
        }
        if (is_array($video_category_id) && count($video_category_id) > 0) {
            $video_category_id = implode(',', $video_category_id);
            if ($search_term) {
                $whereRaw .= " AND videos.Video_category_id IN ($video_category_id)";
            } else {
                $whereRaw .= " videos.Video_category_id IN ($video_category_id) ";
            }
        }
        if ($cat_slug) {
            $whereRaw .= " video_category.slug = '$cat_slug'";
        }
        if($this->inputs['usertype']=="normal_user"){
            $whereRaw .=" And videos.is_visible =1";
        }
        $BasicModel = new BasicModel();
        $data       = $BasicModel->getVideos('studio_videos', $whereRaw);

        if (count($data) > 0) {
            return response()->json(['status' => 'success', 'data' => $data, 'totalvideos' => count($data)]);
        } else {
            return response()->json(['status' => 'success', 'data' =>[]]);
        }
    }

    public function postgenrateinvoice()
    {

        $product_name      = Input::get('product_name');
        $create_time       = Input::get('create_time');
        $paypalid          = Input::get('paypalid');
        $amount            = Input::get('amount');
        $currency_code     = Input::get('currency_code');
        $short_description = Input::get('short_description');
        $userid            = Input::get('userid');
        $packageid         = Input::get('packageid');

        if (isset($product_name) && isset($create_time) && isset($paypalid) && isset($amount) && isset($currency_code) && isset($short_description) && isset($userid) && isset($packageid)) {

            $data = [
                "product_name"      => $product_name,
                "create_time"       => $create_time,
                "paypalid"          => $paypalid,
                "amount"            => $amount,
                "currency_code"     => $currency_code,
                "short_description" => $short_description,
                "userid"            => $userid,
                "packageid"         => $packageid,
                "platform"          => $this->inputs['registration_platform'],
                "end_date"          => date('Y-m-d', strtotime("+30 days", strtotime($create_time))),
            ];
            $result = DB::table('payment_invoices')->insertGetId($data);

            if ($result > 0) {
                $date     = date('Y-m-d H:i:s');
                $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime($date)));
                $data2    = [
                    "package_id" => $packageid,
                    "start_time" => $create_time,
                    "end_time"   => $end_time,
                ];

                DB::table('subscription')->where('user_id', $userid)->update($data2);

                $packages = DB::table('subscription')->where('user_id', $userid)->get();
                $packages = $this->convertStdToArray($packages);

                return response()->json(['status' => 'success', 'message' => 'Payment Recieved', 'package' => $packages]);
            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function convertStdToArray($data)
    {

        return json_decode(json_encode($data), true);
    }

    public function restyle_text($input)
    {

        $input       = number_format($input);
        $input_count = substr_count($input, ',');
        if ($input_count != '0') {
            if ($input_count == '1') {
                return substr($input, 0, -4) . 'k';
            } else if ($input_count == '2') {
                return substr($input, 0, -8) . 'mil';
            } else if ($input_count == '3') {
                return substr($input, 0, -12) . 'bil';
            } else {
                return;
            }
        } else {
            return $input;
        }
    }

    public function time_elapsed_string($datetime, $full = false)
    {

        $now  = new DateTime;
        $ago  = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function getSearchFiltersformobile()
    {
        $origLat      = Input::get('lat');
        $origLon      = Input::get('lng');
        $gender       = Input::get('gender');
        $category     = Input::get('category');
        $min_charges  = Input::get('min_charges');
        $max_charges  = Input::get('max_charges');
        $min_time     = Input::get('min_time');
        $max_time     = Input::get('max_time');
        $min_distance = Input::get('min_distance');
        $max_distance = Input::get('max_distance');
        $usertype     = Input::get('usertype');
        $ispro        = Input::get('ispro');
        $search_item  = Input::get('search_item');
        $country      = Input::get('country');
        $city         = Input::get('city');
  
        if (!isset($min_distance) && !isset($max_distance)) {
            $min_distance = 0;
            $max_distance = 50;
        }

        $whereRaw2 = "";
        
        if (isset($gender) && $gender != "") {
            $whereRaw2 .= " AND `studio_services`.`gender`  = '" . $gender . "'";
        }

        if (isset($category) && $category != "") {

            $whereRaw2 .= "  AND `video_category`.`category_name$this->lng` = '" . $category . "'";

        }

        if (isset($min_charges) && isset($max_charges)) {

            $whereRaw2 .= " AND `studio_services`.`Service_charges` BETWEEN " . $min_charges . " AND " . $max_charges;
        }

        if (isset($min_time) && isset($max_time)) {

            $whereRaw2 .= " AND `studio_services`.`service_time` BETWEEN " . $min_time . " AND " . $max_time;
        }
        if (isset($usertype) && $usertype != "") {
                $whereRaw2 .= " AND `users`.`user_type`  = '" . $usertype . "'";
            } else{

                $whereRaw2 .= "  AND `users`.`user_type`  !=  'normal_user' and users.user_type !='admin' and users.user_type !='sub_admin'";
            }   

        if (isset($search_item) && $search_item != "") {

            $whereRaw2 .= " AND (`studio_services`.`service_title`  like '%" . $search_item . "%' OR `video_category`.`category_name$this->lng` like '%" . $search_item . "%')";
           
        }

        if (isset($city) && $city != "") {
            // $whereRaw2 .= " AND `users`.`Address`  like '%" . $city . "%'"; 
            $whereRaw2 .= " AND (`users`.`Address`  like '%" . $city . "%' OR `users`.`country` like '%" . $city . "%')";
        }
        if (isset($ispro) && $ispro != "") {

            $whereRaw2 .= " AND videos.is_pro =" . $ispro;
        }
          
        if (!isset($max_distance) && !isset($min_distance) || !isset($origLat) && !isset($origLon)) {

            $whereRaw2 = substr($whereRaw2,5);
            $query = "SELECT users.id as studioid,users.Studio_Name,users.Address as Address,users.lat,users.cover_pic,users.cover_pic_big,users.profile_pic,users.profile_pic_big,users.lng,users.Average_ratings,studio_services.gender,studio_services.service_title,studio_services.currency_code,video_category.category_name$this->lng AS Name,studio_services.Service_charges,studio_services.service_time,videos.id as video_id,users.user_type FROM users LEFT JOIN studio_services ON users.id = studio_services.Studio_id
            LEFT JOIN video_category ON video_category.id = studio_services.Category_id
            LEFT JOIN videos ON videos.studio_id = users.id
            WHERE " . $whereRaw2 . " GROUP BY studioid ORDER BY users.Average_ratings DESC";
        } else {

            $query = "SELECT users.id as studioid,users.Address as Address,users.Studio_Name,users.lat, users.cover_pic,users.cover_pic_big,users.profile_pic,users.profile_pic_big,users.lng,users.Average_ratings,studio_services.gender,studio_services.currency_code,video_category.category_name$this->lng AS Name,studio_services.Service_charges,studio_services.service_title,studio_services.service_time,videos.id as video_id,videos.duration,users.user_type, 3956 * 2 * ASIN(SQRT( POWER(SIN(($origLat - users.lat)*pi()/180/2),2)+COS($origLat*pi()/180 )*COS(users.lat*pi()/180)*POWER(SIN(($origLon-users.lng)*pi()/180/2),2))) as distance FROM users LEFT JOIN studio_services ON users.id = studio_services.Studio_id
           LEFT JOIN video_category ON video_category.id = studio_services.Category_id
           LEFT JOIN videos ON videos.studio_id = users.id
            WHERE lng between ($origLon-$max_distance/cos(radians($origLat))*69)
          and ($origLon+$max_distance/cos(radians($origLat))*69)
          and users.lat between ($origLat-($max_distance/69))
          and ($origLat+($max_distance/69)) " . $whereRaw2 . " GROUP BY studioid  having distance  BETWEEN " . $min_distance . " AND " . $max_distance . "  ORDER BY distance ASC, users.Average_ratings DESC";
        }
           
        $data = DB::select($query);
        $data = $this->convertStdToArray($data);

        if ($data != []) {
            foreach ($data as $key => $value) {
               
                $dataarray[$key]['studio_id']       = $data[$key]['studioid'];
                $dataarray[$key]['Studio_Name']     = $data[$key]['Studio_Name'];
                $dataarray[$key]['Average_ratings'] = $data[$key]['Average_ratings'];
                $dataarray[$key]['service_title']   = $data[$key]['service_title'];
               
                if ($data[$key]['cover_pic'] != "") {
                    $dataarray[$key]['cover_pic'] = asset($data[$key]['cover_pic']);
                } else {

                    $dataarray[$key]['cover_pic'] = "";
                }
                if ($data[$key]['cover_pic_big'] != "") {
                    $dataarray[$key]['cover_pic_big'] = asset($data[$key]['cover_pic_big']);
                } else {

                    $dataarray[$key]['cover_pic_big'] = "";
                }

                if ($data[$key]['profile_pic'] != "") {
                    $dataarray[$key]['profile_pic'] = asset($data[$key]['profile_pic']);
                } else {

                    $dataarray[$key]['profile_pic'] = "";
                }
                if ($data[$key]['profile_pic_big'] != "") {
                    $dataarray[$key]['profile_pic_big'] = asset($data[$key]['profile_pic_big']);
                } else {

                    $dataarray[$key]['profile_pic_big'] = "";
                }

                $dataarray[$key]['lat']         = $data[$key]['lat'];
                $dataarray[$key]['lng']         = $data[$key]['lng'];

                $BasicModel = new BasicModel();
               
                if ($data[$key]['video_id'] != "") {
                    $video = DB::table('videos')->leftjoin('video_category', 'video_category.id', '=', 'videos.Video_category_id')->select('videos.*', "video_category.category_name$this->lng as category_name")->whereRaw('videos.studio_id='.$data[$key]['studioid'].' And videos.is_pro = 1');
                    if($this->inputs['usertype']=="normal_user"){
                        $whereRaw_videos =" videos.is_visible = 1";
                        $video = $video->whereRaw($whereRaw_videos)->first();
                    }else{

                        $video = $video->first();
                    }
                   if(count($video)>0) {
             
                    $dataarray[$key]['video']['Video_link']     = "";
                    $dataarray[$key]['video']['videoid']        =  $video->id;
                    $dataarray[$key]['video']['category_id']    = $video->Video_category_id;
                    $dataarray[$key]['video']['studioid']       = $dataarray[$key]['studio_id'];
                    $dataarray[$key]['video']['discribtion']    = $video->discribtion;
                    $dataarray[$key]['video']['title']          = $video->title;
                    $dataarray[$key]['video']['category']       = $video->category_name;
                    $dataarray[$key]['video']['thumbnail']      = $BasicModel->s3_baseurl . $video->thumbnail;
                    $dataarray[$key]['video']['likes']          = $video->likes;
                    $dataarray[$key]['video']['StudioName']     = $data[$key]['Studio_Name'];
                    $dataarray[$key]['video']['profilepic']     = asset($data[$key]['profile_pic']);
                    $dataarray[$key]['video']['views']          = $video->views;
                    $dataarray[$key]['video']['Averageratings'] = $dataarray[$key]['Average_ratings'];
                    $dataarray[$key]['video']['tags']           = $video->tags;
                    $dataarray[$key]['video']['date']           = $video->date;
                    $dataarray[$key]['video']['duration']       = $video->duration;
                } else {
                   
                    $dataarray[$key]['video_id'] = "";
                }
                } else {
                    $dataarray[$key]['video_id'] = "";
                }

                $dataarray[$key]['gender']          = $data[$key]['gender'];
                $dataarray[$key]['category_name']   = $data[$key]['Name'];
                $dataarray[$key]['Service_charges'] = $data[$key]['Service_charges'];
                $dataarray[$key]['service_time']    = $data[$key]['service_time'];
                $dataarray[$key]['user_type']       = $data[$key]['user_type'];
                $dataarray[$key]['currency_code']   = $data[$key]['currency_code'];
              
                if (isset($origLat) && isset($origLon)) {
                    $dataarray[$key]['distance'] = $data[$key]['distance'];
                }

                $dataarray[$key]['Address'] = $data[$key]['Address'];
              
            }
            
            return response()->json(['status' => 'success', 'data' => $dataarray,'currency_rate'=>1,'currency_code'=>'']);
        } else {

            return response()->json(['status' => 'error', 'message' => 'data not found']);
        }
    }
    public function userappstudiodetails(Request $request)
    {
        $id     = Input::get('studioid');
        $userid = Input::get('userid');
        if (isset($id)) {

            $data = DB::table("users")->select('Studio_Name', 'email', 'contact_no as Contact', 'Address', 'cover_pic', 'Average_ratings', 'profile_pic','profile_pic_big','cover_pic_big', 'lat', 'lng', 'user_type','time_zone')->where('id', $id)->first();
            $workinghours = DB::table('working_hours')->select('working_Days', 'working_start', 'working_end','is_daylight_saving','lunch_time_to','lunch_time_from')->where('studio_id', $id)->get();
            if (isset($userid) && $userid != "") {
                $result = DB::table('studio_reviews')->select('comments', 'ratings')->where('user_id', $userid)->get();
                $result = $this->convertStdToArray($result);
            } else {
            $result = array();
            }
            if (null !== $data && is_object($data)) {

                $data->studio_id   = $id;
                if($data->cover_pic == "" || $data->cover_pic === null) {
                    $data->cover_pic = "";
                }else{
                    $data->cover_pic = asset($data->cover_pic);
                }

                if($data->cover_pic_big == "" || $data->cover_pic_big === null) {
                    $data->cover_pic_big = "";
                }else{
                    $data->cover_pic_big = asset($data->cover_pic_big);
                }

                if($data->profile_pic_big == "" || $data->profile_pic_big === null) {
                    $data->profile_pic_big = "";
                }else{
                    $data->profile_pic_big = asset($data->profile_pic_big);
                }

                if ($data->profile_pic == "" || $data->profile_pic === null) {
                    $data->profile_pic = "";
                }else{
                    $data->profile_pic = asset($data->profile_pic);
                }

                if (isset($result[0]['comments'])) {
                    $data->usercomment = $result[0]['comments'];
                } else {
                    $data->usercomment = "";
                }

                if (isset($result[0]['ratings'])) {
                    $data->userratings = $result[0]['ratings'];
                } else {

                    $data->userratings = "";
                }
                $totalsubscribers = DB::table('studio_subscribers')->where('studio_id', $id)->count();
                $data->studio_subscribers = $totalsubscribers;
                $dataarray = $data;
                $select1 = array(
                    'users.contact_no as contact',
                    'users.Studio_Name as name',
                    'staff_member.id as staffrowid',
                    DB::raw('CONCAT("'.asset('').'", users.profile_pic) as member_pic'),
                    DB::raw('CONCAT("'.asset('').'", users.profile_pic_big) as member_pic_big'),
                    'staff_member.parent_id as parent_id',
                );
                $data1 = DB::table("users")->select($select1)
                ->selectRaw("GROUP_CONCAT(video_category.category_name$this->lng) as categoryName")
                ->leftjoin('staff_member', 'staff_member.parent_id', '=', 'users.id')
                ->leftjoin('studio_services', 'staff_member.service_id', '=', 'studio_services.id')
                ->leftjoin('video_category', 'studio_services.Category_id', '=', 'video_category.id')
                ->where('staff_member.studio_id', $id)
                ->groupby('staff_member.parent_id')
                ->get();
                $dataarray1 = $data1;
                $select2 = array(
                    'studio_services.service_time',
                    'studio_services.Service_charges',
                    'studio_services.service_title',
                    'studio_services.gender',
                    'studio_services.happy_hour_to',
                    'studio_services.happy_hour_from',
                    'studio_services.discount_amount',
                    'studio_services.Description',
                    'studio_services.Studio_id',
                    'studio_services.happy_hour_days',
                    'studio_services.currency_code',
                    "video_category.category_name$this->lng as categoryName",
                    'studio_services.id as rowid',
                    DB::raw('IF(studio_services.services_image IS NOT NULL, CONCAT("' . asset('') . '",studio_services.services_image), "") AS services_image'),
                    DB::raw('IF(studio_services.service_image_big IS NOT NULL, CONCAT("' . asset('') . '",studio_services.service_image_big), "") AS services_image_big'),
                );
                    $whereRaw2 = 'studio_services.Studio_id='.$id;
                    $Studio_services = DB::table("studio_services")->select($select2)
                        ->leftjoin('video_category', 'video_category.id', '=', 'studio_services.Category_id')->whereRaw($whereRaw2)->orderByRaw('service_index ASC')->get();

                    $user_data= User::select('time_zone')->where('id',$id)->first();
                    foreach($Studio_services as $key => $value){
                        if(!empty($user_data->time_zone)){
                            date_default_timezone_set($user_data->time_zone);
                            }
                        $days = [];  
                        if(!empty($value->happy_hour_days)){
                            $days  = json_decode($value->happy_hour_days);
                        }
                        $today = Date('l');
                        $start_date_hour_minutes    = date('H:i:s', strtotime($value->happy_hour_from));
                        $end_date_hour_minutes      = date('H:i:s', strtotime($value->happy_hour_to));
                        $now_date_hour_minutes      = date('H:i:s');
                        $Studio_services[$key]->is_discount_show = 'false';
                            if($start_date_hour_minutes<=$now_date_hour_minutes && $end_date_hour_minutes>=$now_date_hour_minutes && in_array($today,$days)) {
                                $Studio_services[$key]->is_discount_show = 'true';
                            } 
                    }
                    // $currency=BasicModel::mulity_currency_with_rate();
                    // $currencyCode  = $currency->currencyCode;
                    // $currency_rate = $currency->coverted_currency;
                    // if($currencyCode!="USD" && $currencyCode!="AUD" && $currencyCode!="CHF" &&  $currencyCode!="EUR" && $currencyCode!="GBP" ){
                    //     $currencyCode  = 'USD';
                    //     $currency_rate =  '1';   
                    // }
                    $currency_rate = 1;
                    $currency_code = '';
                    if(isset($Studio_services[0]->currency_code)){
                        $currency_code=  $Studio_services[0]->currency_code;
                    }
                    // $whereRaw3  = "users.user_type='studio_user' AND videos.studio_id = " . $id;
                    $whereRaw3  = "videos.studio_id = " . $id;
                    if($this->inputs['usertype']=="normal_user"){
                        $whereRaw3 .=" And videos.is_visible =1";
                    }
                    $BasicModel = new BasicModel();
                    $video      = $BasicModel->getVideos(null, $whereRaw3);

            } else {
                return response()->json(['status' => 'error', 'message' => "data not found"]);
            }
            $user_data= User::select('time_zone')->where('id',$id)->first();
            if(!empty($user_data->time_zone)){
                date_default_timezone_set($user_data->time_zone);
                }
            $is_day_light_service = DB::table('working_hours')->where('working_Days',date('l'))->whereRaw('studio_id ='. $id)->count();
                return response()->json(['status' => 'success', 'profile' => $dataarray, 'staffmembers' => $dataarray1, 'Services' => $Studio_services, 'videos' => $video, 'workinghours' => $workinghours,'currency_rate'=>$currency_rate,'currency_code'=>$currency_code,'is_day_light_service'=>$is_day_light_service,'stripe_key'=>env('STRIPE_KEY')]);
        } else {

                return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
    }

    public function studiosubscribe()
    {
        BasicModel::checkParams(array('userid', 'studio_id'));
        $id     = Input::get('studio_id');
        $userid = Input::get('userid');
        $result = DB::table('studio_subscribers')->where(['studio_id' => $id, 'subscribers_id' => $userid])->first();
        if (!$result) {
            DB::table('studio_subscribers')->insertGetId(['studio_id' => $id, 'subscribers_id' => $userid]);
            $totalsubscribers = DB::table('studio_subscribers')->where('studio_id', $id)->count();
            return response()->json(['status' => 'success', 'message' => 'You are sucessfully subsribed', 'total_subscribers' => $totalsubscribers]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'You are already subscribed']);
        }
    }

    public function studioUnSubscribe()
    {
        BasicModel::checkParams(array('userid', 'studio_id'));
        $id     = Input::get('studio_id');
        $userid = Input::get('userid');
        $result = DB::table('studio_subscribers')->where(['studio_id' => $id, 'subscribers_id' => $userid])->delete();
        if ($result) {
            $totalsubscribers = DB::table('studio_subscribers')->where('studio_id', $id)->count();
            return response()->json(['status' => 'success', 'message' => 'You are sucessfully un subsribed', 'total_subscribers' => $totalsubscribers]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'You are not subscribed']);
        }
    }

    public function addlikes()
    {
        BasicModel::checkParams(array('videoid', 'userid', 'studio_id'));
        $id        = Input::get('videoid');
        $studio_id = Input::get('studio_id');
        $userid    = Input::get('userid');
        $result = DB::table('video_likes')->where([['video_id', '=', $id], ['user_id', '=', $userid]])->first();
        if (!$result) {

            DB::table('videos')->where('id', $id)->increment('likes', 1);

            DB::table('video_likes')->insertGetId(['user_id' => $userid, 'studio_id' => $studio_id, 'video_id' => $id]);

            $totallikes = DB::table('videos')->select('likes')->where('id', $id)->first();

            return response()->json(['status' => 'success', 'message' => 'You have liked this video successfully', 'totallikes' => $totallikes->likes]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'You already have liked this video']);
        }
    }

    public function unLike()
    {

        BasicModel::checkParams(array('videoid', 'userid'));

        $id        = Input::get('videoid');
        $studio_id = Input::get('studio_id');
        $userid    = Input::get('userid');

        $result = DB::table('video_likes')->where([['video_id', '=', $id], ['user_id', '=', $userid]])->first();

        if ($result) {

            DB::table('videos')->where('id', $id)->decrement('likes', 1);

            $totallikes = DB::table('videos')->select('likes')->where('id', $id)->first();

            DB::table('video_likes')->where([['video_id', '=', $id], ['user_id', '=', $userid]])->delete();

            return response()->json(['status' => 'success', 'message' => 'You have unliked this video successfully', 'totallikes' => $totallikes->likes]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'You already haven\'t yet liked this video']);
        }
    }

    public function videoDetailsByUser(Request $request)
    {

        BasicModel::checkParams(array('studio_id', 'user_id', 'video_id', 'only_url'));

        $studio_id = $request->studio_id;
        $user_id   = $request->user_id;
        $video_id  = $request->video_id;
        $only_url  = Input::get('only_url');

        $is_subscribe = $is_like = 0;

        if ($user_id > 0 && $studio_id > 0) {
            $subscriptiondata = DB::table("studio_subscribers")->where([['studio_id', '=', $studio_id], ['subscribers_id', '=', $user_id]])->first();
            if ($subscriptiondata) {
                $is_subscribe = "1";
            }
        }
        DB::table('videos')->where('id', $video_id)->increment('views');
        if ($user_id > 0 && $video_id > 0) {
            DB::table('video_views')->insert(['user_id' => $user_id, 'video_id' => $video_id]);
            $likeData = DB::table("video_likes")->where([['video_id', '=', $video_id], ['user_id', '=', $user_id]])->first();

            if ($likeData) {
                $is_like = "1";
            }
            $reportData = DB::table("video_reports")->where([['video_id', '=', $video_id], ['user_id', '=', $user_id]])->first();
            if ($reportData) {
                $is_reported = 1;
            } else {
                $reportData  = '';
                $is_reported = 0;
            }
        }
        if ($video_id > 0) {
            $video_link = '';
            $whereRaw   = "videos.id = " . $video_id;
            if($this->inputs['usertype']=="normal_user"){
                $whereRaw .=" And videos.is_visible =1";
            }
            $BasicModel = new BasicModel();
            $data       = $BasicModel->getVideos('studio_videos', $whereRaw, true);
            // dd($whereRaw);

            $whereRaw     = "videos.Video_category_id =" . $data->Video_category_id . " AND videos.id!=" . $video_id;
            if($this->inputs['usertype']=="normal_user"){
                $whereRaw .=" And videos.is_visible =1";
            }
            $recentvideos = $BasicModel->getVideos('studio_videos', $whereRaw, false, 10);

            if (count($data) > 0 && $data != "") {

                $data->Video_link = BasicModel::signedUrl($data->Video_link);
            }

            if ($only_url == 'true') {

                return response()->json(['status' => 'success', 'video_link' => $data->Video_link]);
            }

            return response()->json(['status' => 'success', 'is_like' => $is_like, 'is_subscribe' => $is_subscribe, 'video_data' => $data, 'recent_videos' => $recentvideos, 'is_reported' => $is_reported, 'report_data' => $reportData]);
        }
    }

    public function conservationlist()
    {
        $id = Input::get('studio_id');
        if (!isset($id)) {
            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
        $data = DB::select("SELECT  id,`Studio_Name` as membername,profile_pic,COUNT(chat_id)  as totalconversations FROM (
        SELECT users.id,users.`Studio_Name` ,users.`email`,users.`profile_pic`,`chats`.`id` AS chat_id FROM users
        JOIN `staff_member` ON `staff_member`.`parent_id`=users.`id`
        JOIN `chats` ON (chats.`receiver`= users.`id` or chats.`start_from`=users.`id`)
        WHERE `staff_member`.`studio_id`= $id  AND `chats`.`receiver`!=$id
        GROUP BY `chat_id`) AS `records` GROUP BY records.id");
          
        for ($i = 0; $i < count($data); $i++) {

            if ($data[$i]->profile_pic != "" && $data[$i]->profile_pic != null) {
                $data[$i]->profile_pic = asset($data[$i]->profile_pic);
            }
        }

        if (count($data) > 0) {

            return response()->json(['status' => 'success', 'data' => $data]);
        } else {

            return response()->json(['status' => 'error', 'message' => 'data not found']);
        }
    }

    public function messages()
    {

        $id     = Input::get('room_id');
        $userid = Input::get('userid');
       
        if (!isset($id)) {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }

        $data = DB::select("SELECT `messages`.`id`,`messages`.`sender_id`,`messages`.`reciver_id`,`messages`.`is_read`,
           `messages`.`room_id` AS room,`messages`.`message`,`messages`.`datetime`,`sender`.`Studio_Name` AS username
            FROM `messages`
            JOIN users AS sender ON sender.`id` = `messages`.`sender_id`
            JOIN users AS receiver ON receiver.`id` = `messages`.`reciver_id`
            WHERE messages.room_id ='$id' AND `messages`.`is_deleted`=0");
        $current_server_time = date('Y-m-d H:i:s');
        $data                = array(
            'current_server_time' => $current_server_time,
            'chats'               => $data,
        );

        if (count($data) > 0) {

          if(isset($userid))
          {
           DB::table('messages')->whereRaw('reciver_id='.$userid.'And room_id='.$id)->update(['is_read'=>'1']);
          }        
            return response()->json(['status' => 'success', 'data' => $data]);
        } else {

            return response()->json(['status' => 'success', 'data' => []]);
        }
    }

    public function userchatinglist()
    {
        $id = Input::get('userid');
        if (!isset($id)) {
            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }
        $chats = DB::select("SELECT * FROM (
        SELECT `chats`.`start_from` AS `chat_start_by`,
        `chats`.`receiver` AS received_by,
        `sender_user_details`.`profile_pic` AS `chat_start_profile_pic`,
        `receiver_user_details`.`profile_pic` AS `received_profile_pic`,
        `sender_user_details`.`Studio_Name` AS `chat_start_name`,
        `receiver_user_details`.`Studio_Name` AS `received_name`,
        `sender_user_details`.`id` AS s_id,`receiver_user_details`.`id` AS r_id,
        messages.* FROM `chats`
        JOIN `messages` ON `messages`.`room_id`=`chats`.`room_id`
        JOIN `users` AS sender_user_details ON `sender_user_details`.`id`=`chats`.`start_from`
        JOIN `users` AS receiver_user_details ON `receiver_user_details`.`id`=`chats`.`receiver`
        WHERE `chats`.`start_from`=$id OR `chats`.`receiver`=$id ORDER BY `messages`.`datetime` DESC
        ) AS `records` GROUP BY records.room_id  ORDER BY records.datetime DESC");

        $current_server_time = date('Y-m-d H:i:s');

        for ($i = 0; $i < count($chats); $i++) {

            if ($chats[$i]->chat_start_profile_pic != "" && $chats[$i]->chat_start_profile_pic != null) {
                $chats[$i]->chat_start_profile_pic = asset($chats[$i]->chat_start_profile_pic);
            }

            if ($chats[$i]->received_profile_pic != "" && $chats[$i]->received_profile_pic != null) {
                $chats[$i]->received_profile_pic = asset($chats[$i]->received_profile_pic);
            }
        }

        $data = array(
            'current_server_time' => $current_server_time,
            'chats'               => $chats,
        );
        // dd($data);

        if (count($data) > 0) {

            return response()->json(['status' => 'success', 'data' => $data]);
        } else {

            return response()->json(['status' => 'error', 'message' => 'data not found']);
        }
    }

    public function getLikedVideos()
    {
        BasicModel::checkParams(array('userid'));
        $BasicModel = new BasicModel();
        $userid     = Input::get('userid');
        $select     = array(
            'videos.id as videoid',
            'videos.studio_id as studioid',
            'videos.title',
            'videos.discribtion',
            'videos.is_pro',
            "video_category.category_name$this->lng as category",
            'videos.likes',
            'users.Studio_Name as StudioName',
            DB::raw('IF(users.cover_pic IS NOT NULL,CONCAT("' . url('') . '",users.cover_pic), "") AS cover_pic'),
            'users.Average_ratings as Averageratings',
            'videos.views',
            'videos.date',
            'videos.tags',
            'video_category.id as videocategoryid',
            DB::raw('IF(videos.Video_link IS NOT NULL ,CONCAT("' . $BasicModel->s3_baseurl . '",videos.Video_link), "") AS Video_link'),
            'videos.Video_link AS Video_link',
            DB::raw('IF(videos.thumbnail != "", CONCAT("' . $BasicModel->s3_baseurl . '",videos.thumbnail), "") AS thumbnail'),
            DB::raw('IF(users.profile_pic IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic), "") AS profilepic'),
            'videos.duration',
        );
        $result     = DB::table('videos')->select($select)->leftjoin('video_likes', 'videos.id', '=', 'video_likes.video_id')->leftjoin('users', 'users.id', '=', 'videos.studio_id')->leftjoin('video_category', 'videos.Video_category_id', '=', 'video_category.id')->where('video_likes.user_id', $userid)->get();
        $subcribers = array();
        if ($result != "") {

            for ($i = 0; $i < count($result); $i++) {

                $countsubcribers = DB::table('studio_subscribers')->select(DB::raw('COUNT(studio_subscribers.`subscribers_id`) AS studio_subscribers'))->where('studio_subscribers.studio_id', $result[$i]->studioid)->get();
                array_push($subcribers, $countsubcribers[0]->studio_subscribers);
            }
            for ($i = 0; $i < count($result); $i++) {

                $result[$i]->subcribers = $subcribers[$i];
            }
        }

        if ($result) {
            return response()->json(['status' => 'success', 'data' => $result]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'no data found']);
        }
    }

    public function studioprofilevideo()
    {
        BasicModel::checkParams(array('videoid', 'studio_id', 'remove'));

        $id     = Input::get('studio_id');
        $video  = Input::get('videoid');
        $remove = Input::get('remove');

        if ($remove == "0") {
            DB::table('videos')->where(['studio_id' => $id])->update(['is_pro' => '0']);
            $result = DB::table('videos')->where(['id' => $video])->update(['is_pro' => '1']);
            if ($result) {
                return response()->json(['status' => 'success', 'message' => 'Your profile video is selected']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'something went wrong try again']);
            }
        } else {
            DB::table('videos')->where(['studio_id' => $id])->update(['is_pro' => '0']);
            return response()->json(['status' => 'success', 'message' => 'Profile video has been removed']);
        }
    }

    public function reportVideo(Request $request)
    {
        BasicModel::checkParams(array('user_id', 'video_id', 'category', 'message', 'status'));
        $report_id = null;
        $user_id   = $request->user_id;
        $video_id  = $request->video_id;
        $category  = $request->category;
        $message   = $request->message;

        if ($request->status == 'delete') {
            BasicModel::checkParams(['report_id']);
            $report_id = $request->report_id;
        }

        if ($report_id) {
            $deleted = DB::table("video_reports")->where([['video_id', '=', $video_id], ['user_id', '=', $user_id]])->delete();

            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'Your report on this video has been removed']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'You didn\'t report this video yet']);
            }
        }
        $report_data = ['user_id' => $user_id, 'video_id' => $video_id, 'category' => $category, 'complain' => $message];
        $reported    = DB::table('video_reports')->insertGetId($report_data);

        if ($reported > 0) {
            $p_arr   = ['type' => 'video_report', 'title' => 'Report filled for video', 'body' => 'User has filled the report on video with id of |' . $video_id . '| and assigned category ' . $category . ' with the message ' . $message, 'push_data' => json_encode($report_data)];
            $push_id = DB::table('push_notifications')->insertGetId($p_arr);
            DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => 1]);
            $report_data['id'] = $reported;
            return response()->json(['status' => 'success', 'message' => 'We have submitted your report on this video', 'report_data' => $report_data, 'is_reported' => 1]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Report on this video can\'t be added at this time']);
        }
    }

    public function subscribed_channels(Request $request)
    {
        BasicModel::checkParams(['user_id']);
        $user_id = $request->user_id;
        $select  = ['id', 'Studio_Name', 'profile_pic', 'username', 'email', 'contact_no', 'user_type', 'country', 'zipcode', 'Average_ratings', 'time_zone', DB::raw('CONCAT("' . asset('') . '",cover_pic) as cover_pic'), DB::raw('CONCAT("' . asset('') . '",profile_pic) as profile_pic')];

        $channels = DB::table('users')->select($select)->whereRaw(" id IN (SELECT studio_id FROM `studio_subscribers` WHERE subscribers_id = '$user_id') ")->get();

        for ($i = 0; $i < count($channels); $i++) {
            $channels[$i]->subscribers = DB::table('studio_subscribers')->where('studio_id', '=', $channels[$i]->id)->count();
        }
        $query ="DELETE FROM `api_logs` WHERE _id NOT IN ( SELECT _id FROM (SELECT _id FROM `api_logs` ORDER BY _id DESC LIMIT 1000 ) foo )";
        @DB::select($query);
        
        if (is_object($channels) && count($channels) > 0) {
            return response()->json(['status' => 'success', 'message' => count($channels) . ' Studios found', 'data' => $channels]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'No subscribed channels']);
        }
    }

    public function liked_videos(Request $request)
    {
        BasicModel::checkParams(['user_id']);
        $user_id    = $request->user_id;
        $BasicModel = new BasicModel();
        $offset = Input::get('offset');
        $limit  = Input::get('limit');
        if(!isset($offset) || $offset=="" && !isset($limit) || $limit==""){
            $offset = 0;
            $limit  = 15;
        }

        $whereraw = " videos.id IN (SELECT video_id FROM `video_likes` WHERE user_id = '$user_id') ";
        if($this->inputs['usertype']=="normal_user"){
            $whereraw .=" And videos.is_visible =1";
        }
        $videos   = $BasicModel->getVideos(null, $whereraw,false, $limit,$offset);
        
        if (is_object($videos) && count($videos) > 0) {
            return response()->json(['status' => 'success', 'message' => count($videos) . ' Videos found', 'data' => $videos]);
        } else {
            return response()->json(['status' => 'success', 'data' => []]);
        }
    }

    public function getNotfications(Request $request, $whereRaw = null, $json = true)
    {
        BasicModel::checkParams(['user_id']);
        $user_id  = $request->user_id;
        $read_all = $request->read_all;
        $offset   = $request->offset;
        $limit    = $request->limit;
        if(!isset($offset) || $offset=="" && !isset($limit) || $limit==""){
            $offset = 0;
            $limit  = 10000;
        }

        if ($whereRaw) {
            $notifications = DB::table('push_notifications as pn')->select('pn.*', 'nd.*', 'nd.id as notification_id')->leftjoin('notification_details as nd', 'nd.push_id', '=', 'pn.id')->where('nd.is_deleted', '=', '0')->where('nd.sent_to', '=', $user_id)->whereRaw($whereRaw)->orderBy('nd.id', 'desc')->skip($offset)->limit($limit)->get();
        } else {
            $notifications = DB::table('push_notifications as pn')->select('pn.*', 'nd.*', 'nd.id as notification_id')->leftjoin('notification_details as nd', 'nd.push_id', '=', 'pn.id')->where('nd.is_deleted', '=', '0')->where('nd.sent_to', '=', $user_id)->orderBy('nd.id', 'desc')->skip($offset)->limit($limit)->get();
        }
        if (is_object($notifications) && count($notifications) > 0) {
            if ($json) {
                if ($this->inputs['registration_platform'] == 'web' && $read_all) {
                    DB::table('notification_details')->where('sent_to', $user_id)->update(['is_read' => 1]);
                }
                if ($this->inputs['registration_platform'] != 'web') {
                    DB::table('notification_details')->where('sent_to', $user_id)->update(['is_read' => 1]);
                }
                return response()->json(['status' => 'success', 'data' => $notifications]);
            } else {
                return count($notifications);
            }
        } else {
            if ($json) {
                return response()->json(['status' => 'success', 'data' => []]);
            } else {
                return 0;
            }
        }
    }

    public function updateNotification(Request $request)
    {
        BasicModel::checkParams(['user_id', 'notification_id']);
        $user_id         = $request->user_id;
        $notification_id = $request->notification_id;
        DB::table('notification_details')->where('id', $notification_id)->update(['is_deleted' => 1, 'is_read' => 1]);
        return response()->json(['status' => 'success', 'message' => "Notification has been deleted"]);
    }

    public function resume_service(Request $request)
    {
        BasicModel::checkParams(['user_id']);
        $user_id           = $request->user_id;
        $checkUser         = DB::table('users')->where('id', $user_id)->first();
        // $this->down_grade_subscribtion($user_id);
        $data              = new \stdClass();
        $data->user_exists = 0;
        if ($checkUser) {
            $data->is_suspended = 0;
            if ($checkUser->user_status == "0") {
                $data->is_suspended = 1;
            }
            DB::table('users')->where('id', $user_id)->update(['device_token' => $request->header('deviceToken')]);
            $data->user_exists = 1;
            unset($checkUser->password);
            unset($checkUser->device_token);
            $checkUser->profile_pic =($checkUser->profile_pic) ? asset('') . $checkUser->profile_pic: '';
            $checkUser->cover_pic_big   = ($checkUser->cover_pic_big) ? asset('') . $checkUser->cover_pic_big : '';
            $checkUser->cover_pic   = ($checkUser->cover_pic) ? asset('') . $checkUser->cover_pic : '';
            $checkUser->profile_pic_big = ($checkUser->profile_pic_big) ? asset('') . $checkUser->profile_pic_big: '';
            $data->user_data        = $checkUser;
            $data->notifications    = $this->getNotfications($request, 'nd.is_read = 0 ', false);
            $data->new_messages     =  DB::table('messages')->whereRaw('is_read=0 and  reciver_id='.$user_id)->count();
            $data->is_staff_member  =  DB::table('staff_member')->whereRaw('parent_id='.$user_id)->count();

            if ($request->header('usertype') == 'normal_user') {
                $data->package_details = DB::table('packages')->where(['user_type' => 'normal_user'])->orderBy('id', 'desc')->first();
            }
        }
            return response()->json(['status' => 'success', 'data' => $data]);
    }

   
    function autocomplete_searching(Request $request){
        BasicModel::checkParams(['lat', 'search_term','long']);
        $search_term = $request->search_term;
        if($search_term != '' && strlen($search_term) <= 2){
            return response()->json(['status' => 'error', 'data' => 'Search text must be 3 characters or more']);
        }
            $origLat = $request->lat;
            $origLon = $request->long;
            $min_distance = '0';
            $max_distance  = '25';
            
            $whereRaw2 = " AND (`studio_services`.`service_title`  like '%" . $search_term . "%')";

            $query = "SELECT users.id as studioid,studio_services.id,studio_services.service_title, 3956 * 2 * ASIN(SQRT( POWER(SIN(($origLat - users.lat)*pi()/180/2),2)+COS($origLat*pi()/180 )*COS(users.lat*pi()/180)*POWER(SIN(($origLon-users.lng)*pi()/180/2),2))) as distance FROM users LEFT JOIN studio_services ON users.id = studio_services.Studio_id
             WHERE lng between ($origLon-$max_distance/cos(radians($origLat))*69)
           and ($origLon+$max_distance/cos(radians($origLat))*69)
           and users.lat between ($origLat-($max_distance/69))
           and ($origLat+($max_distance/69)) " . $whereRaw2 . " GROUP BY studioid having distance  BETWEEN " . $min_distance . " AND " . $max_distance . " ORDER BY distance";

            $search_result =DB::select($query);

            return response()->json(['status' => 'success', 'data' => $search_result]);
           
    }

    function autocomplete_web(Request $request){
        BasicModel::checkParams(['search_term']);
        $search_term = $request->search_term;
        if($search_term != '' && strlen($search_term) <= 2){
            return response()->json(['status' => 'error', 'data' => 'Search text must be 3 characters or more']);
        }  
        $search_result = DB::table('studio_services')->select('id', 'service_title')->where('service_title','like', "%$search_term%")->orderby('id', 'desc')->get();
        return response()->json(['status' => 'success', 'data' => $search_result]);
        
    }
    function update_service_indexes(Request $request){
        BasicModel::checkParams(['studio_id', 'service_index']);
        $studio_id     = $request->studio_id;
        $service_index = $request->service_index;
        $service_id    = $request->service_id;

        foreach($service_index as $key => $value){
           
           DB::table('studio_services')->whereRaw('Studio_id ='.$studio_id. ' and  id='. $service_id[$key])->update(['service_index'=>$value]);
        }
        
           return response()->json(['status' => 'success', 'message' => "Updated"]);
       
    }
    function get_profile_details(Request $request){
        BasicModel::checkParams(['user_id']);
        $ids= array();
        $user_id     = $request->user_id;
        $data        = new \stdClass();
        $select=array(
            'users.id',
            'users.Studio_Name',
                DB::raw('IF(users.cover_pic IS NOT NULL, CONCAT("' . asset('') . '",users.cover_pic), "") AS cover_pic'),
                DB::raw('IF(users.cover_pic_big IS NOT NULL, CONCAT("' . asset('') . '",users.cover_pic_big), "") AS cover_pic_big'),
            'users.username',
            'users.email',
            'users.contact_no',
            'users.address',
            'users.user_type',
            'users.Average_ratings',
            DB::raw('IF(users.profile_pic IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic), "") AS profilepic'),
            DB::raw('IF(users.profile_pic_big IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic_big), "") AS profilepic_big'),
        );
        $data->user_deatils = DB::table('users')->select($select)->where('id', $user_id)->first();
        $staffids = DB::table('staff_member')->select('studio_id')->where('parent_id', $user_id)->get();
    
        foreach($staffids as $value){
            
            array_push($ids,$value->studio_id);

        }
        $data->staffmember = DB::table('users')->select($select)->whereIn('id',$ids)->get();
        
        return response()->json(['status' => 'success', 'data' =>$data]);
    
        }
    public function service_appointments(Request $request){
        BasicModel::checkParams(['studio_id']);
        $studio_id      = $request->studio_id;
        $offset         = $request->offset;
        $limit          = $request->limit;
        $appointment_id = $request->appointment_id;
    
        if(!isset($request->limit)){
            $offset ='0';
            $limit ='50';
        }
        $select = array(
            'users.id as user_id',
            'users.Studio_Name',
            DB::raw('IF(users.cover_pic IS NOT NULL, CONCAT("' . asset('') . '",users.cover_pic), "") AS cover_pic'),
            DB::raw('IF(users.cover_pic_big IS NOT NULL, CONCAT("' . asset('') . '",users.cover_pic_big), "") AS cover_pic_big'),
            'users.Studio_Name',
            'users.email',
            'users.contact_no',
            'users.address',
            'users.user_type',
            'users.lat',
            'users.lng',
            'users.Average_ratings',
            DB::raw('IF(users.profile_pic IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic), "") AS profilepic'),
            DB::raw('IF(users.profile_pic_big IS NOT NULL, CONCAT("' . asset('') . '",users.profile_pic_big), "") AS profilepic_big'),
            DB::raw('GROUP_CONCAT(working_hours.working_Days) as working_Days'),
            DB::raw('GROUP_CONCAT(working_hours.working_start) as working_start'),
            DB::raw('GROUP_CONCAT(working_hours.working_end ) as working_end'),
            DB::raw('GROUP_CONCAT(working_hours.lunch_time_to ) as lunch_time_to'),
            DB::raw('GROUP_CONCAT(working_hours.lunch_time_from ) as lunch_time_from'),
            'studio_services.Description',
            'studio_services.service_time',
            'studio_services.service_title',
            'studio_services.happy_hour_from',
            // 'studio_services.happy_hour_days',
            'studio_services.gender',
            'studio_services.happy_hour_to',
            'studio_services.service_time_unit',
            'service_appointments.status',
            'service_appointments.is_user_rejected',
            'service_appointments.time',
            'service_appointments.message',
            'service_appointments.charges',
            'service_appointments.currency_code',
            'service_appointments.is_happy_hour',
            'service_appointments.reason',
            'service_appointments.user_id as app_user_id',
            'service_appointments.studio_id as app_studio_id',
            'service_appointments.customer_name',
            'service_appointments.customer_number',
            "video_category.category_name$this->lng as categoryName",
            'studio_services.id as parent_rowid',
            'studio_services.discount_amount as discount_price',
            'studio_services.Service_charges as price',
            'service_appointments.id as child_rowid',
            DB::raw('IF(studio_services.services_image IS NOT NULL, CONCAT("' . asset('') . '",studio_services.services_image), "") AS services_image'),
            DB::raw('IF(studio_services.service_image_big IS NOT NULL, CONCAT("' . asset('') . '",studio_services.service_image_big), "") AS services_image_big'),
        );
        if (isset($studio_id)) {
            $result = DB::table("studio_services")->select($select)
            ->leftjoin('video_category', 'video_category.id', '=', 'studio_services.Category_id')->leftjoin('service_appointments', 'service_appointments.service_id', '=', 'studio_services.id')->leftjoin('working_hours', 'working_hours.studio_id', '=', 'service_appointments.Studio_id');
            if($this->inputs['usertype'] == "normal_user"){
                $result = $result->leftjoin('users', 'users.id','=', 'service_appointments.Studio_id');
                 $whereRaw = 'service_appointments.user_id=' . $studio_id;
                //   .' and  service_appointments.is_user_rejected = 1';
                
            } else {
                $result = $result->leftjoin('users', 'users.id','=', 'service_appointments.user_id');
                $whereRaw = 'service_appointments.Studio_id=' . $studio_id;
            }
            if(isset($appointment_id)){
                $whereRaw .= ' And service_appointments.id =' . $appointment_id;
            }
            // dd($whereRaw);
            $result = $result->whereRaw($whereRaw)->groupBy('service_appointments.id')->orderByRaw('service_appointments.id DESC')->skip($offset)->limit($limit)->get();
            
            $user_data= User::select('time_zone')->where('id',$studio_id)->first();
            if(!empty($user_data->time_zone)){
             date_default_timezone_set($user_data->time_zone);
            }
            $is_day_light_service = DB::table('working_hours')->where('working_Days',date('l'))->whereRaw('studio_id ='. $studio_id)->count();
            
            for($i=0;$i<count($result);$i++){
               if($this->inputs['usertype'] != "normal_user"){
                    if($result[$i]->status=="Pending"){
                        $result[$i]->push_data = json_encode(["studio_id"=>$result[$i]->app_studio_id,"service_appointments_id"=>$result[$i]->child_rowid,"user_id"=>$result[$i]->app_user_id]);
                    } 
                } else {
                    // if($result[$i]->type=="appointment_rejected"){
                    if($result[$i]->status=="Pending" || $result[$i]->status=="other_time"){
                        if(!empty($result[$i]->time)){
                        $result[$i]->push_data = json_encode(["time"=>$result[$i]->time,"is_happy_hour"=>$result[$i]->is_happy_hour,"happy_hour_from"=>$result[$i]->happy_hour_from,"happy_hour_to"=>$result[$i]->happy_hour_to,"price"=>$result[$i]->price,"discount_price"=>$result[$i]->discount_price,"currency_code"=>$result[$i]->currency_code,"currency_rate"=>1,"studio_id"=>$result[$i]->app_studio_id,"user_id"=>$result[$i]->app_user_id,"service_appointments_id"=>$result[$i]->child_rowid]);
                        }
                    }
                } 
            }   
                $refound_charges=DB::table('stripe_refund_charges')->first();
                return response()->json(['status' => 'success', 'data' =>$result,'is_day_light_service'=>$is_day_light_service,'refound_charges'=>$refound_charges->amount_percentage]);
        }
        
    }

    public function edit_services()
    {
        BasicModel::checkParams(['service_id']);
        $service_id        = Input::get('service_id');
        $category          = Input::get('categoryid');
        $time              = Input::get('duration');
        $gender            = Input::get('gender');
        $price             = Input::get('price');
        $decribtion        = Input::get('decribtion');
        $service_title     = Input::get('service_title');
        $happy_hour_from   = Input::get('happy_hour_from');
        $happy_hour_to     = Input::get('happy_hour_to');
        $discount_amount   = Input::get('discount_amount');
        $happy_hour_days   = Input::get('happy_hour_days');
        
        $service_time_unit = Input::get('service_time_unit');
        $fileName = "";
        $fileName_big="";
        $arr = [];
        if(!empty($happy_hour_from) && !empty($happy_hour_to)){
            $arr['happy_hour_from']      = $happy_hour_from;
            $arr['happy_hour_to']        = $happy_hour_to;
        }
        $data_array=[];
        if(!empty($happy_hour_days)){
            foreach($happy_hour_days as $value) {
                array_push($data_array,$value);
            }
        $arr['happy_hour_days']      = json_encode($data_array); 
        } 
        if(!empty($service_time_unit) ){
            $arr['service_time_unit'] = $service_time_unit;
        }
        if(isset($discount_amount)){
            $arr['discount_amount'] = $discount_amount;
        }
        if(!empty($category) ){
            $arr['Category_id']     = $category;
        }
        if(!empty($time) ){
            $arr['service_time']    = $time;
        }
        if(!empty($gender) ){
            $arr['gender']          = $gender;
        }
        if(!empty($decribtion) ){
            $arr['Description']     = $decribtion;
        }
        if(!empty($service_title) ){
            $arr['service_title']   = $service_title;
        }
        if(!empty($service_title) ){
            $arr['service_title']   = $service_title;
        }
        if(!empty($price) ){
            $arr['Service_charges']   = $price;
        }

        if(Input::hasFile('service_image')){
        if ($_FILES["service_image"]["size"]>0) {

            $path = public_path() . '/assets/uploads/serviceimg/';
            $path = $path . date('Y') . '/' . date('m') . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            // $path_parts =pathinfo($_FILES["service_image"]["name"]);
            $image_mime=getimagesize($_FILES["service_image"]["tmp_name"]);
            $path_parts=image_type_to_extension($image_mime[2]);
            $extension='.png';
            if(isset($path_parts)){
                $extension=$path_parts;
            }
            $fileName = 'studioapp' . '_up_' . time().$extension;
            Input::file('service_image')->move($path, $fileName);
            $fileName = "public/assets/uploads/serviceimg/" . date('Y') . '/' . date('m') . '/' . $fileName;
            $arr['services_image']  = $fileName;
            if(Input::hasFile('service_image_big')){
                $path = public_path() . '/assets/uploads/service_image_big/';
                $path = $path . date('Y') . '/' . date('m') . '/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                // $path_parts =pathinfo($_FILES["service_image"]["name"]);
                $image_mime=getimagesize($_FILES["service_image_big"]["tmp_name"]);
                $path_parts=image_type_to_extension($image_mime[2]);
                $extension='.png';
                if(isset($path_parts)){
                    $extension=$path_parts;
                }
                $fileName_big = 'studioapp' . '_up_' . time().$extension;
                Input::file('service_image_big')->move($path, $fileName_big);
                $fileName_big = "public/assets/uploads/service_image_big/" . date('Y') . '/' . date('m') . '/' . $fileName_big;
                $arr['service_image_big']=$fileName_big;
         }
        }
     }
        $result = DB::table('studio_services')->where('id',$service_id)->update($arr);
        if ($result > 0) {
            $arr['rowid'] = $service_id;
                if(isset($arr['services_image'])){
                    $arr['services_image']     =  asset('').$arr['services_image'];
                }else{

                    $arr['services_image']  = "";
                }
                if(isset($arr['service_image_big'])){
                    $arr['services_image_big'] = asset('').$arr['service_image_big'];
                }else {

                    $arr['services_image_big'] = "";
                }
            unset($arr['service_image_big']);
           
            $studio_services=DB::table('studio_services')->select('Category_id')->where('id',$service_id)->first();
            $data = DB::table('video_category')->select("category_name$this->lng as category_name")->where('id',$studio_services->Category_id)->first();
            $arr['categoryName'] = $data->category_name;
            
        }
        return response()->json(['status' => 'success', 'message' => 'updated','data'=> $arr]);
    }

    function get_service_by_id(Request $request){
        BasicModel::checkParams(['service_id']);
        $service_id     = $request->service_id;
        $select = array(
            'studio_services.service_time',
            'studio_services.Service_charges',
            'studio_services.service_title',
            'studio_services.gender',
            'studio_services.happy_hour_to',
            'studio_services.happy_hour_from',
            'studio_services.service_time_unit',
            'studio_services.discount_amount',
            'studio_services.Description',
            'studio_services.Category_id',
            'studio_services.happy_hour_days',
            'studio_services.currency_code',
            'studio_services.Studio_id',
            "video_category.category_name$this->lng as categoryName",
            'studio_services.id as rowid',
            DB::raw('IF(studio_services.services_image IS NOT NULL, CONCAT("' . asset('') . '",studio_services.services_image), "") AS services_image'),
            DB::raw('IF(studio_services.service_image_big IS NOT NULL, CONCAT("' . asset('') . '",studio_services.service_image_big), "") AS services_image_big'),
        );
            $whereRaw = 'studio_services.id='.$service_id;
            $data = DB::table("studio_services")->select($select)
                ->leftjoin('video_category', 'video_category.id', '=', 'studio_services.Category_id')->whereRaw($whereRaw)->orderByRaw('service_index ASC')->first();
                
            return response()->json(['status' => 'success', 'data' =>$data]);
    
    }
    
    public function add_appointment(Request $request)
    {
        BasicModel::checkParams(array('studio_id', 'time', 'service_id', 'price', 'customer_name','currency_code'));
        $studio_id        = $request->studio_id;
        $time             = $request->time;
        $price            = $request->price;
        $service_id       = $request->service_id;
        $customer_name    = $request->customer_name;
        $customer_number  = $request->customer_number;
        $currency_code    = $request->currency_code;
        $data=['studio_id'=>$studio_id,'time'=>$time,'status'=>'manual_accepted','charges'=>$price,'customer_name'=>$customer_name,'currency_code'=>$currency_code,'service_id'=>$service_id];
        if(!empty($customer_number)){
            $data['customer_number'] = $customer_number;
        }
        $result =DB::table('service_appointments')->insertGetId($data);
        if($result){
            return response()->json(['status' => 'success', 'message' => 'Service added to your calender']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Service not added to your calender']);
        }

    }

    public function edit_appointment(Request $request)
    {
        BasicModel::checkParams(array('Appointment_id', 'time', 'service_id', 'price', 'customer_name'));
        $Appointment_id   = $request->Appointment_id;
        $time             = $request->time;
        $price            = $request->price;
        $service_id       = $request->service_id;
        $customer_name    = $request->customer_name;
        $customer_number  = $request->customer_number;
        $result =DB::table('service_appointments')->where('id',$Appointment_id)->update(['time'=>$time,'charges'=>$price,'customer_name'=>$customer_name,'customer_number'=>$customer_number,'service_id'=>$service_id]);
        return response()->json(['status' => 'success', 'message' => $result]);

    }
    public function delete_appointment(Request $request)
    {
        BasicModel::checkParams(array('Appointment_id'));
        $Appointment_id   = $request->Appointment_id;
        $result =DB::table('service_appointments')->where('id',$Appointment_id)->delete();
        if($result){
            return response()->json(['status' => 'success', 'message' => 'Appointment remove from your calender']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Appointment not  remove']);
        }

    }

    public function About_us(Request $request)
    {   
        $about_us = DB::table('about_us')->first();
        $services = DB::table('video_category')->select(DB::raw('IF(video_category.service_image IS NOT NULL, CONCAT("' . asset('') . '",video_category.service_image), "") AS services_image'),'id',"category_name$this->lng as category_name",'status','slug',"description$this->lng as description")->where('status','studio')->get();
        $faq              = DB::table('faq')->get();
        $faqs_videos      = DB::table('faqs_videos')->get();
        if($about_us && $services && $faq){
            return response()->json(['status' => 'success','About_us' =>$about_us,'services' =>$services,'faq' =>$faq,'faqs_videos'=>$faqs_videos]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'no data found']);
        }
    }

    public function Add_bank_details(Request $request)
    {   
        BasicModel::checkParams(array('studio_id','first_name','last_Name','iban_num','bank_name','bank_zip','is_terms_confirm'));
        DB::table('bank_account_details')->where('studio_id',$request->studio_id)->delete();
        $result = DB::table('bank_account_details')->insert([$request->all()]);
        if($result){
            return response()->json(['status' => 'success','message' =>'Inserted']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
        }
    }

    public function get_bank_details_by_id(Request $request)
    {   
        BasicModel::checkParams(array('studio_id'));
        $result = DB::table('bank_account_details')->select('*')->where('studio_id',$request->studio_id)->first();
        return response()->json(['status' => 'success','data' =>$result]);
        
    }
    public function change_tutorial_status(Request $request)
    {   
        BasicModel::checkParams(array('studio_id'));
        $result = DB::table('users')->where('id',$request->studio_id)->update(['is_tutorial'=>1]);
        return response()->json(['status' => 'success','data' =>$result]);
        
    }
    public function down_grade_subscribtion($studioid)
    {   
        $user             = DB::table('users')->select('*')->where('id',$studioid)->first();
        if(!empty($user->time_zone)){
            date_default_timezone_set($user->time_zone);
        }
        $presentation_subscription_data=DB::table('presentation_subscription')->where('user_id',$studioid)->first();
       if(isset($presentation_subscription_data->end_time)){ 
        $today                  = date('Y-m-d H:i:s');
        $end_date_hour_minutes  = date('Y-m-d H:i:s',strtotime($presentation_subscription_data->end_time));
        ;
        if(strtotime($today)>strtotime($end_date_hour_minutes)){
            $paypal_card_id = DB::table('presentation_subscription')->where('user_id',$studioid)->delete();
            $video_category=DB::table('video_category')->where('status','custom')->first();
            DB::select("UPDATE videos SET is_visible=0 WHERE studio_id = '$studioid' and Video_category_id='$video_category->id'");
            DB::select("UPDATE videos SET is_pro=0 WHERE studio_id = '$studioid'");
        }
     }
    }
    
    public function url_genrater(Request $request)
    {   
        BasicModel::checkParams(array('link'));

        $link =BasicModel::signedUrl($request->link);
      
        return response()->json(['status' => 'success','data' =>$link]);
        
    }

    public function sign_up_user(Request $request)
    {   
        $allheader=getallheaders();
        $company    = Input::get('company_name');
        $ip         = $allheader['client_ip'];
        $date_time  = date('Y-m-d H:i:s');
        $data_array = array(
           'company_name'=>$company,
           'ip'          =>$ip,
           'date_time'   =>$date_time
        );
        $result = DB::table('sign_up_users')->insertGetId($data_array);    
        return response()->json(['status' => 'success','data' =>$result]);  
    }

    public function get_packages(Request $request)
    {   
        $data = DB::table('packages')->where('user_type','studio_user')->orderby('id', 'DESC')->get(); 
        return response()->json(['status' => 'success','data' =>$data]);  
    }

  
    // public function terms_condition(Request $request)
    // {   
    //     BasicModel::checkParams(array('studio_id'));
    //     $studio_id = Input::get('studio_id');
    //     $result = DB::table('users')->where('studio_id',$request->studio_id)->update(['is_terms_confirm'=>1]);
    //     return response()->json(['status' => 'success','data' =>$result]);
    // }

    public function get_ip_address() {
        foreach (array(
    'HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'REMOTE_ADDR'
        ) as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
}

