<?php
namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\Http\Requests;
use App\BasicModel;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use JWTAuthException;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{

    private $user;
    public $inputs = array();

    public function __construct(User $user, Request $request)
    {   
        BasicModel::api_logs($request);
        $this->headerarray['method'] = $request->path();
        $this->headerarray['REMOTE_ADDR']=$_SERVER['REMOTE_ADDR'];
        $this->user                             = $user;
        $this->inputs['usertype']               = $request->header('usertype');
        $this->inputs['registration_device_id'] = $request->header('deviceToken');
        $this->inputs['registration_platform']  = $request->header('platform');
        $this->inputs['timezone']               = $request->header('timezone');
        // $this->inputs['token']                  = $request->header('token');
        Input::merge(['token' => $request->header('token')]);
        // }
        // else
        // {

        //  $this->inputs['registration_device_id']="";
        //  $this->inputs['registration_platform'] ="";
        //  $this->inputs['token']                 ="";

        // }
    }

    //for mobile services
    public function getmobileregister(Request $request)
    {
        $data = Input::all();
        if (isset($data['Studio_Name']) && isset($data['email'])
            && isset($data['password']) && isset($data['usertype'])) {
            $contact = "";
            $email = $this->user->where("email",$data['email'])->first();
            $email = count($email);
            if ($email > 0) {
                return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Email_account_already_exist','error')]);
            }
            if (isset($data['contact_no'])) {
                $contact = $data['contact_no'];
            }   
                // if(!isset($data['country'])){
                $locationdata = BasicModel::location();
                //     $location=  $locationdata['location'];
                // } else {
                //     $location=  $data['country'];
                // }
                $data['deviceid'] = $this->inputs['registration_device_id'];
                $data['platform'] = $this->inputs['registration_platform'];
                $timezone         = $this->inputs['timezone'];
                $user_status= 0;
                if ($this->inputs['usertype'] == "normal_user") {
                    $user_status = 1;
                }
                $user = $this->user->create([
                    'Studio_Name'  => $data['Studio_Name'],
                    'email'        => $data['email'],
                    'contact_no'   => $contact,
                    'password'     => bcrypt($data['password']),
                    'user_type'    => $data['usertype'],
                    'device_token' => $data['deviceid'],
                    'platform'     => $data['platform'],
                    'time_zone'    => $timezone,
                    'country'      => $locationdata['location'],
                    'lat'          => $locationdata['lat'],
                    'lng'          => $locationdata['lng'],
                    'permissions'  => '',
                    'created_date' => date("Y-m-d H:i:s"),
                    'user_status'  => $user_status
                ]);
                
              
                $credentials = array(
                    'email'    => $data['email'],
                    'password' => $data['password'],
                );
                $token = null;
                try {
                    if (!$token = JWTAuth::attempt($credentials)) {
                        return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Invalid_Email_or_Password','error')]);
                    }
                } catch (JWTAuthException $e) {
                    
                    return response()->json(['status' => 'error', 'message' =>BasicModel::get_messages_lng('failed_to_create_token','error')]);
                }
                $token = compact('token');
                $user  = JWTAuth::toUser($token['token']);
                Input::replace(['email' => $data['email']]);
                $this->send_new_user($user);
                $this->getsendemail();
                if ($this->inputs['usertype'] == "normal_user") {
                    $packageid = 5;
                
                } else if ($this->inputs['usertype'] == "studio_user" || $this->inputs['usertype'] == "freelancer") {
    
                    $packageid = 1;
                }
                $date     = date('Y-m-d H:i:s');
                $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime(date('Y-m-d H:i:s'))));
                DB::table('subscription')->insert(['user_id' => $user['id'], 'package_id' => $packageid,'start_time'=>$date,'end_time'=>$end_time]);
                $currency_code=DB::table('currency_code_new')->select('CurrencyCode')->where('Country',$user['country'])->orWhere('Country', 'like', '%' . $user['country'] . '%')->first();
                $symbol= '$';
                $code  = 'USD';
                if(count($currency_code)>0){
                  $CurrencyCode=BasicModel::get_currencies_symbol($currency_code->CurrencyCode);
                  $symbol = $CurrencyCode;
                  if($symbol!='$')
                    $code   = $currency_code->CurrencyCode;
                }
                return response()->json(['status' => 'success', 'message' => BasicModel::get_messages_lng('Your_account_has_created','success'), 'result' => $user, 'token' => $token['token'],'symbol'=>$symbol,'CurrencyCode'=>$code]);
        } else {

               return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);

        }

    }

    public function dologinmobile(Request $request)
    {
        if ($this->inputs['usertype'] == "") {
            return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Invalid_Email_or_Password','error')]);
        }
        $credentials = $request->only('email', 'password');
        $checkUser   = $this->user->select('user_type')->where('email',$credentials['email'])->first(); 
        if (!isset($checkUser->user_type)) {
            
            return response()->json(['status' => 'error', 'message' =>BasicModel::get_messages_lng('Your_account_doesnt_exist','error')]);
        }

        if ($checkUser->user_type == 'admin' || $checkUser->user_type == 'sub_admin') {

            return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Invalid_Email_or_Password','error')]);
        }

        $user = DB::table('users')->select('user_type')->where('email', $credentials['email'])->first();

        if (count($user) == 0) {

            return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Your_account_doesnt_exist','error')]);

        }
        if ($user->user_type == "normal_user" && $this->inputs['usertype'] == "studio_user") {
            return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('This_account_is_linked_with_user','error')]);
        }
        if ($user->user_type == "studio_user" && $this->inputs['usertype'] == "normal_user") {
            return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('This_account_is_linked_with_studio','error')]);
        }
        if ($user->user_type == "freelancer" && $this->inputs['usertype'] == "normal_user") {
            return response()->json(['status' => 'error', 'message' =>BasicModel::get_messages_lng('Invalid_Email_or_Password','error')]);

        }
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Invalid_Email_or_Password','error')]);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('failed_to_create_token','error')]);
        }
        $token = compact('token');
        $user  = JWTAuth::toUser($token['token']);
        if ($user['profile_pic'] != "") {
            $user['profile_pic'] = asset($user['profile_pic']);
        }
        if ($user['cover_pic'] != "") {

            $user['cover_pic'] = asset($user['cover_pic']);

        }
        if ($user['cover_pic_big'] != "") {
            
                        $user['cover_pic_big'] = asset($user['cover_pic_big']);
            
            }
        if ($user['profile_pic_big'] != "") {
            
                        $user['profile_pic_big'] = asset($user['profile_pic_big']);
            
        }
        if($this->inputs['registration_platform']!="web"){
            DB::table('users')->where('id', $user['id'])->update(['device_token' => $this->inputs['registration_device_id'], 'platform' => $this->inputs['registration_platform'], 'is_online' => 1]);
        }
        $dataarray   = array();
        $dataarray2  = array();
        $result      = array();
        $studioid    = $user['id'];
        $is_verified = 1;
        $codeverify = DB::table('code_verified')->select('status')->where('email', $user['email'])->first();
        $st = isset($codeverify) ? $codeverify : false;
        if ($st) {
            if ($codeverify->status == 0) {
                Input::replace(['email' => $user['email']]);
                $this->getsendemail();
                $is_verified = 0;
            }

        } else{
                Input::replace(['email' => $user['email']]);
                $this->getsendemail();
                $is_verified = 0;

        }
        $whereRaw = "packages.user_type='studio_user' AND packages.is_presentation = 0";
        if ($user['user_type'] == "normal_user") {
            $whereRaw = "packages.user_type='normal_user'";
        }
        $allpackages = DB::table('packages')->select('*')->whereRaw($whereRaw)->get();
        $subscription = DB::table('subscription')->select('*')->where('user_id', $studioid)->first();
        $selectedpackage = "";
        $select = array(
            'id as packageid',
            'limit as video_limit',
            'package_name',
            'size_limit',
            'time_limit',
        );
        $st = isset($subscription->package_id) ? $subscription->package_id : false;
        if ($st) {
            $selectedpackage = DB::table('packages')->select($select)->where('id', $subscription->package_id)->first();
        } else {
            if ($this->inputs['usertype'] == "normal_user") {
                $packageid = 5;

            } else if ($this->inputs['usertype'] == "studio_user") {

                $packageid = 1;
            }
            $date     = date('Y-m-d H:i:s');
            $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime(date('Y-m-d H:i:s'))));
            DB::table('subscription')->insert(['user_id' => $user['id'], 'package_id' => $packageid,'start_time'=>$date,'end_time'=>$end_time]);
            $subscription = DB::table('subscription')->select('*')->where('user_id', $studioid)->first();
            $selectedpackage = DB::table('packages')->select($select)->where('id', $subscription->package_id)->first();
        }
        $is_presentation_free =  DB::table('stripe_refund_charges')->where('id','1')->first();
        if($is_presentation_free->is_presentation_free==1) {
            if($user->user_type == "freelancer" || $this->inputs['usertype'] == "studio_user") {
                $check = DB::table('presentation_subscription')->select('*')->where('user_id', $user['id'])->count();
            
                if ($check==0 && $user['is_first_login']==0) {
                    $presentation_data=['user_id'=>$user['id'],'package_id'=>6,'start_time'=>date('Y-m-d H:i:s'),'end_time'=>date('Y-m-d H:i:s', strtotime("+ 6 months", strtotime(date('Y-m-d H:i:s'))))] ;
                    $f  = DB::table('presentation_subscription')->insert($presentation_data);
                } 
            }
        }
            if($user['is_first_login']==0){
                DB::table('users')->where('id',$user['id'])->update(['is_first_login'=>1]);
            }

            $videos = DB::table('videos')->select('*')->where('studio_id', $studioid)->count();
            $selectedpackage->uploadedvideos = $videos;
            $result['packages']         = $allpackages;
            $result['selectedpackages'] = $selectedpackage;
            $currency_code=DB::table('currency_code_new')->select('CurrencyCode')->where('Country',$user['country'])->orWhere('Country', 'like', '%' . $user['country'] . '%')->first();
            $symbol= '$';
            $code  = 'USD';
            if(count($currency_code)>0){
              $CurrencyCode=BasicModel::get_currencies_symbol($currency_code->CurrencyCode);
              $symbol = $CurrencyCode;
              if($symbol!='$')
                $code   = $currency_code->CurrencyCode;
            }
            return response()->json(['status' => 'success', 'result' => $user, 'token' => $token['token'], 'packages' => $result, 'isverified' => $is_verified,'symbol'=>$symbol,'CurrencyCode'=>$code]);
       
    }
    
    public function getsendemail()
    {

        $email = Input::get('email');
        // $username  = Input::get('username');
        if (isset($email)) {
            // new email;
            $username = DB::table('users')->select('Studio_Name')->where('email', $email)->first();
            $to       = array('name' => '', 'email' => $email);
            $rand     = rand(1000, 9999);
            $data     = array('code' => $rand, 'name' => $username->Studio_Name);
            $view     = View::make('emails.verify_email',$data);
            $contents = (string) $view;
            $emailing = BasicModel::sendEmail($contents, $data, $to, "Verification Code");
            // dd($emailing);

            if ($emailing) {
                DB::table('code_verified')->where('email', $email)->delete();
                DB::table('code_verified')->insert(
                    ['code' => $rand, 'email' => $email]
                );
                return response()->json(['status' => 'success', 'message' => 'Email Send']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Email Not Send']);
            }
        } else {
            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }

    }

    public function contact_email()
    {
        $data   = Input::all();
        if(empty($data['Name'])){
            $data['Name']    = '';
            $data['Subject'] = '';
            $data['Phone']   = '';

        }
       
        if(isset($data['token']) || $data['token']==null){
            unset($data['token']);
        }
        
        $result = DB::table('contact_us')->insertGetId($data);
        // $username  = Input::get('username');
        if ($result) {
            // new email; info@p-b-v.ch 
            $to       = array('name' => $data['Name'], 'email' =>'info@hotspot4studio.com');
            $data     = array('data' => $data);
           
            $view     = View::make('emails.contact_us',$data);
            $contents = (string) $view;
            $emailing = BasicModel::sendEmail($contents, $data, $to, "contact us");
            // dd($emailing);
            if ($emailing) {
                return response()->json(['status' => 'success', 'message' => BasicModel::get_messages_lng('Email_Send','success')]);
            } else {
                return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Email_not_Send','error')]);
            }
        } 

    }

    public function send_new_user($data)
    {        
        // info@hotspot4studio.com
            // info@hotspot4beauty.com
            $to            = array('name' =>'Hotpost', 'email' =>'info@hotspot4beauty.com');
            $data['Name']  = $data['Studio_Name']; 
            $data['Email'] = $data['email']; 
            $data     = array('data' => $data);
            $view     = View::make('emails.contact_us',$data);
            $contents = (string) $view;
            $emailing = BasicModel::sendEmail($contents, $data, $to, "contact us");
            // dd($emailing);
            // if ($emailing) {
            //     return response()->json(['status' => 'success', 'message' => 'Email Send']);
            // } else {
            //     return response()->json(['status' => 'error', 'message' => 'Email Not Send']);
            // }
    }

    public function getverifyemail()
    {

        $verifycode = Input::get('code');
        if (isset($verifycode)) {
            $code  = DB::table('code_verified')->where('code', $verifycode)->get();
            $exist = count($code);
            if ($exist > 0) {
                DB::table('code_verified')->where('code', $verifycode)->update(['status' => 1]);
                return response()->json(['status' => 'success', 'message' => BasicModel::get_messages_lng('verified','success')]);

            } else {

                return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('not_verified','error')]);

            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);
        }

    }

    public function getforgetpassword()
    {

        $email = Input::get('email');
        if (isset($email)) {
            $email2 = DB::table('users')->where('email', $email)->get();
            $exist  = count($email2);
            $username = DB::table('users')->select('*')->where('email', $email)->first();
            // if ($username->user_type == "normal_user" && $this->inputs['usertype'] == "studio_user") {
                
            //     return response()->json(['status' => 'error', 'message' => "Invalid Username or Password"]);

            // }
            // if($username->user_type == "studio_user" && $this->inputs['usertype'] == "normal_user") {

            //     return response()->json(['status' => 'error', 'message' => "Invalid Username or Password"]);

            // }
            if ($exist > 0) {
                $password = $this->generateRandomString();
                // new email;
                $to       = array('name' => '', 'email' => $email);
                $rand     = rand(1000, 9999);
                $data     = array('name' => $username->Studio_Name, 'password' => $password);
                $view     = View::make('forget_password',$data);
                $contents = (string) $view;
                $emailing = BasicModel::sendEmail($contents, $data, $to, "Forget Password");
                // $emailing = BasicModel::sendEmail('forget_password', $data, $to, "Forget Password");
                if ($emailing) {

                    DB::table('users')->where('email', $email)->update(['password' => bcrypt($password)]);
                    return response()->json(['status' => 'success', 'message' =>BasicModel::get_messages_lng('Your_Password_Has_Been_Changed','success')]);

                }
            } else {

                return response()->json(['status' => 'error', 'message' => BasicModel::get_messages_lng('Your_account_doesnt_exist','error')]);

            }
        } else {

            return response()->json(['status' => 'error', 'message' => 'Parameter Missing']);

        }

    }

    public function getRefreshToken(Request $request)
    {
        // DB::table('users')->where('device_token', $this->inputs['registration_device_id'])->update(['device_token' => $this->inputs['registration_device_id'], 'platform' => $this->inputs['registration_platform']]);
        $token = JWTAuth::getToken();
        // if (! $token) {
        //     throw new BadRequestHttpException('Token not provided');
        // }
        // try {
        $token = JWTAuth::refresh($token);
        // } catch (TokenInvalidException $e) {
        //     throw new AccessDeniedHttpException('The token is invalid');
        // }
        $token = compact('token');
        return response()->json(['token' => $token['token']]);
    }

    public function logoutmobile(Request $request)
    {

        $id = Input::get('id');
        $f  = DB::table('users')->where('id', $id)->update(['is_online' => 0, 'device_token' => '']);
        return response()->json(['status' => 'success', 'message' => 'logout']);

    }

    public function generateRandomString($length = 8)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function convertStdToArray($data)
    {

        return json_decode(json_encode($data), true);

    }
   

}
