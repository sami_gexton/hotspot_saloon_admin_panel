<?php
namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use JWTAuthException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\BasicModel;


class AdminUserController extends Controller{
    private $user;

    public function __construct(User $user ,Request $request){
         $this->user = $user;
    }

    public function register(Request $request){
        Session::put('is_webapp', 1);
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
        
        $data=Input::all();
        $user = $this->user->create([
            'Studio_Name' => $data['Studio_Name'],
            'email'       => $data['email'],
            'contact_no'  => $data['contact_no'],
            'password'    => bcrypt($data['password']),
            'user_type'   => $data['usertype'],
            'profile_pic' => "http://localhost/BeautyStudio/public/assets/uploads/studioapp_up_1499413992.png",
            'Address'     => $data['Address'],
            'country'     => $data['country'],
            'zipcode'     => $data['zipcode'],


        ]);
        

        session()->put('register',1);
        return redirect('register');

    }
    
    public function dologin(Request $request){

        $credentials = $request->only('email', 'password');
        $checkUser   = $this->user->select('user_type')->where('email',$credentials['email'])->first();
        if(!empty($checkUser)){
            if ($checkUser->user_type == 'admin' || $checkUser->user_type == 'sub_admin') {
            if (Auth::attempt($credentials)) {
                session()->put('userdetails',Auth::User());
                return redirect()->intended('dashboard'); 
                }
                else{
                    return Redirect('/');
                }
            } else{
                    return Redirect('/');
            }
                exit;
       } else {
            return Redirect('/');
       }
           
    }
    public function getLogout(){

        Auth::logout();
        Session::flush(); 
        return redirect()->intended('login');
      
    }

    public function Checkemail(){
        $email = Input::get('email');
        $email = $this->user->where("email",$email)->get(); 
        $email =count($email);
        
        return response()->json(array(
        'email'=>$email,
        ));

       
    }
}