<?php
namespace App\Http\Controllers;

use App\BasicModel;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\User;
use App\StudioAppointments;
use Cartalyst\Stripe\Stripe as Stripe;

class Stripe_paymentController extends Controller
{

    public $inputs         = array();
    public $headerarray    = array();
    public $stripe_object;
   

    public function __construct(Request $request)
    {  
        $this->stripe_object  = new Stripe;
        $this->headerarray['REMOTE_ADDR']=$_SERVER['REMOTE_ADDR'];
        $this->headerarray['method'] = $request->path();
        if ($this->headerarray['method'] != "make_invoice" && $this->headerarray['method'] != "package_downgrade" && $this->headerarray['method'] != "notifications") {
            BasicModel::api_logs($request);
            $this->headerarray['username'] = $request->header('PHP_AUTH_USER');
            $this->headerarray['password'] = $request->header('PHP_AUTH_PW');
            $this->headerarray['REMOTE_ADDR'] = $request->server('REMOTE_ADDR');

            if ($request->header('usertype') != "") {

                $this->inputs['usertype'] = $request->header('usertype');

            } else {

                echo json_encode(['status' => 'error', 'message' => 'Unauthorized action']);
                exit;
            }
            $this->inputs['registration_platform']  = "";
            $this->inputs['appname']                = "";
            $this->inputs['registration_device_id'] = "";

            $platform    = $request->header('platform');
            $appname     = $request->header('appname');
            $deviceToken = $request->header('deviceToken');
            if ($deviceToken != "") {

                $this->inputs['registration_device_id'] = $request->header('deviceToken');
            }

            if ($platform != "") {
                $this->inputs['registration_platform'] = $request->header('platform');

            }

            if ($appname != "") {
                $this->inputs['appname'] = $request->header('appname');

            }

            if ($request->header('token') != "") {

                Input::merge(['token' => $request->header('token')]);

                $this->middleware('jwt.auth', ['except' => ['authenticate']]);

            } else {

                if ($this->headerarray['username'] != "hotspot" || $this->headerarray['password'] != "admin123!@#.") {

                    echo json_encode(['status' => 'error', 'message' => 'Unauthorized action']);
                    exit;

                }
            }
        }
    }
    public function DoPayment_stripe(Request $request)
    {   
        $parms = array('user_id',
                       'package_id',
        );
        BasicModel::checkParams($parms);
        $packageid         = $request->package_id;
        $user_id           = $request->user_id;
        $is_pre_payment    = '';
        $stripe_token      = $request->stripe_token;
        $last_card_id      = $request->last_card_id;
        $stripe_subscriptions_id = '';
        $packagedata = DB::table('packages')->select('package_name','Price as package_price','is_presentation','stripe_plan_name')->where('id', $packageid)->first();
        if ($packagedata->package_price == "0.0") {
            echo json_encode(['status' => 'error', 'message' => 'Invalid Package']);
            exit;
        }
        if ($packagedata->is_presentation == "1") {
             $is_pre_payment = 1;
             $stripe_subscriptions_id  = DB::table('presentation_subscription')->select('stripe_subscriptions_id')->where('user_id', $user_id)->first();
             if(isset($stripe_subscriptions_id->stripe_subscriptions_id)){
                $stripe_subscriptions_id = $stripe_subscriptions_id->stripe_subscriptions_id;
             }

        } else {
             $is_pre_payment = 0;
             $stripe_subscriptions_id  = DB::table('subscription')->select('stripe_subscriptions_id')->where('user_id', $user_id)->first();
             if(isset($stripe_subscriptions_id->stripe_subscriptions_id)){
               $stripe_subscriptions_id = $stripe_subscriptions_id->stripe_subscriptions_id;
             }
        }
        
        $user = DB::table('users')->select('Studio_Name', 'email','stripe_customer_id')->where('id', $user_id)->first();
        $stripe_customer_id       = $user->stripe_customer_id;
            if(empty($stripe_customer_id)){
                $customer_data=array(
                        'email'     => $user->email,
                        'user_id'   => $user_id
                );     
                $stripe_customer_id =  $this->make_stripe_customer($customer_data);
                DB::table('users')->where('id',$user_id)->update(['stripe_customer_id'=> $stripe_customer_id]);
            }  
            if(empty($last_card_id)){
                $card_data=array(
                    'customer_id'=> $stripe_customer_id,
                    'card_token' => $stripe_token,
                    'user_id'    => $user_id
                );  
                $last_card_id=$this->Add_card_customer_strip($card_data);
             }  
            
                $currency=BasicModel::mulity_currency_with_rate();
                $currency_code             = $currency->currencyCode;
                $coverted_currency         = $currency->coverted_currency;
                $coverted_currency         = number_format((float)($coverted_currency*$packagedata->package_price), 2, '.', '');
                $currencyCode = trim($currency_code);
                if($currencyCode!="USD" && $currencyCode!="AUD" && $currencyCode!="CHF" && $currencyCode!="EUR" && $currencyCode!="GBP" ){
                    
                        $currencyCode='USD';
                        $coverted_currency = $packagedata->package_price;
                }
                $charge=array(
                        'customer_id' => $stripe_customer_id,
                        'currency'    => $currencyCode,
                        'amount'      => $coverted_currency,
                        'last_card'   => $last_card_id,
                        'user_id'     => $user_id,
                        'metadata'    => ['product_name' => $packagedata->package_name,
                                          'amount'       => $coverted_currency,
                                          'user_id'      => $user_id,
                                          'packageid'    => $packageid,
                                          'platform'     => $this->inputs['registration_platform'],
                                        ]
                );
               $payment_data=$this->charge_payment($charge);
               $check_stripe_subscriptions= 0;
               $meta_sub= [
               'product_name'   => $packagedata->package_name,
               'user_id'        => $user_id,
               "packageid"      => $packageid,
               "platform"       => $this->inputs['registration_platform'],
               'is_pre_payment' => $is_pre_payment
               ];
            //    dd($stripe_subscriptions_id);
               if(empty($stripe_subscriptions_id)){
                    $stripe_subscriptions_id = $this->create_subscriptions($stripe_customer_id,$packagedata->stripe_plan_name,$meta_sub,$user_id);
                    $check_stripe_subscriptions= 1;
                    if($is_pre_payment==0){
                        DB::table('subscription')->where('user_id',$user_id)->update(['stripe_subscriptions_id'=> $stripe_subscriptions_id]);
                    } else {
                        DB::table('presentation_subscription')->where('user_id',$user_id)->update(['stripe_subscriptions_id'=> $stripe_subscriptions_id]);
                    }
                }
                if($is_pre_payment==0 && $check_stripe_subscriptions==0){
                    $subscription = $this->stripe_object->subscriptions()->update( $stripe_customer_id, $stripe_subscriptions_id, [
                        'plan'     => $packagedata->stripe_plan_name,
                        // 'source'   => $last_card_id,
                        'metadata' => $meta_sub
                    ]);
                    $this->stripe_object->customers()->update($stripe_customer_id, [
                        'default_source' => $last_card_id,
                    ]);
                }
                $this->update_package($user_id,$packageid,$payment_data['source']['last4'],$is_pre_payment,$payment_data['id'],$stripe_subscriptions_id);
    }

   public function update_package($studio_id,$packageid,$last_four,$is_pre_payment,$payment_id,$subscribtion_id=null) {
        $data   = ['studio_id' => $studio_id, 'package_id' => $packageid, 'paypal_card_id' => $payment_id, 'date' => date('Y-m-d H:i:s'), 'end_date' => date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d H:i:s')))), 'payment_id' => $payment_id];
        DB::table('paypal_payment_cards')->insert($data);
            $date     = date('Y-m-d H:i:s');
            $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime($date)));
            $data = [
                "package_id"         => $packageid,
                "start_time"         => $date,
                "end_time"           => $end_time,
                "credit_card_last"   => $last_four,
                // 'sripe_payment_plan' => $sripe_payment_plan,
            ];
            if ($is_pre_payment > 0) {
                $check = DB::table('presentation_subscription')->select('*')->where('user_id', $studio_id)->count();
                if ($check > 0) {
                    $data['stripe_subscriptions_id'] = $subscribtion_id;
                    $f = DB::table('presentation_subscription')->where('user_id', $studio_id)->update($data);
                    unset($data['stripe_subscriptions_id']);
                  
                } else {
                    $data['user_id'] = $studio_id;
                    $data['stripe_subscriptions_id'] = $subscribtion_id;
                    $f  = DB::table('presentation_subscription')->insert($data);
                    unset($data['stripe_subscriptions_id']);
                }
                     $video_category=DB::table('video_category')->where('status','custom')->first();
                     DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$studio_id' and Video_category_id='$video_category->id'");
            } else {
                $check = DB::table('subscription')->select('*')->where('user_id', $studio_id)->count();
                if ($check > 0) {
                    $f = DB::table('subscription')->where('user_id', $studio_id)->update($data);
                } else {
                    $data['user_id'] = $studio_id;
                    DB::table('subscription')->insert($data);
                }
                $totalvideos = DB::table('videos')->where('studio_id', $studio_id)->count();
                $videolimit  = DB::table('packages')->select('limit')->where('id',$packageid)->first();
               
                $video = (array) $videolimit->limit;
                if ($totalvideos > $video[0]) {
                    $limit = $totalvideos - $video[0];
                    DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$studio_id' and is_pro=0 ORDER BY id ASC LIMIT $limit ");
                } else if($video[0]> $totalvideos) {
                    DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$studio_id' and is_pro=0");
                }
            }
            $username = DB::table('users')->select('Studio_Name', 'email')->where('id', $studio_id)->first();
            $to       = array('name' => '', 'email' => $username->email);
            $data     = array('name' => $username->Studio_Name);
            $view     = View::make('emails.sucess_payment',$data);
            $contents = (string) $view;
            BasicModel::sendEmail($contents, $data, $to, "Payment Details");
            $packagedata = DB::table('packages')->select('package_name')->where('id',$packageid)->first();
            $title            = "Subscribtion";
            $body             = $username->Studio_Name . " has Subscribed to ". $packagedata->package_name;
            $data2 =  new \stdClass();
            $data2->studio_id = $studio_id;
            $p_arr            = array(
                'title'     => $title,
                'body'      => $body,
                'push_data' => json_encode($data2),
                'type'      => 'Subscribed',
            );
            $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
            $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' =>'', 'device_token' =>'', 'is_sent' => 1]);
            echo json_encode(array("status" => "success", 'message' => 'Thank You Payment Recieved SucessFully'));
            exit;
    }

    public function buy_service(Request $request)
    {   
        $parms = array(
                        'user_id',
                        'studio_id',
                        'service_id',
                        'time',
                        'is_happy_hour',
        );
        BasicModel::checkParams($parms);
            $select = array(
                'studio_services.service_time',
                'studio_services.Service_charges',
                'studio_services.gender',
                'studio_services.happy_hour_to',
                'studio_services.happy_hour_from',
                'studio_services.discount_amount',
                'studio_services.Description',
                'studio_services.service_title',
                'studio_services.currency_code',
            );
            $service_id      = $request->service_id;
            $studioid        = $request->studio_id;
            $user_id         = $request->user_id;
            $stripe_token    = $request->stripe_token;
            $time            = $request->time;
            $is_happy_hour   = $request->is_happy_hour;
            $last_card_id    = $request->pre_card_id;
            $message         = $request->message;
            $studio           = DB::table('users')->select('*')->where('id',$user_id)->first();
            if(!empty($studio->time_zone)){
                date_default_timezone_set($studio->time_zone);
            }    
            $today                  = date('Y-m-d H:i:s');
            $end_date_hour_minutes  = date('Y-m-d H:i:s', strtotime($time));
            if($today>$end_date_hour_minutes){
                return response()->json(['status' => 'error', 'message' => "Appointment time must be above current time"]);
            }
            $user = DB::table('users')->select('Studio_Name', 'email','stripe_customer_id')->where('id', $user_id)->first();
            $stripe_customer_id       = $user->stripe_customer_id;
                if(empty($stripe_customer_id)){
                    $customer_data=array(
                            'email'     => $user->email,
                            'user_id'   => $user_id
                    );     
                    $stripe_customer_id =  $this->make_stripe_customer($customer_data);
                    DB::table('users')->where('id',$user_id)->update(['stripe_customer_id'=> $stripe_customer_id]);
                }  
                if(empty($last_card_id)){
                    $card_data=array(
                        'customer_id'=> $stripe_customer_id,
                        'card_token' => $stripe_token,
                        'user_id'    => $user_id
                    );  
                    $last_card_id=$this->Add_card_customer_strip($card_data);
                 }  
                    $this->stripe_object->customers()->update($stripe_customer_id, [
                        'default_source' => $last_card_id,
                    ]);

            if(empty($request->message)){
                $message  ='';
            }
            $check_hh        = DB::table("studio_services")->select($select)->where('id',$service_id)->first();
            $charges             = $check_hh->Service_charges;
            if($is_happy_hour!="false"){
                $charges         = $check_hh->discount_amount;
            }
            
            $payment_id='';
            $charge=array(
                'customer_id' => $stripe_customer_id,
                'currency'    => $check_hh->currency_code,
                'amount'      => $charges,
                'user_id'     => $user_id,
                'metadata'    => [
                                    'product_name'=> $check_hh->service_title,
                                    'amount'      => $charges,
                                    'user_id'     => $user_id,
                                    'studio_id'   => $studioid,
                                    "service_id"  => $service_id,
                                    "platform"    => $this->inputs['registration_platform'],
                ],
                'capture'    => false 
                );  
            $user = DB::table('users')->select('*')->where('id', $user_id)->first(); 
            $payment_id =  $this->charge_payment_service($charge);
            // if($pre_card_id==""){
            //     $payment_id =  $this->charge_payment_service($charge);
            //  } else {
                
            //     $charge['last_card'] = $pre_card_id;
            //     $payment_id =  $this->charge_payment_service($charge);
            //  }
                
                $data=[
                'user_id'       => $user_id,
                'studio_id'     => $studioid,
                'service_id'    => $service_id,
                'time'          => $time,
                'status'        => 'Pending',
                'message'       => $message,
                'charges'       => $charges,
                'payment_id'    => $payment_id,
                'currency_code' => $check_hh->currency_code
                ];
                if($is_happy_hour!='false'){
                    $data['is_happy_hour']=1;
                }
                $service_appointments_id=DB::table('service_appointments')->insertGetId($data);
                if($service_appointments_id){
                    $data2              = new \stdClass();
                    $studio           = DB::table('users')->select('*')->where('id',$studioid)->first();
                    $title            = "Request For Service Appointment";
                    $body             = $user->Studio_Name . " has request to " .$check_hh->service_title ." at ".date('h:i a',strtotime($time));
                    $data2->studio_id = $studioid;
                    $data2->service_appointments_id = $service_appointments_id;
                    $data2->user_id=$user_id;
                    $p_arr          = array(
                        'title'     => $title,
                        'body'      => $body,
                        'push_data' => json_encode($data2),
                        'type'      => 'new_appointment',
                    );
                    $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
                    $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $studioid, 'device_token' =>$studio->device_token, 'is_sent' => 1]);
                    // DB::table('service_appointments')->where('id',$push_id)->update(['push_id'=>$push_id]);
                    if(!empty($studio->device_token)){
                        BasicModel::pushNotification($title, $body, array($studio->device_token), 'new_appointment',$data2);
                    } 
                    return response()->json(['status' => 'success', 'message' => "Appointment has been send to studio"]);
                }
    }
    public function update_appointment(Request $request) {   
        $parms = array(
                        'service_appointments_id',
                        'action',
                        'studioid',
                        'push_id'
        );
        BasicModel::checkParams($parms);
        $service_appointments_id = $request->service_appointments_id;
        $action                  = $request->action;
        $studioid                = $request->studioid;
        $push_id                 = $request->push_id;
        $service_message         = '';
        DB::table('notification_details')->where('id',$push_id)->update(['is_deleted' => 1, 'is_read' => 1]);
        $studio           = DB::table('users')->select('*')->where('id',$studioid)->first();
        $get_appointment=DB::table('service_appointments')->select('*')->where('id',$service_appointments_id)->first();
        if($action!='false'){
            if(!empty($studio->time_zone)){
                date_default_timezone_set($studio->time_zone);
            }
            if($get_appointment->status=='Accepted' || $get_appointment->status=='Rejected' || $get_appointment->status=='other_time'){
                return response()->json(['status' => 'error', 'message' => "Appointment is Already Confirmed or rejected"]);
            }    
        $today                  = date('Y-m-d H:i:s');
        $end_date_hour_minutes  = date('Y-m-d H:i:s', strtotime($get_appointment->time));
        if($today>$end_date_hour_minutes){
            
            return response()->json(['status' => 'error', 'message' => "Appointment is expire"]);
        }
        $studio_service =DB::table('studio_services')->select('*')->where('id',$get_appointment->service_id)->first();
        $charges             = $get_appointment->charges;
        // if($get_appointment->is_happy_hour=='1'){
        //     $charges         = $studio_service->discount_amount;
        //     $this->stripe_object->charges()->update($get_appointment->payment_id,[
        //         'metadata' => [
        //             'amount'      => $charges,  
        //         ],
        //         ]);
        // }
        $f =  $this->charge_payment_service_capture($get_appointment->payment_id,$charges,$studioid);
         if($f){
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Accepted','charges'=>$charges]);
            if($result){
               $title            = "Your  appointment is Accepted";
               $body             =  $studio->Studio_Name . " has confirmed appointment";
               $notify_type      = 'appointment_accepted';
               $service_message  = 'confirmed';
               $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type);
            //    $this->make_invoice($f,$studio_service->service_title,$service_appointments_id,$get_appointment->user_id,$get_appointment->payment_id);
            }
         } else {
            $title     = "Your Payment is not recieved, that's why your appointment is rejected";
            $body      = "Your Payment is not recieved, that's why your appointment is rejected";
            $notify_type      = 'appointment_rejected';
            $reason           = "Error in Doing service Payment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason]);
            $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type);
            return response()->json(['status' => 'error', 'message' => "Error in Doing service Payment"]);
         }
        
    } else {
        $check_hh=DB::table('service_appointments')->select('service_id','status')->where('id',$service_appointments_id)->first();
        $service_id             = $check_hh->service_id;
        $reason                 = $request->reason;
        $time                   = $request->time;
        $notification_data      = null;
        $is_happy_hour   = 0;
        $happy_hour_from = '';
        $happy_hour_to   = '';
        $title            = "Your appointment is Rejected";
        $notify_type      = 'appointment_rejected';
        $service_message  = 'rejected';
        if(!empty($time)){
            if($check_hh->status=='Accepted' || $check_hh->status=='Rejected' || $check_hh->status=='other_time'){
                return response()->json(['status' => 'error', 'message' => "Appointment is Already Confirmed or rejected"]);
            }

            if($check_hh->status=='other_time'){
                return response()->json(['status' => 'error', 'message' => "You Already change time of this Appointment"]);
            }
            // dd($time);
            $studio           = DB::table('users')->select('*')->where('id',$studioid)->first();
            if(!empty($studio->time_zone)){
                date_default_timezone_set($studio->time_zone);
            }    
            $today                  = date('Y-m-d H:i:s');
            $end_date_hour_minutes  = date('Y-m-d H:i:s', strtotime($time));
            if($today>$end_date_hour_minutes){
                return response()->json(['status' => 'error', 'message' => "Appointment time must be above current time"]);
            }
            DB::table('service_appointments')->where('id',$service_appointments_id)->update(['time' =>$time,'is_user_rejected'=>'0','reason'=>$reason,'status'=>'other_time']);
            $body = $studio->Studio_Name." will be Available at " . date('h:i a',strtotime($time));
            $check_hh=DB::table('studio_services')->select('discount_amount','happy_hour_from','happy_hour_to','Service_charges','discount_amount','currency_code','happy_hour_days')->where('id',$service_id)->first();
            $is_show_happy=0;
            if(!empty($studio->time_zone)){
                date_default_timezone_set($studio->time_zone);
            }
            $days = [];  
            if(!empty($check_hh->happy_hour_days)){
                $days  = json_decode($check_hh->happy_hour_days);
            }
            $today = Date('l');
            $start_date_hour_minutes    = date('H:i:s', strtotime($check_hh->happy_hour_from));
            $end_date_hour_minutes      = date('H:i:s', strtotime($check_hh->happy_hour_to));
            $now_date_hour_minutes      = date('H:i:s');
            if($start_date_hour_minutes<=$now_date_hour_minutes && $end_date_hour_minutes>=$now_date_hour_minutes && in_array($today,$days)){
                $is_show_happy= 1 ;
            } 
            // if($check_hh->discount_amount){
                $happy_hour_from = $check_hh->happy_hour_from;
                $happy_hour_to   = $check_hh->happy_hour_to;
                $price           = $check_hh->Service_charges;
                $discount_price  = $check_hh->discount_amount;
                $currency_code   = $check_hh->currency_code;
                $happy_hour_days = $check_hh->happy_hour_days;
                $notification_data=array('time'=>$time,'is_happy_hour'=>$is_show_happy,'happy_hour_from'=>$happy_hour_from,'happy_hour_to'=>$happy_hour_to,'price'=>$price,'discount_price'=>$discount_price,'currency_code'=>$currency_code,'happy_hour_days'=>$happy_hour_days);
            // } 
           
        } else {
            // BasicModel::checkParams(['reason']);
            if($check_hh->status=='Accepted' || $check_hh->status=='Rejected' || $check_hh->status=='other_time'){
                return response()->json(['status' => 'error', 'message' => "Appointment is Already Confirmed or rejected"]);
            }
           
            $body             =  $studio->Studio_Name . " has cancel appointment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$body]);     
            }
            $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type,$notification_data);
        }
        // dd($notification_data); // dd($notification_data);
             return response()->json(['status' => 'success', 'message' => $service_message]);
    }

    public function update_appointment_user(Request $request) {   
        $parms = array(
                        'service_appointments_id',
                        'action',
                        'studioid',
                        'userid',
                        'push_id',
                        'is_happy_hour',
        );
        BasicModel::checkParams($parms);
        $service_appointments_id = $request->service_appointments_id;
        $action                  = $request->action;
        $studioid                = $request->studioid;
        $push_id                 = $request->push_id;
        $userid                  = $request->userid;
        $is_happy_hour           = $request->is_happy_hour;
        DB::table('notification_details')->where('id',$push_id)->update(['is_deleted' => 1, 'is_read' => 1]);
        $studio           = DB::table('users')->select('*')->where('id',$userid)->first();
        $time_zone        = DB::table('users')->select('time_zone')->where('id',$studioid)->first();
        $get_appointment=DB::table('service_appointments')->select('*')->where('id',$service_appointments_id)->first();
        if($action!='false'){
            if($get_appointment->status=='Accepted' || $get_appointment->status=='Rejected'){
                return response()->json(['status' => 'error', 'message' => "Appointment is Already Confirmed or rejected"]);
            }
            if(!empty($time_zone->time_zone)){
                date_default_timezone_set($time_zone->time_zone);
            }    
            $today                  = date('Y-m-d H:i:s');
            $end_date_hour_minutes  = date('Y-m-d H:i:s', strtotime($get_appointment->time));
            if($today>$end_date_hour_minutes){
                return response()->json(['status' => 'error', 'message' => "Appointment is expire,Please Make a New Appointment Request"]);
            }    
         $studio_service =DB::table('studio_services')->select('*')->where('id',$get_appointment->service_id)->first();
         $charges = $get_appointment->charges;
        //  if($is_happy_hour!='false'){
        //     $charges              =  $studio_service->discount_amount;

            //    $this->stripe_object->charges()->update($get_appointment->payment_id,[
            //     'metadata' => [
            //         'amount'      => $charges,  
            //     ],
            //     ]);
        //  }

         $f =  $this->charge_payment_service_capture($get_appointment->payment_id,$charges,$userid);
         DB::table('service_appointments')->where('id',$service_appointments_id)->update(['payment_id'=>$f]);
         if($f){
            $reason           =    'No Reason';
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Accepted','reason'=>'no reason']);
            if($result){
               
               $title            = "Appointment is Confirmed";
               $body             =  $studio->Studio_Name . " has confirmed appointment";
               $notify_type      = 'appointment_accepted';
               $service_message  = 'confirmed';
               $this->service_notification($get_appointment->user_id,$studioid,$service_appointments_id,$title,$body,$notify_type);
            //    $this->make_invoice($f,$studio_service->service_title,$service_appointments_id,$get_appointment->user_id,$get_appointment->payment_id);
            }
         } else {
            $title     = "Your Payment is not recieved, that's why your appointment is rejected";
            $body      = "Your Payment is not recieved, that's why your appointment is rejected";
            $notify_type      = 'User_appointment_rejected';
            $reason           = "Error in Doing service Payment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason,'is_user_rejected'=>'0']);
            $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type);
            return response()->json(['status' => 'error', 'message' => "Error in Doing service Payment"]);
         }
        
    } else {
            if($get_appointment->status=='Accepted' || $get_appointment->status=='Rejected'){
                return response()->json(['status' => 'error', 'message' => "Appointment is Already Confirmed or rejected"]);
            }
            $reason                = $studio->Studio_Name ." is rejected appointment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason,'is_user_rejected'=>'0']);
            $service_message  = 'rejected';
            $notify_type      = 'appointment_rejected';
            $title            = "Your appointment is Rejected";
            $body             =  $studio->Studio_Name . " has Rejected your  appointment";
            $this->service_notification($get_appointment->user_id,$studioid,$service_appointments_id,$title,$body,$notify_type);
        }
           return response()->json(['status' => 'success', 'message' => $service_message]);
    }
    public function user_cancel_appointment(Request $request) {   
        $parms = array(
                        'service_appointments_id',
        );
        BasicModel::checkParams($parms);
        $service_appointments_id = $request->service_appointments_id;
        $get_appointment=DB::table('service_appointments')->select('*')->where('id',$service_appointments_id)->first();
        $studio           = DB::table('users')->select('*')->where('id',$get_appointment->studio_id)->first();
        $user             = DB::table('users')->select('*')->where('id',$get_appointment->user_id)->first();
        if(!empty($studio->time_zone)){
            date_default_timezone_set($studio->time_zone);
        }
        $today                  = date('Y-m-d H:i:s');
        $end_date_hour_minutes  = date('Y-m-d H:i:s',strtotime('+1 day',strtotime($get_appointment->time)));
        if(strtotime($today)>strtotime($end_date_hour_minutes)){
            return response()->json(['status' => 'error', 'message' => "You Can't Cancel a Appointment After 24 Hours"]);
        }
        DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status'=>'Rejected']);
       
        $title            = "Appointment Cancel";
        $body             = "Appointment Cancel by ". $user->Studio_Name;
        $notify_type      = 'appointment_rejected';
        $this->service_notification($get_appointment->user_id,$get_appointment->studio_id,$service_appointments_id,$title,$body,$notify_type);
        $stripe_refund_charges=DB::table('stripe_refund_charges')->select('amount_percentage')->first();
        // dd($get_appointment->charges);
        $charges = $get_appointment->charges * ($stripe_refund_charges->amount_percentage / 100);
        $charges = round($charges);
        $charges = $get_appointment->charges - $charges;
        $refund=$this->Refund_payment($get_appointment->payment_id,$charges,$get_appointment->user_id);
        DB::table('service_appointments')->where('id',$service_appointments_id)->update(['stripe_refund_id'=>$refund['id'],'reason'=>$body]);
        return response()->json(['status' => 'success','message' => "Appointment Canceled"]);
       
    }
    public function make_stripe_invoice(Request $request){
        // $data= $request->data['object']['metadata']['product_name'];
        // $data = DB::table('stripes_webhooks')->insert(['stripe_webhooks_data'=>$data]);
        // return response()->json(['status' => 'success','data' =>$data]);
        if(!isset($request->data['object']['metadata']['product_name'])){
             return response()->json(['status' => 'success']);
        }
        $data = [
            "product_name"      => $request->data['object']['metadata']['product_name'],
            "create_time"       => date('Y-m-d H:i:s'),
            "paypalid"          => $request->data['object']['id'],
            "currency_code"     => $request->data['object']['currency'],
            "short_description" => 'HotSpot Payment Invoice',
            "userid"            => $request->data['object']['metadata']['user_id'],
            "platform"          => $request->data['object']['metadata']['platform'],
            "end_date"          => date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d H:i:s')))),
            "paypal_card_id"    => $request->data['object']['id'],
        ];

        if(isset($request->data['object']['metadata']['packageid'])){  
            $data["packageid"]  = $request->data['object']['metadata']['packageid'];
            $data["payment_type"]  = 'package_payment';
            
        } else {
            $data["userid"]        = $request->data['object']['metadata']['studio_id'];
            $data["packageid"]     = $request->data['object']['metadata']['service_id'];
            
        }
        if(isset($request->data['object']['metadata']['amount'])){  
            $data["amount"]        = $request->data['object']['metadata']['amount'];
          
        }
       $data["payment_status"]  = 'unpaid';
       $result = DB::table('payment_invoices')->insertGetId($data);
       $data=json_encode($request->data['object']);
       $data = DB::table('stripes_webhooks')->insert(['stripe_webhooks_data'=>$data]);
       return response()->json(['status' => 'success','data' =>$data]);
    }

    Public function make_stripe_customer($data = array()){
        $customer='';
        try {
            $customer = $this->stripe_object->customers()->create([
            'email'        =>  $data['email'],
            'description'  => 'Hotpost4beauty Customer Acoount',
            'metadata'     =>  ['hotspot_user_id'=>$data['user_id']]
            ]);
        } catch (Exception $ex) {
            $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in make_stripe_customer", "studio_id" => $data['user_id'], "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
            exit;
        }
        return  $customer['id'];
    }

    Public function Add_card_customer_strip($data = array()){
            $card='';
        try {
            $card = $this->stripe_object->cards()->create($data['customer_id'],$data['card_token']);
            
        } catch (Exception $ex) {
            $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in Add_card_customer_strip", "studio_id" => $data['user_id'], "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
            exit;
        }
          return $card['id'];
    }
  
    Public function create_subscriptions($customer_id,$plan,$mata_data=array(),$user_id){
        try {
            $subscription = $this->stripe_object->subscriptions()->create($customer_id,[
                'plan'     => $plan,
                // 'source'   => $source,
                'metadata' => $mata_data
            ]);
        } catch (Exception $ex) {
            $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in create_subscreation", "studio_id" => $user_id, "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
            exit;
        }
            return $subscription['id'];

    }

    Public function All_customer_cards(Request $request){

        $user_id   = $request->user_id;
        $user_data =  DB::table('users')->select('stripe_customer_id')->where('id',$user_id)->first();
        if(!empty($user_data->stripe_customer_id)){
            try {
                $cards = $this->stripe_object->cards()->all($user_data->stripe_customer_id);
            } catch (Exception $ex) {
                $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in all_cards", "studio_id" => $request->user_id, "date" => date('Y-m-d H:i:s')];
                $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
                echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
                exit;
            }
                return response()->json(['status' => 'success','data' =>$cards]);
                exit;
        } else {
                echo json_encode(array("status" => "error",'data' =>[]));
                exit;
        }
    }
  
    Public function All_customer_subscriptions($customer_id){
        $subscriptions =  $this->stripe_object->subscriptions()->all($customer_id);
        $subscriptions_array=array();
        foreach ($subscriptions['data'] as $subscription) {
            array_push($subscriptions_array,$subscription['plan']['id']);
        }
        return $subscriptions_array;
    }

    Public function charge_payment($data = array()){
         $charge_array = array( 
         'customer' => $data['customer_id'],
         'currency' => $data['currency'],
         'amount'   => $data['amount'],
         'source'   => $data['last_card'],
         'metadata' => $data['metadata'],
         );
        try {
            $charge = $this->stripe_object->charges()->create($charge_array);
                // $charge = $this->stripe_object->charges()->capture($charge_array['source']);
                // return $charge['id'];
        } catch (Exception $ex) {
            $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in charge_payment", "studio_id" => $data['user_id'], "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
            exit;
        }
          return $charge; 
    }

    Public function charge_payment_service($data = array()){
        $charge_array = array( 
        'customer' => $data['customer_id'],  
        'currency' => $data['currency'],
        'amount'   => $data['amount'],
        'metadata' => $data['metadata'],
        'capture'  => $data['capture'],
        );
       try {
           $charge = $this->stripe_object->charges()->create($charge_array);
               // $charge = $this->stripe_object->charges()->capture($charge_array['source']);
               // return $charge['id'];
       } catch (Exception $ex) {
           $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in charge_payment_service", "studio_id" => $data['user_id'], "date" => date('Y-m-d H:i:s')];
           $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
           echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
           exit;
       }
         return $charge['id']; 
   }

    Public function charge_payment_service_capture($card_id,$charges,$user_id){
    try {
        $charge = $this->stripe_object->charges()->capture($card_id,$charges);
    } catch (Exception $ex) {
        $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in charge_payment_service_capture", "studio_id" => $user_id, "date" => date('Y-m-d H:i:s')];
        $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
        echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
        exit;
    }
        return $charge['id']; 
    }

    Public function Refund_payment($payment_id,$amount,$user_id) {
        try {
            $charge_Refund = $this->stripe_object->refunds()->create($payment_id,$amount);
        } catch (Exception $ex) {
            $dataarray = ['paypal_error_msg' => $ex->getMessage(), 'system_error_msg' => "Error in Refund_payment", "studio_id" => $user_id, "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            echo json_encode(array("status" => "error",  'message' => $ex->getMessage()));
            exit;
        }
            return $charge_Refund; 
    }
    
    public function service_notification($studioid,$user_id,$service_appointments_id,$title,$body,$notify_type,$notification_data=null){
        $data2            = new \stdClass();
        if($notification_data!=null){
            $data2->time            = $notification_data['time'];
            $data2->is_happy_hour   = $notification_data['is_happy_hour'];
            $data2->happy_hour_from = $notification_data['happy_hour_from'];
            $data2->happy_hour_to   = $notification_data['happy_hour_to'];  
            $data2->price           = $notification_data['price'];  
            $data2->discount_price  = $notification_data['discount_price'];
            $data2->currency_code   = $notification_data['currency_code']; 
            $data2->happy_hour_days = $notification_data['happy_hour_days'];
            $data2->currency_rate   = 1;
        }
            $data2->studio_id = $studioid;
            $data2->user_id   = $user_id;
            $data2->service_appointments_id  = $service_appointments_id;

        $studio           = DB::table('users')->select('*')->where('id',$user_id)->first();
        $p_arr          = array(
            'title'     => $title,
            'body'      => $body,
            'push_data' => json_encode($data2),
            'type'      => $notify_type,
        );
        // dd($p_arr);
        $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
        $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $user_id, 'device_token' =>$studio->device_token, 'is_sent' => 1]);
        // dd($data2);
        if(!empty($studio->device_token)){
            BasicModel::pushNotification($title, $body, array($studio->device_token), $notify_type, $data2);
        }
    }
    public function downgrade(Request $request){

        if(!isset($request->data['object']['metadata']['is_pre_payment'])){
            return response()->json(['status' => 'success']);
        }
        $is_pre_payment = $request->data['object']['metadata']['is_pre_payment'];
        $studioid       = $request->data['object']['metadata']['user_id'];
        if ($is_pre_payment == 1) {
            $paypal_card_id = DB::table('presentation_subscription')->where('user_id',$studioid)->delete();
            $video_category=DB::table('video_category')->where('status','custom')->first();
            DB::select("UPDATE videos SET is_visible=0 WHERE studio_id = '$studioid' and Video_category_id='$video_category->id'");
            DB::select("UPDATE videos SET is_pro=0 WHERE studio_id = '$studioid'");
            exit;
        }
        $date     = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime($date)));
        $data     = [
            "package_id"       => 1,
            "start_time"       => $date,
            "end_time"         => $end_time,
            "credit_card_last" => '',
            'stripe_subscriptions_id'=>'',
        ];
        DB::table('subscription')->where('user_id',$studioid)->update($data);
        // DB::table('paypal_payment_cards')->where('studio_id', $studioid)->update(['is_working' => 0]);
        $totalvideos = DB::table('videos')->where('studio_id', $studioid)->count();
        $videolimit  = DB::table('packages')->select('limit')->where('id', '1')->first();
        // dd($totalvideos);
        $video = (array) $videolimit->limit;

        if ($totalvideos > $video[0]) {
            $limit = $totalvideos - $video[0];
            DB::select("UPDATE videos SET is_visible=0 WHERE studio_id = '$studioid' and is_pro=0 ORDER BY id ASC LIMIT $limit ");
        }
        $data=json_encode($request->data['object']);
        DB::table('stripes_webhooks')->insert(['stripe_webhooks_data'=>$data]);

    }

    public function getIP() {
        $ip = $_SERVER['SERVER_ADDR'];

        if (PHP_OS == 'WINNT'){
            $ip = getHostByName(getHostName());
        }

        if (PHP_OS == 'Linux'){
            $command="/sbin/ifconfig";
            exec($command, $output);
            // var_dump($output);
            $pattern = '/inet addr:?([^ ]+)/';
            $ip = array();
            foreach ($output as $key => $subject) {
                $result = preg_match_all($pattern, $subject, $subpattern);
                if ($result == 1) {
                    if ($subpattern[1][0] != "127.0.0.1")
                    $ip = $subpattern[1][0];
                }
            //var_dump($subpattern);
            }
        }
        return $ip;
    }
    public function get_ip_address() {
        foreach (array(
    'HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'REMOTE_ADDR'
        ) as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
    public function currencyConverter($from_Currency,$to_Currency,$amount) {
        // $from_Currency = urlencode($from_Currency);
        // $to_Currency = urlencode($to_Currency);
        // $encode_amount = 1;
        $get = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
        $get = explode("<span class=bld>",$get);
        if(isset($get[1])){
          $get = explode("</span>",$get[1]);
          $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
          return $converted_currency;
        }
    }

    public function notifications(Request $request)
    {  
        $notifications = DB::table('push_notifications as pn')->select('pn.*', 'nd.*', 'nd.id as notification_id')->leftjoin('notification_details as nd', 'nd.push_id', '=', 'pn.id')->whereIn('pn.type', ['video_report','new_video','Subscribed'])->orderBy('nd.id', 'desc')->get();
        // dd($notifications);
        if($request->ajax()){
            return response()->json(['status' => 'success', 'data' =>$notifications]);
        } else {
            DB::table('notification_details')->update(['is_admin_read' => 1]);
        }
        return view('notification')->with('notifications',$notifications);
    }

}
