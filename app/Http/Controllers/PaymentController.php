<?php
namespace App\Http\Controllers;

use App\BasicModel;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\User;
use App\StudioAppointments;

class PaymentController extends Controller
{

    public $inputs         = array();
    public $headerarray    = array();
    private $_ClientId     = 'ASUEz0Nsc0VraIc_u7wm-XvYzfvCWeI61DO1P4yZszKikTqgbuje1v5wEW8WS3uvvDLYMVAJ9IJTcUHv';
    private $_ClientSecret = 'EM_fZqnPIBbGx18YC3sfsjsM8d2oowBh4ALJIZkQN50LhbtbtaO1SDyNMFU0kmkQyUKC7ViBuENjSsQf';
    private $_Marchant_id  = '6T7AFQ8M58TQ2';

    public function __construct(Request $request)
    {
        BasicModel::api_logs($request);
        $this->headerarray['REMOTE_ADDR']=$_SERVER['REMOTE_ADDR'];
        $this->headerarray['method'] = $request->path();
        if ($this->headerarray['method'] != "checkpayment") {
            $this->headerarray['username'] = $request->header('PHP_AUTH_USER');
            $this->headerarray['password'] = $request->header('PHP_AUTH_PW');
            $this->headerarray['REMOTE_ADDR'] = $request->server('REMOTE_ADDR');

            if ($request->header('usertype') != "") {

                $this->inputs['usertype'] = $request->header('usertype');

            } else {

                echo json_encode(['status' => 'error', 'message' => 'Unauthorized action']);
                exit;
            }

            $this->inputs['registration_platform']  = "";
            $this->inputs['appname']                = "";
            $this->inputs['registration_device_id'] = "";

            $platform    = $request->header('platform');
            $appname     = $request->header('appname');
            $deviceToken = $request->header('deviceToken');
            if ($deviceToken != "") {

                $this->inputs['registration_device_id'] = $request->header('deviceToken');
            }

            if ($platform != "") {
                $this->inputs['registration_platform'] = $request->header('platform');

            }

            if ($appname != "") {
                $this->inputs['appname'] = $request->header('appname');

            }

            if ($request->header('token') != "") {

                Input::merge(['token' => $request->header('token')]);

                $this->middleware('jwt.auth', ['except' => ['authenticate']]);

            } else {

                if ($this->headerarray['username'] != "hotspot" || $this->headerarray['password'] != "admin123!@#.") {

                    echo json_encode(['status' => 'error', 'message' => 'Unauthorized action']);
                    exit;

                }
            }
        }
    }

    public function DoPayment(Request $request)
    {   
        $parms = array('studio_id',
            'package_id',
            'cardtype',
            'card_number',
            'Expiremonth',
            'Expireyear',
            'Cvv2',
            'is_pre_payment',
        );
        BasicModel::checkParams($parms);

        $packageid      = $request->package_id;
        $studioid       = $request->studio_id;
        $is_pre_payment = $request->is_pre_payment;
        $credit_card    = $request->card_number;

        $credit_card = base64_decode($credit_card);
        // $credit_card = sqrt($credit_card);
        $credit_card = $credit_card - date('Y');
        // dd($credit_card);

        $packagedata = DB::table('packages')->select('package_name','Price as package_price','is_presentation')->where('id', $packageid)->first();

        if ($packagedata->package_price == "0.0") {
            echo json_encode(['status' => 'error', 'message' => 'Invalid Package']);
            exit;

        }
        if ($packagedata->is_presentation == "1") {
             $is_pre_payment = 1;

        } else {

             $is_pre_payment = 0;
        }

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->_ClientId, $this->_ClientSecret)
        );
        $user = DB::table('users')->select('Studio_Name', 'email')->where('id', $studioid)->first();

        $card = new \PayPal\Api\CreditCard();
        $card->setType($request->cardtype)
            ->setNumber($credit_card)
            ->setExpiremonth($request->Expiremonth)
            ->setExpireyear($request->Expireyear)
            ->setCvv2($request->Cvv2)
            ->setFirstName($user->Studio_Name);
        $card->setMerchantId($this->_Marchant_id);
        $card->setExternalCardId("HotSpot" . uniqid());
        $card->setExternalCustomerId($user->email);
        $request = clone $card;
        try {
            $card->create($apiContext);
        } catch (Exception $ex) {
            // dd($ex);
            $dataarray = ['paypal_error_msg' => $ex->getMessage().$ex->getData(), 'system_error_msg' => "Error in create card", "studio_id" => $studioid, "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            // $this->downgrade($studioid,$is_pre_payment);
            return response()->json(['status' => 'error', 'message' => 'something went wrong try again later']);
            // dd("Create Credit Card", "Credit Card", null, $request, $ex);
            exit();
        }
        // dd("Create Credit Card", "Credit Card", $card->getId(), $request, $card);
        return $this->createpayment($studioid, $packageid, $card->getId(), false, $is_pre_payment);

    }

    public function createpayment($studio_id, $packageid, $paypalcard_id, $condition = false, $is_pre_payment = false,$invoice_id=0)
    {   
      
        $packagedata = DB::table('packages')->select('package_name', 'Price as package_price')->where('id', $packageid)->first();

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->_ClientId, $this->_ClientSecret)
        );
        $creditCardToken = new \PayPal\Api\CreditCardToken();
        $creditCardToken->setCreditCardId($paypalcard_id);
        // $creditCardToken->setCreditCardId("card-12345678");

        $fi = new \PayPal\Api\FundingInstrument();
        $fi->setCreditCardToken($creditCardToken);

        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));
        if ($condition != true) {
        $currency=BasicModel::mulity_currency_with_rate();
        $currency_code             = $currency->currencyCode;
        $coverted_currency = $currency->coverted_currency;
        $coverted_currency = number_format((float)($coverted_currency), 2, '.', '');
        $currencyCode = trim($coverted_currency);
        $data['coverted_currency']  = $coverted_currency;
        $data['currencyCode']       = trim($coverted_currency);
        // $data=array('currencyCode'=>$currencyCode,'coverted_currency'=>$coverted_currency);
        // return response()->json(['status' => 'success', 'data' =>$data]);
        if($currencyCode!="USD" && $currencyCode!="AUD" && $currencyCode!="CHF" && $currencyCode!="EUR" && $currencyCode!="GBP" ){
            
                $currencyCode='USD';
                $coverted_currency=$packagedata->package_price;
    
         }
        }
        // dd($coverted_currency);
        // $currencyCode='CHF';
        // $coverted_currency=$packagedata->package_price;
        if ($condition == true) {
            
            $payment= DB::table('payment_invoices')->where('id',$invoice_id)->first();
            $currencyCode      = $payment->currency_code;
            $coverted_currency = $payment->amount;

        }
        // $data=array('currencyCode'=>$currencyCode,'coverted_currency'=>$coverted_currency);
        // return response()->json(['status' => 'success', 'data' =>$data]);

        $item = new \PayPal\Api\Item();
        $item->setName($packagedata->package_name)
            ->setCurrency($currencyCode)
            ->setQuantity(1)
            ->setPrice($coverted_currency);
        $itemList = new \PayPal\Api\ItemList();
        $itemList->setItems(array($item));

        $details = new \PayPal\Api\Details();
        $details->setShipping(0.0)
            ->setTax(0.0)
            ->setSubtotal($coverted_currency);

        $amount = new \PayPal\Api\Amount();
        $amount->setCurrency($currencyCode)
            ->setTotal($coverted_currency)
            ->setDetails($details);

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($packagedata->package_name)
            ->setInvoiceNumber(uniqid());

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));
        $request = clone $payment;

        try {
            $payment->create($apiContext);
        } catch (Exception $ex) { 
            $dataarray = ['paypal_error_msg' => $ex->getMessage().$ex->getData(), 'system_error_msg' => "Error in Doing Payment", "studio_id" => $studio_id, "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            $username  = DB::table('users')->select('Studio_Name', 'email')->where('id', $studio_id)->first();
            $to        = array('name' => '', 'email' => $username->email);
            $data      = array('name' => $username->Studio_Name);
            $view     = View::make('emails.error_payment',$data);
            $contents = (string) $view;
            BasicModel::sendEmail($contents, $data, $to, "Payment Details");
            if ($condition == true) {

                $this->downgrade($studio_id, $is_pre_payment);

            }

            return response()->json(['status' => 'error', 'message' => "something is missing try again later"]);

            // dd("Create Payment using Saved Card", "Payment", null, $request, $ex);
            exit(1);
        }  
        // dd("Create Payment using Saved Card", "Payment", $payment->getId(), $request, $payment);
        $paymentId = $payment->getId();
        // $paymentId = "15111";
        $Payment = new \PayPal\Api\Payment;

        try {
            $payment = $Payment::get($paymentId, $apiContext);
        } catch (Exception $ex) {

            $dataarray = ['paypal_error_msg' =>  $ex->getMessage().$ex->getData(), 'system_error_msg' => "Error in geting Payment", "studio_id" => $studio_id, "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
            $username  = DB::table('users')->select('Studio_Name', 'email')->where('id', $studio_id)->first();
            $to        = array('name' => '', 'email' => $username->email);
            $data      = array('name' => $username->Studio_Name);
            $view     = View::make('emails.error_payment',$data);
            $contents = (string) $view;
            BasicModel::sendEmail($contents, $data, $to, "Payment Details");
            if ($condition == true) {
                $this->downgrade($studio_id, $is_pre_payment);
            }
            return response()->json(['status' => 'error', 'message' => "something is missing try again later"]);
        }

        if ($condition == true) {
            $this->inputs['registration_platform'] = "Cron Job";
        }

        $data = [
            "product_name"      => $packagedata->package_name,
            "create_time"       => date('Y-m-d H:i:s'),
            "paypalid"          => $payment->getId(),
            "amount"            => $payment->transactions[0]->amount->total,
            "currency_code"     => $payment->transactions[0]->amount->currency,
            "short_description" => $payment->transactions[0]->description,
            "userid"            => $studio_id,
            "packageid"         => $packageid,
            "platform"          => $this->inputs['registration_platform'],
            "end_date"          => date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d H:i:s')))),
            "paypal_card_id"    => $paypalcard_id,
        ];

        $result = DB::table('payment_invoices')->insertGetId($data);
        if(count($result)>0) {
            if ($condition == true) {
                
                DB::table('payment_invoices')->where('id',$invoice_id)->update(['status'=>1]);
            }
        }
        $data   = ['studio_id' => $studio_id, 'package_id' => $packageid, 'paypal_card_id' => $paypalcard_id, 'date' => date('Y-m-d H:i:s'), 'end_date' => date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d H:i:s')))), 'payment_id' => $result];
        DB::table('paypal_payment_cards')->insert($data);

        if ($result > 0) {

            $date     = date('Y-m-d H:i:s');
            $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime($date)));

            $data = [
                "package_id"       => $packageid,
                "start_time"       => $date,
                "end_time"         => $end_time,
                "credit_card_last" => $payment->payer->funding_instruments[0]->credit_card_token->last4,

            ];
            if ($is_pre_payment > 0) {
                $check = DB::table('presentation_subscription')->select('*')->where('user_id', $studio_id)->count();
                if ($check > 0) {
                    $f = DB::table('presentation_subscription')->where('user_id', $studio_id)->update($data);
                    // return  $f."update";

                } else {
                $data['user_id'] = $studio_id;
                    $f  = DB::table('presentation_subscription')->insert($data);
                    // return  $f."insert";
                }
                     $video_category=DB::table('video_category')->where('status','custom')->first();
                     DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$studio_id' and Video_category_id='$video_category->id'");
            } else {

                $check = DB::table('subscription')->select('*')->where('user_id', $studio_id)->count();
                if ($check > 0) {
                    $f = DB::table('subscription')->where('user_id', $studio_id)->update($data);

                } else {
                    $data['user_id'] = $studio_id;
                    DB::table('subscription')->insert($data);
                }

                $totalvideos = DB::table('videos')->where('studio_id', $studio_id)->count();
                $videolimit  = DB::table('packages')->select('limit')->where('id',$packageid)->first();
                // dd($totalvideos);
                $video = (array) $videolimit->limit;
                if ($totalvideos > $video[0]) {
                    $limit = $totalvideos - $video[0];
                    DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$studio_id' and is_pro=0 ORDER BY id ASC LIMIT $limit ");
                } else if($video[0]> $totalvideos) {

                    DB::select("UPDATE videos SET is_visible=1 WHERE studio_id = '$studio_id' and is_pro=0");

                }
            }
            $username = DB::table('users')->select('Studio_Name', 'email')->where('id', $studio_id)->first();
            $to       = array('name' => '', 'email' => $username->email);
            $data     = array('name' => $username->Studio_Name);
            $view     = View::make('emails.sucess_payment',$data);
            $contents = (string) $view;
            BasicModel::sendEmail($contents, $data, $to, "Payment Details");
            return response()->json(['status' => 'success', 'message' => 'Thank You Payment Recieved SucessFully']);
            // BasicModel::sendEmail('emails.sucess_payment', $data, $to, "Verification Code");

        } else {

            return response()->json(['status' => 'error', 'message' => 'something went wrong try again later']);
        }

    }
    public function lastpayment(Request $request)
    {
        $studio_id = Input::get('studio_id');
        $parms     = array('studio_id');
        BasicModel::checkParams($parms);
        $subscriptioncard = DB::table('subscription')->select('start_time','end_time','credit_card_last')->where('user_id', $studio_id)->orderBy('id','desc')->first();
        $presentation_subscription = DB::table('presentation_subscription')->select('start_time','end_time','credit_card_last')->where('user_id', $studio_id)->orderBy('id','desc')->first();
        $data = array('subscription_card'=>$subscriptioncard,'presentation_subscription'=>$presentation_subscription);

        if (count($lastpayment) > 0 || count($presentation_subscription) > 0 ) {
            return response()->json(['status' => 'success', 'data' =>$data]);

        } else {
            return response()->json(['status' => 'error', 'message' => "No old payment found"]);

        }

    }

    public function checkpayment(Request $request)
    {     
        // $dataarray=['paypal_error_msg' =>"Run from cron job",'system_error_msg'=>'Run from cron job','studio_id'=>'',"date"=>date('Y-m-d H:i:s')];
        //  DB::table('payment_error_logs')->insert($dataarray);
        // $this->inputs['registration_platform']="Cron Job";
        // $paymentids   = array();
        $enddateyear  = date('Y');
        $enddatemonth = date('m');
        $enddatedate  = date('d');
        $whereRaw = "YEAR(payment_invoices.end_date) = " . $enddateyear . " AND Month(payment_invoices.end_date) = " . $enddatemonth . "  AND  DAY(payment_invoices.end_date) = " . $enddatedate . " AND payment_invoices.status = '0'";
        $select = array('payment_invoices.id','users.time_zone', 'payment_invoices.end_date', 'payment_invoices.userid', 'payment_invoices.packageid', 'payment_invoices.paypal_card_id', 'packages.is_presentation');
        $getpaystudio = DB::table('payment_invoices')->select($select)->leftjoin('users', 'payment_invoices.userid', '=', 'users.id')->leftjoin('packages', 'payment_invoices.packageid', '=', 'packages.id')->whereRaw($whereRaw)->get();
        for ($i = 0; $i < count($getpaystudio); $i++) {
            $is_pre_payment = false;
            date_default_timezone_set($getpaystudio[$i]->time_zone);
            $enddateyear    = date('Y', strtotime($getpaystudio[$i]->end_date));
            $enddatemonth   = date('m', strtotime($getpaystudio[$i]->end_date));
            $enddatedate    = date('d', strtotime($getpaystudio[$i]->end_date));
            $enddatehour    = date('H', strtotime($getpaystudio[$i]->end_date));
            $year  = date('Y');
            $month = date('m');
            $day   = date('d');
            $hour  = date('H');
            // echo  $hour."<br>".  $enddatehour;
            // exit;
            // exit();
            // $year  = '2017';
            // $month = '10';
            // $day   = '13';
            // $hour  = '17';

            if ($year == $enddateyear && $month == $enddatemonth && $day == $enddatedate && $enddatehour <= $hour) {
                if ($getpaystudio[$i]->is_presentation == 1) {
                    $is_pre_payment = 1;
                }
                $status = $this->createpayment($getpaystudio[$i]->userid, $getpaystudio[$i]->packageid, $getpaystudio[$i]->paypal_card_id, true, $is_pre_payment,$getpaystudio[$i]->id);
               

            }

        }

    }

    public function downgrade($studioid, $is_pre_payment){
        
        if ($is_pre_payment == 1) {
         
            $paypal_card_id = DB::table('presentation_subscription')->where('user_id',$studioid)->delete();
            $video_category=DB::table('video_category')->where('status','custom')->first();
            DB::select("UPDATE videos SET is_visible=0 WHERE studio_id = '$studioid' and Video_category_id='$video_category->id'");
            DB::select("UPDATE videos SET is_pro=0 WHERE studio_id = '$studioid'");
            exit;
        }
        $date     = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', strtotime("+ 30 days", strtotime($date)));
        $data     = [
            "package_id"       => 1,
            "start_time"       => $date,
            "end_time"         => $end_time,
            "credit_card_last" => '',
        ];
        DB::table('subscription')->where('user_id', $studioid)->update($data);
        // DB::table('paypal_payment_cards')->where('studio_id', $studioid)->update(['is_working' => 0]);
        $totalvideos = DB::table('videos')->where('studio_id', $studioid)->count();
        $videolimit  = DB::table('packages')->select('limit')->where('id', '1')->first();
        // dd($totalvideos);
        $video = (array) $videolimit->limit;

        if ($totalvideos > $video[0]) {
            $limit = $totalvideos - $video[0];
            DB::select("UPDATE videos SET is_visible=0 WHERE studio_id = '$studioid' and is_pro=0 ORDER BY id ASC LIMIT $limit ");
        }

    }
    public function covert_payment(Request $request){     
        $geoPlugin_array = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        $geoPlugin_array=json_decode($geoPlugin_array,true);
        dd($geoPlugin_array);
    }

    public function buy_service(Request $request)
    {   
        $parms = array(
                        'cardtype',
                        'card_number',
                        'Expiremonth',
                        'Expireyear',
                        'Cvv2',
                        'user_id',
                        'studio_id',
                        'service_id',
                        'time',
                        'is_happy_hour',
        );
        BasicModel::checkParams($parms);
            $select = array(
                'studio_services.service_time',
                'studio_services.Service_charges',
                'studio_services.gender',
                'studio_services.happy_hour_to',
                'studio_services.happy_hour_from',
                'studio_services.discount_amount',
                'studio_services.Description',
                'studio_services.service_title',
                'studio_services.currency_code',
            );
            $service_id      = $request->service_id;
            $studioid        = $request->studio_id;
            $user_id         = $request->user_id;
            $credit_card     = $request->card_number;
            $time            = $request->time;
            $is_happy_hour   = $request->is_happy_hour;
            $pre_card_id     = $request->pre_card_id;
            $message         = $request->message;
            if(empty($request->message)){
                $message  ='';
            }
            $check_hh = DB::table("studio_services")->select($select)->where('id',$service_id)->first();
            $charges         = $check_hh->Service_charges;
            if($is_happy_hour!='false'){
                $charges              = $check_hh->discount_amount;
                // $is_happy_hour_push   = '1';

            // $user_data= User::select('time_zone')->where('id',$studioid)->first();
            // if(!empty($user_data->time_zone)){
            //     date_default_timezone_set($user_data->time_zone);
            //     }
            //     $start_date_hour_minutes    = date('H:i:s', strtotime($check_hh->happy_hour_from));
            //     $end_date_hour_minutes      = date('H:i:s', strtotime($check_hh->happy_hour_to));
            //     $now_date_hour_minutes      = date('H:i:s');
            //     if($end_date_hour_minutes<$now_date_hour_minutes) {
            //         return response()->json(['status' => 'error', 'message' => 'Happy Hour is over now']);
            //     } else if($start_date_hour_minutes<=$now_date_hour_minutes &&       $end_date_hour_minutes>=$now_date_hour_minutes){
            //         $charges         = $check_hh->discount_amount;
            //     } 

            } 
            $payment_id='';
            if($pre_card_id==""){
                $user = DB::table('users')->select('*')->where('id', $user_id)->first();   
                $payment_id = $this->create_card($credit_card,$request->cardtype,$request->Expiremonth,$request->Expireyear,$request->Cvv2,$user->Studio_Name,$user->email,$user_id);

             } else {
                $card_id = DB::table('payment_invoices')->select('paypal_card_id')->where('id',$pre_card_id)->first();
                $payment_id = $card_id->paypal_card_id;

             }
                // $currency=BasicModel::mulity_currency_with_rate();
                // $currencyCode=$currency->currencyCode;
                // if($currencyCode!="USD" && $currencyCode!="AUD" && $currencyCode!="CHF" && $currencyCode!="EUR" && $currencyCode!="GBP" ){
                //         $currencyCode='USD';
                // }
                $data=[
                'user_id'       => $user_id,
                'studio_id'     => $studioid,
                'service_id'    => $service_id,
                'time'          => $time,
                'status'        => 'Pending',
                'message'       => $message,
                'charges'       => $charges,
                'payment_id'    => $payment_id,
                'currency_code' => $check_hh->currency_code
                ];
                if($is_happy_hour!='false'){
                    $data['is_happy_hour']=1;
                }
                $service_appointments_id=DB::table('service_appointments')->insertGetId($data);
                if($service_appointments_id){
                    $data2              = new \stdClass();
                    $studio           = DB::table('users')->select('*')->where('id',$studioid)->first();
                    $title            = "Request For Service Appointment";
                    $body             = $user->Studio_Name . " has request to " .$check_hh->service_title ." at ".$time;
                    $data2->studio_id = $studioid;
                    $data2->service_appointments_id=$service_appointments_id;
                    $data2->user_id=$user_id;
                    $p_arr          = array(
                        'title'     => $title,
                        'body'      => $body,
                        'push_data' => json_encode($data2),
                        'type'      => 'new_appointment',
                    );
                    $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
                    $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $studioid, 'device_token' =>$studio->device_token, 'is_sent' => 1]);
                    if(!empty($studio->device_token)){
                        BasicModel::pushNotification($title, $body, array($studio->device_token), 'new_appointment',$data2);
                    } 
                    return response()->json(['status' => 'success', 'message' => "Appointment has been send to studio"]);
                    
                }
    }
    public function update_appointment(Request $request) {   
        $parms = array(
                        'service_appointments_id',
                        'action',
                        'studioid',
                        'push_id'
        );
        BasicModel::checkParams($parms);
        $service_appointments_id = $request->service_appointments_id;
        $action                  = $request->action;
        $studioid                = $request->studioid;
        $push_id                 = $request->push_id;
        $service_message         = '';

        DB::table('notification_details')->where('id',$push_id)->update(['is_deleted' => 1, 'is_read' => 1]);
        $studio           = DB::table('users')->select('*')->where('id',$studioid)->first();
        $get_appointment=DB::table('service_appointments')->select('*')->where('id',$service_appointments_id)->first();
        if($action!='false'){
         $studio_service =DB::table('studio_services')->select('*')->where('id',$get_appointment->service_id)->first();
         $f = $this->make_payment($get_appointment->payment_id,$studio_service->service_title,$get_appointment->currency_code,$get_appointment->charges,$studioid);
         if($f){
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Accepted']);
            if($result){
               
               $title            = "Your  appointment is Accepted";
               $body             =  $studio->Studio_Name . " has confirmed appointment";
               $notify_type      = 'appointment_accepted';
               $service_message  = 'confirmed';
               $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type);
               $this->make_invoice($f,$studio_service->service_title,$service_appointments_id,$get_appointment->user_id,$get_appointment->payment_id);
            }
         } else {
            $title     = "Your Payment is not recieved, that's why your appointment is rejected";
            $body      = "Your Payment is not recieved, that's why your appointment is rejected";
            $notify_type      = 'appointment_rejected';
            $reason           = "Error in Doing service Payment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason]);
            $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type);
            return response()->json(['status' => 'error', 'message' => "Error in Doing service Payment"]);
         }
        
    } else {
        $check_hh=DB::table('service_appointments')->select('service_id')->where('id',$service_appointments_id)->first();
        $service_id             = $check_hh->service_id;
        $reason                 = $request->reason;
        $time                   = $request->time;
        $notification_data      = null;
        $is_happy_hour   = 0;
        $happy_hour_from = '';
        $happy_hour_to   = '';
        $title            = "Your appointment is Rejected";
        $notify_type      = 'appointment_rejected';
        $service_message  = 'rejected';
        if(!empty($time)){
            // dd($time);
            $time               = $request->time;
            DB::table('service_appointments')->where('id',$service_appointments_id)->update(['time' =>$time]);
            $body = $studio->Studio_Name." will be Available at " .  $time;
            $check_hh=DB::table('studio_services')->select('discount_amount','happy_hour_from','happy_hour_to','Service_charges','discount_amount','currency_code','happy_hour_days')->where('id',$service_id)->first();
            $is_show_happy=0;
            if(!empty($studio->time_zone)){
                date_default_timezone_set($studio->time_zone);
            }
            $days = [];  
            if(!empty($check_hh->happy_hour_days)){
                $days  = json_decode($check_hh->happy_hour_days);
            }
            $today = Date('l');
            $start_date_hour_minutes    = date('H:i:s', strtotime($check_hh->happy_hour_from));
            $end_date_hour_minutes      = date('H:i:s', strtotime($check_hh->happy_hour_to));
            $now_date_hour_minutes      = date('H:i:s');
            if($start_date_hour_minutes<=$now_date_hour_minutes && $end_date_hour_minutes>=$now_date_hour_minutes && in_array($today,$days)){
                $is_show_happy= 1 ;
            } 
            if($check_hh->discount_amount>0){
                $happy_hour_from = $check_hh->happy_hour_from;
                $happy_hour_to   = $check_hh->happy_hour_to;
                $price           = $check_hh->Service_charges;
                $discount_price  = $check_hh->discount_amount;
                $currency_code   = $check_hh->currency_code;
                $notification_data=array('time'=>$time,'is_happy_hour'=>$is_show_happy,'happy_hour_from'=>$happy_hour_from,'happy_hour_to'=>$happy_hour_to,'price'=>$price,'discount_price'=>$discount_price,'currency_code'=>$currency_code);
            } 
           
        } else {
       
        // BasicModel::checkParams(['reason']);
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason]);
            $body             =  $studio->Studio_Name . " has cancel appointment";     
            }
            $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type,$notification_data);
        }
      
        // dd($notification_data); // dd($notification_data);
        return response()->json(['status' => 'success', 'message' => $service_message]);
    }

    public function update_appointment_user(Request $request) {   
        $parms = array(
                        'service_appointments_id',
                        'action',
                        'studioid',
                        'userid',
                        'push_id',
                        'is_happy_hour',
        );
        BasicModel::checkParams($parms);
        $service_appointments_id = $request->service_appointments_id;
        $action                  = $request->action;
        $studioid                = $request->studioid;
        $push_id                 = $request->push_id;
        $userid                  = $request->userid;
        $is_happy_hour           = $request->is_happy_hour;
        DB::table('notification_details')->where('id',$push_id)->update(['is_deleted' => 1, 'is_read' => 1]);
        $studio           = DB::table('users')->select('*')->where('id',$userid)->first();
        $get_appointment=DB::table('service_appointments')->select('*')->where('id',$service_appointments_id)->first();
        if($action!='false'){
         $studio_service =DB::table('studio_services')->select('*')->where('id',$get_appointment->service_id)->first();
         $charges = $get_appointment->charges;
         if($is_happy_hour!='false'){
            $charges              = $studio_service->discount_amount;
            DB::table('service_appointments')->where('id',$service_appointments_id)->update(['charges'=>$charges]);
         }
         $f = $this->make_payment($get_appointment->payment_id,$studio_service->service_title,$get_appointment->currency_code,$charges,$studioid);
         if($f){
            $reason           =    $studio->Studio_Name . " has confirmed appointment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Accepted','reason'=>$reason]);
            if($result){
               
               $title            = "Appointment is Confirmed";
               $body             =  $reason;
               $notify_type      = 'appointment_accepted';
               $service_message  = 'confirmed';
               $this->service_notification($get_appointment->user_id,$studioid,$service_appointments_id,$title,$body,$notify_type);
               $this->make_invoice($f,$studio_service->service_title,$service_appointments_id,$get_appointment->user_id,$get_appointment->payment_id);
            }
         } else {
            $title     = "Your Payment is not recieved, that's why your appointment is rejected";
            $body      = "Your Payment is not recieved, that's why your appointment is rejected";
            $notify_type      = 'User_appointment_rejected';
            $reason           = "Error in Doing service Payment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason]);
            $this->service_notification($studioid,$get_appointment->user_id,$service_appointments_id,$title,$body,$notify_type);
            return response()->json(['status' => 'error', 'message' => "Error in Doing service Payment"]);
         }
        
    } else {
            $reason                = $studio->Studio_Name ." is rejected appointment";
            $result=DB::table('service_appointments')->where('id',$service_appointments_id)->update(['status' =>'Rejected','reason'=>$reason]);
            $service_message  = 'rejected';
            $notify_type      = 'appointment_rejected';
            $title            = "Your appointment is Rejected";
            $body             =  $studio->Studio_Name . " has Rejected your  appointment";
            $this->service_notification($get_appointment->user_id,$studioid,$service_appointments_id,$title,$body,$notify_type);
          
      }
           return response()->json(['status' => 'success', 'message' => $service_message]);
    }

    public function service_notification($studioid,$user_id,$service_appointments_id,$title,$body,$notify_type,$notification_data=null){
        $data2            = new \stdClass();
        if($notification_data!=null){
            $data2->time            = $notification_data['time'];
            $data2->is_happy_hour   = $notification_data['is_happy_hour'];
            $data2->happy_hour_from = $notification_data['happy_hour_from'];
            $data2->happy_hour_to   = $notification_data['happy_hour_to'];  
            $data2->price           = $notification_data['price'];  
            $data2->discount_price  = $notification_data['discount_price'];
            $data2->currency_code   = $notification_data['currency_code'];  
            $data2->currency_rate   = 1;
        }
      
        $data2->studio_id = $studioid;
        $data2->user_id   = $user_id;
        $data2->service_appointments_id  = $service_appointments_id;

        $studio           = DB::table('users')->select('*')->where('id',$user_id)->first();
        $p_arr          = array(
            'title'     => $title,
            'body'      => $body,
            'push_data' => json_encode($data2),
            'type'      => $notify_type,
        );
        // dd($p_arr);
        $push_id        = DB::table('push_notifications')->insertGetId($p_arr);
        $data2->push_id = DB::table('notification_details')->insertGetId(['push_id' => $push_id, 'sent_to' => $user_id, 'device_token' =>$studio->device_token, 'is_sent' => 1]);
        // dd($data2);
        if(!empty($studio->device_token)){
            BasicModel::pushNotification($title, $body, array($studio->device_token), $notify_type, $data2);
        }
    }



    public function create_card($credit_card,$cardtype,$Expiremonth,$Expireyear,$Cvv2,$Studio_Name,$email,$userid=0){

        $credit_card = base64_decode($credit_card);
        // $credit_card = sqrt($credit_card);
        $credit_card = $credit_card - date('Y');
        // dd($credit_card);
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->_ClientId, $this->_ClientSecret)
        );
        $card = new \PayPal\Api\CreditCard();
        $card->setType($cardtype)
            ->setNumber($credit_card)
            ->setExpiremonth($Expiremonth)
            ->setExpireyear($Expireyear)
            ->setCvv2($Cvv2)
            ->setFirstName($Studio_Name);
        $card->setMerchantId($this->_Marchant_id);
        $card->setExternalCardId("HotSpot" . uniqid());
        $card->setExternalCustomerId($email);
        $request = clone $card;

        try {
            $card->create($apiContext);
        } catch (Exception $ex) {
            $dataarray = ['paypal_error_msg' => $ex->getMessage().$ex->getData(), 'system_error_msg' => "Error in create card buy services", "studio_id" => $userid, "date" => date('Y-m-d H:i:s')];
            $result    = BasicModel::InsertData('payment_error_logs', $dataarray);

            echo json_encode(array("status" => "error", "message" => "something went wrong try again later"));
            exit;
        }
         $payment_id = $card->getId();
         return $payment_id;
    }

    public function make_payment($payment_id,$service_title,$currency_code,$charges,$studioid){
        $apiContext = new \PayPal\Rest\ApiContext(
           new \PayPal\Auth\OAuthTokenCredential($this->_ClientId, $this->_ClientSecret)
       );
       $creditCardToken = new \PayPal\Api\CreditCardToken();
       $creditCardToken->setCreditCardId($payment_id);
       //$creditCardToken->setCreditCardId("card-12345678");
       $fi = new \PayPal\Api\FundingInstrument();
       $fi->setCreditCardToken($creditCardToken);
       $payer = new \PayPal\Api\Payer();
       $payer->setPaymentMethod("credit_card")
           ->setFundingInstruments(array($fi));
       $item = new \PayPal\Api\Item();
       $item->setName($service_title)
           ->setCurrency($currency_code)
           ->setQuantity(1)
           ->setPrice($charges);
       $itemList = new \PayPal\Api\ItemList();
       $itemList->setItems(array($item));
       $details = new \PayPal\Api\Details();
       $details->setShipping(0.0)
           ->setTax(0.0)
           ->setSubtotal($charges);
       $amount = new \PayPal\Api\Amount();
       $amount->setCurrency($currency_code)
           ->setTotal($charges)
           ->setDetails($details);
       $transaction = new \PayPal\Api\Transaction();
       $transaction->setAmount($amount)
           ->setItemList($itemList)
           ->setDescription($service_title)
           ->setInvoiceNumber(uniqid());
       $payment = new \PayPal\Api\Payment();
       $payment->setIntent("sale")
           ->setPayer($payer)
           ->setTransactions(array($transaction));
       $request = clone $payment;

       try {
           $payment->create($apiContext);
       } catch (Exception $ex) { 
           $dataarray = ['paypal_error_msg' => $ex->getMessage().$ex->getData(), 'system_error_msg' => "Error in Doing service Payment ", "studio_id" => $studioid, "date" => date('Y-m-d H:i:s')];
           $result    = BasicModel::InsertData('payment_error_logs', $dataarray);
           return false;
           exit;
       } 
           return $payment;
    }

    public function make_invoice($payment_object,$product_name,$serviceid,$userid,$paypalcard_id){
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->_ClientId, $this->_ClientSecret)
        );
        $paymentId = $payment_object->getId();
        // $paymentId = "15111";
        $Payment = new \PayPal\Api\Payment;

        try {
            $payment = $Payment::get($paymentId, $apiContext);
        } catch (Exception $ex) {

        }
        $data = [
            "product_name"      => $product_name,
            "create_time"       => date('Y-m-d H:i:s'),
            "paypalid"          => $paymentId,
            "amount"            => $payment->transactions[0]->amount->total,
            "currency_code"     => $payment->transactions[0]->amount->currency,
            "short_description" => $payment->transactions[0]->description,
            "userid"            => $userid,
            "packageid"         => $serviceid,
            "platform"          => $this->inputs['registration_platform'],
            "end_date"          => date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d H:i:s')))),
            "paypal_card_id"    => $paypalcard_id,
        ];

        $result = DB::table('payment_invoices')->insertGetId($data);
    }


    public function getIP() {
        $ip = $_SERVER['SERVER_ADDR'];

        if (PHP_OS == 'WINNT'){
            $ip = getHostByName(getHostName());
        }

        if (PHP_OS == 'Linux'){
            $command="/sbin/ifconfig";
            exec($command, $output);
            // var_dump($output);
            $pattern = '/inet addr:?([^ ]+)/';

            $ip = array();
            foreach ($output as $key => $subject) {
                $result = preg_match_all($pattern, $subject, $subpattern);
                if ($result == 1) {
                    if ($subpattern[1][0] != "127.0.0.1")
                    $ip = $subpattern[1][0];
                }
            //var_dump($subpattern);
            }
        }
        return $ip;
    }
    public function get_ip_address() {
        foreach (array(
    'HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'REMOTE_ADDR'
        ) as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
    public function currencyConverter($from_Currency,$to_Currency,$amount) {
        // $from_Currency = urlencode($from_Currency);
        // $to_Currency = urlencode($to_Currency);
        // $encode_amount = 1;
        $get = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
        $get = explode("<span class=bld>",$get);
        if(isset($get[1])){
          $get = explode("</span>",$get[1]);
          $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
          return $converted_currency;
        }
    }

}
