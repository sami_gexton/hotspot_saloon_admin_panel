<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use JWTAuthException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Illuminate\Support\Facades\View;
use DateTime;
use Hash;
use App\BasicModel;



class webappController extends Controller{   
     
    public $inputs=array();
    public $headerarray=array();
    public $lng         = '';

    public function __construct(Request $request){

          $this->headerarray['method'] = $request->path();
          BasicModel::api_logs($request);
          $this->headerarray['username']=$request->header('PHP_AUTH_USER');
          $this->headerarray['password']=$request->header('PHP_AUTH_PW');
          $this->headerarray['REMOTE_ADDR']=$_SERVER['REMOTE_ADDR'];
          $this->headerarray['lng']      = $request->header('lng');
          if($this->headerarray['lng']!='en' && !empty($this->headerarray['lng'])){
            $this->lng='_de';
          }
       
          if($request->header('usertype')!="")
          {

             $this->inputs['usertype']     = $request->header('usertype');
              
          }
          else{

             echo json_encode(['status'=>'error','message'=>'Unauthorized action']);
              exit;
          }
         
          $this->inputs['registration_platform'] ="";
          $this->inputs['appname']               ="";
          $this->inputs['registration_device_id'] = "";
         
          $platform    = $request->header('platform');
          $appname     = $request->header('appname');
          $deviceToken = $request->header('deviceToken');
          if($deviceToken!=""){

            $this->inputs['registration_device_id'] = $request->header('deviceToken');
          }
       
          if($platform!="")
          {
            $this->inputs['registration_platform']  = $request->header('platform');
           
          }

          if($appname!="")

          {
            $this->inputs['appname']                = $request->header('appname');

          }

        
        if($request->header('token')!=""){

            Input::merge(['token' => $request->header('token')]);

            $this->middleware('jwt.auth', ['except' => ['authenticate']]);     

        }else{

          if($this->headerarray['username']!="hotspot" || $this->headerarray['password']!="admin123!@#.")
              {

                echo json_encode(['status'=>'error','message'=>'Unauthorized action']);
                  exit;

              }
       }
           
    }
   
    public function getallvideos(){
        $BasicModel = new BasicModel();
        //for studio videos
        $whereRaw = "users.user_type='studio_user' and videos.is_visible =1";
        $studio = $BasicModel->getVideos('studio_videos',$whereRaw);
        
        //for user videos
        $whereRaw = "users.user_type='normal_user' and videos.is_visible =1";
        $users = $BasicModel->getVideos('user_videos',$whereRaw);

        if(count($studio)>0 || count($users) > 0){
            $category=$this->Categoriesandsubcategories(true);
            return response()->json(['status'=>'success','studio'=>$studio, 'users' => $users,'totalvideos'=>(count($studio) + count($users)),'categories'=>$category]);
        }else{
            return response()->json(['status'=>'error','message'=>"No Video Found"]);
        }
    }

      public function getvideosdetails(){
            $BasicModel = new BasicModel();
            $dataarray =array();
            $video_id = Input::get('id');
            $whereRaw = "videos.id = $video_id";
            $video = $BasicModel->getVideos('video_detail',$whereRaw, true);
            if(null === $video){
                return response()->json(['status'=>'error','message'=>"data not found"]);
            }

            $video_category = DB::table("video_category")->select('id',"category_name$this->lng as categoryname", 'slug as category_slug')->get();

            $dataarray['video'] = $video;
            $dataarray['categories'] = $video_category;
            if($video){
                return response()->json(['status'=>'success','data'=>$dataarray]);
            }
            else{
                return response()->json(['status'=>'error','message'=>"data not found"]);
            }
      }


      public function getStudiosonmap(){

            $origLat    = Input::get('lat');
            $origLon    = Input::get('lng');
            if(!isset($origLat) && !isset($origLon)) 
            {

               return response()->json(['status'=>'error','message'=>'Parameter Missing']);
            }
           
            $dist       = 31.0686;
            $tableName  = "users";
          
            $query = "SELECT id as studio_id,Studio_Name, lat, cover_pic,profile_pic,lng,Average_ratings, 3956 * 2 * 
            ASIN(SQRT( POWER(SIN(($origLat - lat)*pi()/180/2),2)
            +COS($origLat*pi()/180 )*COS(lat*pi()/180)
            *POWER(SIN(($origLon-lng)*pi()/180/2),2))) 
            as distance FROM $tableName WHERE 
            lng between ($origLon-$dist/cos(radians($origLat))*69) 
            and ($origLon+$dist/cos(radians($origLat))*69) 
            and lat between ($origLat-($dist/69)) 
            and ($origLat+($dist/69))  and users.user_type='studio_user' 
            having distance < $dist ORDER BY distance"; 

            $data=DB::select($query);
            $data=$this->convertStdToArray($data);
            
            if($data!=[])
            {
             foreach ( $data as $key => $value){

         
              $dataarray[$key]['studio_id']       = $data[$key]['studio_id'];
              $dataarray[$key]['Studio_Name']     = $data[$key]['Studio_Name'];
              $dataarray[$key]['ratings']         = $data[$key]['Average_ratings'];
              if($data[$key]['cover_pic']!="")
              {
               $dataarray[$key]['cover_pic']       = asset($data[$key]['cover_pic']);
              }
              else
              {
                
               $dataarray[$key]['cover_pic']       =  "";

              }
              $dataarray[$key]['profile_pic']     = asset($data[$key]['profile_pic']);
              $dataarray[$key]['lat']             = $data[$key]['lat'];
              $dataarray[$key]['lng']             = $data[$key]['lng'];

              $whereraw ="is_pro=1"; 

              $videos=DB::table('videos')->select('Video_link')->whereRaw($whereraw)->where('Studio_id',$data[$key]['studio_id'])->get();
              
              $videos=$this->convertStdToArray($videos);

             
             
               if(isset($videos[0]['Video_link']))
               {   
                   $BasicModel = new BasicModel();
                   $dataarray[$key]['Video_link']  = $BasicModel->s3_baseurl.$videos[0]['Video_link'];
                  

               }
               else
               { 

                   $dataarray[$key]['Video_link']  = "";
                      
               }
             }

             return response()->json(['status'=>'success','data'=>$dataarray]);
            }

            else
            {

             return response()->json(['status'=>'error','message'=>'data not found']);
            }

          }

         public function getSearchFilters(){

          $origLat         = Input::get('lat');
          $origLon         = Input::get('lng');
          $distance        = Input::get('distance');
          $gender          = Input::get('gender');
          $category        = Input::get('category');
          $subcategory     = Input::get('subcategory');
          $Service_charges = Input::get('Service_charges');
          $service_time    = Input::get('service_time');
          $Studio_Name     = Input::get('Studio_Name');
          $usertype        = Input::get('usertype');
          $ispro           = Input::get('ispro');

          if(!isset($origLat) && !isset($origLon)) 
          {

          return response()->json(['status'=>'error','message'=>'Parameter Missing']);
          }
          $dist       = 31.0686;
          if(isset($distance) &&  $distance!="")
          {

          $dist       = $distance*0.621371;

          }

          // $tableName  = "users";
          $whereRaw2="";  
          if(isset($gender) &&  $gender!="")
          {
          $whereRaw2 .= " AND `studio_services`.`gender`  like '%".$gender."%'";
          }

          if(isset($subcategory) &&  $subcategory!="")
          { 
          $whereRaw2 .= " AND `sub_category`.`category_name`  like '%".$subcategory."%'";
          }
          if(isset($category) &&  $category!="")
          {
          $whereRaw2 .= " AND `video_category`.`category_name$this->lng`  like '%".$category."%'";
          }
          if(isset($Service_charges) &&  $Service_charges!="")
          {
          $whereRaw2 .= " AND `studio_services`.`Service_charges` <= ".$Service_charges;
          }
          if(isset($service_time) &&  $service_time!="")
          {
          $whereRaw2 .= " AND `studio_services`.`service_time`  <= ".$service_time;

          }
          if(isset($Studio_Name) &&  $Studio_Name!="")
          {
          // dd("yes gaya");

          $whereRaw2 .= " AND `users`.`Studio_Name`  like '%".$Studio_Name."%'";

          }
          if(isset($usertype) &&  $usertype!="")
          {

          $whereRaw2 .= " AND `users`.`user_type`  like '%".$usertype."%'";

          // and users.user_type='studio_user'
          }
          else
          {
            $type='studio_user';
            $whereRaw2 .= " AND `users`.`user_type`  like '%".$type."%'";

          }

          if(isset($ispro) &&  $ispro!="")
          {
           
            $whereRaw2 .= " AND videos.is_pro =".$ispro;

          }


          $query = "SELECT users.id as studioid,users.Studio_Name,users.lat, users.cover_pic,users.profile_pic,users.lng,users.Average_ratings,studio_services.gender,sub_category.category_name,video_category.category_name$this->lng AS Name,studio_services.Service_charges,studio_services.service_time,videos.Video_link, 3956 * 2 * ASIN(SQRT( POWER(SIN(($origLat - users.lat)*pi()/180/2),2)+COS($origLat*pi()/180 )*COS(users.lat*pi()/180)*POWER(SIN(($origLon-users.lng)*pi()/180/2),2))) as distance FROM users LEFT JOIN studio_services ON users.id = studio_services.Studio_id LEFT JOIN video_category ON video_category.id = studio_services.Category_id LEFT JOIN sub_category  ON sub_category.id = studio_services.SubCategory_id LEFT JOIN  videos
           ON videos.studio_id = users.id
            WHERE lng between ($origLon-$dist/cos(radians($origLat))*69) 
          and ($origLon+$dist/cos(radians($origLat))*69) 
          and users.lat between ($origLat-($dist/69)) 
          and ($origLat+($dist/69)) ". $whereRaw2 ." GROUP BY studioid having distance < $dist ORDER BY distance"; 

          $data=DB::select($query);

          $data=$this->convertStdToArray($data);
           $BasicModel = new BasicModel();
             

          if($data!=[])
          {
          foreach ( $data as $key => $value){


          $dataarray[$key]['studio_id']       = $data[$key]['studioid'];
          $dataarray[$key]['Studio_Name']     = $data[$key]['Studio_Name'];
          $dataarray[$key]['ratings']         = $data[$key]['Average_ratings'];
          if($data[$key]['cover_pic']!="")
          {
          $dataarray[$key]['cover_pic']       = asset($data[$key]['cover_pic']);
          }
          else
          {

          $dataarray[$key]['cover_pic']       =  "";

          }
          $dataarray[$key]['profile_pic']     = asset($data[$key]['profile_pic']);
          $dataarray[$key]['lat']             = $data[$key]['lat'];
          $dataarray[$key]['lng']             = $data[$key]['lng'];

          // $whereraw ="is_pro=1"; 

          // $videos=DB::table('videos')->select('Video_link')->whereRaw($whereraw)->where('Studio_id',$data[$key]['studioid'])->get();

          // $videos=$this->convertStdToArray($videos);
         
          if($data[$key]['Video_link']!="")
          {   

          $dataarray[$key]['Video_link']  = $BasicModel->$s3_baseurl.$data[$key]['Video_link'];


          }
          else
          { 

          $dataarray[$key]['Video_link']  = "";


          }

          $dataarray[$key]['Average_ratings'] = $data[$key]['Average_ratings'];

          $dataarray[$key]['gender'] = $data[$key]['gender'];
          $dataarray[$key]['category_name'] = $data[$key]['Name'];
          $dataarray[$key]['Sub_category'] = $data[$key]['category_name'];

          $dataarray[$key]['Service_charges'] = $data[$key]['Service_charges'];

          $dataarray[$key]['service_time'] = $data[$key]['service_time'];
          $dataarray[$key]['distance'] = $data[$key]['distance'];


          }

          return response()->json(['status'=>'success','data'=>$dataarray]);

          }

          else
          {



          return response()->json(['status'=>'error','message'=>'data not found']);



          }

       

      }
    
         public function Categoriesandsubcategories($allvideos=null)
         {
             
            if($this->inputs['usertype']=="" && !isset($this->inputs['usertype']))
           {

             return response()->json(['status'=>'error','message'=>'Parameter Missing']);


           }

           if($this->inputs['usertype']=="studio_user")
           {
             $usertype='studio';
           }

           if($this->inputs['usertype']=="normal_user")
           {
             $usertype='user';
           }
 
            $dataarray2        =array();
            $subcatgoriesids2  =array();
            $subcatgoriesName2 =array();

            $data2 = DB::select("SELECT `video_category`.`id`,`video_category`.`category_name$this->lng` AS Name,GROUP_CONCAT(`sub_category`.`id`) AS `sub_cat_ids`,
            GROUP_CONCAT(`sub_category`.`category_name` ) AS `sub_cat_names` FROM `video_category`
            LEFT JOIN `sub_category` ON `sub_category`.`parent_id`= `video_category`.`id` where video_category.status = '$usertype'
            GROUP BY `video_category`.`id`");

            $data2=$this->convertStdToArray($data2);

            foreach ($data2 as $key => $value) {

                $dataarray2[$key]['catogory_id']       = $data2[$key]['id']; 
                $dataarray2[$key]['catogory_name']     = $data2[$key]['Name'];
                $dataarray2[$key]['sub_cat_ids']       = explode(",", $data2[$key]['sub_cat_ids']);
                $dataarray2[$key]['sub_cat_names']     = explode(",", $data2[$key]['sub_cat_names']);

           }
             if($allvideos!=null)
             {

                return $dataarray2;

             }
           return response()->json(['status'=>'success','data'=>$dataarray2]);

       }



     public function convertStdToArray($data){
       
       return json_decode(json_encode($data),true);

     }


     public function restyle_text($input){

        $input = number_format($input);
        $input_count = substr_count($input, ',');
        if($input_count != '0'){
        if($input_count == '1'){
        return substr($input, 0, -4).'k';
        } else if($input_count == '2'){
        return substr($input, 0, -8).'mil';
        } else if($input_count == '3'){
        return substr($input, 0,  -12).'bil';
        } else {
        return;
        }
        } else {
        return $input;
        }
    }

    public function getAllReviews(Request $request){
        $studio_id = $request->studio_id;
        if($studio_id === null || empty($studio_id)){
            return response()->json(['status'=>'error','message'=>'Please provide studio id']);
        }
        // DB::enableQueryLog();
        $data = DB::table("studio_reviews")->leftjoin('users','users.id','=','studio_reviews.user_id')->select('studio_reviews.*', 'users.username', 'users.Studio_Name', 'users.email', DB::raw('IF(users.profile_pic IS NOT NULL, CONCAT("'.asset('').'",users.profile_pic), "") AS profile_pic'),DB::raw('IF(users.profile_pic IS NOT NULL, CONCAT("'.asset('').'",users.profile_pic_big), "") AS profile_pic_big'))->where('studio_reviews.studio_id', $studio_id)->orderBy('studio_reviews.date','desc')->get();

        // dd(DB::getQueryLog());
        $data = $this->convertStdToArray($data);

        if(count($data) > 0){
            return response()->json(['status'=>'success', 'total_reviews' => count($data), 'data'=>$data]);
        }else{
            return response()->json(['status'=>'success', 'total_reviews' => 0, 'data'=> array(), 'message'=> "No reviews found"]);
        }
    }   


    public function insertdata(){
          
         $arr =Input::get('data');
         $tablename = Input::get('tablename');
         $id=BasicModel::insertRecord($arr, $tablename);
         return response()->json(['status'=>'success', 'id'=>$id]);

    }   

     public function getRows(){
          
         
         $table_name = Input::get('tablename');
         $column = Input::get('columnname');
         $criteria = Input::get('criteria');
         $data=BasicModel::getRows($table_name, $column, $criteria, $order = "ASC", true);
         return response()->json(['status'=>'success', 'data'=>$data]);
    }   

    public function getRow(){
        
         $table_name = Input::get('tablename');
         $column = Input::get('columnname');
         $criteria = Input::get('criteria');
         $data=BasicModel::getRows($table_name, $column, $criteria, $order = "ASC");
         return response()->json(['status'=>'success', 'data'=>$data]);
    }

    public function getallsubsvideos(){
        
        $userid=Input::get('userid');
        $id_array=array();
        $studioids=DB::table("studio_subscribers")->select('studio_id')->where('subscribers_id',$userid)->get();

        foreach ($studioids as $key => $value) {
           array_push($id_array,$value->studio_id);
        }
        // dd($id_array);
        $selected_sizes_comma_seprated = implode(',', $id_array);

        $BasicModel = new BasicModel();
        
        //for studio videos
        $whereRaw = "users.user_type='studio_user' and users.id IN ($selected_sizes_comma_seprated)";
        $studio = $BasicModel->getVideos('studio_videos',$whereRaw);
        
        
    
        if(count($studio)>0){
            // $category=$this->Categoriesandsubcategories(true);
            return response()->json(['status'=>'success','studio'=>$studio,'totalvideos'=>(count($studio))]);
        }else{
            return response()->json(['status'=>'error','message'=>"No Video Found"]);
        }
    }   

}