<?php
if (! function_exists('getDays')) {
    function getDays(){
        return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    }
}