<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Staffdetails extends Model
{
  
      protected $table = 'staff_member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'studio_id','Name','contact','service_category','service_subcategory','email' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = false;

}
