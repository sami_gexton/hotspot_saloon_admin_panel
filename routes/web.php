<?php
use App\Http\Controllers\Auth\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('auth/register','AdminUserController@register');

Route::post('auth/login', 'AdminUserController@dologin');

Route::post('auth/applogin', 'UserController@dologinmobile');

Route::get('/', function () {
    return view('login');
});

Route::get('logout','AdminUserController@getLogout');

Route::get('login', function () {
    return view('login');
});

Route::get('upload','AdminController@upload_video');

Route::group(array('before' => 'auth'), function(){
// Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user', 'AdminUserController@getAuthUser');
    // Route::get('/studios',function(){
    //             return view('studiolisting');
    //     });
    Route::get('/studios','AdminController@studiolisting');
    Route::get('/users','AdminController@users');
    Route::get('/package','AdminController@systemsetup');
    Route::get('/aboutus','AdminController@about');
    Route::get('/currency_rate','AdminController@currency_rate');
    Route::POST('/currency_rate','AdminController@currency_rate');
    Route::get('/sub_admin_listing','AdminController@sub_admin_listing');
    // Route::get('/createpackage',function(){
    //             return view('createpackage');
    //     });
    Route::get('/AddCategory','AdminController@getcategories');
    Route::get('/dashboard','AdminController@getDashboard');
    // Route::get('/register', function () {
    //     return view('register');
    // });
    Route::get('/subcategories','AdminController@getsubcategories');

    Route::get('invoices', 'AdminController@Invoices');

    Route::get('dues','AdminController@DueInvoices');
    
    Route::get('Invoiceswithdate/{startdate}','AdminController@Invoiceswithdates');
    Route::get('invoicedetail/{id}','AdminController@invoicedetails');
    Route::post('/package','AdminController@systemsetup');
    Route::get('/convert_rate/{from_Currency}/{to_Currency}/{amount}','AdminController@currencyConverter');
    Route::get('/seo_tool','AdminController@seo_tool');
    Route::post('/update_seo_info','AdminController@update_seo_info');
    Route::get('/translate','AdminController@translations');
    Route::post('/Update_translations','AdminController@Update_translations');
   
});
    
Route::get('Checkemail','AdminController@checkemail');

Route::post('/registerstudio', 'AdminController@postAddstudio');

Route::post('/add_sub_user', 'AdminController@postRegister_sub_admin');

Route::post('upload_video_faqs', 'AdminController@upload_video_faqs');

Route::get('/dashboard','AdminController@getDashboard');
    
Route::get('/addstudio', 'AdminController@getAddstudioview');

Route::get('/allStudio', 'AdminController@getStudios');

Route::post('/deleteuser', 'AdminController@postDeleteuser');

Route::post('/deletestaff', 'AdminController@postDeletestaffuser');

Route::get('/services/{id}','AdminController@getStudioservicedetails');

Route::get('/AddStudioservice/{id}','AdminController@AddStudioservice');


Route::get('/overview/{id}','AdminController@getStudioOverview');

Route::get('/staff/{id}','AdminController@getstaff');

Route::get('staffdetails', 'AdminController@getStaffmembers');

Route::get('staffinfo', 'AdminController@getstaffinfo');

Route::get('reviews/{id}', 'AdminController@getStudioreviews');

Route::get('genral/{id}', 'AdminController@getGenraldetails');

Route::get('genrate_invoice/{id}/{send_mail}', 'AdminController@genrate_Hotpot_inovice');

Route::get('bank_deatails/{id}', 'AdminController@bank_deatails');

Route::get('video/{id}', 'AdminController@getvideos');

Route::get('channel', 'AdminController@getchannel');

Route::get('userlist', 'AdminController@getUsers');

Route::get('studio_finance/{id}/{status}', 'AdminController@studio_finance');

Route::get('userdetails/{id}', 'AdminController@getUserdetails');

Route::get('userchanel/{id}', 'AdminController@getuserchannel');

Route::post('updateuserinfo', 'AdminController@postupdategeninfo');

Route::post('Paid_invocies', 'AdminController@Paid_invocies');

Route::post('Deleteworkinghours', 'AdminController@Deleteworkinghours');

Route::post('registration', 'UserController@getmobileregister');

Route::get('tokenrefresh', 'UserController@getRefreshToken');

Route::get('sendemail', 'UserController@getsendemail');

Route::get('verifyemail', 'UserController@getverifyemail');

Route::get('forgetpassword','UserController@getforgetpassword');

Route::post('updateprofile', 'UserprofileController@postupdateprofile');

Route::post('updateworking', 'UserprofileController@postupdateworkinghours');

Route::post('getservices', 'UserprofileController@postcategories');

Route::get('mapmarkers','UserprofileController@getStudiosmap');

Route::post('updateservices','UserprofileController@postupdateservices');

Route::post('deleteworkinghours','UserprofileController@postdeleteworkinghours');

Route::post('deleteservices','UserprofileController@postdeleteservices');

Route::post('StudioDetails','UserprofileController@getStudioDetails');

Route::post('checkusername','AdminController@Checkusername');

Route::post('searchuser','UserprofileController@getSearchUser');

Route::post('addstaffmember','UserprofileController@getAddstaffmember');

Route::post('getstudioworkinghours','UserprofileController@getStudioworkinghours');

Route::post('getstudioservices','UserprofileController@getStudioServices');

Route::post('deleteStaffMember','UserprofileController@deleteStaffMember');

Route::post('getstaffmember','UserprofileController@getStaffMember');

Route::post('updatecoverpic','UserprofileController@postupdatecover');

Route::get('studiopackageinfo','UserprofileController@getstudiouserpackagesinfo');

Route::get('getvideocategory','UserprofileController@getvideocategory');

Route::get('getuservideocategory','UserprofileController@getuservideocategory');

Route::get('getstudiovideos','UserprofileController@getstudiovideos');

Route::get('getuservideos','UserprofileController@getstudiovideos');

Route::get('getvideostatus','UserprofileController@getvideostatus');

Route::post('addvideo','UserprofileController@postAddvideo');

Route::get('allstudiovideos','UserprofileController@getallstudiovideos');

Route::get('alluservideos','UserprofileController@getalluservideos');

Route::post('updatevideo','UserprofileController@postupdatevideo');

Route::post('updatepassword','UserprofileController@postupdatepassword');

Route::post('addreviews','UserprofileController@postAddreviews');

Route::post('deletevideo','UserprofileController@postDeleteVideos');

Route::get('userpackagesinfo','UserprofileController@getuserpackagesinfo');

Route::get('AllVideos','webappController@getallvideos');

Route::get('videosdetails','webappController@getvideosdetails');

Route::get('monthlychart','AdminController@DatewiseChart');

Route::post('genrateinvoice','UserprofileController@postgenrateinvoice');

Route::get('chartdatewise','AdminController@DatewiseChartdasboard');

Route::get('SearchStudiobylocation','webappController@getStudiosonmap');

Route::get('LocationFliters','webappController@getSearchFilters');

Route::get('mobileLocationFliters','UserprofileController@getSearchFiltersformobile');

Route::get('CategoriesWeb','webappController@Categoriesandsubcategories');

Route::get('studioinfo','UserprofileController@userappstudiodetails');

Route::post('videosbycategory','UserprofileController@videosbycategoryid');

Route::get('all_reviews', 'webappController@getAllReviews');

Route::post('subscribe', 'UserprofileController@studiosubscribe');

Route::post('like', 'UserprofileController@addlikes');

Route::post('addCategory', 'AdminController@AddCategories');

Route::post('/genral/workinghours', 'AdminController@postworkinghours');

Route::post('deletecat', 'AdminController@postDeletecat');

Route::post('editcategory', 'AdminController@editcategory');

Route::post('unsubscribe', 'UserprofileController@studioUnSubscribe');

Route::post('unlike', 'UserprofileController@unLike');

Route::get('videoDetailsByUser', 'UserprofileController@videoDetailsByUser');

Route::post('subcategories', 'AdminController@getsubcategories');

Route::post('Addsubcategory', 'AdminController@Addsubcategory');

Route::post('deletesubcat', 'AdminController@Deletesubcategory');

Route::post('updatesubcat', 'AdminController@updatesubcategory');

Route::post('createpackage', 'AdminController@createpackage');

Route::post('deletepackage', 'AdminController@deletepackage');

Route::post('updatepackage', 'AdminController@updatepackage');

Route::post('addaboutus', 'AdminController@Addaboutus');

Route::post('InsertData','webappController@insertdata');

Route::post('GetRows','webappController@getRows');

Route::post('GetRow','webappController@getRow');

Route::get('staffconversations','UserprofileController@conservationlist');

Route::get('messages','UserprofileController@messages');

Route::get('userchatinglist','UserprofileController@userchatinglist');

Route::get('genratepdf/{id}','AdminController@genratepdf');

Route::get('loginout','UserController@logoutmobile');

Route::post('dopayment','PaymentController@DoPayment');

Route::get('lastpayment','PaymentController@lastpayment');

Route::get('mapdata','AdminController@StudioMapData');

Route::get('LikedVideos','UserprofileController@getLikedVideos');

Route::get('getSubscribedChannel','webappController@getallsubsvideos');

Route::get('checkpayment','PaymentController@checkpayment');

Route::get('setprofilevideo','UserprofileController@studioprofilevideo');

Route::get('getsubcategorybyid','AdminController@getsubcategorybyid');

Route::post('AddService','AdminController@AddService');

Route::post('editService','AdminController@editService');

Route::get('Editservice/{id}/{serviceid}','AdminController@Editservice_by_id');



Route::get('getservicebyid','AdminController@getStudioservicedetailsbyid');

Route::get('deleteservice','AdminController@Deleteservice');

Route::get('Searchstaffmember','AdminController@getSearchstaffmember');

Route::post('Addstaff','AdminController@Addstaffmember');

Route::get('staffserviceinfo','AdminController@getstaffmemberinfobyid');

Route::get('deletevideo','AdminController@Deletevideo');

Route::post('acceptStaffRequest','UserprofileController@acceptStaffRequest');

Route::post('report_video','UserprofileController@reportVideo');

Route::post('subscribed_channels','UserprofileController@subscribed_channels');

Route::post('liked_videos_by_user','UserprofileController@liked_videos');

Route::get('get_notifications','UserprofileController@getNotfications');

Route::post('update_notifications','UserprofileController@updateNotification');

Route::get('on_resume_service','UserprofileController@resume_service');

Route::get('faq','AdminController@faq');

Route::get('add_faq','AdminController@faq_add_page');

Route::post('add_faq','AdminController@faq_add');

Route::get('faq_edit/{id}','AdminController@faq_edit');

Route::post('faq_edit','AdminController@faq_edit');

Route::get('faq/remove/{id}','AdminController@faq_delete');

Route::get('faq_categories','AdminController@faq_cats');

Route::get('faq_categories/edit/{id}','AdminController@faq_edit_cats');

Route::get('faq_categories/remove/{id}','AdminController@remove_faq_category');

Route::post('faq_cat_add','AdminController@faq_cat_add');

Route::post('autocomplete_searching','UserprofileController@autocomplete_searching');

Route::post('update_service_indexes','UserprofileController@update_service_indexes');

Route::post('covert_payment','PaymentController@covert_payment');
Route::post('get_profile_details','UserprofileController@get_profile_details');
Route::post('ActiveUser','AdminController@postActiveUser');
Route::post('autocomplete_web','UserprofileController@autocomplete_web');
Route::get('/add_Sub_Admin','AdminController@getAdd_Sub_Admin');
Route::post('service_appointments','UserprofileController@service_appointments');
Route::post('edit_service','UserprofileController@edit_services');
Route::post('get_service_by_id','UserprofileController@get_service_by_id');

Route::post('buy_package','Stripe_paymentController@DoPayment_stripe');
Route::post('make_invoice','Stripe_paymentController@make_stripe_invoice');
Route::post('package_downgrade','Stripe_paymentController@downgrade');
Route::post('All_user_cards','Stripe_paymentController@All_customer_cards');
Route::post('buy_service','Stripe_paymentController@buy_service');
Route::post('update_appointment','Stripe_paymentController@update_appointment');
Route::post('update_appointment_user','Stripe_paymentController@update_appointment_user');
Route::post('add_appointment','UserprofileController@add_appointment');
Route::post('contact_us','UserController@contact_email');
Route::get('contact_list', 'AdminController@cantact_us');
Route::post('edit_appointment','UserprofileController@edit_appointment');
Route::post('delete_appointment','UserprofileController@delete_appointment');
Route::post('user_cancel_appointment','Stripe_paymentController@user_cancel_appointment');
Route::post('About_us','UserprofileController@About_us');
Route::post('Add_bank_details','UserprofileController@Add_bank_details');
Route::post('get_bank_details_by_id','UserprofileController@get_bank_details_by_id');
Route::post('change_tutorial_status','UserprofileController@change_tutorial_status');
Route::get('delete_faq_video/{id}','AdminController@delete_faq_video');
Route::post('url_genrater','UserprofileController@url_genrater');
Route::post('sign_up_user','UserprofileController@sign_up_user');
Route::get('get_packages','UserprofileController@get_packages');
Route::get('notifications','Stripe_paymentController@notifications');
Route::post('translate','AdminController@translate');
Route::get('add_word/{index}/{eng_word}/{de_word}','AdminController@add_word');
// Route::post('terms_condition','UserprofileController@terms_condition');