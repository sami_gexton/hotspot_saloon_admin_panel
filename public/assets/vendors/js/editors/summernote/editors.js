/*!
 * robust-admin-theme (http://demo.pixinvent.com/robust-admin)
 * Copyright 2017 PIXINVENT
 * Licensed under the Themeforest Standard Licenses
 */
!function(window,document,$){"use strict";var edit=function(){$(".summernote-edit").summernote({focus:!0})},save=function(){$(".summernote-edit").summernote("code");$(".summernote-edit").summernote("destroy")};document.getElementById("edit").onclick=function(){edit()},document.getElementById("save").onclick=function(){save()},$(".summernote").summernote(),$(".summernote-air").summernote({airMode:!0}),$(".summernote-code").summernote({height:350,codemirror:{theme:"monokai"}})}(window,document,jQuery);