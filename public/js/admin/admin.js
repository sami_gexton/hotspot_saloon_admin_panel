//login  js object
var admin = {
    access_token: $("#usertoken").val(),
    userid: $("#userid").val(),
    markerArray: [],
    map: null,
    infoWindows: [],
    readURL: function(input, proimg) {
        var fuData = input;
        var FileUploadPath = fuData.value;
        //To check if user upload any file
        if (FileUploadPath == '') {
            alert("Please upload an image");
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
            //The file uploaded is an image
            // alert(Extension) ; 
            if (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg") {
                // To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.' + proimg).css('background-image', 'url(' + e.target.result + ')');
                    }
                    reader.readAsDataURL(fuData.files[0]);
                    return false;
                }
            }
            //The file upload is NOT an image
            else {
                // alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                imgprofile = $("#imgprofile").val();
                $('.' + proimg).css('background-image', 'url(' + imgprofile + ')');
            }
        }
    },
    mapinitialize: function() {
        var myOptions = {
            zoom: 2,
            center: new google.maps.LatLng(39, 6),
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            navigationControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("mapdata"), myOptions);
        // Here starts the code for the styling of the map
        map.set('styles', [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#444444"
            }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#f2f2f2"
            }]
        }, {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            }, {
                "lightness": 45
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                "color": "#46bcec"
            }, {
                "visibility": "on"
            }]
        }]);
        // Here ends the code for the styling of the map
        var mcOptions = {
            gridSize: 50,
            maxZoom: 15,
            imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m'
        };
        var mc = new MarkerClusterer(map, [], mcOptions);
        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });
        mc.addMarkers(admin.markerArray, true);
        admin.markerArray = [];
        /*//hide loader
        $("#tsLoader").css('display','none')*/
        var infowindow = new google.maps.InfoWindow({
            size: new google.maps.Size(150, 50)
        });
    },
    //create marker for map function call here
    createMarker: function(latlng, studio_id, studio_name, html) {
        var contentString = html;
        var image = {
            url: '//chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|b3d1ff',
            size: new google.maps.Size(20, 32),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 32)
        };
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        admin.infoWindows.push(infowindow);
        var marker = new google.maps.Marker({
            position: latlng,
            map: admin.map,
            icon: login.base_url + 'public/assets/images/chart.png',
            studioid: studio_id,
            title: studio_name,
            animation: google.maps.Animation.DROP,
            //zIndex: Math.round(latlng.lat() * -100000) << 5
        });
        // console.log(studio_id);
        //marker.setAnimation(google.maps.Animation.BOUNCE);
        google.maps.event.addListener(marker, 'click', function() {
            for (var i = 0; i < admin.infoWindows.length; i++) {
                admin.infoWindows[i].close();
            }
            console.log(this.studioid);
            infowindow.open(map, marker);
        });
        admin.markerArray.push(marker); //push local var marker into global array
    },
    // initialize:function() {
    //     var input = document.getElementById('searchTextField');
    //     var autocomplete = new google.maps.places.Autocomplete(input);
    //     google.maps.event.addListener(autocomplete, 'place_changed', function () {
    //         var place = autocomplete.getPlace();
    //         console.log(place.geometry.location.lat());
    //         console.log(place.geometry.location.lng());
    //         $("#lat").val(place.geometry.location.lat());
    //         $("#lng").val(place.geometry.location.lng());
    //         });
    // },       
    // validatePassword:function() {
    // }  
};
$(document).ready(function() {
    $('.active').removeClass('active');
    classname = $("#activeclass").val();
    $("#" + classname).addClass('active');
    if ($('#dateto').length == 1) {
        $("#dateto").datepicker({
            showAnim: "drop",
            dateFormat: 'yy-mm-dd'
        });
        $("#datefrom").datepicker({
            showAnim: "drop",
            dateFormat: 'yy-mm-dd'
        });
    }
    if ($("#videotable").length == 1) {
        var table = $('#videotable').DataTable();
        table.columns.adjust().draw();
    }
    if ($("#mapdata").length == 1) {
        queryString = "";
        result = login.runajax("mapdata", queryString);
        // for()
        // Admin.createMarker(new google.maps.LatLng(result[i]['lat'], result[i]['lng']), result[i]['location']);
        console.log(result.data);
        $.each(result.data, function(i, val) {
            var contentString = '<div id="content">' + ' <div id="siteNotice">' + ' </div>' + ' <h2 id="firstHeading" class="firstHeading"  style="color:red">' + val.Studio_Name + '</h2>' + ' <div id="bodyContent">' + ' <table class="table">' + ' <thead>' + ' <tr>' + ' <th>Address</th>' + ' <th>' + val.Address + '</th>' + ' </tr>' + ' </thead>' + ' <tbody>' + ' <tr>' + ' <td><h5>Contact No</h5></td>' + ' <td><h5>' + val.contact_no + '</h5></td>' + ' </tr>' + ' </tbody>' + ' </table>' + '</div>' + '</div>';
            admin.createMarker(new google.maps.LatLng(val.lat, val.lng, val.Address), val.id, val.Studio_Name, contentString);
        })
        admin.mapinitialize();
    }
    if ($('#chartcontainer').length == 1) {
        queryString = "";
        result = login.runajax("monthlychart", queryString);
        chartdata(result.result);
    }
    if ($('.carousel-item').length == 1) {
        $("#carousel-example").children().children().addClass("active");
    } else {
        $("#carousel-example").children().children().eq(1).addClass("active");
    }
    $('.slider').trigger('click');
    if ($(".js-example-basic-single").length > 0) {
        $(".js-example-basic-single").select2({
            width: '100%'
        });
    }
    if ($('#addstudio').length > 0) {
        $('#addstudio').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            },
        });
    }
    if ($('#add_sub_admin').length > 0) {
        $('#add_sub_admin').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            },
        });
    }
    if ($('#editform').length > 0) {
        $('#editform').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {},
        });
    }
    if ($('#packageform').length > 0) {
        $('#packageform').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {},
        });
    }
    if ($('#addcategory').length > 0) {
        $('#addcategory').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
        });
    }
    if ($('#addsubcat').length > 0) {
        $('#addsubcat').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
        });
    }
    if ($('#Category').length > 0) {
        $('#Category').dataTable({});
    }
    if ($('#packages').length > 0) {
        $('#packages').dataTable({});
    }
    if ($('#invoices-list').length > 0) {
        $('#invoices-list').dataTable({});
    }
    if ($('#studiolisting').length > 0) {
        $('#studiolisting').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": 'allStudio',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                // console.log('hello',nRow);
                // console.log('hello',iDisplayIndex);
                // console.log('hello',aData);
                $("td:first", nRow).html(iDisplayIndex + 1);
                if(aData.user_status==0){
                   
                    switchclass='btn-primary';
                    title     = 'Active';
                    
                }else{
                    switchclass='btn-danger';
                    title     = 'Deactive';

                }
                html2 = `<a href='javascript:void(0)' class='direction btn btn-sm btn-success'><i class='fa fa-map-marker fa-2x' lat='${aData.lat}' lng='${aData.lng}'> </i> </a>`;
                $("td:nth-last-child(2)", nRow).html(html2);
                
                html = `<a class="btn btn-info btn-sm userview" aria-hidden="true" data-toggle="tooltip" title="View" href="javascript:void(0)" userid="${aData.id}">
                <i class="fa fa-eye"></i></a><a class="btn ${switchclass} btn-sm switch_user" title="${title}" aria-hidden="true" data-toggle="tooltip" href="javascript:void(0)" userid="${aData.id}">
               <i class="fa  fa-power-off"></i></a>`;   
                $("td:last-child", nRow).html(html);

                // td:nth-last-child(2).
                setTimeout(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                }, 1000);
                
                return nRow;
            },
            "aoColumns": [{
                    "data": "sr"
                }, {
                    "data": "profile_pic"
                }, {
                    "data": "studio_name"
                }, {
                    "data": "username"
                }, {
                    "data": "user_type"
                }, {
                    "data": "Direction"
                },
                // { "data": "contact_no" },
                {
                    "data": "Action"
                },
            ],
            "columnDefs": [{
                "targets": [0, 5, 1],
                "orderable": false,
            }],
            "order": [
                [0, 'desc']
            ],
        });
        $.fn.dataTable.ext.errMode = 'none';
        setTimeout(function() {
            $("#studiolisting").css('width', '');
        }, 1000);
    }
    if ($('#staffmembers').length > 0) {
        setTimeout(function() {
            $('[data-toggle="tooltip"]').tooltip();
        }, 1000);
        $('#staffmembers').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": '/admin/staffdetails?&id=' + $("#usersid").val(),
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1);
                html = `
            <a class="btn btn-info btn-sm viewstaff" aria-hidden='true' data-toggle='tooltip' title='View' href="${window.location.protocol}//${window.location.hostname}${login.base_url}staffinfo?id=${aData.parent_id}">
                <i class=' fa fa-eye'></i>
            </a>
            <a class='btn btn-warning btn-sm Update' data-toggle="modal" data-target="#updatestaffmember" title='Update' aria-hidden='true'  href="#" userid="${aData.parent_id}">
               <i class='fa fa-pencil'></i>
            </a>
            <a class='btn btn-danger btn-sm deletestaff' title='Delete' aria-hidden='true' data-toggle='tooltip' href="#" userid="${aData.parent_id}">
               <i class='fa fa-trash-o'></i>
            </a>
            
          `;
                $("td:last-child", nRow).html(html);
                return nRow;
            },
            "aoColumns": [{
                "data": "sr"
            }, {
                "data": "profile_pic"
            }, {
                "data": "Name"
            }, {
                "data": "membercontact"
            }, {
                "data": "Action"
            }, ],
            "columnDefs": [{
                "targets": [0, 5, 1],
                "orderable": false,
            }],
            "order": [
                [0, 'desc']
            ],
        });
        $.fn.dataTable.ext.errMode = 'none';
        setTimeout(function() {
            $("#staffmembers").css('width', '');
        }, 1000);
    }
    if ($('#users').length > 0) {
        $('#users').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": 'userlist',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1);
                if(aData.user_status==0){
                    
                     switchclass='btn-primary';
                     title     = 'Active';
                     
                 }else{
                     switchclass='btn-danger';
                     title     = 'Deactive';
 
                 }
                html = `
            <a class="btn btn-info btn-sm userinfo" aria-hidden='true' data-toggle='tooltip' title='View' href="#" userid="${aData.id}">
                <i class=' fa fa-eye'></i>
            </a>
            <a class='btn ${switchclass} btn-sm switch_user' title='${title}' aria-hidden='true' data-toggle='tooltip' href="#" userid="${aData.id}">
            <i class='fa  fa-power-off'></i>
           </a>
          `;
                $("td:last-child", nRow).html(html);
                setTimeout(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                }, 1000);
                return nRow;
            },
            "aoColumns": [{
                "data": "sr"
            }, {
                "data": "studio_name"
            }, {
                "data": "contact_no"
            }, {
                "data": "zipcode"
            }, {
                "data": "email"
            }, {
                "data": "Action"
            }, ],
            "columnDefs": [{
                "targets": [0, 5],
                "orderable": false,
            }],
            "order": [
                [0, 'desc']
            ],
        });
        $.fn.dataTable.ext.errMode = 'none';
    }
});
// $("#addstudio").submit(function (evt) {
//     // 
//     evt.preventDefault();
//     if($("#emailerror").css('display')!="block" && $("#usererror").css('display')!="block" )
//     {
//        userstatus=0;
//     if($(".slider").hasClass('on'))
//       {
//        userstatus=1;
//       }
//     // bg=$(".proimg").css('background-image');;
//     // bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
//     // bg=$("#uploadimg").attr("files")[0];
//     var bg = new FormData($(this)[0]);
//     url         = login.base_url+'registerstudio';
//     queryString = bg+"&token="+admin.access_token+"&user_status="+userstatus;
//     result      = login.runajax(url, queryString,1);
//     console.log(result.result);
//     if(result.result=="Added")
//     {   
//         if($(".slider").hasClass('on'))
//         {
//          $(".slider").removeClass('on');
//         }
//         imgprofile=$("#imgprofile").val();
//         $("#country").val('').trigger('change');
//         console.log(bg);
//         $("#uploadimg").val('');
//         $('.proimg').css('background-image', 'url(' +imgprofile+ ')');
//         $("#addstudioresult").show();
//         $("#addstudio")[0].reset();
//         setTimeout(function(){ $("#addstudioresult").hide(); }, 3000);
//         $( "div.success" ).fadeIn( 300 ).delay( 2000 ).fadeOut( 400 );
//     }
//     }
//      return false;
// });
$(document).off('click', "#btncategory").on('click', "#btncategory", function(e) {
    e.preventDefault();
    $("#addcategory").trigger('submit');
})
$(document).off('click', "#btnsubcategory").on('click', "#btnsubcategory", function(e) {
    evt.preventDefault();
    $("#addsubcat").trigger('submit');
})
$("#addsubcat").submit(function(evt) {
    evt.preventDefault();
    if ($("#newcategory").val() != "") {
        categoryname = $('#catselect :selected').text().trim();
        categoryid = $('#catselect :selected').val();
        subcat = $("#newcategory").val();
        url = 'Addsubcategory';
        queryString = "cat=" + categoryid + "&subcatename=" + subcat;
        result = login.runajax(url, queryString, 1);
        console.log(result);
        swal('SubCategory!', 'SubCategory Added', 'success')
        setTimeout(function() {
            location.reload();
        }, 1000);
    }
    evt.preventDefault();
    return false;
})
$(document).off('click', ".deletsubcat").on('click', ".deletsubcat", function(e) {
    selecter = $(this);
    rowid = $(this).attr('rowid');
    swal({
        title: 'Are you sure?',
        text: "You want to delete this Sub Category",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        swal('Deleted!', 'Your Sub Category has been deleted.', 'success', )
        url = 'deleteservice';
        queryString = "serviceid=" + rowid;
        result = login.runajax(url, queryString);
        url = 'deletesubcat';
        queryString = "catid=" + rowid;
        result = login.runajax(url, queryString, 1);
        selecter.closest('td').parent('tr').remove();
        e.preventDefault();
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'Your SubCategory is safe :)', 'error')
        }
    })
    return false;
})
$("#addcategory").submit(function(evt) {
    evt.preventDefault();
    if ($("#newcategory").val() != "" && $('#usertype :selected').text().trim() != "") {
        img = $(".proimg").css('background-image');
        usertype     = $('#usertype :selected').text().trim();
        categoryname = $("#newcategory").val();
        textarea     = $('textarea').val(); 
        var ImageURL = document.getElementById("service_image_blob").value;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        var blob = b64toBlob(realData, contentType);
        var formData = new FormData(this);
        formData.append('propic',blob);
        $.ajax({
            url: 'addCategory',
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                if (data.status == "Added") {
                    // <a class='btn btn-danger btn-sm deletcat'  title='Delete' aria-hidden='true' data-toggle='tooltip' href="#" userid="${data.rowid}">
                    // <i class='fa fa-trash-o'></i>
                    // </a>
                    swal('Category!', 'Category Added', 'success')
                    element = `<tr role="row" class="odd">
                      <td></td>
                      <td ><div  id="catimageid${data.rowid}" class='img-thumbnail' style='max-height: 40px;background-image:${img};height:40px;width:40px' /> </div></td>
                      <td id="catnameid${data.rowid}">${categoryname}</td>
                        <td id="status${data.rowid}"> ${usertype}
                        </td>
                        <td id="dis${data.rowid}">${textarea}
                        </td>
                       <td>
                       <a class="btn btn-info btn-sm editmodal"  aria-hidden='true' data-toggle="modal" href="#editmodal" userid="${data.rowid}">  <i class='fa fa-pencil'></i></a>
                      
                    </td>
                    </tr>`;
                    $("#dataapend").prepend(element);
                    imgprofile = $("#imgprofile").val();
                    $('.proimg').css('background-image', 'url()');
                    $("#uploadimg").val('');
                    $('.proimg').css('background-image', 'url(' + imgprofile + ')');
                    $("#newcategory").val('');
                    $('textarea').val(' '); 
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    return false;
});
$("#addstudio").submit(function(evt) {
    evt.preventDefault();
    // evt.stopImmediatePropagation();
    if ($("#studio").val() != "") {
        if ($("#emailerror").css('display') != "block" && $("#usererror").css('display') != "block") {
            if ($(".slider").hasClass('on')) {
                // userstatus=1;
                $("#user_status").val(1);
            } else {
                $("#user_status").val(0);
            }
            var formData = new FormData(this);
            var ImageURL = document.getElementById("service_image_blob").value;
            var block = ImageURL.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
            // Convert it to a blob to upload
            var blob = b64toBlob(realData, contentType);
            var formData = new FormData(this);
            formData.append('propic',blob);
            $.ajax({
                url: 'registerstudio',
                type: 'POST',
                data: formData,
                async: false,
                success: function(data) {
                    if (data.result == "Added") {
                        swal('User!', 'User Added SuccessFully  ', 'success')
                        imgprofile = $("#imgprofile").val();
                        $("#country").val('').trigger('change');
                        $("#usertype").val('').trigger('change');
                        $("#uploadimg").val('');
                        $('.proimg').css('background-image', 'url(' + imgprofile + ')');
                        $("#addstudioresult").show();
                        $("#addstudio")[0].reset();
                        $('.slider').trigger('click');
                        $("div.success").fadeIn(300).delay(2000).fadeOut(400);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }
    return false;
});

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
    byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
    }
$("#add_sub_admin").submit(function(evt) {
    evt.preventDefault();
    // evt.stopImmediatePropagation();
    if ($("#studio").val() != "") {
        if ($("#emailerror").css('display') != "block") {
            var formData = new FormData(this);
            var tz = jstz.determine(); // Determines the time zone of the browser client
            var timezone = tz.name();
            formData.append('time_zone',timezone);
            var ImageURL = document.getElementById("service_image_blob").value;
            var block = ImageURL.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
            // Convert it to a blob to upload
            var blob = b64toBlob(realData, contentType);
            var formData = new FormData(this);
            formData.append('propic',blob);
             //'Asia/Kolhata' for Indian Time.
           
            $.ajax({
                url: 'add_sub_user',
                type: 'POST',
                data: formData,
                async: false,
                success: function(data) {
                    if (data.result == "Added") {
                        swal('User!', 'User Added SuccessFully  ', 'success')
                        imgprofile = $("#imgprofile").val();
                        $("#country").val('').trigger('change');
                        $("#uploadimg").val('');
                        $('.proimg').css('background-image', 'url(' + imgprofile + ')');
                        // $("#addstudioresult").show();
                        $("#add_sub_admin")[0].reset();
                        // $("div.success").fadeIn(300).delay(2000).fadeOut(400);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }
    return false;
});

$(document).off('click', "#createpackagebtn").on('click', "#createpackagebtn", function(e) {
    e.preventDefault();
    $("#packageform").trigger('submit');
})
$("#packageform").submit(function(evt) {
    if ($("#package_name").val() != "" && $('#usertype').val() != null) {
        evt.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            url: 'createpackage',
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                console.log(data);
                if(data.status=='success'){
                    swal('Package!', 'Package Added SuccessFully  ', 'success')
                    setTimeout(function() {
                        location.reload()
                    }, 1000);
                    
                } else {
                    swal('Package!', data.message, 'error')
                }
              
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    return false;
});
$(document).off('click', "#updatepackagebtn").on('click', "#updatepackagebtn", function(e) {
    e.preventDefault();
    $("#packageditmodel").trigger('submit');
})
$("#packageditmodel").submit(function(evt) {
    evt.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        url: 'updatepackage',
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            swal('Package!', 'Package Updated SuccessFully  ', 'success')
            setTimeout(function() {
                location.reload()
            }, 1000);
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});
// $(document).on("change", "#selectservice", function(e) {
//     id = $(this).val();
//     $("#selectservicetype").html('');
//     url = 'getsubcategorybyid';
//     queryString = "id=" + id;
//     result = login.runajax(url, queryString);
//     $.each(result.data, function(i, val) {
//         $('#selectservicetype').append(`<option value="${val.id}"> ${val.category_name}</option>`);
//     })
//     $("#selectservicetype").removeAttr('disabled');
//     // console.log(result);
// });
$("#formservice").submit(function(evt) {
    evt.preventDefault();
    var formData = new FormData(this);
    message = "";
    if(eval($('[name=price]').val())<=eval($('[name=price_discount]').val())){
        swal({
        title: "Price",
        text: 'Discount is must be less than Normal Price.',
        type: 'info',
        })
       return false;
    }
    happy_from_date = strtotime(date('Y/m/d')+" "+$('[name=happy_from]').val());
    happy_to   = strtotime(date('Y/m/d')+" "+$('[name=happy_to]').val());
    if($("#is_happy_hour").css('display')=='block'){
        if(eval(happy_from_date)>eval(happy_to) || eval(happy_from_date)==eval(happy_to) ){
        swal({
        title: "Time",
        text: 'From Time is must be less than To Time.',
        type: 'info',
        })
       
        return false;
        }
        count=0;
        $('.days_name').each(function(){
        
        if($(this)[0].checked==false){
            count++;
            console.log($(this)[0].checked);
            }
        });
        if(count==7){
            swal({
                title: "Days",
                text: 'Please Select Days for Happy Hour.',
                type: 'info',
                })
                return false;
        }
    }
    if($('#service_image_blob').val()!=''){
        var ImageURL = document.getElementById("service_image_blob").value;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        var blob = b64toBlob(realData, contentType);
        formData.append('propic',blob);
    }
    if ($("#formsubmit").text().trim() == 'Add Service') {
        servicename = login.base_url+"AddService";
        message = "Service Added";
       
       
    } else {
        servicename = login.base_url+"editService";
        message = "Service Updated";
    }
    $.ajax({
        url: servicename,
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            swal('Service!', message, 'success').then(function(){
                location.reload();
            });
            // setTimeout(function() {
            //     location.reload();
            // }, 1000);
            $("#formservice")[0].reset();
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});
$(document).on("click", ".togglebutton", function(e) {
    butoon = $(this).attr('btnupdate');
    if (butoon == 'Add') {
        $("#formsubmit").html("Add");
        $("#headings").text("Add Services");
    } else {
        $("#formsubmit").html("Update");
        $("#headings").text("update service");
        serviceid = $(this).attr('serviceid');
        $("#setserviceid").val(serviceid);
        url = 'getservicebyid';
        queryString = "serviceid=" + serviceid;
        result = login.runajax(url, queryString);
        var SubCategory_id = result.data.SubCategory_id;
        console.log(result.data);
        $('#selectservice').val(result.data.Category_id).trigger('change.select2');
        $('#gender').val(result.data.gender).trigger('change.select2');
        $("#Amount").val(result.data.Service_charges);
        $("#Description").val(result.data.Description);
        $("#servicetitle").val(result.data.service_title);
        $(".proimg").css('background-image', 'url(' + login.base_url + result.data.services_image + ')');
        // x = result.data.service_time / 1000
        x = convertMillisecondsToDigitalClock(result.data.service_time);
        if (x.hours == "0 Hours") {
            $('#time').val("Minutes").trigger('change.select2');
            x = result.data.service_time / 1000
            x = x / 60
            minutes = x;
            $("#duration").val(minutes);
        } else {
            $('#time').val("Hours").trigger('change.select2');
            myString = x.minutes;
            myString = myString.replace(/\D/g, '');
            console.log(myString);
            x = result.data.service_time / 1000
            x = x / 60
            x = x / 60
            // hours = x % 24
            $("#duration").val(hours);
        }
        id = result.data.Category_id;
        url = 'getsubcategorybyid';
        queryString = "id=" + id;
        result = login.runajax(url, queryString);
        // $("#selectservicetype").html('');
        // $.each(result.data, function(i, val) {
        //     $('#selectservicetype').append(`<option value="${val.id}"> ${val.category_name}</option>`);
        // })
        // $("#selectservicetype").removeAttr('disabled');
        // $('#selectservicetype').val(SubCategory_id).trigger('change.select2');
    }
});
$(document).on("click", ".deleteservice", function(e) {
    e.preventDefault();
    selector = $(this);
    rowid = $(this).attr('rowid');
    swal({
        title: 'Are you sure?',
        text: "You want to delete this service",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        swal('Deleted!', 'Your Services has been deleted.', 'success', )
        url = login.base_url+'deleteservice';
        queryString = "serviceid=" + rowid;
        result = login.runajax(url, queryString);
        if (result.data > 0) {
            // selector.parent().closest("div").remove();
            setTimeout(function() {
                location.reload();
            }, 1000);
        }
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'Your Services is safe :)', 'error')
        }
    })
    return false;
})
$(document).on("click", ".deletevideo", function(e) {
    e.preventDefault();
    selector = $(this);
    rowid = $(this).attr('rowid');
    swal({
        title: 'Are you sure?',
        text: "You want to delete this Video",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        swal('Deleted!', 'Your Video has been deleted.', 'success', )
        url = '/admin/deletevideo';
        queryString = "id=" + rowid;
        // result = login.runajax(url, queryString);
        $.get(url,queryString,function( result ) {
            if (result.data > 0) {
                // selector.parent().closest("div").remove();
                setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        });
        
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'Your Video is safe :)', 'error')
        }
    })
    return false;
})
$('.upload').change(function(e) {
    // e.preventDefault();
    // e.stopPropagation();
    console.log(this);
    // alert(this);
    if ($(this).val()) {
            size_in_kb=this.files[0].size/1024;
        if(size_in_kb > 400){
            alert('The file size can not exceed 400Kb.');
            $('.upload').val('');
            error = false;
        } else{
        // error = false;
        // var filename = $(this).val();
        // $('.imgappend').append('<img>');
        // console.log(filename);
        // admin.readURL(this, 'proimg');
        $('.viral-loader').show();
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#service_divimage').remove();
        $("#service_divimage1").append(`<div id="service_divimage"><img id="serviceimg"   src=""  alt="Picture" height="500px" width="568px"> </div>`);
        $('#serviceimg').attr('src',e.target.result);
        setTimeout(function () {
        initCropper();
        $('.viral-loader').hide();
        }, 1000);
        $('#service_modal_open').trigger('click');
        };
        reader.readAsDataURL(this.files[0]);
        // $("#service_img_file").val('');
        }

    }
});
function initCropper(){
    console.log("Came here");
    var image = document.getElementById('serviceimg');
    var cropper = new Cropper(image, {
    aspectRatio: 1/1,
    center: true,
    highlight: true,
    background: true,
    minContainerWidth:500,
    minContainerHeight:500,
    getCroppedCanvas:{fillcolor: "#FFFFFF"},
    crop: function(e) {
        console.log(e.detail.x);
        console.log(e.detail.y);
    }
    });

    $(document).off("click", "#crop_button").on("click", "#crop_button", function(e) {  
        // $('.viral-loader').show();
        // console.log(cropper.getCroppedCanvas().toDataURL());
        // $("#proimg").attr('src',cropper.getCroppedCanvas().toDataURL());
        $('.proimg').css('background-image', 'url(' + cropper.getCroppedCanvas().toDataURL() + ')');
        $("#service_image_blob").val(cropper.getCroppedCanvas().toDataURL('image/jpeg', 1));
    });
}
$('#file_upload').change(function(e) {
    // e.preventDefault();
    // e.stopPropagation();
    if ($(this).val()) {
        size_in_kb=this.files[0].size/1024;
        if(size_in_kb > 400){
        alert('The file size can not exceed 400Kb.');
        $('#file_upload').val('');
        error = false;
        }else{
        // var filename = $(this).val();
        // $('.img-upload').append('<img>');
        // console.log(filename);
        // admin.readURL(this, 'proimginfo');
        // // e.preventDefault();
        // $("#imageform").trigger('submit');
        $('.viral-loader').show();
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#service_divimage').remove();
        $("#service_divimage1").append(`<div id="service_divimage"><img id="serviceimg"   src=""  alt="Picture" height="500px" width="568px"> </div>`);
        $('#serviceimg').attr('src',e.target.result);
        setTimeout(function () {
        initCropper_update()
        $('.viral-loader').hide();
        }, 1000);
        $('#service_modal_open').trigger('click');
        };
        reader.readAsDataURL(this.files[0]);


        }
    }
});

function initCropper_update(){
    console.log("Came here");
    var image = document.getElementById('serviceimg');
    var cropper = new Cropper(image, {
    aspectRatio: 1/1,
    center: true,
    highlight: true,
    background: true,
    minContainerWidth:500,
    minContainerHeight:500,
    getCroppedCanvas:{fillcolor: "#FFFFFF"},
    crop: function(e) {
        console.log(e.detail.x);
        console.log(e.detail.y);
    }
    });

    $(document).off("click", "#crop_button").on("click", "#crop_button", function(e) {  
        // $('.viral-loader').show();
        // console.log(cropper.getCroppedCanvas().toDataURL());
        // $("#proimg").attr('src',cropper.getCroppedCanvas().toDataURL());
        $('.proimginfo').css('background-image', 'url(' + cropper.getCroppedCanvas().toDataURL() + ')');
        $("#service_image_blob").val(cropper.getCroppedCanvas().toDataURL('image/jpeg', 1));
        $("#imageform").trigger('submit');
    });
}
$(document).off("change", '#uploadimg').on("change", '#uploadimg', function(e) {
    if ($(this).val()) {
        size_in_kb=this.files[0].size/1024;
        if(size_in_kb > 400){
        alert('The file size can not exceed 400Kb.');
        $('#uploadimg').val('');
        error = false;
        }else{
        error = false;
        var filename = $(this).val();
        $('.img-upload').append('<img>');
        console.log(filename);
        admin.readURL(this, 'proimg2');
        }
    }
});
$(document).off("change", '#editcatimage').on("change", '#editcatimage', function(e) {
  
    if ($(this).val()) {
        error = false;
        var filename = $(this).val();
        $('.img-upload').append('<img>');
        console.log(filename);
        admin.readURL(this, 'editimage');
    }
});
$(document).on('click', ".slider", function() {
    selector = $(this);
    if (selector.hasClass('on')) {
        selector.removeClass('on');
    } else {
        selector.addClass('on');
    }
});
$(document).off('click', ".deleteuser").on('click', ".deleteuser", function() {
    selector = $(this);
    swal({
        title: 'Are you sure?',
        text: "You want to delete this Studio",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        swal('Deleted!', 'Your Studio has been deleted.', 'success', )
        userid = selector.attr('userid');
        url = 'deleteuser';
        queryString = "id=" + userid;
        result = login.runajax(url, queryString, 1);
        if (result.status == "1") {
            // table= $('#packages').dataTable({});
            // table.ajax.reload();
            selector.closest('td').parent('tr').remove();
            
        }
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'Your Studio is safe :)', 'error')
        }
    })
});
$(document).off('click', ".switch_user").on('click', ".switch_user", function() {
    selector = $(this);
    if(selector.attr('data-original-title')=='Deactive'){
        text="You want to Deactive this User";
        active='0';
    }else{
        
        text="You want to Active this User";
        active='1';

    }
    if(selector.attr('data-original-title')=='Deactive'){
         text2="Your User has been Deactive";
    } else{
        text2="Your User has been Active";

    }
    swal({
        title: 'Are you sure?',
        text: text ,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then(function() {
        swal('Info!', text2, 'success', )
        userid = selector.attr('userid');
        url = 'ActiveUser';
        queryString = "id=" + userid+'&useractive='+active;
        result = login.runajax(url, queryString, 1);
        if (result.status == "1") {
            location.reload();
        }
    }, function(dismiss) {
        // if (dismiss === 'cancel') {
        //     swal('Cancelled', 'Your Studio is safe :)', 'error')
        // }
    })
});
$(document).off('click', ".userview").on('click', ".userview", function() {
    selector = $(this);
    userid = selector.attr('userid');
    // $("#usersid").val(userid);
    window.open(`${login.base_url}overview/${userid}`, `_self`);
    // $("#formid").submit();
});
$(document).off('click', ".deletestaff").on('click', ".deletestaff", function() {
    selector = $(this);
    swal({
        title: 'Are you sure?',
        text: "You want to delete this staffmember",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        swal('Deleted!', 'Your staffmember has been deleted.', 'success', )
        userid = selector.attr('userid');
        url = '/admin/deletestaff';
        queryString = "id=" + userid;
        $.post( url, queryString,function( result ) {
            if (result.status > 0) {
                // selector.parent().parent().remove();
                location.reload();
            }
        });
        // result = login.runajax(url, queryString, 1);
       
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'Your staffmember is safe :)', 'error')
        }
    })
    return false;
});
//  $(document).off('click',".viewstaff").on('click',".viewstaff",function(){
//       selector    = $(this);
//       userid      = selector.attr('userid');
//       url=window.location.protocol+"//"+window.location.hostname+ login.base_url+"staffinfo?id="+userid+"&token="+admin.access_token;
//       if($("#genral").length>0)
//       {
//       $("#genral").remove();
//       $('body').append("<a href="+url+" id='genral'> </a>");
//       }
//       else{
//       $('body').append("<a href="+url+" id='genral'> </a>");
//       }
//       document.getElementById('genral').click();
// }) ;
$(document).off('click', ".userinfo").on('click', ".userinfo", function() {
    selector = $(this);
    userid = selector.attr('userid');
    url = login.base_url + "userdetails/" + userid;
    if ($("#userinfo").length > 0) {
        $("#userinfo").remove();
        $('body').append("<a href=" + url + " id='userinfo'> </a>");
    } else {
        $('body').append("<a href=" + url + " id='userinfo'> </a>");
    }
    document.getElementById('userinfo').click();
    return false;
});
$(document).on('keyup focusout', "#username", function() {
    username = $("#username").val();
    url = 'checkusername';
    queryString = "username=" + username;
    result = login.runajax(url, queryString, 1);
    console.log(result);
    if (result.username > 0) {
        $("#username").css("border-bottom", "1px solid red");
        $("#usererror").show();
    } else {
        $("#username").css("border-bottom", "");
        $("#usererror").hide();
    }
});
$(function() {
    $('#studiolisting_length').addClass(' select2-container--default ');
    $('#staffmembers_length').addClass(' select2-container--default ');
    $('#users_wrapper').addClass(' select2-container--default ');
});
$('.edit-btn').click(function(e) {
    e.preventDefault();
    selector = $(this);
    if ($(this).parent().find('input').attr("readonly")) {
        selector.parent().find('input').removeAttr("readonly");
        // selector.text(' Update');
        // selector.prepend('<i class="fa fa-pencil"></i>');
        selector.parent().find('input').addClass("editable");
        selector.addClass("bg-color");
    } else {
        // selector.text(' Edit');
        // selector.prepend('<i class="fa fa-pencil"></i>');
        selector.parent().find('input').attr("readonly", "readonly");
        selector.parent().find('input').removeClass("editable");
        selector.removeClass("bg-color");
        columname = selector.attr('columname');
        userid = $("#studio_id").val();
        value = selector.parent().find('input').val();
        url = login.base_url+'updateuserinfo';
        latlng="";
        if(columname=="Address"){
            latlng="&lng="+$("#lng").val()+"&lat="+$("#lat").val();
        }
        queryString = "id=" + userid + "&columname=" + columname + "&value=" + value +latlng;
        result = login.runajax(url, queryString, 1);
    }
    return false;
});
$(document).on("change", '#datefrom', function() {
    from = $("#dateto").val();
    to = $("#datefrom").val();
    queryString = `from=${from}&to=${to}`;
    result = login.runajax("chartdatewise", queryString);
    chartdata(result.result);
})
$(document).on("click", '.direction', function(e) {
    e.preventDefault();
    lat = $(this).find('i.fa').attr('lat');
    lng = $(this).find('i.fa').attr('lng');
    if (lat != "" && lng != "") {
        window.open(`https://maps.google.com/?q=${lat},${lng}`);
    }
})
$(document).on("click", '.workingadd', function() {
    rowid = $(this).attr('rowid');
    selector = $(this);
    queryString ='id='+rowid;
    url=login.base_url+"Deleteworkinghours";
    result = login.runajax(url, queryString,1);
    if(result){
    swal({
        title: "Deleted",
        text: 'working Hours Deleted',
        type: 'info',
        }).then(function(){
            selector.parent().parent().parent().remove();

        })

    }
   
});
$(document).on("click", "#updateworkinghours", function(e) {
    e.preventDefault();
    days = $("#days").val().trim();
    from = $("#from").val();
    to = $("#to").val();
    studio_id= $("#studio_id").val();
    happy_from_date = strtotime(date('Y/m/d')+" "+from);
    happy_to        = strtotime(date('Y/m/d')+" "+to);
    if(eval(happy_from_date)>eval(happy_to) || eval(happy_from_date)==eval(happy_to) ){
        swal({
        title: "Time",
        text: 'From Time is must be less than To Time.',
        type: 'info',
        })
        return false;
    }
    if($('.days_lenghth').length>6){
        swal({
        title: "Error",
        text: 'You Already Add seven days of week.',
        type: 'info',
        })
        
         return false;
    }
    days_text =$('.days_lenghth').text().trim();
    if(days_text.indexOf(days)>-1){
        swal({
        title: "Error",
        text : 'You Already Add '+$("#days").val(),
        type : 'info',
        })
        return false;
    }
    switch_id='off';
    ishappy='No';
    if($('.js-switch').is(":checked")){
        switch_id='on';
        ishappy='yes';
    }
    // rowid = $("#rowid").val();
    queryString = `day=${days}&from=${from}&to=${to}&studio_id=${studio_id}&switch=${switch_id}`;
    result = login.runajax("workinghours", queryString,1);
    // selector = $('#hours' + rowid);
    if($('.js-switch').is(":checked")){
        from = date('h:i a',strtotime("+ 1 hour",strtotime(date('Y/m/d')+' '+from)));
        to   = date('h:i a',strtotime("+ 1 hour",strtotime(date('Y/m/d')+' '+to)));
    } else{
        from = date('h:i a',strtotime(date('Y/m/d')+' '+from));
        to   = date('h:i a',strtotime(date('Y/m/d')+' '+to));

    }
    // if($('.js-switch').is(":checked")){
    //     $('.switchery').trigger('click');
    // }
    // time = selector.closest('td').prev('td').prev('td').text(from + ' -- ' + to);
    // day = selector.closest('td').prev('td').prev('td').prev('td').text(days);
    if (result.message == "updated" || result.message == "samedata") {
        // $("#editform").fadeOut('slow');
        $('#append_data').append(`<tr><td class='days_lenghth'>${days}</td>
        <td>${from + ' -- ' + to}</td><td>${ishappy}</td><td><a href="javascript:void(0)" id="editworking" style="text-decoration:none"><i class="fa fa-trash-o workingadd"  rowid="${result.id}"></i> </a></td></tr>`);
        $('#editform')[0].reset();
      
    }
});
$(document).off('click', "#closepophours").on('click', "#closepophours", function(e) {
    $("#editform").fadeOut('slow');
});
$(document).off('click', ".editmodal").on('click', ".editmodal", function(e) {
    userid = $(this).attr('userid');
    $("#rowid").val(userid);
    var bg = $("#catimageid" + userid).css('background-image');
    bg = bg.replace('url(', '').replace(')', '').replace(/\"replace/gi, "");
    var catname = $('#catnameid' + userid).text().trim();
    var status =  $('#status' + userid).text().trim().toLowerCase();
    var dis =     $('#dis'+userid).attr('text').trim();
    $('#editcatname').val(catname);
    $('.editimage').css('background-image', 'url(' + bg + ')');
    $('#editcatimage').val(bg);
    $('#usertypecat').val(status).trigger('change.select2');
    $('#description').val(dis);
    data = $(this).attr('data');
    data = JSON.parse(data);
    $("#editcatname_de").val(data.category_name_de);
    $("#description_de").val(data.description_de);
    console.log(data);
});
$(document).off("click", ".subeditcategory").on("click", ".subeditcategory", function(e) {
    parentid = $(this).attr('parentid');
    rowid = $(this).attr('rowid');
    text = $(this).closest('td').prev('td').text().trim();
    $('#subcategory').val(parentid).trigger('change.select2');
    $("#newsubcategory").val(text);
    $("#parentid").val(parentid);
    $("#rowid").val(rowid);
});
$(document).off("click", "#updatesubcategory").on("click", "#updatesubcategory", function(e) {
    if ($("#newsubcategory").val().trim() != "") {
        parentid = $("#parentid").val();
        rowid = $("#rowid").val();
        subcate = $("#newsubcategory").val();
        queryString = `rowid=${rowid}&parentid=${parentid}&subcatname=${subcate}`;
        result = login.runajax("updatesubcat", queryString, 1);
        $("#parentcatnameid" + rowid).text($('#subcategory :selected').text().trim());
        $("#subcatname" + rowid).text(subcate);
        $('#subeditcategory').modal('toggle');
    } else {
        $("#newsubcategory").addClass('errorline');
        setTimeout(function() {
            $("#newsubcategory").removeClass('errorline');
        }, 1000);
    }
});
$(document).on("change", "#subcategory", function() {
    parentid = $(this).val();
    $("#parentid").val(parentid);
})
$("#editcat").submit(function(evt) {
    evt.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        url: 'editcategory',
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            swal('Category!', 'Update SuccessFully', 'success').then(function() {
                
                location.reload();

            });
            // console.log(data.data.catname);
            // console.log(data.data.catimage);
            // userid = $("#rowid").val();
            // $('#catnameid' + userid).text(data.data.catname);
            // if (data.data.catimage != "") {
            //     $("#catimageid" + userid).css('background-image', 'url(' + data.data.catimage + ')');
            // }
            // // $("#editmodal").css('display','none');
            // $('#status' + userid).text(data.data.usertype);
            // $('#dis'+ userid).text(data.data.description);
            // $('#editmodal').modal('toggle');
        },
        cache: false,
        contentType: false,
        processData: false
    });
    // location.reload();
});
$('.edit-img').on('click', function(e) {
    $('#file_upload').trigger('click');
});
$(document).on('click', '.deletcat', function(e) {
    id = $(this).attr('userid');
    selector = $(this);
    swal({
        title: 'Are you sure?',
        text: "You want to delete this category",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        swal('Deleted!', 'Your category has been deleted.', 'success', )
        queryString = `id=${id}`;
        result = login.runajax("deletecat", queryString, 1);
        selector.closest('td').parent('tr').remove();
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'Your category is safe :)', 'error')
        }
    })
    return false;
});
$(document).on('click', '.deletpackage', function(e) {
    var id = $(this).attr('rowid');
    var is_presentation = $(this).attr('data-is_presentation');
    // alert(is_presentation);
    selector = $(this);
    if (id == 1 || id == 5 || is_presentation == 1) {
        swal('package', "you can't delete default package", 'error')
    } else {
        swal({
            title: 'Are you sure?',
            text: "You want to delete this package",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function() {
            swal('Deleted!', 'Your package has been deleted.', 'success', )
            queryString = `id=${id}`;
            result = login.runajax("deletepackage", queryString, 1);
            selector.closest('td').parent('tr').remove();
        }, function(dismiss) {
            if (dismiss === 'cancel') {
                swal('Cancelled', 'Your package is safe :)', 'error')
            }
        })
        return false;
    }
});
$(document).on('click', '.packeditmodel', function(e) {
    sizelimit = $(this).closest('td').prev('td').text().trim();
    time = $(this).closest('td').prev('td').prev('td').text().trim();
    package = $(this).closest('td').prev('td').prev('td').prev('td').text().trim();
    price = $(this).closest('td').prev('td').prev('td').prev('td').prev('td').text().trim();
    video = $(this).closest('td').prev('td').prev('td').prev('td').prev('td').prev('td').text().trim();
    pcakagname = $(this).closest('td').prev('td').prev('td').prev('td').prev('td').prev('td').prev('td').text().trim();
    split_string = sizelimit.split(/(\d+)/);
    split_time = time.split(/(\d+)/);
    split_price = price.split(/(\d+)/);
    finalprice = split_price[1] + '.' + split_price[3]
    $("#updatepackname").val(pcakagname);
    if (video == "Unlimited") {
        video = 0;
    }
    if (price == "Free") {
        finalprice = 0;
    }
    if (time == "Unlimited") {
        split_time[1] = 0;
    }
    $("#editvideolimit").val(video);
    $("#editPrice").val(finalprice);
    if (package == "User") package = "normal_user";
    if (package == "Studio") package = "studio_user";
    $("#editusertype").val(package).trigger('change.select2');
    $("#edittimelimit").val(split_time[1]);
    $("#editsizelimit").val(split_string[1]);
    rowid = $(this).attr('rowid');
    $("#editrowid").val(rowid);
});
$(document).on('click', '#updateaboutus', function(e) {
    e.preventDefault();
    $('#aboutus').attr('readonly', 'readonly');
    text = $('#aboutus').val();
    $rowid = $(this).attr('rowid');
    queryString = `text=${text}&rowid=${$rowid}`;
    result = login.runajax("addaboutus", queryString, 1);
    location.reload();
    return false;
});
$(document).on('click', '#editaboutus', function(e) {
    e.preventDefault();
    $('#aboutus').removeAttr('readonly');
    $(this).text('Save');
    $(this).attr('id', 'updateaboutus');
    // location.reload();
    return false;
});
// $(document).on('click', '#editaboutus', function(e) {
//     e.preventDefault();
//     $('#aboutus').removeAttr('readonly');
//     $(this).text('Save');
//     $(this).attr('id', 'updateaboutus');
//     // location.reload();
//     return false;
// });
$(document).on('click', '.icon-calendar5', function(e) {
    // console.log(login.base_url);
    month = $('#invociesto').val();
    window.open(`${login.base_url}Invoiceswithdate/${month}`, `_self`);
});
$(document).on('click', '#downloadpdf', function(e) {
    rowid = $('#rowid').val();
    window.open(`${login.base_url}genratepdf/${rowid}`, `_self`);
});

$("#imageform").submit(function(evt) {
    evt.preventDefault();
    var ImageURL = document.getElementById("service_image_blob").value;
    var block = ImageURL.split(";");
    // Get the content type of the image
    var contentType = block[0].split(":")[1];// In this case "image/gif"
    // get the real base64 content of the file
    var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
    // Convert it to a blob to upload
    var blob = b64toBlob(realData, contentType);
    var formData = new FormData(this);
    formData.append('propic',blob);
    console.log(formData);
    $.ajax({
        url: login.base_url+'updateuserinfo',
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            console.log(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$(document).on('keyup', '#searchuser', function(e) {
    keyword = $(this).val();
    queryString = `keyword=${keyword}`;
    // result = login.runajax("Searchstaffmember", queryString);
    if ($(this).val() == '') {
        $("#outputbox").html('');
        return false;
    } else {
        $.get("/admin/Searchstaffmember",queryString,function( result ) {
                 $("#outputbox").html('');
                $.each(result.data, function(i, val) {
                    $("#outputbox").append(`<li><div class="check"><div class='imgbg' style='background-image:url(${val.profile_pic})'></div>
                <div style="margin-top: 10px;"><a href="#" class='getstaffid' staffid='${val.id}'> ${val.Studio_Name} </a></div></div></li>`);
                });
        });
    }
    // console.log(result.data);
});
// $(document).on('click','.playvideo',function(){
//    videolink=$(this).attr('videolink');
//    // alert(videolink);
//      $("#videosrc").find("#tv_main_channel").attr("src", videolink)
//    // $("#videosrc").attr('src',videolink);  
//    var vid = document.getElementById("videosrc");
//    vid.load();
// })
$(document).on('click', '.videoclose', function() {
    idname = $(this).attr('videosrcid');
    var vid = document.getElementById(idname);
    vid.load();
});
$(document).on('click', '.closestaffbtn', function() {
    $("#outputbox").html('');
    $(".customclass").prop('checked', false);
    $(".updatecustomclass").prop('checked', false);
    $('#searchuser').val('');
    $("#formservice")[0].reset();
    $(".select2-hidden-accessible").val(null).trigger("change");
});
$(document).on('click', '.getstaffid', function() {
    console.log($(this).attr('staffid'));
    text = $(this).text();
    console.log(text);
    $('#searchuser').val(text);
    $("#outputbox").html('');
    $("#staffid").val($(this).attr('staffid'));
    $(".customclass").attr('checked', false);
});
$(document).on('click', '#addstaffmemberbtn', function() {
    if (!$(".customclass").is(":checked") || $("#searchuser").val() == "") {
        swal('Staff Member', "Not Select Any Staff Member Or You Don't Add Any Service Yet", 'error')
        return false;
    }
    service = [];
    parentid = $("#staffid").val();
    studioid = $("#studioid").val();
    $(".customclass").each(function(i, val) {
        if ($(this).is(":checked")) {
            service.push($(this).attr('serviceid'));
        }
    })
     queryString = `service_id=${service}&userid=${parentid}&studio_id=${studioid}`;
     $.post( "/admin/Addstaff",queryString,function(result) {
        // result = login.runajax("Addstaff", queryString, 1);
             console.log(result);
            if (result.status == "success") {
                swal('Staff Member!', 'Added SuccessFully', 'success')
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else if (result.status == "error") {
                swal('Staff Member!', 'This Staff Member is already Added By An Other Studio', 'error')
            }
      });
    
})
$(document).on("click", ".Update", function() {
    $(".updatecustomclass").prop('checked', false);
    staffname = $(this).closest('tr').children('td:eq(2)').text();
    $("#staffname").text(staffname);
    staffid = $(this).attr('userid');
    $("#staffid").val(staffid);
    queryString = `id=${staffid}`;
    // result = login.runajax("staffserviceinfo", queryString);
    $.get("/admin/staffserviceinfo", queryString,function( result ) {
        $.each(result.service, function(i, value) {
            $('#serid'+value.service_id).prop('checked', true);
        })
    });
  })
$(document).on('click', '#Updatestaff', function() {
    service = [];
    parentid = $("#staffid").val();
    studioid = $("#studioid").val();
    $(".updatecustomclass").each(function(i, val) {
        if ($(this).is(":checked")) {
            service.push($(this).attr('service-id'));
        }
    })
    queryString = `service_id=${service}&userid=${parentid}&studio_id=${studioid}&update='true'`;
    // result = login.runajax("Addstaff", queryString, 1);
    $.post("/admin/Addstaff", queryString,function( result ) {
        if (result.status == "success") {
            swal('Staff Service!', 'Update SuccessFully', 'success')
            setTimeout(function() {
                location.reload();
            }, 1000);
        }
    })
   
})

function chartdata(data) {
    Highcharts.chart('chartcontainer', {
        chart: {
            type: 'column'
        },
        // colors: ['#0000FF', '#0066FF', '#00CCFF'],
        title: {
            text: 'Date Wise Subscribtions Chart'
        },
        // subtitle: {
        //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
        // },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total Subscribtions'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> Subscribers<br/>'
        },
        series: [{
            name: 'Subscribtions',
            colorByPoint: true,
            data: data
        }]
    });
}

function convertMillisecondsToDigitalClock(ms) {
    hours = Math.floor(ms / 3600000); // 1 Hour = 36000 Milliseconds
    minutes = Math.floor((ms % 3600000) / 60000); // 1 Minutes = 60000 Milliseconds
    return {
        hours: hours + " Hours",
        minutes: minutes + " minutes",
    };
}