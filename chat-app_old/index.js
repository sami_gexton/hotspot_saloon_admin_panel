const express    = require("express");
const mysql      = require('mysql');
const md5 = require('md5');
const session = require('express-session');
const bodyParser = require('body-parser');
const auth = require('basic-auth');
const port = 3000;
const appUrl = 'http://localhost'


const app = express();
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json())

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'sami',
  password : 'rWuJVTOzxpAemoFM',
  database : 'hotspot_studioApp2018'
});

connection.connect(function(err){
  if(err) throw err;
  console.log("Database is connected");    
});

var server = app.listen(port, function() {
    console.log('listening on ' + port);
});

const io = require('socket.io').listen(server);

io.on('connection', function(sock) {
    socket = sock;

    socket.on('disconnect', function() {

    });
    socket.on('join', function(data) {
        socket.join(data);
        io.emit('tweet', 'tweet');
    });
    socket.on('get-chat', function(data) {
       
    });
});

require('./controllers/main.js')(app, io, connection, auth);
